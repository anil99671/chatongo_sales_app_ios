//
//  GetStateParser.swift
//  ChatOnGo
//
//  Created by Apple on 14/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol GetStatesParserDelegate : NSObjectProtocol
{
    optional func didRecievedStatesResultError(status status : Int)
    func didRevievedStates(states states : [GetStateModel])
}

class GetStateParser: NSObject, AsyncLoaderModelDelegate
{
    var asyncloader : AsyncLoaderModel?
    var delegate : GetStatesParserDelegate?

    func getstates(countryId : Int)
    {
        asyncloader = AsyncLoaderModel()
        asyncloader!.delegate = self
        asyncloader!.getDataFromURLString(webURL: AppConstant.Static.BASE_URL+"GetStateList?CountryId=\(countryId)", dataIndex: -1)
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("responseString State \(responseString!)")
        processData(data: data)
        self.asyncloader = nil
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        if delegate != nil
        {
            delegate!.didRecievedStatesResultError!(status: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func processData(data data : NSData)
    {

        var json : AnyObject?
        do
        {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
            print(json)
            let msgCode = json!.objectForKey("Status") as! Int

            if msgCode == 200
            {
                let stateData = json!.objectForKey("data") as! NSArray


                var statesArray : [GetStateModel] = []

                for i in 0..<stateData.count
                {
                    let states = GetStateModel()

                    states.stateId = stateData[i].valueForKey("StateId") as! Int!
                    states.stateName = stateData[i].valueForKey("Name") as! String!
                    statesArray.append(states)
                }
                if (delegate != nil)
                {
                    delegate!.didRevievedStates(states: statesArray)
                }
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didRecievedStatesResultError!(status: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
    
}
