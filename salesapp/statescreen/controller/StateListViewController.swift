//
//  StateListViewController.swift
//  ChatOnGo
//
//  Created by Apple on 14/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol StateListViewControllerDelegate : NSObjectProtocol
{
    func didRecievedUserSelectedState(stateName : String, stateId : Int)
}

class StateListViewController: UIViewController, NimapNavigationBarViewDelegate, UITableViewDataSource, UITableViewDelegate, GetStatesParserDelegate, NimapAlertViewDelegate, NimapSearchBarViewDelegate
{

    // MARK: - Variable Declarations

    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?

    var navigationHeight : CGFloat = 0.0
    var pageHeight : CGFloat?
    var padding : CGFloat?
    var searchBarHeight : CGFloat = 0.0

    var alert : NimapAlertView?

    var statesTable : UITableView?
    weak var delegate : StateListViewControllerDelegate?
    var stateParser : GetStateParser?
    var arrayOfStates : [GetStateModel!]!
    var searchStates : [GetStateModel!]!
    var state : GetStateModel?
    var activity : UIActivityIndicatorView?

    var stateId : Int?
    var countryId : Int?
    var isSearching = false
    var searchBar : NimapSearchBarView?
    var searchView : UIView?

    var statedelegate: StateListViewControllerDelegate?

    //MARK: ViewController Life Cycle Methods

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    init()
    {
        super.init(nibName: nil, bundle: nil)
        deviceManager = DeviceManager.sharedDeviceManagement()

        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(animated: Bool)
    {
        loadPage()
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    func loadPage()
    {
        loadNavigationBar()
        loadActivity()
        loadStateParser()
    }

    //MARK: loading & Unloading Methods

    func loadNavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }

            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"State List")
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)

            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)

            navigationHeight = navigationBar!.frame.size.height
            pageHeight = self.view.frame.height - navigationHeight
            padding = deviceManager!.deviceYCGFloatValue(yPos: 10.0)
        }

    }

    func loadActivity()
    {
        activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activity!.color = UIColor.appActivityColor()
        activity!.center = self.view.center
        self.view.addSubview(activity!)
        activity!.startAnimating()
    }

    func loadStateParser(){

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        stateParser = GetStateParser()
        stateParser!.delegate = self
        stateParser!.getstates(countryId!)
    }

    func loadStateTable()
    {
        if statesTable == nil
        {
            statesTable = UITableView(frame: CGRectMake(0, searchBarHeight + deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING), self.view.frame.width, self.view.frame.height - navigationHeight - searchBarHeight - deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING)))
            statesTable!.delegate = self
            statesTable!.dataSource = self
            statesTable!.separatorStyle = UITableViewCellSeparatorStyle.None
            searchView!.addSubview(statesTable!)
        }
    }

    //MARK: Unloading Methods

    func unloadPage()
    {
        unloadNavigationBar()
        unloadCityTable()
        unloadViews()
        unloadSearchBar()
        unloadArrays()
        unloadAlert()
        unloadActivity()
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
    }

    func unloadCityTable()
    {
        if statesTable != nil
        {
            statesTable!.removeFromSuperview()
            statesTable = nil
        }
    }

    func unloadViews()
    {
        if searchView != nil
        {
            searchView!.removeFromSuperview()
            searchView = nil
        }
    }

    func unloadSearchBar()
    {
        if searchBar != nil
        {
            searchBar!.removeFromSuperview()
            searchBar = nil
        }
    }

    func unloadArrays()
    {
        if arrayOfStates != nil
        {
            arrayOfStates!.removeAll(keepCapacity: false)
            arrayOfStates = nil
        }

        if searchStates != nil
        {
            searchStates!.removeAll(keepCapacity: false)
            searchStates = nil
        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func unloadActivity()
    {
        if activity != nil
        {
            activity!.removeFromSuperview()
            activity = nil
        }
    }

    //MARK: TableView Delegates

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearching == true
        {
            return searchStates!.count
        }
        else
        {
            return arrayOfStates!.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        state = arrayOfStates[indexPath.row]
        var cell : StateListTableViewCell?
        cell = statesTable!.dequeueReusableCellWithIdentifier("cell") as! StateListTableViewCell?

        if cell == nil
        {
            cell = StateListTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell", cellWidth: self.view.frame.size.width, cellHeigth: deviceManager!.deviceYCGFloatValue(yPos: 45))
        }

        if isSearching == true
        {
            state = searchStates![indexPath.row]
        }
        else
        {
            state = arrayOfStates![indexPath.row]
        }

        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell!.StateNameLable!.text = state!.stateName

        return cell!
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return deviceManager!.deviceYCGFloatValue(yPos: 45)
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var selectedState = arrayOfStates![indexPath.row]

        if isSearching == true
        {
            selectedState = searchStates![indexPath.row]

        }
        else
        {
            selectedState = arrayOfStates![indexPath.row]
        }

        if selectedState.stateId != -1
        {
            statedelegate!.didRecievedUserSelectedState(selectedState.stateName!, stateId: selectedState.stateId!)

            searchBar!.resignFirstResponder()
            self.navigationController!.popViewControllerAnimated(true)
        }

    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    //MARK: state parser callbacks

    func didRevievedStates(states states : [GetStateModel])
    {
        stateParser = nil
        unloadActivity()

        if searchView == nil
        {
            searchView = UIView(frame: CGRectMake(0,navigationHeight + deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING),self.view.frame.width,pageHeight!))
            self.view.addSubview(searchView!)
        }
        if searchBar == nil
        {
            searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)

            searchBar = NimapSearchBarView(frame: CGRectMake(padding!,0,self.view.frame.width - (2 * padding!),searchBarHeight))
            searchBar!.center = CGPointMake(self.view.center.x, (searchBar!.frame.size.height/2.0))
            searchBar!.delegate = self
            searchBar!.searchTextField!.placeholder = "Search State"
            searchView!.addSubview(searchBar!)
        }

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if(arrayOfStates != nil)
        {
            arrayOfStates = nil
        }

        arrayOfStates = states
        loadStateTable()
        statesTable!.reloadData()
    }

   func didRecievedStatesResultError(status status: Int)
    {
        var errorMessage = ""
        var errorTag = ""

        if status == AppConstant.Static.CONNECTION_ERROR
        {
            errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
            errorTag = "error"
        }
        else if status == AppConstant.Static.PROCESSING_ERROR
        {
            errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
            errorTag = "error"
        }

        else if status == AppConstant.Static.UNAUTHORISED_USER
        {
            errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
            errorTag = "unauthorisedUserError"
        }

        alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
        alert!.delegate = self
        self.view.addSubview(alert!)
        
    }


    //MARK: NimapAlertViewDelegate Methods

    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        if tag == "error"
        {
            if stateParser != nil
            {
                stateParser = nil
            }
            stateParser = GetStateParser()
            stateParser!.delegate = self
            stateParser!.getstates(countryId!)

        }
        else if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
    }

    // MARK: NimapNavigationBarViewDelegate

    func backButtonPressed(){
        self.navigationController!.popViewControllerAnimated(true)
    }

    func rightButtonPressed()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK: NimapSearchBarViewDelegate callbacks

    func nimapSearchBarViewEnteredCharacters(string: String)
    {
        if searchStates == nil
        {
            searchStates = []
        }

        searchStates!.removeAll(keepCapacity: false)

        if string.isEmpty
        {
            isSearching = false
        }
        else
        {
            for i in 0..<arrayOfStates!.count
            {
                let code = arrayOfStates![i]

                if code.stateName!.lowercaseString.indexOf(string.lowercaseString) == 0
                {
                    searchStates!.append(code)
                }
            }

            isSearching = true

            if searchStates!.count <= 0
            {

                let state = GetStateModel()
                state.stateId = -1
                state.stateName = "No state found"
                searchStates!.append(state)
            }
        }
        statesTable!.reloadData()
    }
}



