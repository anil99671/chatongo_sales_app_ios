//
//  CountryListViewController.swift
//  ChatOnGo
//
//  Created by Apple on 14/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol CountryDelegate : NSObjectProtocol
{
    func didSelectedCountry(countryName : String,countryId : Int)
}

class CountryListViewController:UIViewController, NimapNavigationBarViewDelegate, UITableViewDataSource, UITableViewDelegate, GetCountryParserDelegate, NimapAlertViewDelegate, NimapSearchBarViewDelegate
{

    // MARK: - Variable Declarations

    var deviceManager : DeviceManager?
    weak var delegate : GetCountryParserDelegate?

    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    var pageHeight : CGFloat?
    var searchBarHeight : CGFloat = 0.0
    var padding : CGFloat?

    var alert : NimapAlertView?

    var countryTable : UITableView?
    var countryParser : GetCountryParser?
    var arrayOfCountry : [GetCountryModel!]!
    var country : GetCountryModel?
    var activity : UIActivityIndicatorView?

    var stateId : Int?
    var searchView : UIView?
    var searchCountry : [GetCountryModel!]!
    var isSearching = false
    var searchBar : NimapSearchBarView?

    var countrydelegate: CountryDelegate!?

    //MARK: viewcontrollor lifecycle Methods

    override func viewDidLoad()
    {
        super.viewDidLoad()

    }

    override func viewWillAppear(animated: Bool)
    {
        loadPage()
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }

    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    init()
    {
        super.init(nibName: nil, bundle: nil)
        deviceManager = DeviceManager.sharedDeviceManagement()

        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func loadPage()
    {
        loadnavigationBar()
        loadActivity()
        loadStateParser()
    }

    func loadnavigationBar()
    {
            if navigationBar == nil
            {
                if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
                {
                    navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
                }
                else
                {
                    navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
                }

                navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"Country List")
                navigationBar!.delegate = self
                self.view.addSubview(navigationBar!)

                navigationBar!.addLeftButtonWithIcon()
                navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)

                navigationHeight = navigationBar!.frame.size.height
                pageHeight = self.view.frame.height - navigationHeight
            }

    }

    func loadActivity()
    {
        activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activity!.color = UIColor.appActivityColor()
        activity!.center = self.view.center
        self.view.addSubview(activity!)
        activity!.startAnimating()
    }

    func loadStateParser()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        countryParser = GetCountryParser()
        countryParser!.delegate = self
        countryParser!.getCountry()
    }

    func loadStateTable()
    {
        if countryTable == nil
        {
            countryTable = UITableView(frame: CGRectMake(0, navigationHeight + searchBarHeight + deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING), self.view.frame.width, self.view.frame.height - navigationHeight - searchBarHeight - deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING)))
            countryTable!.delegate = self
            countryTable!.dataSource = self
            countryTable!.separatorStyle = UITableViewCellSeparatorStyle.None
            self.view.addSubview(countryTable!)
        }
    }

    //MARK: Unloading Methods

    func unloadPage()
    {
        unloadNavigationBar()
        unloadCityTable()
        unloadViews()
        unloadSearchBar()
        unloadArrays()
        unloadAlert()
        unloadActivity()
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
    }

    func unloadCityTable()
    {
        if countryTable != nil
        {
            countryTable!.removeFromSuperview()
            countryTable = nil
        }
    }

    func unloadViews()
    {
        if searchView != nil
        {
            searchView!.removeFromSuperview()
            searchView = nil
        }
    }

    func unloadSearchBar()
    {
        if searchBar != nil
        {
            searchBar!.removeFromSuperview()
            searchBar = nil
        }
    }

    func unloadArrays()
    {
        if arrayOfCountry != nil
        {
            arrayOfCountry!.removeAll(keepCapacity: false)
            arrayOfCountry = nil
        }

        if searchCountry != nil
        {
            searchCountry!.removeAll(keepCapacity: false)
            searchCountry = nil
        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func unloadActivity()
    {
        if activity != nil
        {
            activity!.removeFromSuperview()
            activity = nil
        }
    }

    //MARK: TableView Delegates

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearching == true
        {
            return searchCountry!.count
        }
        else
        {
            return arrayOfCountry!.count
        }

    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        country = arrayOfCountry[indexPath.row]
        var cell : CountryListTableViewCell?
        cell = countryTable!.dequeueReusableCellWithIdentifier("cell") as! CountryListTableViewCell?

        if cell == nil
        {
            cell = CountryListTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell", cellWidth: self.view.frame.width, cellHeigth: deviceManager!.deviceYCGFloatValue(yPos: 45))
        }
        if isSearching == true
        {
            country = searchCountry![indexPath.row]
        }
        else
        {
            country = arrayOfCountry![indexPath.row]
        }

        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell!.countryNameLable!.text = country!.countryName

        return cell!
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return deviceManager!.deviceYCGFloatValue(yPos: 45)
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var selectedCountry = arrayOfCountry![indexPath.row]

        if isSearching == true
        {
            selectedCountry = searchCountry![indexPath.row]
        }
        else
        {
            selectedCountry = arrayOfCountry![indexPath.row]
        }

        if selectedCountry.countryId != -1
        {
            countrydelegate!.didSelectedCountry(selectedCountry.countryName!, countryId: selectedCountry.countryId!)

            searchBar!.resignFirstResponder()
            self.navigationController!.popViewControllerAnimated(true)
        }
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    //MARK: country parser callbacks

    func didRevievedCountry(country country : [GetCountryModel])
    {
        countryParser = nil
        unloadActivity()

        if searchView == nil
        {
            searchView = UIView(frame: CGRectMake(0,navigationHeight + deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING),self.view.frame.width,pageHeight!))
            self.view.addSubview(searchView!)
        }
        if searchBar == nil
        {
            searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
            padding = deviceManager!.deviceYCGFloatValue(yPos: 10)
            searchBar = NimapSearchBarView(frame: CGRectMake(padding!,0,self.view.frame.width - (2 * padding!),searchBarHeight))
            searchBar!.center = CGPointMake(self.view.center.x, (searchBar!.frame.size.height/2.0))
            searchBar!.delegate = self
            searchBar!.searchTextField!.placeholder = "Search Country"
            searchView!.addSubview(searchBar!)
        }
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        if(arrayOfCountry != nil)
        {
            arrayOfCountry = nil
        }
        arrayOfCountry = country
        loadStateTable()
        countryTable!.reloadData()
    }


    func didRecievedCountryResultError(status status: Int)
    {
        var errorMessage = ""
        var errorTag = ""

        if status == AppConstant.Static.CONNECTION_ERROR
        {
            errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
            errorTag = "error"
        }
        else if status == AppConstant.Static.PROCESSING_ERROR
        {
            errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
            errorTag = "error"
        }

        else if status == AppConstant.Static.UNAUTHORISED_USER
        {
            errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
            errorTag = "unauthorisedUserError"
        }

        alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
        alert!.delegate = self
        self.view.addSubview(alert!)
        
    }

    //MARK: NimapAlertViewDelegate Methods

    func didEnterText(tag : String!, text: String!)
    {

    }

    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        if tag == "error"
        {
            if countryParser != nil
            {
                countryParser = nil
            }
            countryParser = GetCountryParser()
            countryParser!.delegate = self
            countryParser!.getCountry()
        }
        else if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }

    }

    func didSecondaryActionButtonPressed(tag: String!)
    {

    }

    // MARK: NimapNavigationBarViewDelegate

    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }

    func rightButtonPressed()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK: NimapSearchBarViewDelegate callbacks

    func nimapSearchBarViewEnteredCharacters(string: String)
    {
        if searchCountry == nil
        {
            searchCountry = []
        }

        searchCountry!.removeAll(keepCapacity: false)

        if string.isEmpty
        {
            isSearching = false
        }
        else
        {
            for i in 0..<arrayOfCountry!.count
            {
                let code = arrayOfCountry![i]

                if code.countryName!.lowercaseString.indexOf(string.lowercaseString) == 0
                {
                    searchCountry!.append(code)
                }
            }
            isSearching = true

            if searchCountry!.count <= 0{

                let country = GetCountryModel()
                country.countryId = -1
                country.countryName = "No country found"
                searchCountry!.append(country)
            }

        }
        countryTable!.reloadData()
    }
    
}