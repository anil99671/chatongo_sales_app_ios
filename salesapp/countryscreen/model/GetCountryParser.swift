//
//  GetCountryParser.swift
//  ChatOnGo
//
//  Created by Apple on 14/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol GetCountryParserDelegate : NSObjectProtocol
{
    optional func didRecievedCountryResultError(status status : Int)
    func didRevievedCountry(country country : [GetCountryModel])
}


class GetCountryParser: NSObject, AsyncLoaderModelDelegate {

    var asyncloader : AsyncLoaderModel?
    var delegate : GetCountryParserDelegate?

    func getCountry()
    {
        asyncloader = AsyncLoaderModel()

        asyncloader!.getDataFromURLString(webURL: AppConstant.Static.BASE_URL+"GetCountryList", dataIndex: -1)
        asyncloader!.delegate = self
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        if delegate != nil
        {
            delegate!.didRecievedCountryResultError!(status: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("responseString Country \(responseString!)")
        processData(data: data)
        self.asyncloader = nil
    }

    func processData(data data : NSData)
    {
        var json : AnyObject?
        do
        {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary


            let msgCode = json!.objectForKey("Status") as! Int

            if msgCode == 200
            {
                let stateData = json!.objectForKey("data") as! NSArray


                var statesArray : [GetCountryModel] = []

                for i in 0..<stateData.count
                {
                    let states = GetCountryModel()

                    states.countryId = stateData[i].valueForKey("CountryId") as! Int!
                    states.countryName = stateData[i].valueForKey("Name") as! String!
                    statesArray.append(states)
                }

                if (delegate != nil)
                {
                    delegate!.didRevievedCountry(country: statesArray)
                }
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didRecievedCountryResultError!(status: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}