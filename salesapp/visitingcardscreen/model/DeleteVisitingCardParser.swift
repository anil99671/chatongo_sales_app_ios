//
//  DeleteVisitingCardParser.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

protocol DeleteVisitingCardParserDelegate : NSObjectProtocol
{
    func didRecievedDeleteBrochureResultError(resultCode : Int)
}

class DeleteVisitingCardParser: NSObject, AsyncLoaderModelDelegate
{
    static let DELETE_BROCHURE_SUCCESS = 3

    var brochureID : Int?
    var shopID : Int?

    let salesDB = SalesAppDBModel.sharedInstance()

    var asyncloader : AsyncLoaderModel?
    var delegate : DeleteVisitingCardParserDelegate?

    func deleteBrochure(brochureId brochureId : Int, shopId : Int)
    {
        brochureID = brochureId
        shopID = shopId
        asyncloader = AsyncLoaderModel()
        asyncloader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"DeleteVisitingCard?ImageId=\(brochureId)", dataIndex: -1)
        asyncloader!.delegate = self

        let url = AppConstant.Static.BASE_URL + "DeleteVisitingCard?ImageId=\(brochureId)"
        print("url \(url)")
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        if delegate != nil
        {
            delegate!.didRecievedDeleteBrochureResultError(AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {

//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("DeleteVisitingCard responseString is \(responseString!)")
        processDeleteBrochure(data: data)
        self.asyncloader = nil

    }

    func processDeleteBrochure(data data : NSData)
    {
        var result : NSDictionary?
        do
        {
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary


            let msgCode = result!.objectForKey("Status") as! Int

            if msgCode == 200
            {

                salesDB!.deleteBrochureForId(brochureId: brochureID!)

                let fileName = "shop_\(shopID)_brochure_\(brochureID)"
                print("brochure filename \(fileName)")

                let fileManager = SaveDataInDocumentDirectory.sharedInstance()

                if fileManager.isFileExist(fileName: fileName)
                {
                    fileManager.removeFile(name: fileName)
                }

                let offlineFileDB = AsynFileManagerDB.sharedInstance()

                offlineFileDB!.deleteOfflineFileForId(offlineID: fileName)

                if delegate != nil
                {
                    delegate!.didRecievedDeleteBrochureResultError(DeleteVisitingCardParser.DELETE_BROCHURE_SUCCESS)

                }

            }
            else if msgCode == 401
            {
                delegate!.didRecievedDeleteBrochureResultError(AppConstant.Static.UNAUTHORISED_USER)
            }
            else
            {
                delegate!.didRecievedDeleteBrochureResultError(AppConstant.Static.PROCESSING_ERROR)
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didRecievedDeleteBrochureResultError(AppConstant.Static.CONNECTION_ERROR)
            }
        }
    }
}

