//
//  BrochureModel.swift
//  VendorAppDB
//
//  Created by Apple on 05/07/16.
//  Copyright © 2016 nimap. All rights reserved.
//

import UIKit

class BrochureModel: NSObject {
    var brochureId : Int?
    var brochureURL : String?
    var brochureImage : UIImage?
    var loader : AsyncLoaderModel?
    var modifiedDate : String?
    var shopId : Int?
}
