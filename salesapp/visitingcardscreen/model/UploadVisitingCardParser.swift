//
//  UploadVisitingCardParser.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

@objc protocol UploadVisitingCardParserDelegate : NSObjectProtocol
{
    func didUploadVisitingCardResult(resultCode: Int)
}

class UploadVisitingCardParser: NSObject, AsyncLoaderModelDelegate {

    static var BROCHURE_UPLOAD_SUCCESS = 3

    var loader : AsyncLoaderModel?
    weak var delegate : UploadVisitingCardParserDelegate?
    var fileOperation : SaveDataInDocumentDirectory?

    var shopId : Int?
    var uploadBrochureFileName : String?

    func addBrochure(shopId shopId : Int, fileName : String)
    {
        fileOperation = SaveDataInDocumentDirectory.sharedInstance()

        self.shopId = shopId
        uploadBrochureFileName = fileName

        let jsonDictionary = NSMutableDictionary()
        jsonDictionary.setObject(shopId, forKey: "ShopId")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("userID")!, forKey: "UserId")
        jsonDictionary.setObject(fileOperation!.getImageBase64String(imageFileName: fileName) , forKey: "Image")
        jsonDictionary.setObject(9 , forKey: "FeatureId")

        var jsonData: NSData?

        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch
        {
            jsonData = nil
        }
        let jsonDataLenght = "\(jsonData!.length)"

//        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
//        print("UploadVisitingCard requestString is \(requestString!)")

        let url = NSURL(string: AppConstant.Static.BASE_URL+"UploadVisitingCard")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData

        loader = AsyncLoaderModel()
        loader!.getDataFromRequest(request: request, dataIndex: -1)
        loader!.delegate = self
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("UploadVisitingCard responseString is \(responseString!)")

        processToUploadBrochure(data: data)
        self.loader = nil
    }

    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            delegate!.didUploadVisitingCardResult(AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func processToUploadBrochure(data data : NSData)
    {
        let resultMaster : NSDictionary?

        do{

            try resultMaster = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let Status = resultMaster!.objectForKey("Status") as! Int

            if Status == 200
            {
                let salesDB = SalesAppDBModel.sharedInstance()

                let result = resultMaster!.objectForKey("data") as? NSDictionary

                let brochureDetail = BrochureModel()

                brochureDetail.brochureId = result!.objectForKey("Id") as? Int
                brochureDetail.shopId = shopId
                brochureDetail.brochureURL = result!.objectForKey("Image") as? String
                brochureDetail.modifiedDate = result!.objectForKey("ModifiedDate") as? String

                // converting mm/dd/yyyy into mm-dd-yyyy
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"

                let newStr = brochureDetail.modifiedDate!.stringByReplacingOccurrencesOfString("/", withString: "-", options: NSStringCompareOptions.LiteralSearch, range: nil)
                let serverModifiedImageDate = dateFormatter.dateFromString(newStr)
                brochureDetail.modifiedDate = dateFormatter.stringFromDate(serverModifiedImageDate!)

                if (salesDB!.isBrochureExists(brochureId: brochureDetail.brochureId!) == true)
                {
                    salesDB!.updateIntoBrochure(shopId: brochureDetail.shopId!, brochureId: brochureDetail.brochureId!, brochureURL: brochureDetail.brochureURL!, modifiedDate: brochureDetail.modifiedDate!)
                }
                else
                {
                    salesDB!.insertIntoBrochure(shopId: brochureDetail.shopId!, brochureId: brochureDetail.brochureId!, brochureURL: brochureDetail.brochureURL!, modifiedDate: brochureDetail.modifiedDate!)
                }

                let fileName = "shop_\(brochureDetail.shopId!)_brochure_\(brochureDetail.brochureId!)"

                let fileManager = SaveDataInDocumentDirectory.sharedInstance()

                if fileManager.isFileExist(fileName: fileName)
                {
                    fileManager.removeFile(name: fileName)
                }

                fileOperation!.updateData(data: fileOperation!.readData(fileName: uploadBrochureFileName!), withFileName: fileName)

                let offlineFileDB = AsynFileManagerDB.sharedInstance()

                let imageModifiedDate = offlineFileDB!.selectModifiedDateFromOfflineFileWhere(fileName: fileName)

                if imageModifiedDate == nil
                {
                    offlineFileDB!.insertIntoOfflineFile(offlineId: fileName, modifiedDate: brochureDetail.modifiedDate!, type: AsynFileManagerDB.Static.IMAGE_FILE)
                }
                else
                {
                    offlineFileDB!.updateOfflineFile(offlineId: fileName, modifiedDate: brochureDetail.modifiedDate!, type: AsynFileManagerDB.Static.IMAGE_FILE)
                }

                fileManager.removeFile(name: uploadBrochureFileName!)

                if delegate != nil
                {
                    delegate!.didUploadVisitingCardResult(UploadVisitingCardParser.BROCHURE_UPLOAD_SUCCESS)
                }
            }
            else
            {
                if delegate != nil
                {
                    delegate!.didUploadVisitingCardResult(AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            if delegate != nil
            {
                delegate!.didUploadVisitingCardResult(AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
    
}
