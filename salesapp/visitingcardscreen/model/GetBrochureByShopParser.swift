//
//  GetBrochureByShopParser.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

protocol GetBrochureByShopParserDelegate : NSObjectProtocol
{
    func didReceivedAllBrochureResult(resultCode resultCode : Int)
}

class GetBrochureByShopParser: NSObject, AsyncLoaderModelDelegate
{
    static let GET_ALL_BROCHURE_SUCCESS = 3
    static let NO_BROCHURE_FOUND = -1

    var asyncloader : AsyncLoaderModel?
    var delegate : GetBrochureByShopParserDelegate?

    var responseString : String?
    var brochureModels : [BrochureModel]?

    var salesDB = SalesAppDBModel.sharedInstance()
    var fileManagerDB = AsynFileManagerDB.sharedInstance()

    var dateFormatter = NSDateFormatter()

    func getAllBrochureData(shopId shopId : Int)
    {
        asyncloader = AsyncLoaderModel()
        asyncloader!.delegate = self
        asyncloader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"GetShopVisitingCard?ShopId=\(shopId)", dataIndex: -1)

        let url = AppConstant.Static.BASE_URL+"GetShopVisitingCard?ShopId=\(shopId)"

        print("url \(url)")
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        print("didReceivedErrorLoader")

        if delegate != nil
        {
            delegate!.didReceivedAllBrochureResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {

        responseString = NSString(data: data, encoding: NSUTF8StringEncoding) as? String
        print("GetShopVisitingCard responseString is \(responseString!)")
        processAllBrochures(data: data)
        self.asyncloader = nil

    }

    func processAllBrochures(data data : NSData)
    {
        var result : NSDictionary?
        do
        {
            try result =  NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let Status = result!.objectForKey("Status") as! Int

            if Status == 200
            {
                brochureModels = []

                let data = result!.objectForKey("data") as! NSArray

                if data.count == 0
                {
                    delegate!.didReceivedAllBrochureResult(resultCode: GetBrochureByShopParser.NO_BROCHURE_FOUND)
                }
                else
                {
                    for i in 0..<data.count
                    {
                        let brochure = data[i] as! NSDictionary

                        /*
                         This condition is needed if image is not loaded in the server we should
                         ignore it
                         */

                        let imageURL =  brochure.objectForKey("Image") as! String

                        if !(imageURL=="na") || !(imageURL=="")
                        {
                            let brochureModel = BrochureModel()
                            brochureModel.brochureId = brochure.objectForKey("Id") as? Int
                            brochureModel.shopId = brochure.objectForKey("ShopId") as? Int
                            brochureModel.brochureURL = brochure.objectForKey("Image") as? String
                            brochureModel.modifiedDate = brochure.objectForKey("ModifiedDate") as? String

                            // converting mm/dd/yyyy into mm-dd-yyyy
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"

                            let newStr = brochureModel.modifiedDate!.stringByReplacingOccurrencesOfString("/", withString: "-", options: NSStringCompareOptions.LiteralSearch, range: nil)
                            let serverModifiedImageDate = dateFormatter.dateFromString(newStr)
                            brochureModel.modifiedDate = dateFormatter.stringFromDate(serverModifiedImageDate!)

                            if (salesDB!.isBrochureExists(brochureId: brochureModel.brochureId!) == true)
                            {
                                salesDB!.updateIntoBrochure(shopId: brochureModel.shopId!, brochureId: brochureModel.brochureId!, brochureURL: brochureModel.brochureURL!, modifiedDate: brochureModel.modifiedDate!)
                            }
                            else
                            {
                                salesDB!.insertIntoBrochure(shopId: brochureModel.shopId!, brochureId: brochureModel.brochureId!, brochureURL: brochureModel.brochureURL!, modifiedDate: brochureModel.modifiedDate!)
                            }


                            let fileName = "shop_\(brochureModel.shopId!)_brochure_\(brochureModel.brochureId!)"

                            let fileManager = SaveDataInDocumentDirectory.sharedInstance()

                            fileManager!.removeOldFile(fileName: fileName, modifiedDate: brochureModel.modifiedDate!)

                        }
                    }
                    if (delegate != nil)
                    {
                        delegate!.didReceivedAllBrochureResult(resultCode: GetBrochureByShopParser.GET_ALL_BROCHURE_SUCCESS)
                    }
                }


            }
            else if Status == 404
            {
                delegate!.didReceivedAllBrochureResult(resultCode: GetBrochureByShopParser.NO_BROCHURE_FOUND)
            }
            else if Status == 401
            {
                delegate!.didReceivedAllBrochureResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
            }
            else
            {
                if (delegate != nil)
                {
                    delegate!.didReceivedAllBrochureResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didReceivedAllBrochureResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }

    //MARK: Date from String

    func getDateFromString(dateString : String) -> NSDate
    {
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.dateFromString(dateString)!
    }
}