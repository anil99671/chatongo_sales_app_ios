//
//  AddVisitingCardViewController.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit
import MobileCoreServices

class AddVisitingCardViewController: UIViewController, NimapNavigationBarViewDelegate, UIZoomableViewDelegate, UIScrollViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UploadVisitingCardParserDelegate, NimapAlertViewDelegate, DeleteVisitingCardParserDelegate, GetBrochureByShopParserDelegate {

    static let TEMP_FILE_NAME = "tempBrochureImage"

    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?

    var alert : NimapAlertView?

    var shopID : Int?
    var shop: ShopModel?
    var currentIndex : Int = 0

    var pageHeight : CGFloat?
    var navigationHeight : CGFloat = 0.0
    var padding : CGFloat = 0.0

    var salesDB : SalesAppDBModel?

    var headerBGView : UIView?
    var shopLabel : UILabel?

    var noBrochureImageView : UIImageView?

    var brochures : [BrochureModel]?

    var brochureGridView : NimapScrollView?
    var zoomView : UIZoomableView?

    var uploadButton : UIButton?
    var deleteButton : UIButton?

    var imagePicker : UIImagePickerController?
    var fileOperation : SaveDataInDocumentDirectory?

    var getVisitingParser : GetBrochureByShopParser?
    var uploadVisitingparser : UploadVisitingCardParser?
    var deleteVisitingParser : DeleteVisitingCardParser?

    var maintainTheState : Bool?

    //MARK: Constructor

    init(shopID : Int, currentIndex : Int)
    {
        self.shopID = shopID
        self.currentIndex =  currentIndex

        super.init(nibName: nil, bundle: nil)

        maintainTheState = false

        deviceManager = DeviceManager.sharedDeviceManagement()
        salesDB = SalesAppDBModel.sharedInstance()
        fileOperation = SaveDataInDocumentDirectory.sharedInstance()

        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None

        padding = deviceManager!.deviceYCGFloatValue(yPos: 8.0)
    }


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool)
    {
        if maintainTheState == false
        {
            loadPage()
        }
        maintainTheState = false
    }

    override func viewDidDisappear(animated: Bool)
    {
        if maintainTheState == false
        {
            unloadPage()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }

    //MARK: Loading Methods

    func loadPage()
    {
        shop = salesDB!.selectShopForId(shopId: shopID!)
        loadNavigationBar()
        if shop != nil
        {
            loadShopNameLabel()
            loadUploadBrochureButton()
            loadGetAllbrochuresParser()
        }
    }

    func loadNavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }
            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"Visiting Card")
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)

            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)

            navigationHeight = navigationBar!.frame.size.height
        }

    }

    func loadShopNameLabel()
    {
        if headerBGView == nil{

            headerBGView = UILabel(frame: CGRectMake(0,navigationHeight,self.view.frame.size.width,deviceManager!.deviceYCGFloatValue(yPos: 30.0)))
            headerBGView!.backgroundColor = UIColor.appAlertBGColor()
            self.view.addSubview(headerBGView!)
        }

        if shopLabel == nil {

            shopLabel = UILabel(frame:CGRectMake(padding,navigationHeight,self.view.frame.size.width - (2 * padding),deviceManager!.deviceYCGFloatValue(yPos: 30.0)))
            shopLabel!.text = shop!.name
            shopLabel!.textColor = UIColor.whiteColor()
            shopLabel!.font = UIFont.appFontMedium(forSize: 16.0)
            self.view.addSubview(shopLabel!)
        }

        pageHeight = self.view.frame.height - navigationHeight - headerBGView!.frame.size.height

        loadDeleteBrochureButton()
    }


    func loadGetAllbrochuresParser(){

        if NSUserDefaults.standardUserDefaults().boolForKey("downloadAllBrochures\(shopID!)") == false{
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }

            alert = NimapAlertView(frame: CGRectZero, tag: "registeringUser", isActivity: true, animate: false)
            alert!.delegate = self
            self.view.addSubview(alert!)

            if getVisitingParser == nil {

                print("add Flag downloadAllBrochures\(shopID!)")

                getVisitingParser = GetBrochureByShopParser()
                getVisitingParser!.delegate = self
                getVisitingParser!.getAllBrochureData(shopId: shopID!)
            }
        }
        else{
            loadBrochureGrid()
        }
    }

    func loadBrochureGrid()
    {
        brochures = salesDB!.selectALLFromBrochureFromId(shopId: shopID!)

        if brochures!.count > 1
        {
            uploadButton!.hidden = true
        }
        else
        {
            uploadButton!.hidden = false
        }


        if brochures!.count > 0{

            if uploadButton != nil {

                pageHeight = self.view.frame.height - navigationHeight - headerBGView!.frame.size.height
                pageHeight = pageHeight! - uploadButton!.frame.size.height
            }

            brochureGridView = NimapScrollView(frame: CGRectMake(padding,navigationHeight + headerBGView!.frame.size.height + padding,self.view.frame.size.width - (2 * padding),pageHeight! - padding - padding))
            brochureGridView!.backgroundColor = UIColor.clearColor()
            brochureGridView!.pagingEnabled = true
            self.view.addSubview(brochureGridView!)

            var contentWidth : CGFloat = 0.0

            var posX : CGFloat = brochureGridView!.frame.size.width/2.0

            for i in 0..<brochures!.count{

                let brochure = brochures![i]

                let fileName = "shop_\(brochure.shopId!)_brochure_\(brochure.brochureId!)"

                let thumbnail = AsyncView(frame: CGRectMake(0,0,brochureGridView!.frame.size.width,brochureGridView!.frame.size.height))
                thumbnail.modifiedDate = brochure.modifiedDate!
                thumbnail.getViewForURL(brochure.brochureURL!, imgIndex: i, offline: true, imageFileName: fileName)
                thumbnail.center = CGPointMake(posX,brochureGridView!.frame.size.height/2.0)
                brochureGridView!.addSubview(thumbnail)
                thumbnail.tag = i + 1

                posX = posX + brochureGridView!.frame.size.width
                contentWidth = contentWidth + brochureGridView!.frame.size.width
            }

            brochureGridView!.contentSize = CGSizeMake(contentWidth, brochureGridView!.frame.size.height)

            brochureGridView!.setContentOffset(CGPointMake(CGFloat(currentIndex) * brochureGridView!.frame.size.width, 0.0), animated: false)

            brochureGridView!.delegate = self


            currentIndex = currentIndex + 1

            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AddVisitingCardViewController.zoomImage(_:)))
            tapGesture.numberOfTapsRequired = 2
            brochureGridView!.addGestureRecognizer(tapGesture)
            self.view.bringSubviewToFront(uploadButton!)

            deleteButton!.hidden = false
        }
        else{
            noBrochureImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "nobrochure")))
            noBrochureImageView!.center = self.view.center
            self.view.addSubview(noBrochureImageView!)

            deleteButton!.hidden = true
        }
    }

    func loadUploadBrochureButton(){

        var buttonHeight : CGFloat = 0.0
        var fontSize : CGFloat = 0.0

        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
        {
            buttonHeight =  40.0
            fontSize = 16.0
        }
        else
        {
            buttonHeight =  deviceManager!.deviceYCGFloatValue(yPos: 40.0)
            fontSize =  deviceManager!.deviceXCGFloatValue(xPos: 16.0)
        }

        uploadButton = UIButton(frame:CGRectMake(0.0,self.view.frame.size.height - (buttonHeight),self.view.frame.size.width,buttonHeight))
        uploadButton!.setTitle("Upload Brochure", forState: UIControlState.Normal)
        uploadButton!.titleLabel!.font = UIFont.appFontBold(forSize: fontSize)
        uploadButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        uploadButton!.backgroundColor = UIColor.primaryColor()
        uploadButton!.addTarget(self, action: #selector(AddVisitingCardViewController.uploadBrochureButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(uploadButton!)
    }

    func loadUploadBrochureParser(){

        if uploadVisitingparser == nil {


            currentIndex = currentIndex - 1

            if currentIndex <= 0 {
                currentIndex = 0
            }
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }

            alert = NimapAlertView(frame: CGRectZero, tag: "registeringUser", isActivity: true, animate: false)
            alert!.delegate = self
            self.view.addSubview(alert!)

            uploadVisitingparser = UploadVisitingCardParser()
            uploadVisitingparser!.delegate = self
            uploadVisitingparser!.addBrochure(shopId: shopID!, fileName: AddVisitingCardViewController.TEMP_FILE_NAME)
        }
    }

    func loadDeleteBrochureParser(){

        if deleteVisitingParser == nil {

            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }

            alert = NimapAlertView(frame: CGRectZero, tag: "registeringUser", isActivity: true, animate: false)
            alert!.delegate = self
            self.view.addSubview(alert!)

            let brochure = brochures![currentIndex-1]

            deleteVisitingParser = DeleteVisitingCardParser()
            deleteVisitingParser!.delegate = self
            deleteVisitingParser!.deleteBrochure(brochureId: brochure.brochureId!, shopId: brochure.shopId!)
        }
    }

    func loadDeleteBrochureButton(){

        var buttonHeight : CGFloat = 0.0
        var fontSize : CGFloat = 0.0

        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
        {
            buttonHeight =  30.0
            fontSize = 20.0
        }
        else
        {
            buttonHeight =  deviceManager!.deviceYCGFloatValue(yPos: 30.0)
            fontSize =  deviceManager!.deviceXCGFloatValue(xPos: 20.0)
        }

        deleteButton = UIButton(frame:CGRectMake(self.view.frame.size.width - buttonHeight,navigationHeight,buttonHeight,buttonHeight))
        deleteButton!.setTitle(AppConstant.Static.DELETE_ICON, forState: UIControlState.Normal)
        deleteButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        deleteButton!.titleLabel!.font = UIFont.appFontAwesome(forSize: fontSize)
        deleteButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        deleteButton!.backgroundColor = UIColor.clearColor()
        deleteButton!.addTarget(self, action: #selector(AddVisitingCardViewController.deleteBrochureButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(deleteButton!)

        deleteButton!.center = CGPointMake(deleteButton!.center.x, shopLabel!.center.y)
    }

    //MARK: Unloading Methods

    func unloadPage()
    {
        unloadNavigationBar()
        unloadBrochureGrid()
        unloadShopLabel()
        unloadUploadBrochureButton()
        unloadUploadBrochureParser()
        unloadDelteBrochureButton()
        unloadDeleteBrochureParser()
        shop = nil
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
    }

    func unloadBrochureGrid()
    {
        if brochureGridView != nil {
            brochureGridView!.removeFromSuperview()
            brochureGridView = nil
        }

        if noBrochureImageView != nil {

            noBrochureImageView!.removeFromSuperview()
            noBrochureImageView = nil
        }

        if brochures != nil{

            brochures = nil
        }
    }

    func unloadShopLabel()
    {
        if shopLabel != nil {
            shopLabel!.removeFromSuperview()
            shopLabel = nil
        }

        if headerBGView != nil {
            headerBGView!.removeFromSuperview()
            headerBGView = nil
        }
    }

    func unloadUploadBrochureButton(){

        if uploadButton != nil {

            uploadButton!.removeFromSuperview()
            uploadButton = nil
        }
    }

    func unloadUploadBrochureParser(){

        if uploadVisitingparser != nil {
            uploadVisitingparser!.loader?.cancel()
            uploadVisitingparser!.delegate = nil
            uploadVisitingparser = nil
        }
    }

    func unloadDelteBrochureButton(){

        if deleteButton != nil {

            deleteButton!.removeFromSuperview()
            deleteButton = nil
        }
    }

    func unloadDeleteBrochureParser(){

        if deleteVisitingParser != nil {
            deleteVisitingParser!.asyncloader!.cancel()
            deleteVisitingParser!.delegate = nil
            deleteVisitingParser = nil
        }
    }

    //MARK: UIScrollViewDelegate Methods

    func scrollViewDidEndDecelerating(scrollView: UIScrollView){

        currentIndex = Int((brochureGridView!.contentOffset.x+(0.5*brochureGridView!.frame.size.width))/brochureGridView!.frame.width)+1
    }

    //MARK: Local methods

    func showPickImageView(){

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)

        // Create the actions.
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: .Default) { _ in

            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){

                if self.imagePicker == nil {

                    self.maintainTheState = true

                    self.imagePicker = UIImagePickerController()
                    self.imagePicker!.delegate = self
                    self.imagePicker!.sourceType = UIImagePickerControllerSourceType.Camera;
                    self.imagePicker!.mediaTypes = [kUTTypeImage as String]
                    self.imagePicker!.allowsEditing = false
                    self.imagePicker!.modalPresentationStyle = UIModalPresentationStyle.CurrentContext

                    self.presentViewController(self.imagePicker!, animated: true, completion: nil)
                }

            }
        }

        let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .Default) { _ in

            if self.imagePicker == nil {

                self.maintainTheState = true

                self.imagePicker = UIImagePickerController()
                self.imagePicker!.delegate = self
                self.imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                self.imagePicker!.mediaTypes = [kUTTypeImage as String]
                self.imagePicker!.allowsEditing = false
                self.imagePicker!.modalPresentationStyle = UIModalPresentationStyle.CurrentContext

                self.presentViewController(self.imagePicker!, animated: true, completion: nil)
            }
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in
        }

        // Add the actions.
        alertController.addAction(takePhotoAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)

        // Configure the alert controller's popover presentation controller if it has one.
        if let popoverPresentationController = alertController.popoverPresentationController {

            if deviceManager!.deviceType == deviceManager!.iPad{
                popoverPresentationController.sourceRect = CGRectMake(0.0, self.view.frame.size.height - 600.0, self.view.frame.size.width, 5.0)
            }
            else{
                popoverPresentationController.sourceRect = CGRectMake(0.0, self.view.frame.size.height - 5.0, self.view.frame.size.width, 5.0)
            }
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.permittedArrowDirections = .Up
        }

        presentViewController(alertController, animated: true, completion: nil)
    }

    //MARK: Webservices Callbacks

    //MARK: GetBrochureByShopParserDelegate method
    func didReceivedAllBrochureResult(resultCode resultCode : Int){

        getVisitingParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == GetBrochureByShopParser.GET_ALL_BROCHURE_SUCCESS{

            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "downloadAllBrochures\(shopID!)")

            loadBrochureGrid()
        }
        else{

            var errorMessage = ""
            var errorTag = ""

            if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            else if resultCode == GetBrochureByShopParser.NO_BROCHURE_FOUND
            {
                errorMessage = "You have not uploaded any brochures yet!!. Upload one."
                errorTag = "NobrochureError"

                if noBrochureImageView == nil {
                    noBrochureImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "nobrochure")))
                    noBrochureImageView!.center = self.view.center
                    self.view.addSubview(noBrochureImageView!)

                }
                else{
                    noBrochureImageView!.hidden = false
                }
                deleteButton!.hidden = true
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "updateError"
            }
            else if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "updateError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message:errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    //MARK: UploadVisitingCardParserDelegate method

    func didUploadVisitingCardResult(resultCode: Int)
    {
        uploadVisitingparser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }


        if resultCode == UploadVisitingCardParser.BROCHURE_UPLOAD_SUCCESS
        {
            if uploadButton == nil {
                loadUploadBrochureButton()
            }
            else{
                uploadButton!.hidden = false
            }

            if deleteButton == nil {
                loadDeleteBrochureButton()
            }
            else{
                deleteButton!.hidden = false
            }

            unloadBrochureGrid()
            loadBrochureGrid()
        }
        else{

            var errorMessage = ""
            var errorTag = ""

            if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "updateError"
            }
            else if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "updateError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message:errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }


    }

    //MARK: DeleteVisitingCardParserDelegate methods
    func didRecievedDeleteBrochureResultError(resultCode : Int){

        deleteVisitingParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == DeleteVisitingCardParser.DELETE_BROCHURE_SUCCESS{

            if currentIndex >= brochures!.count{

                currentIndex = currentIndex - 2
            }

            if deleteButton == nil {
                loadDeleteBrochureButton()
            }
            else{
                deleteButton!.hidden = false
            }

            if uploadButton == nil {
                loadUploadBrochureButton()
            }
            else{
                uploadButton!.hidden = false
            }

            unloadBrochureGrid()
            loadBrochureGrid()
        }
        else{

            var errorMessage = ""
            var errorTag = ""

            if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "updateError"
            }
            else if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "updateError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message:errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    //MARK: Event Handlers

    func uploadBrochureButtonPressed(){

        showPickImageView()
    }

    func deleteBrochureButtonPressed(){

        print("current \(currentIndex)")

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        alert = NimapAlertView(frame: CGRectZero, choosetag: "delete", title: "Delete", message: "Do you want to delete this image / brochure?", actionButtonTitle: "Yes", secondActionButtonTitle: "No", animate: true, keyboardType: UIKeyboardType.Default)
        alert!.delegate = self
        self.view.addSubview(alert!)
    }

    //MARK: NimapNavigationBarDelegate

    func backButtonPressed(){

        self.navigationController!.popViewControllerAnimated(true)
    }

    //MARK: UIGestureRecognizer Events

    func zoomImage(sender : UIGestureRecognizer)
    {
        if zoomView == nil
        {
            zoomView = UIZoomableView(frame: self.view.frame, zoomView: (brochureGridView!.viewWithTag(currentIndex)! as! AsyncView).bg!)
            zoomView!.delegate = self
            self.view.addSubview(zoomView!)
        }
    }

    //MARK: UIZoomableViewDelegate
    func didCancel(){

        if zoomView != nil
        {
            zoomView!.delegate = nil
            zoomView!.removeFromSuperview()
            zoomView = nil
        }
    }

    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){

        // Need to save the image data in the temp file and once that is done we need to send the image data to the server.

        if fileOperation!.isFileExist(fileName: AddVisitingCardViewController.TEMP_FILE_NAME) {

            fileOperation!.removeFile(name: AddVisitingCardViewController.TEMP_FILE_NAME)
        }

        let image = (info["UIImagePickerControllerOriginalImage"] as! UIImage).fixOrientation()

        fileOperation!.updateData(data: UIImageJPEGRepresentation(image, 0.3)!, withFileName: AddVisitingCardViewController.TEMP_FILE_NAME)


        if imagePicker != nil{

            imagePicker!.dismissViewControllerAnimated(true, completion: nil)
            imagePicker = nil
        }

        // need to send image data to the chat server

        loadUploadBrochureParser()
    }


    func imagePickerControllerDidCancel(picker: UIImagePickerController){

        if imagePicker != nil{

            imagePicker!.dismissViewControllerAnimated(true, completion: nil)
            imagePicker = nil
        }
    }

    //MARK: NimapAlertViewDelegate methods

    func didActionButtonPressed(tag : String!){

        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }

        if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
        else if tag == "delete"{

            loadDeleteBrochureParser()
        }
    }

    func didSecondaryActionButtonPressed(tag : String!){
        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }
    }
}
