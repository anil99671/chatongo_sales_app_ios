//
//  GetBusinessCategoriesParser.swift
//  VendorApp
//
//  Created by Zubin - Nimap on 12/04/16.
//  Copyright © 2016 ChatOnGo. All rights reserved.
//

import UIKit

class GetBusinessCategoriesParser: NSObject,AsyncLoaderModelDelegate
{
    var loader : AsyncLoaderModel?
    var salesDB : SalesAppDBModel?
    
    
    //MARK: Various Webservices calling methods
    func getCategoriesBanners()
    {
        loader = AsyncLoaderModel()
        loader!.getDataFromURLStringWithOutSecurity(webURL:AppConstant.Static.BASE_URL+"GetBusinessCategoryWithoutImage", dataIndex: -1)
        loader!.delegate = self
    }
    
    
    //MARK: AsyncLoaderModelDelegate callbacks
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("GetBusiCategory: responseString \(responseString!)")
        processData(data: data)
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        self.loader = nil
    }
    
    //MARK: Various processing  methods
    
    func processData(data data : NSData)
    {
        var result : NSDictionary?
        
        do{
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = result!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                let data = result!.objectForKey("data") as! [NSDictionary]

                salesDB = SalesAppDBModel.sharedInstance()
                salesDB!.deleteBusinessCategory()
            
                for i in 0..<data.count
                {
                    let businessCategory = data[i]
                    
                    let BusinessCategoryName = businessCategory.objectForKey("BusinessCategoryName") as! String
                    let BusinessCategoryId = businessCategory.objectForKey("BusinessCategoryId") as! Int

                    if (salesDB!.isBusinessCategoryExist(businessCategoryId: BusinessCategoryId) == true)
                    {
                        salesDB!.updateIntoBusinessCategory(businessCategoryId: BusinessCategoryId, businessCategoryName: BusinessCategoryName, isChecked: 0)
                    }
                    else
                    {
                        salesDB!.insertIntoBusinessCategory(businessCategoryId: BusinessCategoryId, businessCategoryName: BusinessCategoryName)
                    }
                }
            }
        }
        catch{
            // Error Occured
        }
    }
}