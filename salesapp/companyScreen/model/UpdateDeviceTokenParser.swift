//
//  UpdateDeviceTokenParser.swift
//  ChatOnGo
//
//  Created by Apple on 09/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol UpdateDeviceTokenParserDelegate : NSObjectProtocol
{
    func didReceivedUpdateDeviceTokenResult(resultCode resultCode : Int)
}

class UpdateDeviceTokenParser: NSObject,AsyncLoaderModelDelegate
{
    struct Static {
        static var UPDATE_USER_DEVICE_TOKEN_SUCCESS = 3
    }

    //MARK: Varibale Decalaration

    var loader : AsyncLoaderModel?
    weak var delegate : UpdateDeviceTokenParserDelegate?

    func deviceTokenParser()
    {
        let jsonDictionary = NSMutableDictionary()
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("userID") as! Int, forKey: "UserId")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("userTypeID") as! Int, forKey: "UserTypeId")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("name") as! String, forKey: "name")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("mobileNo") as! String, forKey: "MobileNo")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("countryCode") as! String, forKey: "CountryCode")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("contactNo") as! String, forKey: "ContactNo")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("email") as! String, forKey: "Email")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("gender") as! String, forKey: "Gender")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("dob") as! String, forKey: "dob")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("country") as! String, forKey: "Country")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("state") as! String, forKey: "State")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("city") as! String, forKey: "City")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("streetAddress") as! String, forKey: "StreetAddress")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("zipCode") as! String, forKey: "ZipCode")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("imeiNo") as! String, forKey: "ImeiNo")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("deviceToken") as! String, forKey: "DeviceToken")

        jsonDictionary.setObject("I", forKey: "OsType")

        jsonDictionary.setObject("", forKey: "Image")

        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }

//        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
//        print("DeviceTokenParser requestString is \(requestString!)")
        //
        let jsonDataLenght = "\(jsonData!.length)"


        let url = NSURL(string: AppConstant.Static.BASE_URL+"UpdateDevicetoken")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData

        loader = AsyncLoaderModel()
        loader!.getDataFromRequest(request: request, dataIndex: -1)
        loader!.delegate = self
    }


    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("DeviceTokenParser responseString is \(responseString!)")

        processDeviceToken(data: data)

        self.loader = nil
    }

    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            delegate!.didReceivedUpdateDeviceTokenResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    /*
     this method will extract the data from the JSON into the preferences which will remain persistent
     */

    func processDeviceToken(data data : NSData)
    {
        var resultMaster : NSDictionary!
        do{
            try resultMaster = NSJSONSerialization.JSONObjectWithData(data
                , options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let Status = resultMaster!.objectForKey("Status") as! Int

            if Status == 200
            {
                if delegate != nil{

                    delegate!.didReceivedUpdateDeviceTokenResult(resultCode: UpdateDeviceTokenParser.Static.UPDATE_USER_DEVICE_TOKEN_SUCCESS)
                }
            }
            else if Status == 401
            {
                if delegate != nil{

                    delegate!.didReceivedUpdateDeviceTokenResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
                }
            }
            else
            {
                if delegate != nil{

                    delegate!.didReceivedUpdateDeviceTokenResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            if delegate != nil{
                
                delegate!.didReceivedUpdateDeviceTokenResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}
