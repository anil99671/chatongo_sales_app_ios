//
//  LoginUserParser.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 10/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

protocol LoginUserParserDelegate : NSObjectProtocol
{
    func didReceivedLoginUserResult(resultCode resultCode : Int)
}

class LoginUserParser: NSObject,AsyncLoaderModelDelegate {
   
    //MARK: Varibale Decalaration
    
    var loader : AsyncLoaderModel?
    weak var delegate : LoginUserParserDelegate?

    func loginUserWith(countryCode countryCode : String!,mobileno : String!, emailid : String!)
    {
        let jsonDictionary = NSMutableDictionary()
        
        let appVersion = NSUserDefaults.standardUserDefaults().objectForKey("AppVersion") != nil ? NSUserDefaults.standardUserDefaults().objectForKey("AppVersion")! as! String : "1.0"
        
        jsonDictionary.setObject(4, forKey: "UserTypeId")
        jsonDictionary.setObject(countryCode!, forKey: "CountryCode")
        jsonDictionary.setObject(emailid!, forKey: "EmailId")
        jsonDictionary.setObject(mobileno!, forKey: "MobileNo")
        if UIDevice.currentDevice().modelName == "Simulator"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            jsonDictionary.setObject(appDelegate!.imeiNumber, forKey: "ImeiNo")
        }
        else
        {
            jsonDictionary.setObject(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKey: "ImeiNo")
        }
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("userID")!, forKey: "UserId")
        
         jsonDictionary.setObject(appVersion, forKey: "AppVersion")
        
        jsonDictionary.setObject("I", forKey: "OsType")
        
        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        
        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        print("LoginUserParser requestString is \(requestString!)")
        
        let jsonDataLenght = "\(jsonData!.length)"
        
        
        let url = NSURL(string: AppConstant.Static.BASE_URL+"UserLogin")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.getDataFromRequest(request: request, dataIndex: -1)
        loader!.delegate = self
        
    }
    
    
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("LoginUserParser responseString is \(responseString!)")
        
        processLoginUserData(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            
            delegate!.didReceivedLoginUserResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }
    
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    
    func processLoginUserData(data data : NSData)
    {
        
        var resultMaster : NSDictionary!
        
        do{
            
            try resultMaster = NSJSONSerialization.JSONObjectWithData(data
                , options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = resultMaster!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                
                let result = resultMaster!.objectForKey("data") as? NSDictionary
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserId"), forKey: "userID")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserTypeId"), forKey: "userTypeID")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserPassword") as! String, forKey: "password")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Name") as! String, forKey: "name")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("MobileNo"), forKey: "mobileNo")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ContactNo") as! String, forKey: "contactNo")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Email") as! String, forKey: "email")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Gender") as! String, forKey: "gender")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Dob") as! String, forKey: "dob")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("CountryCode") as! String, forKey: "countryCode")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Country") as! String, forKey: "country")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("State") as! String, forKey: "state")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("City") as! String, forKey: "city")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("CountryId") , forKey: "countryId")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("StreetAddress") as! String, forKey: "streetAddress")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ZipCode") as! String, forKey: "zipCode")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("IsActive"), forKey: "active")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ImeiNo") as! String, forKey: "imeiNo")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("DeviceToken") as! String, forKey: "deviceToken")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Image"), forKey: "ProfileImage")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("OsType") as! String, forKey: "osType")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("CreatedDate") as! String, forKey: "createDate")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ModifiedDate") as! String, forKey: "modifiedDate")
                
                NSUserDefaults.standardUserDefaults().setInteger(Int(result!.objectForKey("Otp") as! String)!, forKey: "otp")

                // Some additional settings for internal screen
                
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "donwloadEnquiries")
                
                NSUserDefaults.standardUserDefaults().synchronize()
                
                if delegate != nil{
                    
                    delegate!.didReceivedLoginUserResult(resultCode: AppConstant.Static.LOGIN_USER_SUCCESS)
                }
            }
            else if Status == 401
            {
                if delegate != nil
                {
                    
                    delegate!.didReceivedLoginUserResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
                }
            }
            else if Status == 404
            {
                if delegate != nil
                {
                    
                    delegate!.didReceivedLoginUserResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
                }
            }
            else
            {
                if delegate != nil{
                    
                    delegate!.didReceivedLoginUserResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
            
        }
        catch
        {
            if delegate != nil{
                
                delegate!.didReceivedLoginUserResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}
