//
//  BusinessCategoryModel.swift
//  VendorAppDB
//
//  Created by Apple on 05/07/16.
//  Copyright © 2016 nimap. All rights reserved.
//

import UIKit

class BusinessCategoryModel: NSObject {
    var businessCategoryId : Int?
    var imageURL : String?
    var businessCategoryName : String?
    var businessCategoryImage : UIImage?
    var loader : AsyncLoaderModel?
    var ModifiedDate : String?

    var shopId : Int?
    var ImageId : Int?
    var isImageLoaded : Bool =  false
    var isChecked : Int?
    var isSelected : Bool = false;
    var shopIconColorId : Int? = 0
    var iconHexColor : String?
    var initials : String?
}
