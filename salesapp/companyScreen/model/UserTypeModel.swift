//
//  UserTypeModel.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 01/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

class UserTypeModel: NSObject
{
    var userTypeID : Int?
    var userType : String?
}
