//
//  GetUserTypesParser.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 01/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

protocol GetUserTypesParserDelegate : NSObjectProtocol
{
    func didReceivedGetUserTypesResult(resultCode resultCode : Int)
}

class GetUserTypesParser: NSObject, AsyncLoaderModelDelegate {
    
    //MARK: Varibale Decalaration
    
    var loader : AsyncLoaderModel?
    weak var delegate : GetUserTypesParserDelegate?
    
    func getUserTypes()
    {
        loader = AsyncLoaderModel()
        loader!.getDataFromURLStringWithOutSecurity(webURL: AppConstant.Static.BASE_URL+"GetUserTypes", dataIndex: -1)
        loader!.delegate = self
    }
    
    //MARK: AsyncLoaderModelDelegate methods
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("GetUserTypesParser responseString \(responseString!)")
        processUserTypes(data: data)
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil{
            
            delegate!.didReceivedGetUserTypesResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }
    
    
    //MARK: Processing methods
    /*
    This method we process the UserType data and insert into the local DB.
    Thus we first check whether userTypeID exists if yes than we update the 
    UserType table else we insert new value into UserType
    
    */
    func processUserTypes(data data : NSData!)
    {
        var result : NSDictionary?
        
        do {
            
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = result!.objectForKey("Status") as? Int
            
            if Status == 200
            {
//                let appDB = CustomerAppDB.sharedInstance()

                let results = result!.objectForKey("data") as? [NSDictionary]
                
                for i in 0..<results!.count{
                    
                    let usertypedata = results![i]
                    var userType = ""
                    var userTypeID = 4
                    
                    userType = usertypedata.objectForKey("UserTypeName") as! String!
                    userTypeID = Int(usertypedata.objectForKey("UserTypeId") as! String!)!
                    
//                    if appDB.selectUserTypeID(userTypeID: userTypeID) == true
//                    {
//                        appDB.updateUserTypeWith(userTypeID: userTypeID, userType: userType)
//                    }
//                    else
//                    {
//                        appDB.insertIntoUserType(userTypeID: userTypeID, userType: userType)
//                    }
                }
                
                if delegate != nil{
                    
                    delegate!.didReceivedGetUserTypesResult(resultCode: AppConstant.Static.GET_USER_TYPE_SUCCESS)
                }
            }
            else
            {
                if delegate != nil{
                    
                    delegate!.didReceivedGetUserTypesResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
            
        }
        catch
        {
            if delegate != nil{
                
                delegate!.didReceivedGetUserTypesResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}
