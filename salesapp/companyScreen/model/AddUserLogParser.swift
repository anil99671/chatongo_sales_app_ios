//
//  AddUserLogParser.swift
//  ChatOnGoCustomer
//
//  Created by Zubin - Nimap on 24/05/16.
//  Copyright © 2016 ChatOnGo. All rights reserved.
//

import UIKit

protocol AddUserLogParserDelegate : NSObjectProtocol
{
    func didReceivedAddUserLogParserResult(appVersionStatus status : Int)
}

class AddUserLogParser: NSObject,AsyncLoaderModelDelegate
{
    
    //MARK: Varibale Decalaration
    
    var loader : AsyncLoaderModel?
    weak var delegate : AddUserLogParserDelegate?
    
    
    static let APP_VERSION_UNABLE_TO_PROCESS = 0;
    static let APP_VERSION_SAME = 1;
    static let APP_VERSION_DIFFERENT_FORCE_UPDATE = 2;
    static let APP_VERSION_DIFFERENT_NO_FORCE_UPDATE = 3;
    
    func addUserLog()
    {
        
        let jsonDictionary = NSMutableDictionary()
        
        jsonDictionary.setValue(4, forKey: "UserTypeId")// For the sales app it will always be 4
        if UIDevice.currentDevice().modelName == "Simulator"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            jsonDictionary.setObject(appDelegate!.imeiNumber, forKey: "ImeiNo")
        }
        else
        {
            jsonDictionary.setObject(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKey: "ImeiNo")
        }
        jsonDictionary.setObject("I", forKey: "OsType")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("UserId") != nil ? NSUserDefaults.standardUserDefaults().objectForKey("UserId")! as! String : "-1", forKey: "userID")
        
        
        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        
        let jsonDataLenght = "\(jsonData!.length)"
        
//        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
//        print("AddUserLog requestString is \(requestString!)")
        
        let url = NSURL(string: AppConstant.Static.BASE_URL+"AddUserLog")
        
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.getDataFromRequestWithOutSecurity(request: request, dataIndex: -1)
        loader!.delegate = self
        
    }
    
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("AddUserLog responseString is \(responseString!)")
        
        processAddUserData(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil{
            
            delegate!.didReceivedAddUserLogParserResult(appVersionStatus: AppConstant.Static.CONNECTION_ERROR)
        }
    }
    
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    
    func processAddUserData(data data : NSData)
    {
        
        var resultMaster : NSDictionary!
        
        do{
            
            try resultMaster = NSJSONSerialization.JSONObjectWithData(data
                , options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = resultMaster!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                 // It is successful response from the server we can process the json file
                var  result1 =  false;
                var  isForceUpdate = false;
                
                let results = resultMaster!.objectForKey("data") as! NSArray
                
                for i in 0..<results.count
                {
                    
                    if results[i].valueForKey("OsType") as! String! == "I" && results[i].valueForKey("AppType") as! String! == "Sales"
                    {
                        result1 = true;
                        isForceUpdate = results[i].valueForKey("ForceUpdate") as! Bool
                        
                        NSUserDefaults.standardUserDefaults().setObject(results[i].objectForKey("Version"), forKey: "AppVersion")
                        
                        break
                    }

                }
                if(result1 == false)
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedAddUserLogParserResult(appVersionStatus: AddUserLogParser.APP_VERSION_UNABLE_TO_PROCESS)
                    }
                }
                else
                {
                    var appVersion: String?
                    
                    appVersion = NSUserDefaults.standardUserDefaults().objectForKey("AppVersion") != nil ? NSUserDefaults.standardUserDefaults().objectForKey("AppVersion")! as! String : "na"
                    
                    print("appVersion \(appVersion!)")
                    
                    if appVersion == "na"
                    {
                        if delegate != nil
                        {
                            delegate!.didReceivedAddUserLogParserResult(appVersionStatus: AddUserLogParser.APP_VERSION_UNABLE_TO_PROCESS)
                            
                        }
                    }
                    else
                    {
                        let nsObject: AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"]
                        
                        let version = nsObject as! String
                        
                        print("version \(version)")
                        
                        if version == appVersion
                        {
                            if delegate != nil
                            {
                                delegate!.didReceivedAddUserLogParserResult(appVersionStatus: AddUserLogParser.APP_VERSION_SAME)
                            }
                        }
                        else
                        {
                            if isForceUpdate
                            {
                                if delegate != nil
                                {
                                    delegate!.didReceivedAddUserLogParserResult(appVersionStatus: AddUserLogParser.APP_VERSION_DIFFERENT_FORCE_UPDATE)
                                }
                            }
                            else
                            {
                                if delegate != nil
                                {
                                    delegate!.didReceivedAddUserLogParserResult(appVersionStatus: AddUserLogParser.APP_VERSION_DIFFERENT_NO_FORCE_UPDATE)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if delegate != nil{
                    
                    delegate!.didReceivedAddUserLogParserResult(appVersionStatus:AddUserLogParser.APP_VERSION_UNABLE_TO_PROCESS)
                }
            }
        }
        catch
        {
            if delegate != nil{
                
                delegate!.didReceivedAddUserLogParserResult(appVersionStatus:AddUserLogParser.APP_VERSION_UNABLE_TO_PROCESS)
            }
        }
    }
}
