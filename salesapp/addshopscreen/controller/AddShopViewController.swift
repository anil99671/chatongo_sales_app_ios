//
//  AddShopViewController.swift
//  ChatOnGoVendor
//
//  Created by Zubin Gala on 25/05/16.
//  Copyright © 2016 nimapinfotech. All rights reserved.
//

import UIKit

protocol AddShopViewControllerDelegate : NSObjectProtocol
{
    func didShopAdded(resultCode : Int)
}
class AddShopViewController: UIViewController,NimapNavigationBarViewDelegate,UITextFieldDelegate,UITextViewDelegate,NimapAlertViewDelegate,AddShopViewDelegate,StateListViewControllerDelegate,CityListViewControllerDelegate,AddShopParserDelegate,LocationManagerDelegate,NimapDualRoundButtonViewDelegate,VendorDetailsSViewDelegate
{
    //MARK: Variable Declaration
    
    static let SHOP_ADDED_SUCCESSFULLY = 0
    static let SHOP_ADDED_CANCEL = 1
    
    weak var delegate: AddShopViewControllerDelegate?

    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    var pageHeight : CGFloat?
    
    var deviceManager : DeviceManager?
    
    var addShopView : AddShopView?
    var addShopParser : AddShopParser?
    var alert : NimapAlertView?
    var actionButton : NimapDualRoundButtonView?
    var baseView : UIView?
    var fullBgView : UIView?
    var vendorDetailView : VendorDetailsSView?
    
    var countryname : String?
    var countryid : Int?
    var vendorDetailButton : UIButton?
    var addShopButton : UIButton?
    var pickLocationButton : UIButton?
    
    var businessCategoryIds : [Int]?
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var statename : String?
    var stateid : Int?
    
    var businessCategories : [BusinessCategoryModel!]?
    var locationManager : LocationManager?
    
    var SalesDB : SalesAppDBModel?

    var businessController : BusinessCategoriesViewController?
    var countryController : CountryListViewController?
    var stateController : StateListViewController?
    var cityController : CityListViewController?

    var shopID : Int?
    var vendorName : String? = ""
    var vendorNumber : String? = ""
    var vendorEmail : String? = ""


    //MARK: Constructers
    init(){
        super.init(nibName: nil, bundle: nil)

        self.shopID = -1
        stateid = -1
    }

    init(shopID shopId : Int)
    {
        super.init(nibName: nil, bundle: nil)

        self.shopID = shopId
        stateid = -1
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: ViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
        deviceManager = DeviceManager.sharedDeviceManagement()
        SalesDB = SalesAppDBModel.sharedInstance()
        SalesDB!.updateAllBusinessCategoryCheckedToIdeal()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        loadnavigationBar()
        loadPage()
        addShopView!.setToolbar()
        commitBusinessCategories()
        
        countryid = NSUserDefaults.standardUserDefaults().valueForKey("countryId") as? Int
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }

     //MARK: Loading Methods
    
    func loadnavigationBar()
    {
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
        {
            navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
        }
        else
        {
            navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
        }
        
        pageHeight = self.view.frame.height - navigationHeight
        
        if navigationBar == nil
        {
            var title = ""
            if shopID != -1
            {
                title = "Update Shop"
            }
            else{
                title = "Add Shop"
            }
            
            navigationBar = NimapNavigationBarView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, navigationHeight), title: title)
            navigationBar!.delegate = self
            navigationBar!.backgroundColor = UIColor.primaryColor()
            
            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)
            
            self.view.addSubview(navigationBar!)
            
        }
        
    }
    
    func loadPage()
    {

        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: 30)

        if addShopView == nil
        {
            addShopView = AddShopView(frame: CGRectMake(0, navigationHeight, self.view.frame.size.width  ,self.view.frame.size.height - (buttonHeight + navigationHeight)))
            addShopView!.backgroundColor = UIColor.whiteColor()
            addShopView!.delegate = self
            self.view.addSubview(addShopView!)
        }
        


        var buttonWidth : CGFloat = 0.0
        let posY = self.view.frame.height - buttonHeight
        var posX : CGFloat = 0.0
        
        if shopID != -1
        {
            buttonWidth = (self.view.frame.size.width - 2.0)/2
        }
        else{
            buttonWidth = (self.view.frame.size.width - 2.0)/3

            if vendorDetailButton == nil
            {
                vendorDetailButton = UIButton(frame: CGRectMake(0,posY,buttonWidth,buttonHeight))
                vendorDetailButton!.setTitle("Vendor Detail", forState: UIControlState.Normal)
                vendorDetailButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                vendorDetailButton!.backgroundColor = UIColor.primaryColor()
                vendorDetailButton!.addTarget(self, action: #selector(AddShopViewController.vendorDetailsButtonPressed), forControlEvents: UIControlEvents.TouchDown)
                vendorDetailButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
                self.view.addSubview(vendorDetailButton!)

                posX = vendorDetailButton!.frame.origin.x  + vendorDetailButton!.frame.size.width + 1.0
            }

        }

        if pickLocationButton == nil
        {
            pickLocationButton = UIButton(frame: CGRectMake(posX,posY,buttonWidth,buttonHeight))
            pickLocationButton!.setTitle("Pick Location", forState: UIControlState.Normal)
            pickLocationButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            pickLocationButton!.backgroundColor = UIColor.primaryColor()
            pickLocationButton!.addTarget(self, action: #selector(AddShopViewController.pickLocationButtonPressed), forControlEvents: UIControlEvents.TouchDown)
            pickLocationButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
            self.view.addSubview(pickLocationButton!)

            posX = pickLocationButton!.frame.origin.x  + pickLocationButton!.frame.size.width + 1.0
        }

        if addShopButton == nil
        {
            addShopButton = UIButton(frame: CGRectMake(posX,posY,buttonWidth,buttonHeight))
            if shopID != -1
            {
                addShopButton!.setTitle("Update Shop", forState: UIControlState.Normal)
                addShopView!.businessAddressTextView!.textColor = UIColor.blackColor()
            }
            else{
                addShopButton!.setTitle("Add Shop", forState: UIControlState.Normal)
            }
            addShopButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            addShopButton!.backgroundColor = UIColor.primaryColor()
            addShopButton!.addTarget(self, action: #selector(AddShopViewController.addShopButtonPressed), forControlEvents: UIControlEvents.TouchDown)
            addShopButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
            self.view.addSubview(addShopButton!)
        }

        


        if shopID != -1{

            let shop = SalesDB!.selectShopForId(shopId: shopID!)

            addShopView!.shopNameTextfield!.text = shop.name
            addShopView!.mobileNumberTextfield!.text = shop.mobileNo
            addShopView!.alternateNumberTextfield!.text = "\(shop.contactNo!)"
            addShopView!.businessAddressTextView!.text = shop.streetAddress
            addShopView!.businessAddressTextView!.textColor = UIColor.blackColor()
            addShopView!.countryTextField!.text = "India"
            addShopView!.stateLabel!.text = shop.state
            addShopView!.stateLabel!.textColor = UIColor.primaryTextColor()
            addShopView!.cityLabel!.text = shop.city
            addShopView!.cityLabel!.textColor = UIColor.primaryTextColor()

            addShopView!.zipTextField!.text = shop.zipcode
            addShopView!.localAreaTextfield!.text = shop.locality

            let timings1 = shop.timings!.characters.split{$0 == "-"}.map(String.init)

            addShopView!.fromTimingTextfield!.text = timings1[0]
            addShopView!.toTimingTextfield!.text = timings1[1]

            let categories = SalesDB!.selectAllSelectedBusinessCategory(shopId: shopID!)

            if categories.count > 0
            {
                for i in 0..<categories.count
                {
                    SalesDB!.updateBusinessCategoryAsChecked(businessCategoryId: categories[i].businessCategoryId!)
                }
            }
            else
            {
                addShopView!.selectBusinessCategoryLabel!.text = "Select Business Category"
            }
        }
    }

    func loadVenderDetailView()
    {
        addShopView!.userInteractionEnabled = false
        vendorDetailButton!.userInteractionEnabled = false
        pickLocationButton!.userInteractionEnabled = false
        addShopButton!.userInteractionEnabled = false


        fullBgView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height))
        fullBgView!.center = self.view.center
        fullBgView!.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        fullBgView!.userInteractionEnabled = false
        self.view.addSubview(fullBgView!)

        let height : CGFloat = (0.32 * self.view.frame.size.height)
        baseView = UIView(frame: CGRectMake(0,0,(0.7 * self.view.frame.size.width),height + deviceManager!.deviceYCGFloatValue(yPos:30.0)))
        baseView!.center = self.view.center
        baseView!.layer.cornerRadius = 10.0
        baseView!.backgroundColor = UIColor.whiteColor()
        baseView!.userInteractionEnabled = true
        self.view.addSubview(baseView!)

        vendorDetailView =  VendorDetailsSView(frame:CGRectMake(0,0,(0.7 * self.view.frame.size.width),height))
        vendorDetailView!.delegate = self
        baseView!.addSubview(vendorDetailView!)

        if vendorName != ""
        {
            vendorDetailView!.nameTextField!.text = vendorName
        }
        if vendorNumber != ""
        {
            vendorDetailView!.mobileTextField!.text = vendorNumber
        }
        if vendorEmail != ""
        {
            vendorDetailView!.emailTextField!.text = vendorEmail
        }

        actionButton = NimapDualRoundButtonView(frame: CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: 10.0),0,vendorDetailView!.frame.size.width - deviceManager!.deviceXCGFloatValue(xPos: 20.0), deviceManager!.deviceYCGFloatValue(yPos:30.0)))
        actionButton!.center = CGPointMake(vendorDetailView!.frame.size.width/2, vendorDetailView!.frame.size.height)
        actionButton!.delegate = self
        baseView!.addSubview(actionButton!)
        actionButton!.leftActionButton!.setTitle("Ok", forState: UIControlState.Normal)
        actionButton!.rightActionButton!.setTitle("Cancel", forState: UIControlState.Normal)
    }

    func commitBusinessCategories()
    {
        
        if businessCategories != nil
        {
            businessCategories = nil
        }

        businessCategories = SalesDB!.getAllCheckedBusinessCategories()
        
        if businessCategories!.count > 0
        {
            var appendData : String = ""
            for i in 0..<businessCategories!.count
            {
                appendData = appendData + "\(businessCategories![i].businessCategoryName!)"
                
                if i != businessCategories!.count - 1
                {
                    appendData = appendData + ","
                }
            }
            addShopView!.selectBusinessCategoryLabel!.text = appendData
            addShopView!.selectBusinessCategoryLabel!.textColor = UIColor.blackColor() 
        }
        else
        {
            addShopView!.selectBusinessCategoryLabel!.text = "Select Business Category"
        }
    }
    
    //MARK: Unloading Methods
    
    func unloadPage()
    {
        unloadNavigationBar()
        unloadButtons()
        unloadAlert()
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
    }

    func unloadButtons()
    {
        if addShopButton != nil
        {
            addShopButton!.removeFromSuperview()
            addShopButton = nil
        }

        if pickLocationButton != nil
        {
            pickLocationButton!.removeFromSuperview()
            pickLocationButton = nil
        }

        if vendorDetailButton != nil
        {
            vendorDetailButton!.removeFromSuperview()
            vendorDetailButton = nil
        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }
    
    //MARK: Various Callback Methods

    //MARK: AddShopParserDelegate callback

    func didReceivedAddShopResult(resultCode resultCode : Int, shopId : Int!)
    {
        addShopParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == AddShopParser.ADD_SHOP_SUCCESS{

            SalesDB!.updateAllBusinessCategoryCheckedToIdeal()

            if shopId != -1{
                NSUserDefaults.standardUserDefaults().setObject(false, forKey: "shopUpgrade")
            }
            else{
                NSUserDefaults.standardUserDefaults().setObject(true, forKey: "shopUpgrade")
            }
            
            if delegate != nil {
                delegate!.didShopAdded(AddShopViewController.SHOP_ADDED_SUCCESSFULLY)
                
            }

            self.navigationController!.popViewControllerAnimated(true)
        }
        else{
            var errorMessage = ""
            var errorTag = ""

            if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "addShopError"
            }
            else if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "addShopError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message:errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }
    
    //MARK : AddShopViewDelegate callback
    
    func formValidationResult(status : Int, message : String)
    {
        var isVendorDetailFill = true

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if shopID == -1
        {
            if (vendorName == "" || vendorNumber == "" || vendorEmail == "")
            {
                isVendorDetailFill = false
            }
        }
        
        if status == AddShopView.FORM_VALIDATION_SUCCESS && isVendorDetailFill == true
        {
            if addShopParser == nil
            {
                
                if businessCategoryIds != nil
                {
                    businessCategoryIds = nil
                }
                
                businessCategoryIds = []

                for i in 0..<businessCategories!.count
                {
                    businessCategoryIds!.append(businessCategories![i].businessCategoryId!)
                }
                
                alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
                alert!.delegate = self
                self.view.addSubview(alert!)
                
                
                addShopParser = AddShopParser()
                addShopParser!.delegate = self

                if shopID == -1{
                    addShopParser!.addShop(vendorName: vendorName!, vendorMobileNo: vendorNumber!, vendorCountryCode: "+91", vendorEmailId: vendorEmail!, shopName: addShopView!.shopNameTextfield!.text!, mobileNo: addShopView!.mobileNumberTextfield!.text!, contactNo: addShopView!.alternateNumberTextfield!.text!, country: addShopView!.countryTextField!.text!, state: addShopView!.stateLabel!.text!, city: addShopView!.cityLabel!.text!, zipCode: addShopView!.zipTextField!.text!, latitude: "\(latitude)", longitude: "\(longitude)", timings: "\(addShopView!.fromTimingTextfield!.text!) - \(addShopView!.toTimingTextfield!.text!)", streetAddress: addShopView!.businessAddressTextView!.text!, locality: addShopView!.localAreaTextfield!.text!, businessCategoryId: businessCategoryIds!)
                }
                else{
                    let shop = SalesDB!.selectShopForId(shopId: shopID!)

                    addShopParser!.updateShop(shopID!, shopName: addShopView!.shopNameTextfield!.text!, mobileNo: addShopView!.mobileNumberTextfield!.text!, contactNo: addShopView!.alternateNumberTextfield!.text!, country: addShopView!.countryTextField!.text!, state: addShopView!.stateLabel!.text!, city: addShopView!.cityLabel!.text!, zipCode: addShopView!.zipTextField!.text!, latitude: "\(latitude)", longitude: "\(longitude)", timings: "\(addShopView!.fromTimingTextfield!.text!) - \(addShopView!.toTimingTextfield!.text!)", streetAddress: addShopView!.businessAddressTextView!.text!, locality: addShopView!.localAreaTextfield!.text!, businessCategoryId: businessCategoryIds!, osType: shop.osType!)
                }

            }
            
        }
        else
        {
            if isVendorDetailFill == false && shopID == -1
            {
                alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "formValidationError", message: "Please enter Vendor detail first!", actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
            else
            {
                alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "formValidationError", message: message, actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }
    }
    
    //MARK:  StateParserDelegate callback
    
    func didRecievedUserSelectedState(stateName: String, stateId: Int)
    {
        self.statename = stateName
        self.stateid = stateId
        addShopView!.cityLabel!.text = "City"

        NSUserDefaults.standardUserDefaults().setObject(stateName , forKey: "state")
        NSUserDefaults.standardUserDefaults().setObject(stateId , forKey: "stateId")
        addShopView!.stateLabel!.text = stateName
        addShopView!.stateLabel!.textColor = UIColor.primaryTextColor()
        
    }
    
    //MARK:  CityParserDelegate callback
    
    func didRecievedUserSelectedCity(cityName: String, cityId: Int)
    {
        self.countryname = cityName
        self.countryid = cityId
        NSUserDefaults.standardUserDefaults().setObject(cityName , forKey: "city")
        addShopView!.cityLabel!.text = cityName
        addShopView!.cityLabel!.textColor = UIColor.primaryTextColor()
    }
    //MARK: LocationManagerDelegate methods
    
    func didReceiveLocation(currentLocation location : LocationManagerModel?)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        
        if location != nil {
            
            if location!.formattedAddess != nil {
                addShopView!.businessAddressTextView!.text = location!.formattedAddess
                addShopView!.businessAddressTextView!.textColor = UIColor.blackColor()
            }
            
            if location!.name != nil {
                addShopView!.shopNameTextfield!.text = location!.name
            }
            
            if location!.country != nil {
                addShopView!.countryTextField!.text = location!.country
            }
            
            if location!.administrativeArea != nil {
                addShopView!.stateLabel!.text = location!.administrativeArea
            }
            
            if location!.subAdministrativeArea != nil {
                addShopView!.cityLabel!.text = location!.subAdministrativeArea
            }
            
            if location!.postalCode != nil {
                addShopView!.zipTextField!.text = location!.postalCode
            }
            
            latitude = location!.latitude!
            longitude = location!.longitude!
        }
        
        locationManager = nil
    }
    
    //MARK: UITouch Events

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        let touch = touches.first

        if touch!.view == addShopView!.selectBusinessCategoryLabel
        {
            addShopView!.placeholderText = ""

            businessController = BusinessCategoriesViewController(shopId: shopID!)
            self.navigationController!.pushViewController(businessController!, animated: true)

        }
        if touch!.view == addShopView!.countryTextField!
        {
            addShopView!.countryTextField!.resignFirstResponder()
            countryController = CountryListViewController()
            self.navigationController!.pushViewController(countryController!, animated: true)

        }

        if touch!.view == addShopView!.stateLabel!
        {
            addShopView!.cityLabel!.text = "City"
            if countryid != -1
            {
                addShopView!.stateLabel!.resignFirstResponder()
                stateController = StateListViewController()
                stateController!.countryId = NSUserDefaults.standardUserDefaults().valueForKey("countryId") as? Int
                stateController!.statedelegate = self
                self.navigationController!.pushViewController(stateController!, animated: true)
            }
            else
            {
                alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "formValidationError", message: AddShopView.STATE_ERROR, actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }

        if touch!.view == addShopView!.cityLabel!
        {
            if stateid != -1
            {
                addShopView!.cityLabel!.resignFirstResponder()
                cityController = CityListViewController()
                cityController!.stateId = NSUserDefaults.standardUserDefaults().valueForKey("stateId") as? Int
                cityController!.citydelegate = self
                self.navigationController!.pushViewController(cityController!, animated: true)
            }
            else
            {
                alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "formValidationError", message: AddShopView.CITY_ERROR, actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }
    }

    //MARK: VendorDetailViewDelegate delegate

    func vendorFormValidationResult(status : Int, message : String)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        else if status == RegisterationView.FORM_VALIDATION_SUCCESS
        {
            vendorName = vendorDetailView!.nameTextField!.text!
            vendorNumber = vendorDetailView!.mobileTextField!.text!
            vendorEmail = vendorDetailView!.emailTextField!.text!

            fullBgView!.removeFromSuperview()
            fullBgView = nil
            baseView!.removeFromSuperview()
            baseView = nil
            addShopView!.userInteractionEnabled = true
            vendorDetailButton!.userInteractionEnabled = true
            pickLocationButton!.userInteractionEnabled = true
            addShopButton!.userInteractionEnabled = true


        }
        else
        {
            alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "formValidationError", message: message, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

     //MARK: Event Handlers
    
    //MARK: NimapAlertViewDelegate
    func didActionButtonPressed(tag: String!)
    {

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    //MARK: NimapDualRoundButtonViewDelegate

    func didPrimaryDualRoundActionButtonPressed()
    {
        vendorDetailView!.dismissAllTextFileds()
        translateViewBy(yPos: deviceManager!.deviceYCGFloatValue(yPos: 240.0))
        vendorDetailView!.validateForm()
    }

    func didSecondaryDualRoundActionButtonPressed()
    {
        fullBgView!.removeFromSuperview()
        fullBgView = nil
        baseView!.removeFromSuperview()
        baseView = nil
        addShopView!.userInteractionEnabled = true
        vendorDetailButton!.userInteractionEnabled = true
        pickLocationButton!.userInteractionEnabled = true
        addShopButton!.userInteractionEnabled = true


    }

    //MARK: NimapNavigationBarDelegate

    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }

    
    //MARK: Button Events
    func addShopButtonPressed()
    {
        addShopView!.validateForm()
    }
    
    func pickLocationButtonPressed()
    {
        if locationManager == nil{
            
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }
            
            alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
            alert!.delegate = self
            self.view.addSubview(alert!)
            
            
            locationManager = LocationManager(isForceEnable: true, isReverserGeoLocationEnable: true,isLocationContinous: false)
            locationManager!.delegate = self
            locationManager!.startLocationService()
            
        }
        
    }

    func vendorDetailsButtonPressed()
    {
        loadVenderDetailView()
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }

    //MARK: Animation Methods

    func translateViewBy(yPos yPos :CGFloat)
    {
        UIView.animateWithDuration(0.5, delay: 0.1, options: UIViewAnimationOptions.AllowAnimatedContent, animations: {
            self.baseView!.center.y = yPos
            }, completion: nil)
    }

    func canMoveWithDisplacement(yPos : CGFloat)
    {
        translateViewBy(yPos: yPos)
    }
   
  
}


