//
//  AddShopView.swift
//  ChatOnGoVendor
//
//  Created by ranka on 02/06/2016.
//  Copyright © 2016 nimapinfotech. All rights reserved.
//

import UIKit

@objc protocol AddShopViewDelegate : NSObjectProtocol
{
    optional func formValidationResult(status : Int, message : String)
}

class AddShopView: UIView,UITextViewDelegate,UITextFieldDelegate
{
    var deviceManager : DeviceManager?
    weak var delegate : AddShopViewDelegate?

    var padding: CGFloat?
    var labelWidth : CGFloat?
    var labelHeight : CGFloat?
    var posY : CGFloat?
    var posX : CGFloat?

    var shopNameTextfield : UITextField?
    var selectBusinessCategoryLabel : UILabel?
    var mobileNumberTextfield : UITextField?
    var alternateNumberTextfield : UITextField?
    var businessAddressTextView : UITextView?
    var countryTextField : UITextField?
    var stateLabel : UILabel?
    var cityLabel : UILabel?
    var zipTextField : UITextField?
    var localAreaTextfield : UITextField?
    var fromTimingTextfield : UITextField?
    var toTimingTextfield : UITextField?

    var fromTimingDatePicker  : UIDatePicker?
    var toTimingDatePicker : UIDatePicker?
    let toolBar = UIToolbar()

    var placeholderText :String?

    static let FORM_VALIDATION_IDEAL = 0
    static let SHOP_NAME_BLANK = 1
    static let BUSINESS_NAME_BLANK = 2
    static let MOBILE_NO_BLANK = 3
    static let MOBILE_NO_INVALID = 4
    static let ALTERNATE_NO_BLANK = 5
    static let ADDRESS_BLANK = 6
    static let COUNTRY_BLANK = 7
    static let STATE_BLANK = 8
    static let CITY_BLANK = 9
    static let ZIPCODE_BLANK = 10
    static let ZIPCODE_INAVLID = 11
    static let LOCAL_AREA_BLANK = 12
    static let FROM_TIMING_BLANK = 13
    static let TO_TIMING_BLANK = 14
    static let FORM_VALIDATION_SUCCESS = 15
    static let VENDOR_DETAIL_BLANK = 15

    static let SHOP_BLANK_MESSAGE = "Shop name cannot be blank"
    static let BUSINESS_NAME_BLANK_MESSAGE = "You need to select atleast one business category."
    static let MOBILE_BLANK_MESSAGE = "Mobile number cannot be blank"
    static let MOBILE_INVALID_MESSAGE = "Invalid Mobile number"
    static let ALTERNATE_NUMBER_BLANK_MESSAGE = "Alternate number cannot be blank"
    static let ADDRESS_BLANK_MESSAGE = "Address field cannot be blank"
    static let COUNTRY_BLANK_MESSAGE = "Country field cannot be blank"
    static let STATE_BLANK_MESSAGE = "State field cannot be blank"
    static let STATE_ERROR = "First select the country"
    static let CITY_BLANK_MESSAGE = "City field cannot be blank"
    static let CITY_ERROR = "First select the state"
    static let ZIPCODE_BLANK_MESSAGE = "Zipcode field cannot be blank"
    static let ZIPCODE_INVALID_MESSAGE = "Zipcode field cannot have more then 6 characters"
    static let LOCAL_AREA_BLANK_MESSAGE = "Local area field cannot be blank"
    static let FROM_TIMING_BLANK_MESSAGE = "Please enter the opening timing of the shop"
    static let TO_TIMING_BLANK_MESSAGE = "Please enter the closing timing of the shop"


    override init(frame: CGRect)
    {
        super.init(frame: frame)

        deviceManager  =   DeviceManager.sharedDeviceManagement()

        padding = deviceManager!.deviceXCGFloatValue(xPos: 10)
        labelWidth = self.frame.size.width - (2*padding!)
        labelHeight = deviceManager!.deviceYCGFloatValue(yPos: 30)
        posY = deviceManager!.deviceYCGFloatValue(yPos: 5)

        shopNameTextfield = UITextField(frame:CGRectMake(padding!,posY!,labelWidth! ,labelHeight!))
        shopNameTextfield!.delegate = self
        shopNameTextfield!.placeholder = "Shop Name"
        shopNameTextfield!.enabled = true
        shopNameTextfield!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        shopNameTextfield!.textColor = UIColor.primaryTextColor()
        self.addSubview(shopNameTextfield!)

        posY = shopNameTextfield!.frame.origin.y + shopNameTextfield!.frame.size.height + padding!

        selectBusinessCategoryLabel = UILabel(frame:CGRectMake(padding!,posY!,labelWidth!,labelHeight!))
        placeholderText = "Select Business Category"
        selectBusinessCategoryLabel!.text = placeholderText
        selectBusinessCategoryLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        selectBusinessCategoryLabel!.userInteractionEnabled = true
        selectBusinessCategoryLabel!.textColor = UIColor.lightGrayColor()
        selectBusinessCategoryLabel!.numberOfLines = 2
        self.addSubview(selectBusinessCategoryLabel!)

        posY = selectBusinessCategoryLabel!.frame.origin.y + selectBusinessCategoryLabel!.frame.size.height + padding!


        mobileNumberTextfield = UITextField(frame:CGRectMake(padding!,posY!,labelWidth!,labelHeight!))

        mobileNumberTextfield!.placeholder = "Mobile Number"
        mobileNumberTextfield!.keyboardType = .NumberPad
        mobileNumberTextfield!.inputAccessoryView = self.toolBar
        mobileNumberTextfield!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        mobileNumberTextfield!.textColor = UIColor.primaryTextColor()
        self.addSubview(mobileNumberTextfield!)

        posY = mobileNumberTextfield!.frame.origin.y + mobileNumberTextfield!.frame.size.height + padding!

        alternateNumberTextfield = UITextField(frame:CGRectMake(padding!,posY!,labelWidth!,labelHeight!))
        alternateNumberTextfield!.keyboardType = .NumberPad
        alternateNumberTextfield!.inputAccessoryView = self.toolBar
        alternateNumberTextfield!.placeholder = "Alternate Number"
        alternateNumberTextfield!.delegate = self
        alternateNumberTextfield!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        alternateNumberTextfield!.textColor = UIColor.primaryTextColor()
        self.addSubview(alternateNumberTextfield!)

        posY = alternateNumberTextfield!.frame.origin.y + alternateNumberTextfield!.frame.size.height + padding!

        businessAddressTextView = UITextView(frame:CGRectMake(padding!,posY!,labelWidth!,labelHeight! + (2*padding!)))
        businessAddressTextView!.text = "Business Address"
        businessAddressTextView!.textColor = UIColor.lightGrayColor()
        businessAddressTextView!.delegate = self
        businessAddressTextView!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        businessAddressTextView!.textAlignment = NSTextAlignment.Left
        businessAddressTextView!.layer.borderColor = UIColor.secondaryTextColor().CGColor
        businessAddressTextView!.layer.borderWidth = 1.0;
        self.addSubview(businessAddressTextView!)

        posY = businessAddressTextView!.frame.origin.y + businessAddressTextView!.frame.size.height + padding!

        countryTextField = UITextField(frame:CGRectMake(padding!,posY!,labelWidth!/2.0,labelHeight!))
        countryTextField!.delegate = self
        countryTextField!.enabled = false
        countryTextField!.text = "India"
        countryTextField!.userInteractionEnabled = true
        countryTextField!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        countryTextField!.textColor = UIColor.primaryTextColor()
        self.addSubview(countryTextField!)

        posX = countryTextField!.frame.origin.x + countryTextField!.frame.size.width + padding!
        posY = businessAddressTextView!.frame.origin.y + businessAddressTextView!.frame.size.height + padding!

        stateLabel = UILabel(frame:CGRectMake(posX!,posY!,labelWidth!/2.0 - padding!,labelHeight!))
        stateLabel!.userInteractionEnabled = true
        stateLabel!.text = "State"
        stateLabel!.textColor = UIColor.blackColor()
        stateLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        self.addSubview(stateLabel!)

        posY = countryTextField!.frame.origin.y + countryTextField!.frame.size.height + padding!

        cityLabel = UILabel(frame:CGRectMake(padding!,posY!,labelWidth!/2.0,labelHeight!))
        cityLabel!.text = "City"
        cityLabel!.textColor = UIColor.blackColor()
        cityLabel!.userInteractionEnabled = true
        cityLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        self.addSubview(cityLabel!)

        posX = cityLabel!.frame.origin.x + cityLabel!.frame.size.width + padding!
        posY = stateLabel!.frame.origin.y + stateLabel!.frame.size.height + padding!

        zipTextField = UITextField(frame:CGRectMake(posX!,posY!,labelWidth!/2.0 - padding!,labelHeight!))
        zipTextField!.delegate = self
        zipTextField!.placeholder = "Zip"
        zipTextField!.keyboardType = .NumberPad
        zipTextField!.inputAccessoryView = toolBar
        zipTextField!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        zipTextField!.textColor = UIColor.primaryTextColor()
        self.addSubview(zipTextField!)

        posY = cityLabel!.frame.origin.y + cityLabel!.frame.size.height + padding!

        localAreaTextfield = UITextField(frame:CGRectMake(padding!,posY!,labelWidth!,labelHeight!))
        localAreaTextfield!.delegate = self
        localAreaTextfield!.placeholder = "Local Area"
        localAreaTextfield!.inputAccessoryView = toolBar
        localAreaTextfield!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        localAreaTextfield!.textColor = UIColor.primaryTextColor()
        self.addSubview(localAreaTextfield!)

        posY = localAreaTextfield!.frame.origin.y + localAreaTextfield!.frame.size.height + padding!

        fromTimingTextfield = UITextField(frame:CGRectMake(padding!,posY!,labelWidth!/2.0,labelHeight!))
        fromTimingTextfield!.delegate = self
        fromTimingTextfield!.placeholder = "From Timing"
        fromTimingTextfield!.inputAccessoryView = toolBar
        fromTimingTextfield!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        fromTimingTextfield!.textColor = UIColor.primaryTextColor()
        self.addSubview(fromTimingTextfield!)

        posX = fromTimingTextfield!.frame.origin.x + fromTimingTextfield!.frame.size.width + padding!
        posY = localAreaTextfield!.frame.origin.y + localAreaTextfield!.frame.size.height + padding!

        toTimingTextfield = UITextField(frame:CGRectMake(posX!,posY!,labelWidth!/2.0 - padding!,labelHeight!))
        toTimingTextfield!.delegate = self
        toTimingTextfield!.placeholder = "To Timing"
        toTimingTextfield!.inputAccessoryView = toolBar
        toTimingTextfield!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 13.0))
        toTimingTextfield!.textColor = UIColor.primaryTextColor()
        self.addSubview(toTimingTextfield!)

        loadTextFieldBorders()

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    func loadTextFieldBorders()
    {
        shopNameTextfield!.showOnlyBottomBorder()
        selectBusinessCategoryLabel!.showOnlyBottomBorder()
        mobileNumberTextfield!.showOnlyBottomBorder()
        alternateNumberTextfield!.showOnlyBottomBorder()
        countryTextField!.showOnlyBottomBorder()
        stateLabel!.showOnlyBottomBorder()
        cityLabel!.showOnlyBottomBorder()
        zipTextField!.showOnlyBottomBorder()
        localAreaTextfield!.showOnlyBottomBorder()
        fromTimingTextfield!.showOnlyBottomBorder()
        toTimingTextfield!.showOnlyBottomBorder()
    }

    //MARK: validate method

    func validateForm()
    {
        var errorMessage = ""
        var errorStatus = AddShopView.FORM_VALIDATION_IDEAL

        if mobileNumberTextfield!.text!.characters.count == 10
        {
            let index = mobileNumberTextfield!.text!.startIndex
            let characterStr = mobileNumberTextfield!.text![index]
            if (!(characterStr == "9" || characterStr == "8" || characterStr == "7"))
            {
                errorMessage = AddShopView.MOBILE_INVALID_MESSAGE
                errorStatus = AddShopView.MOBILE_NO_INVALID
            }
        }
        if shopNameTextfield!.text!.isEmpty{

            errorMessage = AddShopView.SHOP_BLANK_MESSAGE
            errorStatus = AddShopView.SHOP_NAME_BLANK
        }
        else if selectBusinessCategoryLabel!.text!.isEmpty
        {
            errorMessage = AddShopView.BUSINESS_NAME_BLANK_MESSAGE
            errorStatus = AddShopView.BUSINESS_NAME_BLANK
        }
        else if selectBusinessCategoryLabel!.text == "Select Business Category"
        {
            errorMessage = AddShopView.BUSINESS_NAME_BLANK_MESSAGE
            errorStatus = AddShopView.BUSINESS_NAME_BLANK
        }
        else if mobileNumberTextfield!.text!.isEmpty{

            errorMessage = AddShopView.MOBILE_BLANK_MESSAGE
            errorStatus = AddShopView.MOBILE_NO_BLANK
        }
        else if mobileNumberTextfield!.text!.characters.count != 10
        {
            errorMessage = AddShopView.MOBILE_INVALID_MESSAGE
            errorStatus = AddShopView.MOBILE_NO_INVALID
        }
        else if businessAddressTextView!.text!.isEmpty || businessAddressTextView!.text == "Business Address"{

            errorMessage = AddShopView.ADDRESS_BLANK_MESSAGE
            errorStatus = AddShopView.ADDRESS_BLANK
        }
        else if stateLabel!.text!.isEmpty || stateLabel!.text! == "State"{

            errorMessage = AddShopView.STATE_BLANK_MESSAGE
            errorStatus = AddShopView.STATE_BLANK
        }
        else if cityLabel!.text!.isEmpty || cityLabel!.text! == "City"
        {
            errorMessage = AddShopView.CITY_BLANK_MESSAGE
            errorStatus = AddShopView.CITY_BLANK
        }
        else if zipTextField!.text!.isEmpty{

            errorMessage = AddShopView.ZIPCODE_BLANK_MESSAGE
            errorStatus = AddShopView.ZIPCODE_BLANK
        }
        else if zipTextField!.text!.characters.count != 6
        {
            errorMessage = AddShopView.ZIPCODE_INVALID_MESSAGE
            errorStatus = AddShopView.ZIPCODE_INAVLID
        }
        else if localAreaTextfield!.text!.isEmpty{

            errorMessage = AddShopView.LOCAL_AREA_BLANK_MESSAGE
            errorStatus = AddShopView.LOCAL_AREA_BLANK
        }
        else if fromTimingTextfield!.text!.isEmpty{

            errorMessage = AddShopView.FROM_TIMING_BLANK_MESSAGE
            errorStatus = AddShopView.FROM_TIMING_BLANK
        }
        else if toTimingTextfield!.text!.isEmpty{

            errorMessage = AddShopView.TO_TIMING_BLANK_MESSAGE
            errorStatus = AddShopView.TO_TIMING_BLANK
        }



        if errorMessage == ""
        {
            errorStatus = AddShopView.FORM_VALIDATION_SUCCESS
        }

        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(AddShopViewDelegate.formValidationResult(_:message:)))
            {
                delegate!.formValidationResult!(errorStatus, message: errorMessage)
            }
        }
    }

    //MARK: UItextFieldDelegate method

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        setScreenToNormal()
        return true
    }

    func textFieldDidBeginEditing(textField: UITextField)
    {
        if textField == countryTextField
        {
            self.scrollTextField(countryTextField!)
        }
        else if textField == zipTextField
        {
            self.scrollTextField(zipTextField!)
        }

        else if textField == localAreaTextfield
        {
            self.scrollTextField(localAreaTextfield!)
        }
        else if textField == toTimingTextfield
        {
            self.dateField(toTimingTextfield!)
            self.scrollTextField(toTimingTextfield!)
        }
        else if textField == fromTimingTextfield
        {
            self.dateField(fromTimingTextfield!)
            self.scrollTextField(fromTimingTextfield!)
        }
    }

    func dateField(textfield: UITextField)
    {
        if textfield == fromTimingTextfield
        {
            fromTimingDatePicker = UIDatePicker()
            fromTimingDatePicker!.datePickerMode = UIDatePickerMode.Time
            textfield.inputView = fromTimingDatePicker
            fromTimingDatePicker!.addTarget(self, action: #selector(AddShopView.handleFromTimingDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        }

        if textfield == toTimingTextfield
        {
            toTimingDatePicker = UIDatePicker()
            toTimingDatePicker!.datePickerMode = UIDatePickerMode.Time
            textfield.inputView = toTimingDatePicker
            toTimingDatePicker!.addTarget(self, action: #selector(AddShopView.handleToTimingDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        }
    }

    func scrollTextField(textfield : UITextField)
    {
        switch(textfield)
        {
        case countryTextField!:
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations:
                {
                    self.frame = (CGRectMake(self.frame.origin.x, -self.countryTextField!.frame.origin.y + self.deviceManager!.deviceYCGFloatValue(yPos: 200), self
                        .frame.size.width, self.frame.size.height))
                }, completion: nil)
            break

        case zipTextField!:
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations:
                {
                    self.frame = (CGRectMake(self.frame.origin.x, -self.zipTextField!.frame.origin.y + self.deviceManager!.deviceYCGFloatValue(yPos: 200), self
                        .frame.size.width, self.frame.size.height))
                }, completion: nil)
            break

        case localAreaTextfield!:
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations:
                {
                    self.frame = (CGRectMake(self.frame.origin.x, -self.localAreaTextfield!.frame.origin.y + self.deviceManager!.deviceYCGFloatValue(yPos: 200), self
                        .frame.size.width, self.frame.size.height))
                }, completion: nil)
            break

        case fromTimingTextfield!:
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations:
                {
                    self.frame = (CGRectMake(self.frame.origin.x, -self.fromTimingTextfield!.frame.origin.y + self.deviceManager!.deviceYCGFloatValue(yPos: 200), self
                        .frame.size.width, self.frame.size.height))
                }, completion: nil)
            break

        case toTimingTextfield!:
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations:
                {
                    self.frame = (CGRectMake(self.frame.origin.x, -self.toTimingTextfield!.frame.origin.y + self.deviceManager!.deviceYCGFloatValue(yPos: 200), self
                        .frame.size.width, self.frame.size.height))
                }, completion: nil)
            
            break
        default: break
            
        }
    }

    //MARK: Date Picker method

    func handleFromTimingDatePicker(datepicker: UIDatePicker)
    {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateStyle = .NoStyle
        timeFormatter.timeStyle = .ShortStyle
        fromTimingTextfield!.text = timeFormatter.stringFromDate(datepicker.date)
    }

    func handleToTimingDatePicker(datepicker: UIDatePicker)
    {
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateStyle = .NoStyle
        timeFormatter.timeStyle = .ShortStyle
        toTimingTextfield!.text = timeFormatter.stringFromDate(datepicker.date)
    }

    //MARK: UItextViewDelegate method

    func textViewDidBeginEditing(textView: UITextView)
    {
        if textView.textColor == UIColor.lightGrayColor()
        {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
        self.scrollTextView(textView)
    }


    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Business Address"
            textView.textColor = UIColor.lightGrayColor()
        }
    }

    func scrollTextView(textView : UITextView)
    {
        switch(textView)
        {
        case businessAddressTextView!:

            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveEaseInOut, animations:
                {

                    self.frame = (CGRectMake(self.frame.origin.x, -self.businessAddressTextView!.frame.origin.y + self.deviceManager!.deviceYCGFloatValue(yPos: 200), self
                        .frame.size.width, self.frame.size.height))

                }, completion: nil)

            break

        default: break

        }
    }

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            textView.resignFirstResponder()
            setScreenToNormal()
            return false
        }
        return true
    }

    //MARK: Event handler for screen

    func setToolbar()
    {
        toolBar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: #selector(AddShopView.doneTapped))
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.backgroundColor = UIColor.clearColor()
        toolBar.userInteractionEnabled = true

    }


    func setScreenToNormal()
    {
        self.endEditing(true)
        UIView.animateWithDuration(0.1, delay: 0.1, options: .CurveEaseInOut, animations: {

            self.frame = (CGRectMake(0, 48, self.frame.size.width  ,self.frame.size.height))

            }, completion: nil)

    }

   
    func doneTapped()
    {
        shopNameTextfield!.resignFirstResponder()
        selectBusinessCategoryLabel!.resignFirstResponder()
        mobileNumberTextfield!.resignFirstResponder()
        alternateNumberTextfield!.resignFirstResponder()
        stateLabel!.resignFirstResponder()
        cityLabel!.resignFirstResponder()
        zipTextField!.resignFirstResponder()
        localAreaTextfield!.resignFirstResponder()
        fromTimingTextfield!.resignFirstResponder()
        toTimingTextfield!.resignFirstResponder()
        setScreenToNormal()
    }
  
}
