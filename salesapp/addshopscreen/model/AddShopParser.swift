//
//  AddShopParser.swift
//  JSONPractice
//
//  Created by Zubin Gala on 05/05/16.
//  Copyright © 2016 Nimapinfotech. All rights reserved.
//

import UIKit

protocol AddShopParserDelegate : NSObjectProtocol
{
    func didReceivedAddShopResult(resultCode resultCode : Int, shopId : Int!)
}


class AddShopParser: NSObject,AsyncLoaderModelDelegate {

    static let ADD_SHOP_SUCCESS = 3
    static let SHOP_ALREADY_EXIST = 5

    var loader : AsyncLoaderModel?
    weak var delegate : AddShopParserDelegate?
    var businessCategoryIds : [Int]?
    var shopId : Int?
    var osType : String = ""
    
    let salesDB = SalesAppDBModel.sharedInstance()
    
    func addShop(vendorName vendorName : String, vendorMobileNo : String, vendorCountryCode : String, vendorEmailId : String , shopName : String, mobileNo: String, contactNo : String, country : String,state : String, city : String, zipCode : String, latitude : String, longitude : String, timings : String, streetAddress : String, locality : String, businessCategoryId : [Int])
    {
        self.businessCategoryIds = businessCategoryId
        osType = "Web"

        let vendorDictionary = NSMutableDictionary()
        let shopDictionary = NSMutableDictionary()

        vendorDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("userID")!, forKey: "UserId")
        vendorDictionary.setObject(1, forKey: "UserTypeId")
        vendorDictionary.setObject(vendorCountryCode, forKey: "CountryCode")
        vendorDictionary.setObject(vendorMobileNo, forKey: "MobileNo")
        vendorDictionary.setObject(vendorEmailId, forKey: "EmailId")
        vendorDictionary.setObject(vendorName, forKey: "Name")
        vendorDictionary.setObject(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKey: "ImeiNo")

        vendorDictionary.setObject(osType, forKey: "OsType")
        vendorDictionary.setObject("NA", forKey: "DeviceToken")
        vendorDictionary.setObject("", forKey: "MobileBrand")
        vendorDictionary.setObject("", forKey: "MobileModel")
        vendorDictionary.setObject("", forKey: "MobileName")
        vendorDictionary.setObject("", forKey: "MobileCarrier")

        let appVersion = NSUserDefaults.standardUserDefaults().objectForKey("appVersion") != nil ? NSUserDefaults.standardUserDefaults().objectForKey("appVersion")! as! String : "1.0"
        vendorDictionary.setObject(appVersion, forKey: "AppVersion")

        vendorDictionary.setObject("NA", forKey: "Gender")
        vendorDictionary.setObject("1/1/1900", forKey: "Dob")
        vendorDictionary.setObject("NA", forKey: "Country")
        vendorDictionary.setObject("NA", forKey: "State")
        vendorDictionary.setObject("NA", forKey: "City")
        vendorDictionary.setObject("NA", forKey: "StreetAddress")
        vendorDictionary.setObject(zipCode, forKey: "ZipCode")
        vendorDictionary.setObject(false, forKey: "IsAuto")


        shopDictionary.setObject(shopName, forKey: "Name")
        shopDictionary.setObject(mobileNo, forKey: "MobileNo")
        shopDictionary.setObject(contactNo, forKey: "ContactNo")
        shopDictionary.setObject(country, forKey: "Country")
        shopDictionary.setObject(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKey: "ImeiNo")
        shopDictionary.setObject(state, forKey: "State")
        shopDictionary.setObject(city, forKey: "City")
        shopDictionary.setObject(zipCode, forKey: "ZipCode")
        shopDictionary.setObject(latitude, forKey: "Latitude")
        shopDictionary.setObject(longitude, forKey: "Longitude")
        shopDictionary.setObject(timings, forKey: "Timing")
        shopDictionary.setObject("NA", forKey: "Info")
        shopDictionary.setObject(streetAddress, forKey: "StreetAddress")
        shopDictionary.setObject(locality, forKey: "Locality")
        
        let keyword_list = businessCategoryIds! as NSArray
        shopDictionary.setObject(keyword_list, forKey: "Ids")

        vendorDictionary.setObject(shopDictionary, forKey: "shop")
        
        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(vendorDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        
        let jsonDataLenght = "\(jsonData!.length)"
        
//        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
//        print("RegisterShopFromSalesApp requestString is \(requestString!)")

        let url = NSURL(string: AppConstant.Static.BASE_URL+"RegisterShopFromSalesApp")
        
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.delegate = self
        loader!.getDataFromRequest(request: request, dataIndex: -1)
        
    }
    
    func updateShop(shopId : Int, shopName : String, mobileNo: String, contactNo : String, country : String,state : String, city : String, zipCode : String, latitude : String, longitude : String, timings : String, streetAddress : String, locality : String, businessCategoryId : [Int], osType : String?)
    {
        self.businessCategoryIds = businessCategoryId
        self.osType = osType!
        
        let jsonDictionary = NSMutableDictionary()
        
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("userID")!, forKey: "UserId")
        jsonDictionary.setObject(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKey: "ImeiNo")

        jsonDictionary.setObject(shopId, forKey: "ShopId")
        jsonDictionary.setObject(shopName, forKey: "Name")
        jsonDictionary.setObject(mobileNo, forKey: "MobileNo")
        jsonDictionary.setObject(contactNo, forKey: "ContactNo")
        jsonDictionary.setObject(country, forKey: "Country")
        jsonDictionary.setObject(state, forKey: "State")
        jsonDictionary.setObject(city, forKey: "City")
        jsonDictionary.setObject(zipCode, forKey: "ZipCode")
        jsonDictionary.setObject(latitude, forKey: "Latitude")
        jsonDictionary.setObject(longitude, forKey: "Longitude")
        jsonDictionary.setObject(timings, forKey: "Timing")
        jsonDictionary.setObject("NA", forKey: "Info")
        jsonDictionary.setObject(streetAddress, forKey: "StreetAddress")
        jsonDictionary.setObject(locality, forKey: "Locality")
        jsonDictionary.setObject(osType!, forKey: "OsType")

        let keyword_list = businessCategoryIds! as NSArray
        jsonDictionary.setObject(keyword_list, forKey: "Ids")
        
        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        
        let jsonDataLenght = "\(jsonData!.length)"
        
        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        print("UpdateShop requestString is \(requestString!)")

        let url = NSURL(string: AppConstant.Static.BASE_URL+"UpdateShop")
        
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.delegate = self
        loader!.getDataFromRequest(request: request, dataIndex: -1)
        
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("AddShopParser/UpdateShop responseString is \(responseString!)")

        processAddShopData(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil{
            
            delegate!.didReceivedAddShopResult(resultCode: AppConstant.Static.CONNECTION_ERROR,shopId: shopId)
        }
    }
    
    func processAddShopData(data data : NSData)
    {
        
        var resultMaster : NSDictionary!
        
        do{
            
            try resultMaster = NSJSONSerialization.JSONObjectWithData(data
                , options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = resultMaster!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                // It is successful response from the server we can process the json file

                let shopJSON = resultMaster!.objectForKey("data") as? NSDictionary
                let subscriptionObject = shopJSON!.objectForKey("subscriptiondata") as? NSDictionary
                
                shopId = shopJSON!.objectForKey("ShopId") as? Int

                let name = shopJSON!.objectForKey("Name") as? String
                let shopName = shopJSON!.objectForKey("ShopName") as? String
                let userId = shopJSON!.objectForKey("UserId") as? Int
                let streetAdd = shopJSON!.objectForKey("StreetAddress") as? String
                let mobileNo = shopJSON!.objectForKey("MobileNo") as? String
                let contactNo = shopJSON!.objectForKey("ContactNo") as? String
                let country = shopJSON!.objectForKey("Country") as? String
                let state = shopJSON!.objectForKey("State") as? String
                let city = shopJSON!.objectForKey("City") as? String
                let zipCode = shopJSON!.objectForKey("ZipCode") as? String
                let latitude = shopJSON!.objectForKey("Latitude") as? String
                let longitute = shopJSON!.objectForKey("Longitude") as? String
                let info = shopJSON!.objectForKey("Info") as? String
                let timing = shopJSON!.objectForKey("Timing") as? String
                let locality = shopJSON!.objectForKey("Locality") as? String
                let subscriptionTypeId = subscriptionObject!.objectForKey("SubscriptionTypeId") as? Int
                let startDate = subscriptionObject!.objectForKey("StartDate") as? String
                let endDate = subscriptionObject!.objectForKey("EndDate") as? String
                let visitDate = shopJSON!.objectForKey("VisitDate") as? String
                let isVisited = shopJSON!.objectForKey("IsVisited") as? String
                let isAppDownloaded = shopJSON!.objectForKey("IsAppDownloaded") as? String
                let isPending = shopJSON!.objectForKey("IsPending") as? String
                let id = shopJSON!.objectForKey("Id") as? Int
//                let osType = shopJSON!.objectForKey("OsType") as! String
                
                
                if ((salesDB!.isShopExists(shopId: shopId!)) == true)
                {
                    salesDB!.updateShopDetails(shopId: shopId!, userId: userId!, name: name!, streetAddress: streetAdd!, mobileNo: mobileNo!, contactNo: contactNo!, country: country!, state: state!, city: city!, zipCode: zipCode!, latitude: Double(latitude!)!, longitude: Double(longitute!)!, info: info!, timings: timing!, locality: locality!, subscriptionTypeId: String(subscriptionTypeId!), startDate: startDate!, endDate: endDate!, osType: osType)
                }
                else
                {
                    salesDB!.insertIntoShopDetails(id: id!, shopId: shopId!, vendorId: userId!, name: shopName!, streetAddress: streetAdd!, mobileNo: mobileNo!, contactNo: contactNo!, country: country!, state: state!, city: city!, zipCode: zipCode!, latitude: Double(latitude!)!, longitude: Double(longitute!)!, visitDate: visitDate!, isVisited: isVisited!, isAppDownloaded: isAppDownloaded!, info: info!, timings: timing!, locality: locality!, subscriptionTypeId: String(subscriptionTypeId!), startDate: startDate!, endDate: endDate!, isPending: isPending!,osType: osType)
                }

                salesDB!.deleteSelectedBusinessShopForId(shopId: shopId!)
                
                let businessCategoryData = shopJSON!.objectForKey("businesscategorydata") as? NSArray

                for j in 0 ..< businessCategoryData!.count
                {
                    let businessDict = businessCategoryData![j] as! NSDictionary

                    let buninessCategoryId = businessDict.objectForKey("BusinessCategoryId") as! Int
                    let businessCategoryName = businessDict.objectForKey("BusinessCategoryName") as! String


                    if  salesDB!.isBusinessCategoryExistForShop(shopId: shopId!, businessCategoryId: buninessCategoryId) == true
                    {
                        salesDB!.updateIntoSelectedBusinessShop(shopId: shopId!, businessCategoryId: buninessCategoryId, businessCategoryName: businessCategoryName, isChecked: 1)
                    }
                        
                    else
                    {
                        salesDB!.insertIntoSelectedBusinessShop(shopId: shopId!, businessCategoryId: buninessCategoryId, businessCategoryName: businessCategoryName, isChecked: 1)
                    }
                }

                if delegate != nil
                {
                    
                    delegate!.didReceivedAddShopResult(resultCode: AddShopParser.ADD_SHOP_SUCCESS,shopId: shopId)
                }
            }
            else if(Status == 401)
            {
                if delegate != nil
                {

                    delegate!.didReceivedAddShopResult(resultCode: AppConstant.Static.UNAUTHORISED_USER, shopId: shopId)
                }
            }
            else if(Status == 409)
            {
                if delegate != nil
                {

                    delegate!.didReceivedAddShopResult(resultCode: AddShopParser.SHOP_ALREADY_EXIST, shopId: shopId)
                }
            }
            else
            {
                if delegate != nil{
                    
                    delegate!.didReceivedAddShopResult(resultCode: AppConstant.Static.PROCESSING_ERROR,shopId: shopId)
                }
            }
            
            
        }
        catch
        {
            if delegate != nil{
                
                delegate!.didReceivedAddShopResult(resultCode: AppConstant.Static.PROCESSING_ERROR,shopId: shopId)
            }
            
            
        }
        
    }
    
    
    
    
}
