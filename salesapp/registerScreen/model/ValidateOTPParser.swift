//
//  ValidateOTPParser.swift
//  VendorApp
//
//  Created by Zubin - Nimap on 26/03/16.
//  Copyright © 2016 ChatOnGo. All rights reserved.
//

import UIKit

@objc protocol ValidateOTPParserDelegate : NSObjectProtocol
{
    func didReceivedValidateUserResult(resultCode: Int)
}
class ValidateOTPParser: NSObject,AsyncLoaderModelDelegate
{
    
    //MARK: Varibale Decalaration
    
    var loader : AsyncLoaderModel?
    weak var delegate : ValidateOTPParserDelegate?
    
    static let VALIDATE_OPT_SUCCESS = 3
    
    func validateOTPFor(otp : Int)
    {
        let jsonDictionary = NSMutableDictionary()
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("userID")!, forKey: "UserId")

        jsonDictionary.setValue(4, forKey: "UserTypeId")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("mobileNo") as! String, forKey: "MobileNo")
        
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("countryCode") as! String, forKey: "CountryCode")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("email") as! String, forKey: "EmailId")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("imeiNo")!, forKey: "ImeiNo")
        
        jsonDictionary.setObject(otp, forKey: "OtpNo")
        
        var jsonData: NSData?
        
        
        
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        let jsonDataLenght = "\(jsonData!.length)"
        
        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        print("Validate OTP requestString is \(requestString!)")
        
        let url = NSURL(string: AppConstant.Static.BASE_URL+"ValidateOTP")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.getDataFromRequestWithOutSecurity(request: request, dataIndex: -1)
        loader!.delegate = self
    }
    
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("validate OTP responseString is \(responseString!)")
        
        processValidateOTP(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {

            delegate!.didReceivedValidateUserResult(AppConstant.Static.CONNECTION_ERROR)
        }
    }
    
    func processValidateOTP(data data : NSData)
    {
        
        let resultMaster : NSDictionary?
        
        do{
            
            try resultMaster = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = resultMaster!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                let result = resultMaster!.objectForKey("data") as? NSDictionary
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserId"), forKey: "userID")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("HmacKey"), forKey: "HmacKey")

                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isVerified")


                if delegate != nil
                {
                    delegate!.didReceivedValidateUserResult(AppConstant.Static.ADD_USER_SUCCESS)
                }
            }
            else
            {
                if delegate != nil
                {
                    delegate!.didReceivedValidateUserResult(AppConstant.Static.PROCESSING_ERROR)
                }
            }
            
            
        }
        catch
        {
            if delegate != nil
            {
                delegate!.didReceivedValidateUserResult(AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
    
}
