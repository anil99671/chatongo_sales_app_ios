//
//  AddUserParser.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 07/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
// AA898DD3-2D91-4BAB-9761-236A810EA4C6

import UIKit

protocol AddUserParserDelegate : NSObjectProtocol
{
    func didReceivedAddUserResult(resultCode resultCode : Int)
}

class AddUserParser: NSObject,AsyncLoaderModelDelegate {
    
    //MARK: Varibale Decalaration
    
    var loader : AsyncLoaderModel?
    weak var delegate : AddUserParserDelegate?
    
    func addUserWith(mobileno: String!)
    {
        
        let jsonDictionary = NSMutableDictionary()
        
        // We need set the 4 to always Sales App
        jsonDictionary.setValue(-1, forKey: "UserId")
        jsonDictionary.setValue(4, forKey: "UserTypeId")
        jsonDictionary.setObject(mobileno, forKey: "MobileNo")
        jsonDictionary.setObject("+91", forKey: "CountryCode")
        if UIDevice.currentDevice().modelName == "Simulator"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            jsonDictionary.setObject(appDelegate!.imeiNumber, forKey: "ImeiNo")
        }
        else
        {
            jsonDictionary.setObject(UIDevice.currentDevice().identifierForVendor!.UUIDString, forKey: "ImeiNo")
        }
        jsonDictionary.setObject("I", forKey: "OsType")
        jsonDictionary.setObject("NA", forKey: "DeviceToken")
        jsonDictionary.setObject("", forKey: "MobileBrand")
        jsonDictionary.setObject("", forKey: "MobileModel")
        jsonDictionary.setObject("", forKey: "MobileName")
        jsonDictionary.setObject("", forKey: "MobileCarrier")
        jsonDictionary.setObject("", forKey: "AppVersion")
        
        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        
        let jsonDataLenght = "\(jsonData!.length)"
        
        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        print("SalesUserRegister requestString is \(requestString!)")
        
        let url = NSURL(string: AppConstant.Static.BASE_URL+"SalesUserRegister")
        
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.getDataFromRequestWithOutSecurity(request: request, dataIndex: -1)
        loader!.delegate = self
        
    }
    
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("SalesUserRegister responseString is \(responseString!)")
        
        processAddUserData(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil{
            
            delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }
    
    /*
    this method will extract the data from the JSON into the preferences which will remain persistent
    */
    
    func processAddUserData(data data : NSData)
    {
        
        var resultMaster : NSDictionary!
        
        do{
            
            try resultMaster = NSJSONSerialization.JSONObjectWithData(data
                , options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = resultMaster!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                  // It is successful response from the server we can process the json file
                let result = resultMaster!.objectForKey("data") as? NSDictionary
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserId"), forKey: "userID")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserTypeId") as! Int, forKey: "userTypeID")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("UserPassword") as! String, forKey: "password")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Name") as! String, forKey: "name")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("MobileNo"), forKey: "mobileNo")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("CountryCode") as! String, forKey: "countryCode")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ContactNo") as! String, forKey: "contactNo")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Email") as! String, forKey: "email")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Gender") as! String, forKey: "gender")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Dob") as! String, forKey: "dob")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Country") as! String, forKey: "country")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("State") as! String, forKey: "state")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("City") as! String, forKey: "city")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("StreetAddress") as! String, forKey: "streetAddress")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ZipCode") as! String, forKey: "zipCode")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("IsActive"), forKey: "active")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ImeiNo") as! String, forKey: "imeiNo")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("DeviceToken") as! String, forKey: "deviceToken")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("Image"), forKey: "ProfileImage")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("OsType") as! String, forKey: "osType")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("CreatedDate") as! String, forKey: "createDate")
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("ModifiedDate") as! String, forKey: "modifiedDate")
                NSUserDefaults.standardUserDefaults().setInteger(Int(result!.objectForKey("Otp") as! String)!, forKey: "otp")

                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("CountryId") , forKey: "countryId")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("IsValidEmail") , forKey: "isValidEmail")
                
                NSUserDefaults.standardUserDefaults().setObject(result!.objectForKey("AppVersion") , forKey: "appVersion")
                
                if NSUserDefaults.standardUserDefaults().objectForKey("countryCode") as? String == "+91"
                {
                    NSUserDefaults.standardUserDefaults().setObject("India", forKey: "country")
                }
                
                
                NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "StateId")
                NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "CityId")
                
                NSUserDefaults.standardUserDefaults().synchronize()
                
                if delegate != nil{
                    
                    delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.ADD_USER_SUCCESS)
                }
            }
            else if Status == 1580
            {
                if delegate != nil{

                    delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.ADD_USER_EMAIL_INVALID)
                }
            }
            else if Status == 1581
            {
                if delegate != nil{

                    delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.ADD_USER_MOBILE_INVALID)
                }
            }
            else if Status == 401
            {
                if delegate != nil{

                    delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
                }
            }
            else
            {
                if delegate != nil{

                    delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            if delegate != nil{
                
                delegate!.didReceivedAddUserResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}
