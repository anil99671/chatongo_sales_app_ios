//
//  GenerateOTPParser.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 09/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol GenerateOTPParserDelegate : NSObjectProtocol
{
    func didReceivedOTP(otp : Int, timeLimit: Int)
    optional func didReceivedOTPError(errorCode: Int)
}

class GenerateOTPParser: NSObject,AsyncLoaderModelDelegate {
   
    //MARK: Varibale Decalaration
    
    var loader : AsyncLoaderModel?
    weak var delegate : GenerateOTPParserDelegate?
    

    func generateOTPFor(mobileno mobileno: String)
    {
        let jsonDictionary = NSMutableDictionary()
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("userID")!, forKey: "UserId")
        jsonDictionary.setObject(4, forKey: "UserTypeId")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("countryCode")!, forKey: "CountryCode")
            jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().valueForKey("email") as! String, forKey: "EmailId")
            jsonDictionary.setObject(mobileno, forKey: "MobileNo")
        jsonDictionary.setObject(NSUserDefaults.standardUserDefaults().objectForKey("imeiNo")!, forKey: "ImeiNo")
     
        
        
        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        let jsonDataLenght = "\(jsonData!.length)"

        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        print("Generate OTP requestString is \(requestString!)")
        
        let url = NSURL(string: AppConstant.Static.BASE_URL+"GenerateOTP")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData
        
        loader = AsyncLoaderModel()
        loader!.getDataFromRequestWithOutSecurity(request: request, dataIndex: -1)
        loader!.delegate = self
    }
    
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("Generate OTP responseString is \(responseString!)")
        
        processGenerateOTP(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            delegate!.didReceivedOTPError!(AppConstant.Static.CONNECTION_ERROR)
        }
    }
    
    /*
    {"otp":8371,"time":3,"mobileNo":9819312721}
    */
    
    func processGenerateOTP(data data : NSData)
    {
        //var error : NSError?
        
        let result : NSDictionary?
        
        do{
            
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let Status = result!.objectForKey("Status") as! Int
            
            if Status == 200
            {
                let otp = Int(result!.objectForKey("OTP") as! String)!
                let limit =  Int(result!.objectForKey("Limit") as! String)!
                if delegate != nil
                {
                    delegate!.didReceivedOTP(otp, timeLimit: limit)
                }
            }
            else
            {
                delegate!.didReceivedOTPError!(AppConstant.Static.PROCESSING_ERROR)
            }
            
            
        }
        catch
        {
            if delegate != nil
            {
                delegate!.didReceivedOTPError!(AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
    
}
