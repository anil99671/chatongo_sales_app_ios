//
//  RegisterationView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 26/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol RegisterationViewDelegate : NSObjectProtocol
{
    optional func canMoveWithDisplacement(yPos : CGFloat)
    optional func formValidationResult(status : Int, message : String)
}

class RegisterationView:  UIView, UITextFieldDelegate {

    var deviceManager : DeviceManager?

    weak var delegate : RegisterationViewDelegate?

    var bgCardView : NimapCardView?
    var logoImageView : UIImageView?

//    var nameTextField : UITextField?
    var countryLabel : UILabel?
    var mobileTextField : UITextField?
//    var emailTextField : UITextField?

    var baseViewCenterY : CGFloat?

    static let FORM_VALIDATION_IDEAL = 0
    static let MOBILE_NO_INVALID = 1
//    static let NAME_BLANK = 2
//    static let EMAIL_BLANK = 3
//    static let EMAIL_INVALID = 4
    static let MOBILE_NO_BLANK = 5
    static let FORM_VALIDATION_SUCCESS = 6
    static let APPLE_REVIEWER_DEMO = 7

    //MARK: Constructors
    override init(frame: CGRect) {

        super.init(frame: frame)

        deviceManager  =   DeviceManager.sharedDeviceManagement()

        bgCardView = NimapCardView(frame: CGRectMake(0,0,frame.size.width,frame.size.height))
        self.addSubview(bgCardView!)

        baseViewCenterY = bgCardView!.center.y


        var logoCenterY : CGFloat = deviceManager!.deviceYCGFloatValue(yPos: 24.0)
        if deviceManager!.deviceType != deviceManager!.iPhone
        {
            logoCenterY = deviceManager!.deviceYCGFloatValue(yPos: 22.0)
        }

        logoImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "splashscreenlogo")))
        logoImageView!.center = CGPointMake(self.center.x, logoCenterY)
        self.addSubview(logoImageView!)

        var topMargin =  logoCenterY + deviceManager!.deviceYCGFloatValue(yPos:60.0)
        let fieldHeight = deviceManager!.deviceYCGFloatValue(yPos:35.0)
        let fieldPaddingX = deviceManager!.deviceYCGFloatValue(yPos:20.0)
        let fieldPaddingY = deviceManager!.deviceYCGFloatValue(yPos:4.0)
        let countryCodeWidth = deviceManager!.deviceXCGFloatValue(xPos:50.0)
        let countryPadding = deviceManager!.deviceXCGFloatValue(xPos:16.0)

        let fieldWidth = self.frame.width - ((2 * fieldPaddingX) + countryCodeWidth + countryPadding)

//        nameTextField = UITextField(frame: CGRectMake(fieldPaddingX,topMargin,fieldWidth,fieldHeight))
//        nameTextField!.translatesAutoresizingMaskIntoConstraints = false
//        nameTextField!.text = "Priyank"
//        nameTextField!.placeholder = "Name"
//        nameTextField!.delegate = self
//        nameTextField!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
//        nameTextField!.textColor = UIColor.primaryTextColor()
//        nameTextField!.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSForegroundColorAttributeName: UIColor.secondaryTextColor()])
//        nameTextField!.clearButtonMode = UITextFieldViewMode.WhileEditing
//        self.addSubview(nameTextField!)
//
//        topMargin = nameTextField!.frame.origin.y + nameTextField!.frame.size.height + fieldPaddingY

        countryLabel = UILabel(frame: CGRectMake(fieldPaddingX,topMargin,countryCodeWidth,fieldHeight))
        countryLabel!.translatesAutoresizingMaskIntoConstraints = false
        countryLabel!.text = "+91"
        countryLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        countryLabel!.textColor = UIColor.primaryTextColor()
        countryLabel!.userInteractionEnabled = false
        countryLabel!.textAlignment = NSTextAlignment.Center
        self.addSubview(countryLabel!)

        let leftMargin = countryLabel!.frame.origin.x + countryLabel!.frame.size.width + countryPadding
//        let mobileTextWidth = (self.frame.width - fieldPaddingX) - leftMargin

        mobileTextField = UITextField(frame: CGRectMake(leftMargin,topMargin,fieldWidth,fieldHeight))
        mobileTextField!.translatesAutoresizingMaskIntoConstraints = false
        mobileTextField!.placeholder = "Mobile Number"
        mobileTextField!.delegate = self
        mobileTextField!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        mobileTextField!.textAlignment = NSTextAlignment.Center
        mobileTextField!.textColor = UIColor.primaryTextColor()
        mobileTextField!.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes: [NSForegroundColorAttributeName: UIColor.secondaryTextColor()])
        mobileTextField!.keyboardType = UIKeyboardType.PhonePad
        mobileTextField!.clearButtonMode = UITextFieldViewMode.WhileEditing

        // Tool bar to mobileTextField

        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: #selector(RegisterationView.doneTapped))
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.backgroundColor = UIColor.clearColor()
        toolBar.userInteractionEnabled = true
        mobileTextField!.inputAccessoryView = toolBar
        self.addSubview(mobileTextField!)

        topMargin = mobileTextField!.frame.origin.y + mobileTextField!.frame.size.height + fieldPaddingY
//
//        emailTextField = UITextField(frame: CGRectMake(fieldPaddingX,topMargin,fieldWidth,fieldHeight))
//        emailTextField!.translatesAutoresizingMaskIntoConstraints = false
//        emailTextField!.text = "priyank@chatongo.com"
//        emailTextField!.placeholder = "Email"
//        emailTextField!.delegate = self
//        emailTextField!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
//        emailTextField!.textColor = UIColor.primaryTextColor()
//        emailTextField!.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName: UIColor.secondaryTextColor()])
//        emailTextField!.keyboardType = UIKeyboardType.EmailAddress
//        emailTextField!.clearButtonMode = UITextFieldViewMode.WhileEditing
//        self.addSubview(emailTextField!)

        loadTextFieldBorders()
    }


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: Loading methods
    func loadTextFieldBorders()
    {
//        nameTextField!.showOnlyBottomBorder()
//        emailTextField!.showOnlyBottomBorder()
        mobileTextField!.showOnlyBottomBorder()
        countryLabel!.showOnlyBottomBorder()
    }


    //MARK: Animation Methods

    func translateViewBy(yPos yPos :CGFloat)
    {
        UIView.animateWithDuration(0.5, delay: 0.1, options: UIViewAnimationOptions.AllowAnimatedContent, animations: {
            self.center.y = yPos
            }, completion: nil)
    }

    //MARK: Helper Methods

    func dismissAllTextFileds()
    {
//        nameTextField!.resignFirstResponder()
        mobileTextField!.resignFirstResponder()
//        emailTextField!.resignFirstResponder()
    }

    /*Form validation will happen and if all fields are verified addUser parser will be called to add the user into the server and coresponding alert will be shown with relevant message.
     */
    func validateForm()
    {
        var errorMessage = ""
        var errorStatus = RegisterationView.FORM_VALIDATION_IDEAL

        if mobileTextField!.text == "9900990099"{

            //this indicates it is apple demo account

            errorMessage = "Apple Demo"
            errorStatus = RegisterationView.APPLE_REVIEWER_DEMO
        }
        else{

            if mobileTextField!.text!.characters.count == 10
            {
                let index = mobileTextField!.text!.startIndex
                let characterStr = mobileTextField!.text![index]
                if (!(characterStr == "9" || characterStr == "8" || characterStr == "7"))
                {
                    errorMessage = AppConstant.Static.REGISTRATION_FORM_MOBILE_NO_INVALID_ERROR_MESSAGE
                    errorStatus = RegisterationView.MOBILE_NO_INVALID
                }
            }
//            if nameTextField!.text!.isEmpty{
//
//                errorMessage = AppConstant.Static.REGISTRATION_FORM_NAME_VALIDATION_ERROR_MESSAGE
//                errorStatus = RegisterationView.NAME_BLANK
//            }
            else if mobileTextField!.text!.isEmpty{

                errorMessage = AppConstant.Static.REGISTRATION_FORM_MOBILE_VALIDATION_ERROR_MESSAGE
                errorStatus = RegisterationView.MOBILE_NO_BLANK
            }
            else if mobileTextField!.text!.characters.count != 10{

                errorMessage = AppConstant.Static.REGISTRATION_FORM_MOBILE_NO_INVALID_ERROR_MESSAGE
                errorStatus = RegisterationView.MOBILE_NO_INVALID
            }
//            else if !emailTextField!.text!.isEmail{
//
//                errorMessage = AppConstant.Static.REGISTRATION_FORM_EMAIL_VALIDATION_ERROR_MESSAGE
//                errorStatus = RegisterationView.EMAIL_INVALID
//            }
//            else if emailTextField!.text!.isEmpty{
//
//                errorMessage = AppConstant.Static.REGISTRATION_FORM_EMAIL_VALIDATION_NOT_PRESENT_ERROR_MESSAGE
//                errorStatus = RegisterationView.EMAIL_BLANK
//            }

            if errorMessage == ""
            {
                errorStatus = RegisterationView.FORM_VALIDATION_SUCCESS
            }
        }



        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(RegisterationViewDelegate.formValidationResult(_:message:)))
            {
                delegate!.formValidationResult!(errorStatus, message: errorMessage)
            }
        }
    }

    //MARK: Event Handlers

    //MARK: UITextFeildDelegate Callbacks
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(RegisterationViewDelegate.canMoveWithDisplacement(_:)))
            {
//                if textField == nameTextField!{
//
//                    delegate!.canMoveWithDisplacement!(deviceManager!.deviceYCGFloatValue(yPos: 120.0))
//                }
//
//                if textField == emailTextField!{
//
//                    delegate!.canMoveWithDisplacement!(deviceManager!.deviceYCGFloatValue(yPos: 120.0))
//                }

                if textField == mobileTextField!{

                    delegate!.canMoveWithDisplacement!(deviceManager!.deviceYCGFloatValue(yPos: 120.0))
                }
            }
        }

    }

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()

        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(RegisterationViewDelegate.canMoveWithDisplacement(_:)))
            {
                delegate!.canMoveWithDisplacement!(deviceManager!.deviceYCGFloatValue(yPos: 240.0))
            }
        }
        return true
    }

    //MARK: Tool bar handler for mobile textfield

    func doneTapped()
    {
        mobileTextField!.resignFirstResponder()
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(RegisterationViewDelegate.canMoveWithDisplacement(_:)))
            {
                delegate!.canMoveWithDisplacement!(deviceManager!.deviceYCGFloatValue(yPos: 240.0))
            }
        }
    }
}
