//
//  TermsAndConditionsViewController.swift
//  VendorApp
//
//  Created by Zubin - Nimap on 14/03/16.
//  Copyright © 2016 ChatOnGo. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController, NimapNavigationBarViewDelegate, UIWebViewDelegate
{
    //MARK: VARIABLE
    
    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    var deviceManager : DeviceManager?
    var termsWebView : UIWebView?

     //MARK: life cycle methods

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
     //   self.navigationController!.setNavigationBarHidden(true, animated: false)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
        }
        else
        {
            navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
        }
        
        if navigationBar == nil
        {

            navigationBar = NimapNavigationBarView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, navigationHeight), title: "Terms & Conditions")
            navigationBar?.delegate = self
            navigationBar!.alignNavigationOnylWithRightButton()
            navigationBar!.rightButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "cross")), forState: UIControlState.Normal)
            self.view.addSubview(navigationBar!)
            

        }
        
        loadPage()

    }
    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     //MARK: load and unload
    
    func loadPage()
    {
        termsWebView = UIWebView(frame: CGRectMake(0, navigationBar!.frame.origin.y + navigationHeight, self.view.frame.size.width, self.view.frame.size.height - navigationHeight))
        termsWebView!.delegate = self
        self.view.addSubview(termsWebView!)
        var URL: NSURL
        URL = NSURL(string: "http://chatongo.com/terms-of-use.html")!
        let uRLRequest: NSURLRequest = NSURLRequest(URL: URL)
        termsWebView!.loadRequest(uRLRequest)
    }
    func unloadPage()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
        if termsWebView != nil
        {
            termsWebView!.removeFromSuperview()
            termsWebView = nil
        }
    
    }
    //MARK: Navigationbar delegate Methods
    
    func rightButtonPressed()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
