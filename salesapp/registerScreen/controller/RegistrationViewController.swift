//
//  RegistrationViewController.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 24/06/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

//CountryCodeViewControllerDelegate,GetAllSubscriptionsParserDelegate
class RegistrationViewController: UIViewController,UITextFieldDelegate, NimapAlertViewDelegate, RegisterationViewDelegate,AddUserParserDelegate,GenerateOTPParserDelegate, ValidateOTPParserDelegate
{

    //MARK: Varibale Decalaration
    var deviceManager : DeviceManager?

    var baseView : UIView?
    var registrationBackgroundView : RegisterationView?
    var actionButton : UIButton?

    var alert : NimapAlertView?

    var addParser : AddUserParser?
    var generateOTPParser : GenerateOTPParser?
    var validateOTPParser : ValidateOTPParser?

    // OTP verification variables
    var generatedOTP : Int?
    var enteredOTP : Int?
    var timeLimit : Int?
    var startDate : NSDate?

    //    var termsButton : UIButton?

    //    var isNextScreenCountryCode = false // this flag is used to indicate whether next screen in country screen or not. If next screen is county code screen than we wont remove the registration screen content because delegate will send the feedback and view of controller wont be loaded and thus will be crashed

    //MARK: constructors

    init() {

        super.init(nibName: nil, bundle: nil)
        deviceManager       =   DeviceManager.sharedDeviceManagement()
        addParser = nil
        generateOTPParser = nil
        validateOTPParser = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: ViewController Life Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {

        self.view.addGradiant(topColor: UIColor.startScreenTopGradiantColor(), bottomColor: UIColor.startScreenBottomGradiantColor())
    }

    override func viewDidAppear(animated: Bool) {
        loadPage()
    }

    override func viewDidDisappear(animated: Bool) {

        unloadPage()
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    //MARK: Loading Methods

    func loadPage()
    {


        let height : CGFloat = (0.30 * self.view.frame.size.height)
        baseView = UIView(frame: CGRectMake(0,0,(0.8 * self.view.frame.size.width),height + deviceManager!.deviceYCGFloatValue(yPos:30.0)))
        baseView!.center = self.view.center
        baseView!.backgroundColor = UIColor.clearColor()
        baseView!.userInteractionEnabled = true
        self.view.addSubview(baseView!)

        registrationBackgroundView =  RegisterationView(frame:CGRectMake(0,0,(0.8 * self.view.frame.size.width),height))
        registrationBackgroundView!.delegate = self
        baseView!.addSubview(registrationBackgroundView!)

        let buttonPadding = deviceManager!.deviceXCGFloatValue(xPos: 20.0)

        actionButton = UIButton(frame: CGRectMake(buttonPadding,0,registrationBackgroundView!.frame.size.width - (2*buttonPadding), deviceManager!.deviceYCGFloatValue(yPos:30.0)))
        actionButton!.center = CGPointMake(registrationBackgroundView!.frame.size.width/2, registrationBackgroundView!.frame.size.height)
        actionButton!.backgroundColor = UIColor.primaryColor()
        actionButton!.layer.cornerRadius = deviceManager!.deviceYCGFloatValue(yPos:15.0)
        actionButton!.addTarget(self, action: #selector(RegistrationViewController.registerationButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        actionButton!.setTitle("Register", forState: UIControlState.Normal)
        actionButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 20.0))
        actionButton!.layer.shadowColor = UIColor.blackColor().CGColor
        actionButton!.layer.shadowOffset = CGSizeMake(0, 5)
        actionButton!.layer.shadowRadius = 5
        actionButton!.layer.shadowOpacity = 0.7
        baseView!.addSubview(actionButton!)
    }

    //MARK: Unloading Methods


    func unloadPage()
    {
        self.view.removeGradiant()

        if actionButton != nil
        {
            actionButton!.removeFromSuperview()
            actionButton = nil
        }

        if registrationBackgroundView != nil
        {
            registrationBackgroundView!.removeFromSuperview()
            registrationBackgroundView = nil
        }

        if baseView != nil
        {
            baseView!.removeFromSuperview()
            baseView = nil
        }
    }

    //MARK: Animation Methods

    func translateViewBy(yPos yPos :CGFloat)
    {
        UIView.animateWithDuration(0.5, delay: 0.1, options: UIViewAnimationOptions.AllowAnimatedContent, animations: {
            self.baseView!.center.y = yPos
            }, completion: nil)
    }

    //MARK: Various Callback Methods

    //MARK: RegisterationViewDelegate

    func canMoveWithDisplacement(yPos : CGFloat){

        translateViewBy(yPos: yPos)
    }

    func formValidationResult(status : Int, message : String)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if status == RegisterationView.APPLE_REVIEWER_DEMO{

            createAppleDemoUser()
            navigateToHomeScreen()
        }
        else if status == RegisterationView.FORM_VALIDATION_SUCCESS{

            alert = NimapAlertView(frame: CGRectZero, tag: "registeringUser", isActivity: true, animate: false)
            alert!.delegate = self
            self.view.addSubview(alert!)

            if addParser == nil
            {
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isAutoRegister")
                addParser = AddUserParser()
                addParser!.delegate = self
                addParser!.addUserWith(registrationBackgroundView!.mobileTextField!.text)
            }

        }
        else
        {
            alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "formValidationError", message: message, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    //MARK: AddUserParserDelegate Callback

    func didReceivedAddUserResult(resultCode resultCode : Int)
    {
        addParser = nil

        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == AppConstant.Static.ADD_USER_SUCCESS
        {
            // Need to call generate OTP parser

            // on successful registration of user we need to present user with OTP verification alert view along with respective action. At the same time we need to call the generate OTP web service to get the OTP on mobile phone (only for indian numbers) and email.

            alert = NimapAlertView(frame: CGRectZero, tag: "otp", title: "OTP", message: "Verify OTP", placeHolderText: "Enter Valid OTP", actionButtonTitle: "Verify", secondActionButtonTitle: "Generate New", animate: true, keyboardType: UIKeyboardType.NumberPad)
            alert!.delegate = self
            self.view.addSubview(alert!)

            if generateOTPParser == nil
            {
                generateOTPParser = GenerateOTPParser()
                generateOTPParser!.delegate = self
                generateOTPParser!.generateOTPFor(mobileno: registrationBackgroundView!.mobileTextField!.text!)

            }


        }
        else
        {
            var errorMessage : String = ""

            if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                alert = NimapAlertView(frame: CGRectZero, tag: "addingUserError", title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                alert = NimapAlertView(frame: CGRectZero, tag: "addingUserError", title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            }

            else if resultCode == AppConstant.Static.ADD_USER_EMAIL_INVALID
            {
                errorMessage = AppConstant.Static.ADD_USER_EMAIL_INVALID_ERROR_MESSAGE
                alert = NimapAlertView(frame: CGRectZero, tag: "addingUserError", title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            }

            else if resultCode == AppConstant.Static.ADD_USER_MOBILE_INVALID
            {
                errorMessage = AppConstant.Static.ADD_USER_MOBILE_INVALID_ERROR_MESSAGE
                alert = NimapAlertView(frame: CGRectZero, tag: "addingUserError", title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            }

            else if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.SALES_REGISTRATION_FAIL_ERROR
                alert = NimapAlertView(frame: CGRectZero, tag: "addingUserError", title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            }

            alert!.delegate = self
            self.view.addSubview(alert!)

        }
    }

    //MARK: GenerateOPTParserDelegate callbacks
    func didReceivedOTP(otp : Int, timeLimit: Int)
    {
        generateOTPParser = nil

        generatedOTP = otp
        self.timeLimit = timeLimit * 60 // need to convert into the seconds
        startDate = NSDate()

    }

    func didReceivedOTPError(errorCode: Int)
    {
        generateOTPParser = nil
    }

    //MARK: ValidateOPTParserDelegate callbacks
    func didReceivedValidateUserResult(resultCode: Int)
    {
        validateOTPParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == ValidateOTPParser.VALIDATE_OPT_SUCCESS
        {
            // navigate to the home screen
            //
            navigateToHomeScreen()

        }
        else
        {
            if resultCode == AppConstant.Static.CONNECTION_ERROR
            {

                alert = NimapAlertView(frame: CGRectZero, tag: "validateOTPUserError", title: "Error", message: AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE, actionButtonTitle: "OK", animate : true)
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                alert = NimapAlertView(frame: CGRectZero, tag: "validateOTPUserError", title: "Error", message:  AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE, actionButtonTitle: "OK", animate : true)

            }
            alert!.delegate = self
            self.view.addSubview(alert!)

        }
    }

    //MARK: Event Handlers

    //MARK: Button pressed function

    func registerationButtonPressed()
    {
        //Register button called

        registrationBackgroundView!.dismissAllTextFileds()
        translateViewBy(yPos: deviceManager!.deviceYCGFloatValue(yPos: 240.0))
        registrationBackgroundView!.validateForm()
    }

    //MARK: NimapAlertViewDelegate
    func didActionButtonPressed(tag : String!)
    {
        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }

        if tag == "otp"
        {
            // Check OTP and take respective steps
            //this will be triggered when user presses the Verify button
            // we need to call the verification method for the otp..We will check the tag of the alert view and compare the otp entered and otp received with the time limit. If all things are proper than we will validate teh OTP to get HMac else we show an alert for retrying to enter the otp again
            if generatedOTP == enteredOTP{
                //if entered otp is same than we need to check the timelimit. if otp is entered in timelimit than user is navigated to the category page else alert is shown and button which will prompt user to generate new OTP

                let elapsedTime = NSDate().timeIntervalSinceDate(startDate!)
                let duration = Int(elapsedTime)
                if duration <= timeLimit{

                    if validateOTPParser == nil
                    {
                        alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
                        alert!.delegate = self
                        self.view.addSubview(alert!)

                        validateOTPParser = ValidateOTPParser()
                        validateOTPParser!.delegate = self
                        validateOTPParser!.validateOTPFor(generatedOTP!)
                    }
                }
                else{
                    alert = NimapAlertView(frame: CGRectZero, tag: "timeLimitError", title: "Error", message: AppConstant.Static.OTP_TIME_LIMIT_ERROR_MESSAGE, actionButtonTitle: "GENERATE OTP", animate : true)
                    alert!.delegate = self
                    self.view.addSubview(alert!)
                }
            }
            else{
                alert = NimapAlertView(frame: CGRectZero, tag: "invalidOTPError", title: "Error", message: AppConstant.Static.OTP_INVALID_ERROR_MESSAGE, actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }
        else if tag == "invalidOTPError"
        {
            //  When user presses OK on invlaid OTP new alert comes up again for user to enter the otp again

            alert = NimapAlertView(frame: CGRectZero, tag: "otp", title: "OTP", message: "Verify OTP", placeHolderText: "Enter Valid OTP", actionButtonTitle: "Verify", secondActionButtonTitle: "Generate New", animate: true,keyboardType: UIKeyboardType.NumberPad)
            alert!.delegate = self
            self.view.addSubview(alert!)

        }
        else if tag == "timeLimitError"
        {
            // this will be triggered when OTP time limit is over and we need to call generate OTP web service again

            alert = NimapAlertView(frame: CGRectZero, tag: "otp", title: "OTP", message: "Verify OTP", placeHolderText: "Enter Valid OTP", actionButtonTitle: "Verify", secondActionButtonTitle: "Generate New", animate: true, keyboardType: UIKeyboardType.NumberPad)
            alert!.delegate = self
            self.view.addSubview(alert!)

            if generateOTPParser == nil
            {
                generateOTPParser = GenerateOTPParser()
                generateOTPParser!.delegate = self
                generateOTPParser!.generateOTPFor(mobileno: registrationBackgroundView!.mobileTextField!.text!)
            }


        }
        else if tag == "validateOTPUserError"
        {
            // We need to call the validate OTP parser again
            if validateOTPParser == nil
            {
                alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
                alert!.delegate = self
                self.view.addSubview(alert!)

                validateOTPParser = ValidateOTPParser()
                validateOTPParser!.delegate = self
                validateOTPParser!.validateOTPFor(generatedOTP!)
            }
        }

    }

    func didSecondaryActionButtonPressed(tag : String!)
    {
        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }

        if tag == "otp"
        {
            // need to generate new OTP

            alert = NimapAlertView(frame: CGRectZero, tag: "otp", title: "OTP", message: "Verify OTP", placeHolderText: "Enter Valid OTP", actionButtonTitle: "Verify", secondActionButtonTitle: "Generate New", animate: true, keyboardType: UIKeyboardType.NumberPad)
            alert!.delegate = self
            self.view.addSubview(alert!)

            if generateOTPParser == nil
            {
                generateOTPParser = GenerateOTPParser()
                generateOTPParser!.delegate = self
               generateOTPParser!.generateOTPFor(mobileno: registrationBackgroundView!.mobileTextField!.text!)

            }
        }
    }

    func didEnterText(tag : String!, text: String!)
    {
        if tag == "otp"
        {
            enteredOTP = Int(text)
        }
    }

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {

        registrationBackgroundView!.dismissAllTextFileds()
        translateViewBy(yPos: deviceManager!.deviceYCGFloatValue(yPos: 240.0))
    }

    //MARK: Local Helper methods


    //MARK: Navigation Functions
    func navigateToHomeScreen()
    {
        let searchScreenViewController = SearchScreenViewController()
        self.navigationController!.pushViewController(searchScreenViewController, animated: true)

    }

    //MARK: APPPLE DEMO USER
    func createAppleDemoUser()
    {

        NSUserDefaults.standardUserDefaults().setInteger(1, forKey: "isAppleDemo")
        NSUserDefaults.standardUserDefaults().setObject(12020, forKey: "userID")
        NSUserDefaults.standardUserDefaults().setObject(2, forKey: "userTypeID")
        NSUserDefaults.standardUserDefaults().setObject("NA", forKey: "password")
        NSUserDefaults.standardUserDefaults().setObject("Apple Demo", forKey: "name")
        NSUserDefaults.standardUserDefaults().setObject("9900990099", forKey: "mobileNo")
        NSUserDefaults.standardUserDefaults().setObject("-", forKey: "contactNo")
        NSUserDefaults.standardUserDefaults().setObject("apple@chatongo.com", forKey: "email")
        NSUserDefaults.standardUserDefaults().setObject("-", forKey: "gender")
        NSUserDefaults.standardUserDefaults().setObject("01/01/1900", forKey: "dob")
        NSUserDefaults.standardUserDefaults().setObject("+91", forKey: "countryCode")
        NSUserDefaults.standardUserDefaults().setObject("India", forKey: "country")
        NSUserDefaults.standardUserDefaults().setObject("NA", forKey: "state")
        NSUserDefaults.standardUserDefaults().setObject("NA", forKey: "city")
        NSUserDefaults.standardUserDefaults().setObject("101" , forKey: "countryId")
        NSUserDefaults.standardUserDefaults().setObject("NA", forKey: "streetAddress")
        NSUserDefaults.standardUserDefaults().setObject("0", forKey: "zipCode")

        NSUserDefaults.standardUserDefaults().setObject("True", forKey: "active")

        NSUserDefaults.standardUserDefaults().setObject("97126B32-A923-476F-9D86-DE836385604E", forKey: "imeiNo")

        NSUserDefaults.standardUserDefaults().setObject("NA", forKey: "ProfileImage")
        NSUserDefaults.standardUserDefaults().setObject("I", forKey: "osType")
        NSUserDefaults.standardUserDefaults().setObject("6/18/2016 9:03:26 AM", forKey: "createDate")
        NSUserDefaults.standardUserDefaults().setObject("6/18/2016 9:03:26 AM", forKey: "modifiedDate")

        NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "otp")

        // Some additional settings for internal screen

        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "donwloadEnquiries")
        NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "lastMessageId")

        if NSUserDefaults.standardUserDefaults().objectForKey("countryCode") as? String == "+91"
        {
            NSUserDefaults.standardUserDefaults().setObject("India", forKey: "country")
        }

        NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "StateId")
        NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "CityId")

        NSUserDefaults.standardUserDefaults().setObject(" 0", forKey: "HmacKey")

        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isVerified")
        
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}



