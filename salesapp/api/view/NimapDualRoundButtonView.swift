//
//  NimapDualRoundButtonView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 26/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol NimapDualRoundButtonViewDelegate : NSObjectProtocol
{
    func didPrimaryDualRoundActionButtonPressed()
    func didSecondaryDualRoundActionButtonPressed()
}

class NimapDualRoundButtonView: UIView {

    
    var deviceManager : DeviceManager?
    
    var leftActionButton : UIButton?
    var rightActionButton : UIButton?
    var buttonDivider : UILabel?
    
    weak var delegate : NimapDualRoundButtonViewDelegate?
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        deviceManager  =   DeviceManager.sharedDeviceManagement()
        
        backgroundColor = UIColor.primaryColor()
        layer.cornerRadius = frame.size.height/2.0
        addShadow(shadowOpacity: 0.4)
        
        leftActionButton = UIButton(frame:CGRectMake(0.0,0,(self.frame.size.width/2.0),self.frame.size.height))
        leftActionButton!.setTitle("Skip", forState: UIControlState.Normal)
        leftActionButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        leftActionButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        leftActionButton!.backgroundColor = UIColor.clearColor()
        leftActionButton!.addTarget(self, action: #selector(NimapDualRoundButtonView.leftActionButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(leftActionButton!)
        
        rightActionButton = UIButton(frame:CGRectMake(self.frame.size.width/2.0,0,(self.frame.size.width/2.0),self.frame.size.height))
        rightActionButton!.setTitle("Register", forState: UIControlState.Normal)
        rightActionButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        rightActionButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        rightActionButton!.backgroundColor = UIColor.clearColor()
        rightActionButton!.addTarget(self, action: #selector(NimapDualRoundButtonView.rightActionButtonPressed), forControlEvents: UIControlEvents.TouchDown)

        self.addSubview(rightActionButton!)
        
        buttonDivider = UILabel(frame: CGRectMake(0,0,deviceManager!.deviceXCGFloatValue(xPos: 1.0),self.frame.size.height))
        buttonDivider!.backgroundColor = UIColor.dualButtonDividerColor()
        buttonDivider!.center = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0)
        self.addSubview(buttonDivider!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: Event Handlers 
    
    func rightActionButtonPressed()
    {
        if delegate != nil
        {
            delegate!.didSecondaryDualRoundActionButtonPressed()
        }
    }
    
    func leftActionButtonPressed()
    {
        if delegate != nil
        {
            delegate!.didPrimaryDualRoundActionButtonPressed()
        }
    }
}
