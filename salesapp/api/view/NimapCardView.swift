//
//  NimapCardView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 25/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

class NimapCardView: UIView {

    var deviceManager : DeviceManager?
    
    override init(frame: CGRect) {
        
        super.init(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        self.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        self.addShadow(shadowOpacity: 0.3)
    }

    init(frame: CGRect, radius : CGFloat, shadowOpacity : Float, shadowHeight : CGFloat) {
        
        super.init(frame: CGRectMake(0, 0, frame.size.width, frame.size.height))
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        self.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = deviceManager!.deviceXCGFloatValue(xPos: radius)
        self.addShadow(shadowOpacity: shadowOpacity, shadowHeight: shadowHeight)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
