//
//  NimapRatingView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 03/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

class NimapRatingView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    static let MEDIUM_SIZE = 1
    static let SMALL_SIZE = 2
    static let EXTRA_SMALL_SIZE = 3
    
    static var STAR_ICON = "\u{f006}"
    static var STAR_ICON1 = "\u{f123}"
    static var STAR_ICON2 = "\u{f005}"
    
    var deviceManager : DeviceManager?
    
    var ratingLabel : UILabel?
    
    var ratingViewContrainer : NimapStarReviewView?
    var ratingCaptionLabel : UILabel?
    
    var starIconHeight : CGFloat = 0.0
    var contentHeight : CGFloat = 0.0

    var type : Int = 0
    
    init(frame: CGRect, ratings : Double, ratingCount : Int, reviewCount : Int, type : Int) {
        
        super.init(frame: frame)
        
        self.type = type
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        var labelHeight : CGFloat = 0.0
        var paddingY : CGFloat = 0.0
        var fontSize : CGFloat = 0.0
        
        if type == NimapRatingView.MEDIUM_SIZE {
            starIconHeight = deviceManager!.deviceYCGFloatValue(yPos: 28.0)
            labelHeight = deviceManager!.deviceYCGFloatValue(yPos: 20.0)
            paddingY = deviceManager!.deviceYCGFloatValue(yPos: 4.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 14.0)
        }
        else if type == NimapRatingView.SMALL_SIZE{
            starIconHeight = deviceManager!.deviceYCGFloatValue(yPos: 14.0)
            labelHeight = deviceManager!.deviceYCGFloatValue(yPos: 12.0)
            paddingY = deviceManager!.deviceYCGFloatValue(yPos: 2.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        }
        else{
            starIconHeight = deviceManager!.deviceYCGFloatValue(yPos: 10.0)
            labelHeight = deviceManager!.deviceYCGFloatValue(yPos: 10.0)
            paddingY = deviceManager!.deviceYCGFloatValue(yPos: 4.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
        }
        
        var yPos : CGFloat = paddingY
        
        contentHeight = 0.0
        
        ratingLabel = UILabel(frame: CGRectMake(0,yPos,frame.size.width,labelHeight))
        ratingLabel!.text = "Ratings"
        ratingLabel!.textAlignment = NSTextAlignment.Center
        ratingLabel!.font = UIFont.appFontMedium(forSize: fontSize)
        ratingLabel!.textColor = UIColor.primaryColor()
        self.addSubview(ratingLabel!)
        
        yPos = yPos + labelHeight + paddingY
        
        contentHeight = contentHeight + yPos
        
        
        ratingViewContrainer = NimapStarReviewView(frame: CGRectMake(0,yPos,frame.size.width,starIconHeight),type : type)
        ratingViewContrainer!.backgroundColor = UIColor.clearColor()
        self.addSubview(ratingViewContrainer!)
        
        //loadStarIcons()
        
        ratingViewContrainer!.center = CGPointMake(self.frame.size.width/2.0, ratingViewContrainer!.frame.size.height/2.0 + contentHeight)
        
        yPos = yPos + ratingViewContrainer!.frame.size.height
        
        contentHeight =  yPos
        
        ratingCaptionLabel = UILabel(frame: CGRectMake(0,yPos,frame.size.width,labelHeight))
        ratingCaptionLabel!.text = "Ratings | Reviews"
        ratingCaptionLabel!.textAlignment = NSTextAlignment.Center
        ratingCaptionLabel!.font = UIFont.appFontMedium(forSize: fontSize)
        ratingCaptionLabel!.backgroundColor = UIColor.clearColor()
        ratingCaptionLabel!.textColor = UIColor.primaryColor()
        self.addSubview(ratingCaptionLabel!)
        
        yPos = yPos + labelHeight + paddingY
        
        contentHeight = yPos
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    //MARK: Update methods
    
    func updateRatingView(ratings : (shopCount : Int,reviewCount : Int, rating : Double)){
        
        let shopCount = ratings.shopCount
        let reviewCount = ratings.reviewCount
        //let rating = ratings.rating

        if shopCount <= 0{
           
            ratingCaptionLabel!.text = "Be first to rate"
        }
        else{
            ratingCaptionLabel!.text = "\(shopCount) Ratings | \(reviewCount) Reviews"
        }
        
        ratingViewContrainer!.updateRatingView(ratings)
    }
}
