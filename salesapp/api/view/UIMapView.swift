//
//  MapView.swift
//  Project2
//
//  Created by Priyank Ranka on 19/01/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit
import MapKit

@objc protocol UIMapViewDelegate : NSObjectProtocol{
    
    optional func didAnnotationSelected(index:Int)
}

class UIMapView: UIView, MKMapViewDelegate{

    var map: MKMapView?
    
    var mapAnnotations : [BridgeAnnotation]?
    var currentIndex: Int?
    
    var delegate : UIMapViewDelegate?
    
    var showInitialView : Bool?
    var initials : String?
    
    init(frame: CGRect, showInitialView: Bool,initials : String) {
        super.init(frame: frame)
        
        self.showInitialView = showInitialView
        self.initials = initials
        
        map = MKMapView(frame: CGRectMake(0,0,frame.size.width,frame.size.height))
        map!.delegate = self
        self.addSubview(map!)
        
        mapAnnotations = []
        
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Local methods
    
    func addMapAnnotations(){
        
        map!.addAnnotations(mapAnnotations!)
    }
    
    func removeMapAnnotations()
    {
        map!.removeAnnotations(mapAnnotations!)
        mapAnnotations!.removeAll(keepCapacity: false)
    }
    
    func addAnnotation(latitude : Double,longitude: Double,title:String,subtitle:String,icon: UIImage,index: Int){
        
        let annotation = BridgeAnnotation(latitude: latitude, longitude: longitude)
        annotation.strTitle =   title
        annotation.strSubTitle  =   subtitle
        annotation.annotationIcon   =   icon
        annotation.annotationIndex  =   index
        
        mapAnnotations!.append(annotation)
    }
    
    func centerMapWithAnnotation(annotationIndex : Int){
        
        if mapAnnotations!.count > 0{
            
            let annotation = mapAnnotations![annotationIndex]
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(annotation.latitude!,annotation.longitude!), 500, 500);
            let adjustedRegion =  map!.regionThatFits(viewRegion)
            map!.setRegion(adjustedRegion, animated: true)
        }
    }
    
    func showAutoMapAnnotationViewForPinIndex(pinIndex : Int){
        
        let annotation = mapAnnotations![pinIndex]
        map!.selectAnnotation(annotation, animated: true)
    }
    
    func showDirections(pinIndex : Int){
     
        let annotation = mapAnnotations![pinIndex]
        
        if UIApplication.sharedApplication().canOpenURL(NSURL(string:"comgooglemaps://")!) == true{
            
            //opens google maps
            let openURL = NSURL(string: "comgooglemaps://?center=\(annotation.latitude!),\(annotation.longitude!)&q=\(annotation.latitude!),\(annotation.longitude!)")
            
            UIApplication.sharedApplication().openURL(openURL!)
        }
        else{
            
            //opens apple maps
            let placemark = MKPlacemark(coordinate: CLLocationCoordinate2DMake(annotation.latitude!,annotation.longitude!), addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            
            mapItem.name =  annotation.subtitle
            var options : [String : AnyObject] = [:]
            options[MKLaunchOptionsDirectionsModeKey] = MKLaunchOptionsDirectionsModeDriving
            mapItem.openInMapsWithLaunchOptions(options)
        }
    }
    
    //MARK: MKMapViewDelegate
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        var pinView : MKPinAnnotationView?
        
        pinView = map!.dequeueReusableAnnotationViewWithIdentifier("map") as! MKPinAnnotationView!
        
        if pinView == nil
        {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "map")
        }
        
        pinView!.pinColor = MKPinAnnotationColor.Green
        pinView!.animatesDrop = true
        pinView!.canShowCallout = true
        
        if showInitialView == false{
            let leftView = UIImageView(image: (annotation as! BridgeAnnotation).annotationIcon)
            pinView!.leftCalloutAccessoryView = leftView;
        }
        else{
            
            let iconLabel = UILabel(frame: CGRectMake(0.0,0.0,40.0,40.0))
            iconLabel.layer.cornerRadius = iconLabel.frame.size.width/2
            iconLabel.clipsToBounds = true
            iconLabel.textColor = UIColor.whiteColor()
            iconLabel.textAlignment = .Center
            iconLabel.font = UIFont.appFontMedium(forSize: 20.0)
            iconLabel.backgroundColor = UIColor.primaryColor()
            iconLabel.text = initials
            pinView!.leftCalloutAccessoryView = iconLabel
        }
        
        let rightButton = UIButton(type: UIButtonType.DetailDisclosure)
        rightButton.addTarget(self, action: #selector(UIMapView.annotaionPressed), forControlEvents: UIControlEvents.TouchDown)
        pinView!.rightCalloutAccessoryView = rightButton;
        
        return pinView!
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView)
    {
        currentIndex = (view.annotation as! BridgeAnnotation).annotationIndex;
    }

    //MARK: Event Handlers
    func annotaionPressed()
    {
        if delegate != nil{
            
            if delegate!.respondsToSelector(#selector(UIMapViewDelegate.didAnnotationSelected(_:)))
            {
                delegate!.didAnnotationSelected!(currentIndex!)
            }
        }
    }
    
}
