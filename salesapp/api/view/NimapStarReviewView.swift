//
//  NimapStarReviewView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 06/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol NimapStarReviewViewDelegate : NSObjectProtocol {
    
    optional func didUpdateRatings(ratings : Float)
}

class NimapStarReviewView: UIView {
    
    
    static var STAR_ICON = "\u{f006}"
    static var STAR_ICON1 = "\u{f123}"
    static var STAR_ICON2 = "\u{f005}"
    
    static let MEDIUM_SIZE = 1
    static let SMALL_SIZE = 2
    static let EXTRA_SMALL_SIZE = 3
    static let LARGE_SIZE = 4
    
    var deviceManager : DeviceManager?

    weak var delegate : NimapStarReviewViewDelegate?
    
    var starIconLabel1 : UILabel?
    var starIconLabel2 : UILabel?
    var starIconLabel3 : UILabel?
    var starIconLabel4 : UILabel?
    var starIconLabel5 : UILabel?
    
    var starIconHeight : CGFloat = 0.0
    var type : Int = NimapStarReviewView.MEDIUM_SIZE
    
    var ratings : Float = 0.0
    
    init(frame: CGRect, type : Int)
    {
        super.init(frame: frame)
        
        self.userInteractionEnabled = false
        deviceManager = DeviceManager.sharedDeviceManagement()
        self.starIconHeight = frame.size.height
        self.type = type
        loadStarIcons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func loadStarIcons(){
        
        var xPos : CGFloat = 0.0
        var paddingX : CGFloat = 0.0
        var fontSize : CGFloat = 0.0
        
        if type == NimapStarReviewView.MEDIUM_SIZE {
            paddingX = deviceManager!.deviceXCGFloatValue(xPos: 2.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 32.0)
        }
        else if type == NimapStarReviewView.LARGE_SIZE {
            paddingX = deviceManager!.deviceXCGFloatValue(xPos: 2.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 48.0)
        }
        else if type == NimapStarReviewView.SMALL_SIZE{
            paddingX = deviceManager!.deviceXCGFloatValue(xPos: 1.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 16.0)
        }
        else{
            paddingX = deviceManager!.deviceXCGFloatValue(xPos: 1.0)
            fontSize = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        }
        
        if deviceManager!.deviceType == deviceManager!.iPad{
            
            fontSize = fontSize - deviceManager!.deviceXCGFloatValue(xPos: 4.0)
        }
        
        starIconLabel1 = UILabel(frame:CGRectMake(xPos, 0,starIconHeight,starIconHeight))
        starIconLabel1!.backgroundColor = UIColor.clearColor()
        starIconLabel1!.textColor = UIColor.primaryColor()
        starIconLabel1!.textAlignment = .Center
        starIconLabel1!.font = UIFont.appFontAwesome(forSize: fontSize)
        starIconLabel1!.text = NimapRatingView.STAR_ICON
        self.addSubview(starIconLabel1!)
        
        xPos = xPos + starIconHeight + paddingX
        
        starIconLabel2 = UILabel(frame:CGRectMake(xPos, 0,starIconHeight,starIconHeight))
        starIconLabel2!.backgroundColor = UIColor.clearColor()
        starIconLabel2!.textColor = UIColor.primaryColor()
        starIconLabel2!.textAlignment = .Center
        starIconLabel2!.font = UIFont.appFontAwesome(forSize: fontSize)
        starIconLabel2!.text = NimapRatingView.STAR_ICON
        self.addSubview(starIconLabel2!)
        
        xPos = xPos + starIconHeight + paddingX
        
        starIconLabel3 = UILabel(frame:CGRectMake(xPos, 0,starIconHeight,starIconHeight))
        starIconLabel3!.backgroundColor = UIColor.clearColor()
        starIconLabel3!.textColor = UIColor.primaryColor()
        starIconLabel3!.textAlignment = .Center
        starIconLabel3!.font = UIFont.appFontAwesome(forSize: fontSize)
        starIconLabel3!.text = NimapRatingView.STAR_ICON
        self.addSubview(starIconLabel3!)
        
        xPos = xPos + starIconHeight + paddingX
        
        starIconLabel4 = UILabel(frame:CGRectMake(xPos, 0,starIconHeight,starIconHeight))
        starIconLabel4!.backgroundColor = UIColor.clearColor()
        starIconLabel4!.textColor = UIColor.primaryColor()
        starIconLabel4!.textAlignment = .Center
        starIconLabel4!.font = UIFont.appFontAwesome(forSize: fontSize)
        starIconLabel4!.text = NimapRatingView.STAR_ICON
        self.addSubview(starIconLabel4!)
        
        xPos = xPos + starIconHeight + paddingX
        
        starIconLabel5 = UILabel(frame:CGRectMake(xPos, 0,starIconHeight,starIconHeight))
        starIconLabel5!.backgroundColor = UIColor.clearColor()
        starIconLabel5!.textColor = UIColor.primaryColor()
        starIconLabel5!.textAlignment = .Center
        starIconLabel5!.font = UIFont.appFontAwesome(forSize: fontSize)
        starIconLabel5!.text = NimapRatingView.STAR_ICON
        self.addSubview(starIconLabel5!)
        
        xPos = xPos + starIconHeight + paddingX
        
        self.frame = CGRectMake(0.0,self.frame.origin.y,xPos,self.frame.size.height)
    }
    
    //MARK: Update methods
    
    func makeRatingViewTouchEnable()
    {
        self.userInteractionEnabled = true
        starIconLabel1!.userInteractionEnabled = true
        starIconLabel2!.userInteractionEnabled = true
        starIconLabel3!.userInteractionEnabled = true
        starIconLabel4!.userInteractionEnabled = true
        starIconLabel5!.userInteractionEnabled = true
    }
    
    func diableTouchEvents()
    {
        self.userInteractionEnabled = false
    }
    
    func updateRatingView(ratings : (shopCount : Int,reviewCount : Int, rating : Double)){
        
        let rating = ratings.rating
        
        if (rating >= 0.0 && rating < 0.5)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
            
        }
        else if (rating >= 0.5 && rating < 1.0)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
        }
        else if (rating >= 1.0 && rating < 1.5)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON1
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
        }
        else if (rating >= 1.5 && rating < 2.0)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
        }
        else if (rating >= 2.0 && rating < 2.5)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON1
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
            
        }
        else if (rating >= 2.5 && rating < 3.0)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON2
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
            
        }
        else if (rating >= 3.0 && rating < 3.5)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON2
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON1
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
            
        }
        else if (rating >= 3.5 && rating < 4.0)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON2
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON2
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON
            
        }
        else if (rating >= 4.0 && rating < 4.5)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON2
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON2
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON1
            
        }
        else if (rating >= 4.5 && rating <= 5.0)
        {
            starIconLabel1!.textColor = UIColor.primaryColor()
            starIconLabel1!.text = NimapRatingView.STAR_ICON2
            starIconLabel2!.textColor = UIColor.primaryColor()
            starIconLabel2!.text = NimapRatingView.STAR_ICON2
            starIconLabel3!.textColor = UIColor.primaryColor()
            starIconLabel3!.text = NimapRatingView.STAR_ICON2
            starIconLabel4!.textColor = UIColor.primaryColor()
            starIconLabel4!.text = NimapRatingView.STAR_ICON2
            starIconLabel5!.textColor = UIColor.primaryColor()
            starIconLabel5!.text = NimapRatingView.STAR_ICON2
        }
    }
    
    func roundRating(){
        
        if (ratings >= 0.0 && ratings < 0.5)
        {
            ratings = 0.0
            
        }
        else if (ratings >= 0.5 && ratings < 1.0)
        {
            ratings = 1.0
        }
        else if (ratings >= 1.0 && ratings < 1.5)
        {
            ratings = 1.5
        }
        else if (ratings >= 1.5 && ratings < 2.0)
        {
            ratings = 2.0
        }
        else if (ratings >= 2.0 && ratings < 2.5)
        {
            ratings = 2.5
            
        }
        else if (ratings >= 2.5 && ratings < 3.0)
        {
            ratings = 3.0
            
        }
        else if (ratings >= 3.0 && ratings < 3.5)
        {
            ratings = 3.5
            
        }
        else if (ratings >= 3.5 && ratings < 4.0)
        {
           ratings = 4.0
            
        }
        else if (ratings >= 4.0 && ratings < 4.5)
        {
            ratings = 4.5
            
        }
        else if (ratings >= 4.5 && ratings <= 5.0)
        {
            ratings = 5.0
        }
    }
    
    func updateRatingsWithTouch(touch : UITouch)
    {
        
        let widthFactor : CGFloat  = (self.bounds.size.width / 6.0)
        
        let location = touch.locationInView(self)
        ratings = Float ((location.x) / widthFactor)
        
        if ratings <= 0.0
        {
            ratings = 0.0
        }
        
        if ratings >= 5.0
        {
            ratings = 5.0
        }
        
        updateRatingView((shopCount: 0, reviewCount: 0, rating: Double(ratings)))
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        updateRatingsWithTouch(touches.first!)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        updateRatingsWithTouch(touches.first!)
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        updateRatingsWithTouch(touches.first!)
        
        if delegate != nil{
            
            if delegate!.respondsToSelector(#selector(NimapStarReviewViewDelegate.didUpdateRatings(_:))) == true{
                roundRating()
                delegate!.didUpdateRatings!(ratings)
            }
        }
    }
    
}
