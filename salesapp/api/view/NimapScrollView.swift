//
//  NimapScrollView.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 27/06/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol NimapScrollViewDelegate : NSObjectProtocol
{
    optional func nimapScrollViewTouchesEnded(touches: NSSet, withEvent event: UIEvent)
}

class NimapScrollView: UIScrollView {

    
    weak var touchDelegate : NimapScrollViewDelegate?
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
  
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //Mark: Touch Events
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if self.dragging == false
        {
            if touchDelegate != nil
            {
                if touchDelegate!.respondsToSelector(Selector("nimapScrollViewTouchesEnded:withEvent:"))
                {
                    touchDelegate!.nimapScrollViewTouchesEnded!(touches, withEvent: event!)
                }
            }
        }
        else
        {
            super.touchesEnded(touches, withEvent: event)
        }
    }
    
}
