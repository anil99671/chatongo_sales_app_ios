//
//  UIZoomableView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 04/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol UIZoomableViewDelegate : NSObjectProtocol {
    
    func didCancel()
}

class UIZoomableView: UIView, UIScrollViewDelegate {

    var deviceManager : DeviceManager?
    
    var baseScrollView : UIScrollView?
    
    var childView : UIImageView?
    
    var helpLabel : UILabel?
    var padding : CGFloat = 0.0
    
    var cancelButton : UIButton?
    
    weak var delegate : UIZoomableViewDelegate?
    
    init(frame: CGRect, zoomView : UIImageView) {
       
        super.init(frame: frame)
        
        childView = UIImageView(frame: CGRectMake(0,0,self.frame.size.width,self.frame.size.height))
        childView!.image = zoomView.image
        childView!.contentMode = UIViewContentMode.ScaleAspectFit
        
        self.backgroundColor = UIColor.blackColor()
        
        deviceManager = DeviceManager.sharedDeviceManagement()

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
        
        baseScrollView = UIScrollView(frame: CGRectMake(0,0,self.frame.size.width,self.frame.size.height))
        baseScrollView!.center = self.center
        baseScrollView!.userInteractionEnabled = true
        baseScrollView!.pagingEnabled = true
        baseScrollView!.minimumZoomScale = 1.0
        baseScrollView!.maximumZoomScale = 3.0
        baseScrollView!.zoomScale = 0.3
        baseScrollView!.clipsToBounds = true
        baseScrollView!.delegate = self
        self.addSubview(baseScrollView!)
        
        
        childView!.center = CGPointMake(baseScrollView!.frame.size.width/2.0, baseScrollView!.frame.size.height/2.0)
        baseScrollView!.addSubview(childView!)
        
        
        baseScrollView!.contentSize = childView!.frame.size
        
        if helpLabel == nil {
            
            helpLabel = UILabel(frame:CGRectMake(padding,self.frame.size.height - deviceManager!.deviceYCGFloatValue(yPos: 30.0),self.frame.size.width - (2 * padding),deviceManager!.deviceYCGFloatValue(yPos: 30.0)))
            helpLabel!.text = "Double tap to get original image"
            helpLabel!.textAlignment = NSTextAlignment.Center
            helpLabel!.textColor = UIColor.whiteColor()
            helpLabel!.font = UIFont.appFontMedium(forSize: 16.0)
            self.addSubview(helpLabel!)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(UIZoomableView.defaultZoomLevel))
        tapGesture.numberOfTapsRequired = 2
        baseScrollView!.addGestureRecognizer(tapGesture)
        
        cancelButton = UIButton(frame:CGRectMake(self.frame.size.width - deviceManager!.deviceXCGFloatValue(xPos: 50.0),padding,deviceManager!.deviceXCGFloatValue(xPos: 30.0),deviceManager!.deviceYCGFloatValue(yPos: 30.0)))
        cancelButton!.setTitle(AppConstant.Static.CANCEL_ICON, forState: UIControlState.Normal)
        cancelButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        cancelButton!.titleLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 22.0))
        cancelButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        cancelButton!.backgroundColor = UIColor.clearColor()
        cancelButton!.addTarget(self, action: #selector(UIZoomableView.cancelButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(cancelButton!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView?{
        return childView
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView){
        
        //let zoomView = scrollView.delegate!.viewForZoomingInScrollView!(scrollView
    }
    
    func defaultZoomLevel(){
        baseScrollView!.zoomScale = 1.0
    }
    
    func cancelButtonPressed(){
        
        if delegate != nil {
            delegate!.didCancel()
        }
    }
}
