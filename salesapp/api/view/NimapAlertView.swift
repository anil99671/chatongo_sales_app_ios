//
//  NimapAlertView.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 02/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol NimapAlertViewDelegate : NSObjectProtocol
{
    optional func didActionButtonPressed(tag : String!)
    optional func didSecondaryActionButtonPressed(tag : String!)
    optional func didEnterText(tag : String!, text: String!)
}

class NimapAlertView: UIView, NimapDualRoundButtonViewDelegate,NimapStarReviewViewDelegate{

    //MARK: VAriable Section
    
    weak var delegate : NimapAlertViewDelegate?
    
    var deviceManager : DeviceManager?
    
    var viewDictionary : [String: AnyObject]?//NSMutableDictionary?
    var metricDictionary : [String: AnyObject]?//NSMutableDictionary?
    
    
    var alertBodyView : UIView?
    
    //this view is needed when we have buttons outside frame of alertBodyView
    var alertBodyViewBaseView : UIView?
    
    var alertTag : String? // this variable will let listener knows which alert view has responded to listerner. Listener will compare the tag screen and change the control flow
    
    // animation related variables
    
    var isShown = false
    var isAnimate = false
    var animator : UIDynamicAnimator?
    
    var logoImageView : UIImageView?
    var messageLabel : UILabel?
    var actionButton : UIButton?
    var containsKeyboardElements  = false
    var dualActionButtons : NimapDualRoundButtonView?
    var buttonIndex = -1
    
    var inputTextFiled : UITextField?
    var inputTextView : UITextView?
    var inputTextViewPlaceHolderLabel : UILabel?
    
    var ratings : Float?
    var ratingContainer : NimapStarReviewView?
    
    // Custom Animation
    var timer : NSTimer?
    var nimapActivity : UIImageView?
    var activity : UIActivityIndicatorView?
    var angel : CGFloat?
    
    func updateActivity()
    {
        angel = angel! + 10.0
        
        if angel >= 360.0
        {
            angel = 0.0
        }
        
        let angelDegree = angel! * CGFloat(M_PI / 180.0)
        
        nimapActivity!.transform = CGAffineTransformMakeRotation(angelDegree)
    }
    
    
    //MARK: Constructor Section
    /*
    We design the alert view with full screen and make it 50% transparent.
    We add the view at center and on top of it we place the title, description
    and dismiss button. All the elements will be initialize in the constructor 
    with appropriate properties and look and feel colors will be set by the 
    category or extensions in swift
    */
    init(frame : CGRect, tag : String, title : String!, message : String!, actionButtonTitle: String!, animate : Bool)
    {
        
        super.init(frame: UIScreen.mainScreen().bounds)
        
        isShown = false
        alertTag = tag
        isAnimate = animate
        
        baseLayout()
        
        let padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
        let paddingTitleVertical = deviceManager!.deviceYCGFloatValue(yPos:8.0)
        let paddingButton = deviceManager!.deviceXCGFloatValue(xPos: 24.0)
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
        let messageHeight = alertBodyView!.frame.size.height - buttonHeight/2.0
        
        messageLabel!.text = message
        
        messageLabel!.frame = CGRectMake(padding, paddingTitleVertical, alertBodyView!.frame.size.width - (2 * padding), messageHeight)
        messageLabel!.backgroundColor = UIColor.clearColor()
        
        actionButton = UIButton(frame: CGRectMake(paddingButton, alertBodyView!.frame.size.height - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight))
        
        actionButton!.setTitle(actionButtonTitle, forState: UIControlState.Normal)
        actionButton!.setTitleColor(UIColor.appDarkFontColor(), forState: UIControlState.Normal)
        actionButton!.backgroundColor = UIColor.primaryColor()
        actionButton!.addTarget(self, action: #selector(NimapAlertView.actionButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        actionButton!.layer.cornerRadius = deviceManager!.deviceYCGFloatValue(yPos: 20.0)
        actionButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        alertBodyViewBaseView!.addSubview(actionButton!)
        actionButton!.addShadow(shadowOpacity: 0.4)
        
        //actionButton!.frame = CGRectMake(paddingButton, alertBodyView!.frame.size.height - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight)
        
        
        // As button is going half out its parent view and the area which is outside the parent view will not be clickable thus we need to add another layer of view with height of baseAlertview plus half height of button
        startAlertAnimation()
    }
    
    /*
    The following constructor will create alert view with message and loading there will not be any dismiss button. It will be dismissed automatically which will be controllered by the class which creats it. You can configure whether you need the activity on alert or not.
    */
    init(frame : CGRect, tag : String, title : String!, message : String!, isActivity:Bool, animate : Bool)
    {
        
        super.init(frame: UIScreen.mainScreen().bounds)
        
        isShown = false
        alertTag = tag
        isAnimate = animate
        
        baseLayout()
        
        messageLabel!.text = message
        messageLabel!.backgroundColor = UIColor.clearColor()
        let logoBaseY = Float(logoImageView!.frame.origin.y + logoImageView!.frame.size.height)
        
        viewDictionary = [String: AnyObject]()//NSMutableDictionary()
        viewDictionary!["messageLabel"] = messageLabel!
        //viewDictionary!.setObject(messageLabel!, forKey: "messageLabel")
        
        metricDictionary = [String: AnyObject]()//NSMutableDictionary()
        metricDictionary!["padding"] = NSNumber(float: deviceManager!.deviceXvalue(xPos: AppConstant.Static.NIMAP_ALERT_LOGO_Y_PADDING_FLOAT))
        metricDictionary!["paddingTitleVertical"] = NSNumber(float: deviceManager!.deviceXvalue(xPos: AppConstant.Static.NIMAP_ALERT_LOGO_Y_PADDING_FLOAT) + logoBaseY)
        metricDictionary!["buttonHeight"] = NSNumber(float: deviceManager!.deviceYvalue(yPos: AppConstant.Static.BUTTON_HEIGHT))
        
        //Horizontal Constraints
        alertBodyView!.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-padding-[messageLabel]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        //Vertical Constraints
        alertBodyView!.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-paddingTitleVertical-[messageLabel]-paddingTitleVertical-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        // We are checking whether the activity needs to be shown or not and accordingly we will set the vertical contraints for the Alert box with or without activity
        if isActivity == true
        {
            let activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
            activity.center = CGPointMake(alertBodyView!.frame.size.width/2.0, (alertBodyView!.frame.size.height) - deviceManager!.deviceYCGFloatValue(yPos: 20.0))
            activity.color = UIColor.appActivityColor()
            activity.startAnimating()
            alertBodyView!.addSubview(activity)
        }
        
        startAlertAnimation()
        
    }
    
    
    /*
    The following constructor will create alert view with loading there will not be any dismiss button. It will be dismissed automatically which will be controllered by the class which creats it. You can configure whether you need the activity on alert or not.
    */
    init(frame : CGRect, tag : String, isActivity:Bool, animate : Bool)
    {
        
        super.init(frame: UIScreen.mainScreen().bounds)
        
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.appAlertBGColor()
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        isShown = false
        alertTag = tag
        isAnimate = animate
        
        activity = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50,50))
        
        activity!.color =  UIColor(red: (9.0/255.0), green: (164.0/255.0), blue: (230.0/255.0), alpha: 1.0)
        
        activity!.transform = CGAffineTransformMakeScale(1.5, 1.5)
        
        activity!.layer.cornerRadius = 10
        
        activity!.center = self.center;
        
        activity!.backgroundColor = UIColor.whiteColor()
        
         self.addSubview(activity!)
        
        activity!.startAnimating()
      
    }
    
    /*The following constructor will create alert view with message as heading, textField to take the input */
    init(frame : CGRect, tag : String, title : String!, message : String!, placeHolderText : String!,actionButtonTitle: String!, secondActionButtonTitle: String!, animate : Bool, keyboardType : UIKeyboardType = UIKeyboardType.Default)
    {
        
        super.init(frame: UIScreen.mainScreen().bounds)
        
        isShown = false
        alertTag = tag
        isAnimate = animate
        
        containsKeyboardElements = true
        
        baseLayout()
        
        let paddingTitleVertical = deviceManager!.deviceYCGFloatValue(yPos:8.0)
        let paddingButton = deviceManager!.deviceXCGFloatValue(xPos: 24.0)
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
        
        messageLabel!.text = message
        // to show you need to set the frame
        messageLabel!.backgroundColor = UIColor.clearColor()
        messageLabel!.hidden = true
        
        dualActionButtons = NimapDualRoundButtonView(frame: CGRectMake(paddingButton, alertBodyView!.frame.size.height - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight))
        dualActionButtons!.leftActionButton!.setTitle(actionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.rightActionButton!.setTitle(secondActionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.delegate = self
        alertBodyViewBaseView!.addSubview(dualActionButtons!)
        
        //messageLabel!.frame = CGRectMake(padding, paddingTitleVertical, alertBodyView!.frame.size.width - (2 * padding), messageHeight)
        
        inputTextFiled = UITextField(frame: CGRectMake(paddingButton, paddingTitleVertical, alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight))
        inputTextFiled!.center = CGPointMake(alertBodyView!.frame.size.width/2.0, alertBodyView!.frame.size.height/2.0 - ((buttonHeight/2.0)))
        inputTextFiled!.text = ""
        inputTextFiled!.placeholder = placeHolderText
        inputTextFiled!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        inputTextFiled!.textColor = UIColor.primaryTextColor()
        inputTextFiled!.attributedPlaceholder = NSAttributedString(string: placeHolderText, attributes: [NSForegroundColorAttributeName: UIColor.appLightFontColor()])
        inputTextFiled!.borderStyle = UITextBorderStyle.None
        inputTextFiled!.textAlignment = NSTextAlignment.Center
        inputTextFiled!.keyboardType = keyboardType
        inputTextFiled!.clearButtonMode = UITextFieldViewMode.WhileEditing
        inputTextFiled!.showOnlyBottomBorder()
        alertBodyView!.addSubview(inputTextFiled!)

        if tag == "otp"
        {
            let hintLabel = UILabel(frame: CGRectMake(paddingButton, dualActionButtons!.frame.origin.y - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight/2.0))
            hintLabel.text = "OTP has been sent!!"
            hintLabel.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 10.0))
            hintLabel.textColor = UIColor.dividerColor()
            hintLabel.textAlignment = NSTextAlignment.Center
            alertBodyView!.addSubview(hintLabel)
        }

        startAlertAnimation()
        
        inputTextFiled!.becomeFirstResponder()
    }
    
    /*The following constructor will create alert view with message as heading, and two buttons */
    init(frame : CGRect, choosetag : String, title : String!, message : String!,actionButtonTitle: String!, secondActionButtonTitle: String!, animate : Bool, keyboardType : UIKeyboardType = UIKeyboardType.Default)
    {
        
        super.init(frame: UIScreen.mainScreen().bounds)
        
        isShown = false
        alertTag = choosetag
        isAnimate = animate
        
        containsKeyboardElements = false
        
        baseLayout()
        
        let paddingTitleVertical = deviceManager!.deviceYCGFloatValue(yPos:8.0)
        let paddingButton = deviceManager!.deviceXCGFloatValue(xPos: 24.0)
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos:
            AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
        let messageHeight = alertBodyView!.frame.size.height - buttonHeight/2.0
        
        messageLabel!.text = message
        messageLabel!.backgroundColor = UIColor.clearColor()
        messageLabel!.hidden = false
        messageLabel!.frame = CGRectMake(paddingTitleVertical, paddingTitleVertical, alertBodyView!.frame.size.width - (2 * paddingTitleVertical), messageHeight)
        
        dualActionButtons = NimapDualRoundButtonView(frame: CGRectMake(paddingButton, alertBodyView!.frame.size.height - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight))
        dualActionButtons!.leftActionButton!.setTitle(actionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.rightActionButton!.setTitle(secondActionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.delegate = self
        alertBodyViewBaseView!.addSubview(dualActionButtons!)
        
        startAlertAnimation()
    }
    
    /*
    The following constructor will create alert view with message as heading, UITextView to take the input and ratings stars. this is used for rating and reviews alert box.. When user will press ok that time we will send ratings as well as review text with | separator to the listener
    */
    
    init(frame : CGRect, ratingtag : String, titleForTextView : String!, message : String!,actionButtonTitle: String!, secondActionButtonTitle: String!, ratings:  Float!, animate : Bool)
    {
        
        super.init(frame: UIScreen.mainScreen().bounds)
        
        isShown = false
        alertTag = ratingtag
        isAnimate = animate
        
        containsKeyboardElements = true
        
        baseLayoutWithHugeHeight()
        
        let padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
        let paddingY = deviceManager!.deviceXCGFloatValue(xPos: 4.0)
        let paddingTitleVertical = deviceManager!.deviceYCGFloatValue(yPos:8.0)
        let paddingButton = deviceManager!.deviceXCGFloatValue(xPos: 24.0)
        let starHeight = deviceManager!.deviceXCGFloatValue(xPos: 30.0)
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
        let placeHolderWidth = deviceManager!.deviceXCGFloatValue(xPos: 58.0)
        
        
        var yPos = paddingTitleVertical + paddingY
        
        messageLabel!.text = titleForTextView
        messageLabel!.frame = CGRectMake(padding, yPos, alertBodyView!.frame.size.width - (2 * padding), deviceManager!.deviceYCGFloatValue(yPos: 24.0))
        messageLabel!.backgroundColor = UIColor.clearColor()
        
        yPos = yPos + messageLabel!.frame.size.height + padding
        
        ratingContainer = NimapStarReviewView(frame: CGRectMake(padding, yPos, alertBodyView!.frame.size.width - (2 * padding), starHeight), type: NimapStarReviewView.MEDIUM_SIZE)
        ratingContainer!.backgroundColor = UIColor.whiteColor()
        alertBodyView!.addSubview(ratingContainer!)
        ratingContainer!.center = CGPointMake(alertBodyView!.frame.size.width/2.0, ratingContainer!.frame.size.height/2.0 + yPos)
        ratingContainer!.makeRatingViewTouchEnable()
        ratingContainer!.delegate = self
        ratingContainer!.updateRatingView((shopCount:0, reviewCount: 0, rating: Double(ratings)))
       
        yPos = yPos + ratingContainer!.frame.size.height + padding
        
        let messageHeight = alertBodyView!.frame.size.height - ((buttonHeight/2.0) + yPos + padding)
        
        inputTextView = UITextView(frame: CGRectMake(padding, yPos, alertBodyView!.frame.size.width - (2 * padding), messageHeight))
        if message == "na"
        {
            inputTextView!.text = ""
        }
        else
        {
            inputTextView!.text = message
        }
        inputTextView!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        inputTextView!.textColor = UIColor.secondaryTextColor()
        inputTextView!.layer.borderWidth = 1
        inputTextView!.layer.borderColor = UIColor.primaryColor().CGColor
        inputTextView!.keyboardType = UIKeyboardType.Default
        inputTextView!.backgroundColor = UIColor.clearColor()
        alertBodyView!.addSubview(inputTextView!)

        inputTextViewPlaceHolderLabel =  UILabel(frame: CGRectMake(padding+padding, yPos - padding, placeHolderWidth, (padding + padding)))
        inputTextViewPlaceHolderLabel!.text = ratingtag
        inputTextViewPlaceHolderLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 10.0))
        inputTextViewPlaceHolderLabel!.textColor = UIColor.blackColor()
        inputTextViewPlaceHolderLabel!.backgroundColor = UIColor.whiteColor()
        
        alertBodyView!.addSubview(inputTextViewPlaceHolderLabel!)
        
        dualActionButtons = NimapDualRoundButtonView(frame: CGRectMake(paddingButton, alertBodyView!.frame.size.height - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight))
        dualActionButtons!.leftActionButton!.setTitle(actionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.rightActionButton!.setTitle(secondActionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.delegate = self
        alertBodyViewBaseView!.addSubview(dualActionButtons!)
    
        startAlertAnimation()
        
        inputTextView!.becomeFirstResponder()
    }



    /*
     The following constructor will create alert view with message as heading, UITextView to take the input. this is used for notes alert box.. When user will press ok that we will capture the note in one variable.
     */

    init(frame : CGRect, noteTag : String, titleForTextView : String!, message : String!, actionButtonTitle: String!, secondActionButtonTitle: String!, animate : Bool)
    {

        super.init(frame: UIScreen.mainScreen().bounds)

        isShown = false
        alertTag = noteTag
        isAnimate = animate

        containsKeyboardElements = true

        baseLayout()

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 15.0)
        let paddingY = deviceManager!.deviceXCGFloatValue(xPos: 4.0)
        let paddingTitleVertical = deviceManager!.deviceYCGFloatValue(yPos:8.0)
        let paddingButton = deviceManager!.deviceXCGFloatValue(xPos: 24.0)
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: 35.0)

        messageLabel!.text = titleForTextView
        messageLabel!.frame = CGRectMake(padding, paddingTitleVertical, alertBodyView!.frame.size.width - (2 * padding), deviceManager!.deviceYCGFloatValue(yPos: 20.0))
        messageLabel!.backgroundColor = UIColor.clearColor()

        let yPos = messageLabel!.frame.size.height + padding

        let messageHeight = alertBodyView!.frame.size.height - ((buttonHeight/2.0) + yPos + padding)

        inputTextView = UITextView(frame: CGRectMake(padding, yPos, alertBodyView!.frame.size.width - (2 * padding), messageHeight))
        if message == "na"
        {
            inputTextView!.text = ""
        }
        else
        {
            inputTextView!.text = message
        }
        inputTextView!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        inputTextView!.textColor = UIColor.primaryTextColor()
        inputTextView!.layer.borderWidth = 1
        inputTextView!.layer.borderColor = UIColor.primaryColor().CGColor
        inputTextView!.keyboardType = UIKeyboardType.Default
        inputTextView!.backgroundColor = UIColor.clearColor()
        alertBodyView!.addSubview(inputTextView!)

        dualActionButtons = NimapDualRoundButtonView(frame: CGRectMake(paddingButton, alertBodyView!.frame.size.height - (buttonHeight/2.0), alertBodyView!.frame.size.width - (2 * paddingButton), buttonHeight))
        dualActionButtons!.leftActionButton!.setTitle(actionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.rightActionButton!.setTitle(secondActionButtonTitle, forState: UIControlState.Normal)
        dualActionButtons!.delegate = self
        alertBodyViewBaseView!.addSubview(dualActionButtons!)
        
        startAlertAnimation()
        
        inputTextView!.becomeFirstResponder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: Base Layout methods
    //this method will create the basic layout which will come in all the type of alert view, customized elements will be handled in the respective constructor
    func baseLayout()
    {
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.appAlertBGColor()
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: 40.0)
        
        let baseHeight = deviceManager!.deviceYCGFloatValue(yPos:130.0) + buttonHeight/2.0
        
        alertBodyViewBaseView = UIView(frame: CGRectMake(0, 0, deviceManager!.deviceXCGFloatValue(xPos:280.0),baseHeight))
        alertBodyViewBaseView!.backgroundColor = UIColor.clearColor()
        self.addSubview(alertBodyViewBaseView!)
        
        alertBodyView = UIView(frame: CGRectMake(0, 0, deviceManager!.deviceXCGFloatValue(xPos:280.0), deviceManager!.deviceYCGFloatValue(yPos: 130.0)))
        alertBodyView!.layer.cornerRadius = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        alertBodyView!.backgroundColor = UIColor.whiteColor()
        alertBodyViewBaseView!.addSubview(alertBodyView!)
        alertBodyView!.addShadow(shadowOpacity: 0.3)
        
        // if animation is needed than we need to set the view outside the screen and if it is false it should be in center of the screen
        if isAnimate == false
        {
            alertBodyViewBaseView!.center = self.center
        }
        else
        {
            alertBodyViewBaseView!.center = CGPointMake(self.center.x, -alertBodyViewBaseView!.frame.size.height)
        }
        
        messageLabel = UILabel()
        messageLabel!.textColor = UIColor.primaryTextColor()
        messageLabel!.textAlignment = NSTextAlignment.Center
        messageLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        messageLabel!.numberOfLines = -1
        alertBodyView!.addSubview(messageLabel!)
    }
    
    //this method will create the basic layout which will come in all the type of alert view but with bit more heught to accomodate bit bigger sub views, customized elements will be handled in the respective constructor
    func baseLayoutWithExtraHeight()
    {
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.appAlertBGColor()
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        alertBodyView = UIView(frame: CGRectMake(0, 0, deviceManager!.deviceXCGFloatValue(xPos: AppConstant.Static.NIMAP_ALERT_BASE_VIEW_WIDTH), deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NIMAP_ALERT_BASE_VIEW_EXTRA_HEIGHT)))
        alertBodyView!.backgroundColor = UIColor.appAlertBoxBGColor()
        self.addSubview(alertBodyView!)
        
        logoImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "logoalert")))
        logoImageView!.center = CGPointMake(alertBodyView!.frame.size.width/2.0,0.0)
        alertBodyView!.addSubview(logoImageView!)
        
        let leftTopLine = UIView(frame: CGRectMake(alertBodyView!.frame.origin.x, alertBodyView!.frame.origin.y, (alertBodyView!.frame.size.width / 2.0) - (logoImageView!.frame.size.width / 2.0), 1.0))
        leftTopLine.backgroundColor = UIColor.appButtonBorderColor()
        alertBodyView!.addSubview(leftTopLine)
        
        
        let rightTopLine = UIView(frame: CGRectMake((alertBodyView!.frame.size.width / 2.0) + (logoImageView!.frame.size.width / 2.0), alertBodyView!.frame.origin.y, (alertBodyView!.frame.size.width / 2.0) - (logoImageView!.frame.size.width / 2.0), 1.0))
        rightTopLine.backgroundColor = UIColor.appButtonBorderColor()
        alertBodyView!.addSubview(rightTopLine)
        
        let bottomLine = UIView(frame: CGRectMake(alertBodyView!.frame.origin.x, alertBodyView!.frame.origin.y + alertBodyView!.frame.size.height,alertBodyView!.frame.size.width, 1.0))
        bottomLine.backgroundColor = UIColor.appButtonBorderColor()
        alertBodyView!.addSubview(bottomLine)
        
        let leftLine = UIView(frame: CGRectMake(alertBodyView!.frame.origin.x,0.0,1.0,alertBodyView!.frame.size.height))
        leftLine.backgroundColor = UIColor.appButtonBorderColor()
        alertBodyView!.addSubview(leftLine)
        
        let rightLine = UIView(frame: CGRectMake(alertBodyView!.frame.origin.x + alertBodyView!.frame.size.width - 1.0,0.0,1.0,alertBodyView!.frame.size.height))
        rightLine.backgroundColor = UIColor.appButtonBorderColor()
        alertBodyView!.addSubview(rightLine)
        
        // if animation is needed than we need to set the view outside the screen and if it is false it should be in center of the screen
        if isAnimate == false
        {
            alertBodyView!.center = self.center
        }
        else
        {
            alertBodyView!.center = CGPointMake(self.center.x, -alertBodyView!.frame.size.height)
        }
        
        messageLabel = UILabel()
        messageLabel!.translatesAutoresizingMaskIntoConstraints = false
        messageLabel!.textColor = UIColor.appDarkFontColor()
        
        messageLabel!.textAlignment = NSTextAlignment.Center
        messageLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 16.0))//systemFontOfSize(deviceManager!.deviceYCGFloatValue(yPos: 16.0))
        messageLabel!.numberOfLines = -1
        alertBodyView!.addSubview(messageLabel!)
        
        
        
        
    }
    
    //this method will create the basic layout which will come in all the type of alert view but with bit huge height to accomodate bit bigger sub views, customized elements will be handled in the respective constructor. We mostly use this for review box
    func baseLayoutWithHugeHeight()
    {
        self.userInteractionEnabled = true
        self.backgroundColor = UIColor.appAlertBGColor()
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        let buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: 40.0)
        
        let baseHeight = deviceManager!.deviceYCGFloatValue(yPos:200.0) + buttonHeight/2.0
        
        alertBodyViewBaseView = UIView(frame: CGRectMake(0, 0, deviceManager!.deviceXCGFloatValue(xPos:280.0),baseHeight))
        alertBodyViewBaseView!.backgroundColor = UIColor.clearColor()
        self.addSubview(alertBodyViewBaseView!)
        
        alertBodyView = UIView(frame: CGRectMake(0, 0, deviceManager!.deviceXCGFloatValue(xPos:280.0), deviceManager!.deviceYCGFloatValue(yPos: 200.0)))
        alertBodyView!.layer.cornerRadius = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        alertBodyView!.backgroundColor = UIColor.whiteColor()
        alertBodyViewBaseView!.addSubview(alertBodyView!)
        alertBodyView!.addShadow(shadowOpacity: 0.3)
        
        // if animation is needed than we need to set the view outside the screen and if it is false it should be in center of the screen
        if isAnimate == false
        {
            alertBodyViewBaseView!.center = self.center
        }
        else
        {
            alertBodyViewBaseView!.center = CGPointMake(self.center.x, -alertBodyViewBaseView!.frame.size.height)
        }
        
        messageLabel = UILabel()
        messageLabel!.textColor = UIColor.primaryTextColor()
        messageLabel!.textAlignment = NSTextAlignment.Center
        messageLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        messageLabel!.numberOfLines = -1
        alertBodyView!.addSubview(messageLabel!)
    }
    
    //MARK: Animation Functions
    
    func startAlertAnimation()
    {
        if isShown == false
        {
            isShown = true
            
            if animator == nil
            {
                animator = UIDynamicAnimator(referenceView: self)
            }
            animator!.removeAllBehaviors()
            
            let gravityBehavior = UIGravityBehavior(items: [alertBodyViewBaseView!])
            gravityBehavior.gravityDirection = CGVectorMake(0, 1) // moving down
            animator!.addBehavior(gravityBehavior)
            
            // if any elements in alert will bring keyboard in that case we will require to have alert view shown bit high than the center vertically so that alert is always seen. Collision boundary will change accordingle
            
            
            let collisionBehavior = UICollisionBehavior(items: [alertBodyViewBaseView!])
            
            if containsKeyboardElements == true
            {
                collisionBehavior.addBoundaryWithIdentifier("left_open", fromPoint: CGPointMake(alertBodyViewBaseView!.frame.origin.x, self.center.y), toPoint: CGPointMake(alertBodyViewBaseView!.frame.origin.x + alertBodyViewBaseView!.frame.size.width, self.center.y))
            }
            else
            {
                collisionBehavior.addBoundaryWithIdentifier("left_open", fromPoint: CGPointMake(alertBodyViewBaseView!.frame.origin.x, self.center.y + alertBodyViewBaseView!.frame.size.height/2.0), toPoint: CGPointMake(alertBodyView!.frame.origin.x + alertBodyViewBaseView!.frame.size.width, self.center.y + alertBodyViewBaseView!.frame.size.height/2.0))
            }
            
            
            
            
            animator!.addBehavior(collisionBehavior)
        }
    }
    
    func endAlertAnimation()
    {
        if isShown == true
        {
            if animator == nil
            {
                animator = UIDynamicAnimator(referenceView: self)
            }
            animator!.removeAllBehaviors()
            
            let gravityBehavior = UIGravityBehavior(items: [alertBodyViewBaseView!])
            gravityBehavior.gravityDirection = CGVectorMake(0, 1) // moving down
            animator!.addBehavior(gravityBehavior)
            
            let collisionBehavior = UICollisionBehavior(items: [alertBodyViewBaseView!])
            collisionBehavior.addBoundaryWithIdentifier("left_close", fromPoint: CGPointMake(alertBodyViewBaseView!.frame.origin.x, self.frame.size.height +  alertBodyViewBaseView!.frame.size.height), toPoint: CGPointMake(alertBodyViewBaseView!.frame.origin.x + alertBodyViewBaseView!.frame.size.width, self.frame.size.height + alertBodyViewBaseView!.frame.size.height))
            animator!.addBehavior(collisionBehavior)
            
            NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(NimapAlertView.removeUserAlertAfterAnimation), userInfo: nil, repeats: false)
        }
    }
    
    func removeUserAlertAfterAnimation()
    {
        isShown = false
        
        // depending which button was press respective call bac is send,
        if delegate != nil
        {
            if buttonIndex == 0
            {
                // we need to send the data entered into the textfield and than the button action callback.. We need to check whether the alert view contains text field and also check which component taking the input
                if containsKeyboardElements == true
                {
                    if delegate!.respondsToSelector(#selector(NimapAlertViewDelegate.didEnterText(_:text:)))
                    {
                        if inputTextFiled != nil
                        {
                            delegate!.didEnterText!(alertTag!, text: inputTextFiled!.text)
                        }
                        else if inputTextView != nil
                        {
                            
                            if ratings != nil
                            {
                                print("ratings \(ratings!)")
                                
                                if ratings < 1.0{
                                    delegate!.didEnterText!(alertTag!, text: "0|\(inputTextView!.text)")
                                }
                                else{
                                    if inputTextView!.text.isEmpty
                                    {
                                        delegate!.didEnterText!(alertTag!, text: "\(ratings!)|na")
                                    }
                                    else
                                    {
                                        delegate!.didEnterText!(alertTag!, text: "\(ratings!)|\(inputTextView!.text)")
                                    }
                                }
                                
                                
                            }
                            else
                            {
                                delegate!.didEnterText!(alertTag!, text: "0|\(inputTextView!.text)")
                            }
                            
                        }
                        
                    }
                }
                if delegate!.respondsToSelector(#selector(NimapAlertViewDelegate.didActionButtonPressed(_:)))
                {
                    delegate!.didActionButtonPressed!(alertTag!)
                }
            }
            else if buttonIndex == 1
            {
                if delegate!.respondsToSelector(#selector(NimapAlertViewDelegate.didSecondaryActionButtonPressed(_:)))
                {
                    delegate!.didSecondaryActionButtonPressed!(alertTag!)
                }
                
                
            }
        }
    }
    
    //MARK: actionButton callback
    /*
    This method will send the callback to listener when user presses the button on 
    the alert view.
    */
    func actionButtonPressed()
    {
        buttonIndex = 0
        endAlertAnimation()
    }
    
    /*
    This method will send the callback to listener when user presses the secondary button on the alert view.
    */
    func secondActionButtonPressed()
    {
        buttonIndex = 1
        endAlertAnimation()
    }
    
    //MARK: NimapDualRoundButtonDelegate callback
    
    func didPrimaryDualRoundActionButtonPressed()
    {
        buttonIndex = 0
        endAlertAnimation()
    }
    
    func didSecondaryDualRoundActionButtonPressed()
    {
        buttonIndex = 1
        endAlertAnimation()
    }
    
    //MARK: NimapStarViewDelegate
    func didUpdateRatings(ratings : Float)
    {
        self.ratings = ratings
    }
}
