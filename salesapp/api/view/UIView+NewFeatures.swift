//
//  UIView+NewFeatures.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 25/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

extension UIView{
    
    func addGradiant(topColor topColor : UIColor, bottomColor : UIColor)
    {
        let colorTop = topColor.CGColor
        let colorBottom = bottomColor.CGColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop,colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        
        backgroundColor = UIColor.clearColor()
        gradientLayer.frame = frame
        layer.insertSublayer(gradientLayer,atIndex: 0)
    }
    
    func removeGradiant()
    {
        if let sublayers = layer.sublayers {
            for layer in sublayers {
                // note, the following WILL crash:
                // layer.removeFromSuperlayer()
                // this works well as we need to know which sublayer we have added
                if layer.isKindOfClass(CAGradientLayer)
                {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    func addShadow(shadowOpacity shadowOpacity : Float)
    {
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = 5.0
    }
    
    func addShadow(shadowOpacity shadowOpacity : Float, shadowHeight height : CGFloat)
    {
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0.0, height: height)
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = 5.0
    }
    
    func addBlurEffect(){
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.tag = 10001
        blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
    func removeBlurEffect(){
        
        self.viewWithTag(10001)!.removeFromSuperview()

    }
}
