//
//  NimapSearchBarView.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 29/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol NimapSearchBarViewDelegate : NSObjectProtocol
{
    optional func nimapSearchBarViewDidBeginEditing(textField: UITextField)
    optional func nimapSearchBarViewEnteredCharacters(string: String)
}

class NimapSearchBarView: UIView, UITextFieldDelegate {

    var deviceManager : DeviceManager?
    
    var baseView : NimapCardView?
    var searchTextField : UITextField?
    
    weak var delegate : NimapSearchBarViewDelegate?
    
    override init(frame: CGRect)
    {

        super.init(frame: CGRectMake(0,0,frame.size.width,frame.size.height))
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        baseView = NimapCardView(frame: CGRectMake(0.0,0.0,self.frame.width,self.frame.height), radius: 3, shadowOpacity: 0.3, shadowHeight: 6.0)
        self.addSubview(baseView!)
        
        let searchWidth = self.frame.size.width
        
        searchTextField = UITextField(frame: CGRectMake((0.05 * searchWidth),0.0,(0.9 * searchWidth),self.frame.size.height))
        searchTextField!.text = ""
        searchTextField!.placeholder = "Looking for"
        searchTextField!.textColor = UIColor.primaryTextColor()
        searchTextField!.borderStyle = UITextBorderStyle.None
        searchTextField!.delegate = self
        searchTextField!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        
        let attributes = [
            NSForegroundColorAttributeName: UIColor.secondaryTextColor(),
            NSFontAttributeName : UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))!]
        
        searchTextField!.attributedPlaceholder = NSAttributedString(string: "Looking for", attributes:attributes)
        
//        let imageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "enquiryselect")))
//        imageView.tintColor = UIColor.primaryColor()
//        imageView.contentMode = .ScaleAspectFit
//        
//        searchTextField!.leftViewMode = .Always
//        searchTextField!.leftView = imageView

        self.addSubview(searchTextField!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func resignFirstResponder() -> Bool {
        searchTextField!.resignFirstResponder()
        return true
    }

    //MARK: UITextFieldDelegate methods
    func textFieldDidBeginEditing(textField: UITextField)
    {
        if delegate != nil
        {
        if delegate!.respondsToSelector(#selector(NimapSearchBarViewDelegate.nimapSearchBarViewDidBeginEditing(_:)))
                {
                    delegate!.nimapSearchBarViewDidBeginEditing!(searchTextField!)
            }
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        var updatedTextString : NSString = textField.text! as NSString
        
        updatedTextString = updatedTextString.stringByReplacingCharactersInRange(range, withString: string)
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(NimapSearchBarViewDelegate.nimapSearchBarViewEnteredCharacters(_:)))
            {
                delegate!.nimapSearchBarViewEnteredCharacters!(updatedTextString as String)
            }
        }
        
        return true
    }
}
