//
//  NimapNavigationBarView.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 28/06/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

/*

This class represents the navigation bar which can be cusotmize to what ever level. It is singleton class as it's states needs to be maintained throughout the application.

*/

@objc protocol NimapNavigationBarViewDelegate: NSObjectProtocol
{
    optional func backButtonPressed()
    optional func rightButtonPressed()
    optional func addToCartButtonPressed()
    optional func filterButtonPressed()
}

class NimapNavigationBarView: UIView {
    
    //MARK: VAriable Section
    
    weak var delegate : NimapNavigationBarViewDelegate?
    
    var leftButton : UIButton?
    var titleLabel : UILabel?
    var rightButton : UIButton?
    
    var additionalAddToCartButton : UIButton?
    var additionalFilterButton : UIButton?
    
    var notificationBadgeLabel : UILabel?
    
    var viewDictionary : [String: AnyObject]?//NSMutableDictionary?
    var metricDictionary : [String: AnyObject]?//NSMutableDictionary?
    
    var deviceManager : DeviceManager?
    
    var padding: Float?
    
    var bottomLine : UIView?
    
    //MARK: Constructor Section
    init(frame: CGRect, title : String) {

        super.init(frame: frame)
        deviceManager = DeviceManager.sharedDeviceManagement()
        viewDictionary = [String: AnyObject]()//NSMutableDictionary()
        metricDictionary = [String: AnyObject]()//NSMutableDictionary()
        
        self.backgroundColor = UIColor.primaryColor()
        self.addShadow(shadowOpacity: 0.4, shadowHeight: 2.0)
        
        loadTitle(title)
    }
    
    func loadTitle(title:String)
    {
        titleLabel = UILabel(frame: CGRectMake(0,0,(0.7 * self.frame.size.width),self.frame.size.height))
        
        titleLabel!.text = title
        titleLabel!.font = UIFont.appFontBold(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        titleLabel!.backgroundColor = UIColor.clearColor()
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.center = CGPointMake(self.center.x,self.frame.size.height/2.0)
        
        self.addSubview(titleLabel!)
    }
    
    func addLeftButton()
    {
        leftButton = UIButton(frame:CGRectMake(0.0,0.0,(self.frame.size.width/2.0),self.frame.size.height))
        leftButton!.setTitle("Skip", forState: UIControlState.Normal)
        leftButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        leftButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 12.0))
        leftButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        leftButton!.backgroundColor = UIColor.clearColor()
        leftButton!.addTarget(self, action: #selector(NimapNavigationBarView.backButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(leftButton!)
        leftButton!.sizeToFit()
        
        leftButton!.frame = CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: 8.0),0.0,leftButton!.frame.size.width, self.frame.size.height)
    }
    
    func addRightButton()
    {
        rightButton = UIButton(frame:CGRectMake(0.0,0.0,(self.frame.size.width/2.0),self.frame.size.height))
        rightButton!.setTitle("Cancel", forState: UIControlState.Normal)
        rightButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        rightButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 12.0))
        rightButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        rightButton!.backgroundColor = UIColor.clearColor()
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarView.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(rightButton!)
        rightButton!.sizeToFit()
        
        let rightPos = self.frame.size.width - (deviceManager!.deviceXCGFloatValue(xPos: 8.0) + rightButton!.frame.size.width)
        
        rightButton!.frame = CGRectMake(rightPos,0.0,rightButton!.frame.size.width, self.frame.size.height)
    }
    
    func addRightButtonWithIcon(){
        
        rightButton = UIButton(frame:CGRectMake(0.0,0.0,(self.frame.size.width/2.0),self.frame.size.height))
        rightButton!.setTitle(AppConstant.Static.CANCEL_ICON, forState: UIControlState.Normal)
        rightButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        rightButton!.titleLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 20.0))
        rightButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        rightButton!.backgroundColor = UIColor.clearColor()
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarView.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(rightButton!)
        rightButton!.sizeToFit()
        
        let rightPos = self.frame.size.width - (deviceManager!.deviceXCGFloatValue(xPos: 8.0) + rightButton!.frame.size.width)
        
        rightButton!.frame = CGRectMake(rightPos,0.0,rightButton!.frame.size.width, self.frame.size.height)
    }
    
    func addLeftButtonWithIcon(){
        leftButton = UIButton(frame:CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: 10),0.0,(self.frame.size.width/2.0),self.frame.size.height))
        leftButton!.setTitle(AppConstant.Static.BACK_ICON, forState: UIControlState.Normal)
        leftButton!.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        leftButton!.titleLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 20.0))
        leftButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        leftButton!.backgroundColor = UIColor.clearColor()
        leftButton!.addTarget(self, action: #selector(NimapNavigationBarView.backButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(leftButton!)
        leftButton!.sizeToFit()
        
        leftButton!.frame = CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: 8.0),0.0,leftButton!.frame.size.width, self.frame.size.height)
    }
    
    func updateLeftButtonTitle(title : String)
    {
        if title == AppConstant.Static.BACK_ICON{
            leftButton!.titleLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 30.0))
            leftButton!.setTitle(title, forState: UIControlState.Normal)
        }
        else{
            
            if title.characters.count > 14 {
                
                let subTitle : String  =  (title as NSString).substringWithRange(NSRange(location: 0, length: 12)) + ".."
                leftButton!.setTitle(subTitle, forState: UIControlState.Normal)
            }
            else{
                leftButton!.setTitle(title, forState: UIControlState.Normal)
            }
        }
        
        leftButton!.sizeToFit()
        leftButton!.frame = CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: 8.0),0.0,leftButton!.frame.size.width, self.frame.size.height)
    }
    
    func updateRightButtonTitle(title : String)
    {
        rightButton!.setTitle(title, forState: UIControlState.Normal)
        rightButton!.sizeToFit()
        
        let rightPos = self.frame.size.width - (deviceManager!.deviceXCGFloatValue(xPos: 8.0) + rightButton!.frame.size.width)
        
        rightButton!.frame = CGRectMake(rightPos,0.0,rightButton!.frame.size.width, self.frame.size.height)
    }
    
    func removeRightButton()
    {
        if rightButton != nil
        {
            rightButton!.removeFromSuperview()
            rightButton = nil
        }
        
    }
    
    func removeLeftButton()
    {
        if leftButton != nil
        {
            leftButton!.removeFromSuperview()
            leftButton = nil
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK Create methods depending upon the needs.
    
    func alignNavigationOnylWithRightButton()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else
        {
            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
            
        }
        
        rightButton = UIButton(type: UIButtonType.Custom) as UIButton
        rightButton!.translatesAutoresizingMaskIntoConstraints = false
       // rightButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName:"cross")), forState: UIControlState.Normal)
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(rightButton!)
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.text = "TITLE"
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 24.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["rightButton"] = rightButton!
        
        //viewDictionary!.setObject(titleLabel!, forKey: "titleLabel")
        //viewDictionary!.setObject(rightButton!, forKey: "rightButton")
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        
        //metricDictionary!.setValue(buttonWidth, forKey: "buttonWidth")
        //metricDictionary!.setValue(padding, forKey: "padding")
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[titleLabel][rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        
    }
    
    func alignNavigationlWithRightButtonWithOutAddToCart()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else
        {
            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
            
        }
        
        rightButton = UIButton(type: UIButtonType.Custom) as UIButton
        rightButton!.translatesAutoresizingMaskIntoConstraints = false
        rightButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName:"sidemenu")), forState: UIControlState.Normal)
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(rightButton!)
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.text = "TITLE"
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 24.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["rightButton"] = rightButton!
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[titleLabel][rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        
    }
    
    func alignNavigationWithRightButtonsAndCart()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else
        {
            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
        }
    
        additionalAddToCartButton = UIButton(type: UIButtonType.Custom) as UIButton
        additionalAddToCartButton!.translatesAutoresizingMaskIntoConstraints = false
        additionalAddToCartButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "cart")), forState: UIControlState.Normal)
        additionalAddToCartButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        additionalAddToCartButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.addToCartButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(additionalAddToCartButton!)
        
        
        rightButton = UIButton(type: UIButtonType.Custom) as UIButton
        rightButton!.translatesAutoresizingMaskIntoConstraints = false
        rightButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName:"sidemenu")), forState: UIControlState.Normal)
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        rightButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(rightButton!)
        
        notificationBadgeLabel = UILabel()
        notificationBadgeLabel!.translatesAutoresizingMaskIntoConstraints = false
        notificationBadgeLabel!.text = ""
        notificationBadgeLabel!.textAlignment = NSTextAlignment.Center
        notificationBadgeLabel!.textColor = UIColor.appBGColor()
        notificationBadgeLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        self.addSubview(notificationBadgeLabel!)
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.text = "TITLE"
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 24.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["rightButton"] = rightButton!
        viewDictionary!["additionalAddToCartButton"] = additionalAddToCartButton!
        viewDictionary!["notificationBadgeLabel"] = notificationBadgeLabel!
        
        //viewDictionary!.setObject(titleLabel!, forKey: "titleLabel")
        //viewDictionary!.setObject(rightButton!, forKey: "rightButton")
        //viewDictionary!.setObject(additionalAddToCartButton!, forKey: "additionalAddToCartButton")
        //viewDictionary!.setObject(notificationBadgeLabel!, forKey: "notificationBadgeLabel")
        
        let badgeHeight = self.frame.size.height/2.0
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        metricDictionary!["badgeHeight"] = badgeHeight
        
//        metricDictionary!.setValue(buttonWidth, forKey: "buttonWidth")
//        metricDictionary!.setValue(padding, forKey: "padding")
//        metricDictionary!.setValue(badgeHeight, forKey: "badgeHeight")
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[titleLabel][additionalAddToCartButton(buttonWidth)]-padding-[rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("[notificationBadgeLabel(buttonWidth)][rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[additionalAddToCartButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[notificationBadgeLabel(badgeHeight)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
    }
    
    func alignNavigationWithBothButtonsWithCart()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else
        {
            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
            
        }
        
        leftButton = UIButton(type: UIButtonType.Custom) as UIButton
        leftButton!.translatesAutoresizingMaskIntoConstraints = false
        leftButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "back")), forState: UIControlState.Normal)
        leftButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.backButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        leftButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(leftButton!)

        additionalAddToCartButton = UIButton(type: UIButtonType.Custom) as UIButton
        additionalAddToCartButton!.translatesAutoresizingMaskIntoConstraints = false
        additionalAddToCartButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "cart")), forState: UIControlState.Normal)
        additionalAddToCartButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        additionalAddToCartButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.addToCartButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(additionalAddToCartButton!)

        
        rightButton = UIButton(type: UIButtonType.Custom) as UIButton
        rightButton!.translatesAutoresizingMaskIntoConstraints = false
        rightButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName:"sidemenu")), forState: UIControlState.Normal)
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        rightButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(rightButton!)
        
        notificationBadgeLabel = UILabel()
        notificationBadgeLabel!.translatesAutoresizingMaskIntoConstraints = false
        notificationBadgeLabel!.text = ""
        notificationBadgeLabel!.textAlignment = NSTextAlignment.Center
        notificationBadgeLabel!.textColor = UIColor.appBGColor()
        notificationBadgeLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        self.addSubview(notificationBadgeLabel!)

        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.text = "TITLE"
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 24.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["rightButton"] = rightButton!
        viewDictionary!["leftButton"] = leftButton!
        viewDictionary!["additionalAddToCartButton"] = additionalAddToCartButton!
        viewDictionary!["notificationBadgeLabel"] = notificationBadgeLabel!
    
        //viewDictionary!.setObject(titleLabel!, forKey: "titleLabel")
        //viewDictionary!.setObject(rightButton!, forKey: "rightButton")
        //viewDictionary!.setObject(leftButton!, forKey: "leftButton")
        //viewDictionary!.setObject(additionalAddToCartButton!, forKey: "additionalAddToCartButton")
        //viewDictionary!.setObject(notificationBadgeLabel!, forKey: "notificationBadgeLabel")
        
        let badgeHeight = self.frame.size.height/2.0
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        metricDictionary!["badgeHeight"] = badgeHeight
        
        //metricDictionary!.setValue(buttonWidth, forKey: "buttonWidth")
        //metricDictioary!.setValue(padding, forKey: "padding")
        //metricDictionary!.setValue(badgeHeight, forKey: "badgeHeight")
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[leftButton(buttonWidth)][titleLabel][additionalAddToCartButton(buttonWidth)]-padding-[rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("[notificationBadgeLabel(buttonWidth)][rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[additionalAddToCartButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[notificationBadgeLabel(badgeHeight)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))

    }
    
    func alignNavigationWithAllButtons()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else
        {
            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
            
        }
        
        leftButton = UIButton(type: UIButtonType.Custom) as UIButton
        leftButton!.translatesAutoresizingMaskIntoConstraints = false
        leftButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "back")), forState: UIControlState.Normal)
        leftButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.backButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        leftButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(leftButton!)
        
        rightButton = UIButton(type: UIButtonType.Custom) as UIButton
        rightButton!.translatesAutoresizingMaskIntoConstraints = false
//        rightButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "sidemenu")), forState: UIControlState.Normal)
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        rightButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(rightButton!)
        
        additionalAddToCartButton = UIButton(type: UIButtonType.Custom) as UIButton
        additionalAddToCartButton!.translatesAutoresizingMaskIntoConstraints = false
        additionalAddToCartButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "cart")), forState: UIControlState.Normal)
        additionalAddToCartButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        additionalAddToCartButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.addToCartButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.addSubview(additionalAddToCartButton!)
        
        additionalFilterButton = UIButton(type: UIButtonType.Custom) as UIButton
        additionalFilterButton!.translatesAutoresizingMaskIntoConstraints = false
        additionalFilterButton!.setImage(UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "filter")), forState: UIControlState.Normal)
        additionalFilterButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.filterButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        additionalFilterButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(additionalFilterButton!)
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.text = "TITLE"
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 24.0))
        self.addSubview(titleLabel!)
        
        notificationBadgeLabel = UILabel()
        notificationBadgeLabel!.translatesAutoresizingMaskIntoConstraints = false
        notificationBadgeLabel!.text = ""
        notificationBadgeLabel!.textAlignment = NSTextAlignment.Center
        notificationBadgeLabel!.textColor = UIColor.appBGColor()
        notificationBadgeLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        self.addSubview(notificationBadgeLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["leftButton"] = leftButton!
        viewDictionary!["rightButton"] = rightButton!
        viewDictionary!["additionalAddToCartButton"] = additionalAddToCartButton!
        viewDictionary!["additionalFilterButton"] = additionalFilterButton!
        viewDictionary!["notificationBadgeLabel"] = notificationBadgeLabel!
        
//        viewDictionary!.setObject(titleLabel!, forKey: "titleLabel")
//        viewDictionary!.setObject(leftButton!, forKey: "leftButton")
//        viewDictionary!.setObject(rightButton!, forKey: "rightButton")
//        viewDictionary!.setObject(additionalAddToCartButton!, forKey: "additionalAddToCartButton")
//        viewDictionary!.setObject(additionalFilterButton!, forKey: "additionalFilterButton")
//        viewDictionary!.setObject(notificationBadgeLabel!, forKey: "notificationBadgeLabel")
        
        let badgeHeight = self.frame.size.height/2.0
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        metricDictionary!["badgeHeight"] = badgeHeight
        
//        metricDictionary!.setValue(buttonWidth, forKey: "buttonWidth")
//        metricDictionary!.setValue(padding, forKey: "padding")
//        metricDictionary!.setValue(badgeHeight, forKey: "badgeHeight")
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[leftButton(buttonWidth)][titleLabel]-padding-[additionalFilterButton(buttonWidth)]-padding-[additionalAddToCartButton(buttonWidth)]-padding-[rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("[notificationBadgeLabel(buttonWidth)][rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[additionalFilterButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[additionalAddToCartButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[notificationBadgeLabel(badgeHeight)]", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))

    }
    
    func alignNavigationWithOnlyLeftRightButtons()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            
            buttonWidth = 107

//            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
//            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else if deviceManager!.deviceType == deviceManager!.iPhone6
        {
            buttonWidth = 125
            
//            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
//            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
        }
        else if deviceManager!.deviceType == deviceManager!.iPhone6plus
        {
            buttonWidth = 138
        }
        else if deviceManager!.deviceType == deviceManager!.iPad
        {
            buttonWidth = 256
        }
        else
        {
            
        }
        
        
        leftButton = UIButton(type: UIButtonType.Custom) as UIButton
        leftButton!.translatesAutoresizingMaskIntoConstraints = false
        leftButton!.titleLabel!.textAlignment = .Left
        leftButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.backButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        leftButton!.titleLabel!.lineBreakMode = .ByTruncatingTail
        leftButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        leftButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        self.addSubview(leftButton!)
        
        rightButton = UIButton(type: UIButtonType.Custom) as UIButton
        rightButton!.translatesAutoresizingMaskIntoConstraints = false
        rightButton!.addTarget(self, action: #selector(NimapNavigationBarViewDelegate.rightButtonPressed), forControlEvents: UIControlEvents.TouchDown)
//        rightButton!.contentHorizontalAlignment = .Right;
        rightButton!.titleLabel!.lineBreakMode = .ByTruncatingHead
        rightButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        rightButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        self.addSubview(rightButton!)
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.textAlignment = NSTextAlignment.Center
        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.lineBreakMode = .ByTruncatingTail

//        titleLabel!.backgroundColor = UIColor.grayColor()

        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 20.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["rightButton"] = rightButton!
        viewDictionary!["leftButton"] = leftButton!
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[leftButton(buttonWidth)][titleLabel(buttonWidth)]-[rightButton(buttonWidth)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
    }
    
    func alignNavigationWithOnlyLeftButton()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =
                deviceManager!.deviceXCGFloatValue(xPos: 120)
            
//                AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding =
                AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else if deviceManager!.deviceType == deviceManager!.iPhone6
        {
            buttonWidth =  deviceManager!.deviceXCGFloatValue(xPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
            
        }
        
        leftButton = UIButton(type: UIButtonType.Custom) as UIButton
        leftButton!.translatesAutoresizingMaskIntoConstraints = false
        leftButton!.addTarget(self, action: #selector(NimapNavigationBarView.backButtonPressed), forControlEvents: UIControlEvents.TouchUpInside)
        leftButton!.titleLabel!.textAlignment = .Left
        leftButton!.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        leftButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14))
        
        self.addSubview(leftButton!)
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false

        titleLabel!.textAlignment = NSTextAlignment.Left
        titleLabel!.textColor = UIColor.whiteColor()

        titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 20.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        viewDictionary!["leftButton"] = leftButton!
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        
        
        //
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[leftButton(buttonWidth)][titleLabel]", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        
        
         self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
       


    }

    func getCharactersFromString(string : String , characters : Int ) -> String {
        
        var resultString = String()
        var xPosition : CGFloat = 0.0

        if string != "Select Area"
        {
            if string.characters.count > characters
            {
                resultString = (string as NSString).substringWithRange(NSRange(location: 0, length: characters))
                resultString = resultString + ".."
            }
            else
            {
                resultString = string
            }

            if resultString.characters.count <= 5
            {
                xPosition = 40.0
            }
            else if resultString.characters.count == 6
            {
                xPosition = 45.0
            }
            else if resultString.characters.count == 7
            {
                xPosition = 50.0
            }
            else
            {
                xPosition = 60.0
            }

        }
        else
        {
            resultString = "Select Area"
            xPosition = 68.0
        }

        let iconYposition = (leftButton!.frame.height/2) - deviceManager!.deviceXCGFloatValue(xPos: 3.0)
        let height_width = deviceManager!.deviceXCGFloatValue(xPos: 9.0)

        let iconLabel = UILabel(frame: CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: xPosition),iconYposition,height_width,height_width))
        iconLabel.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 7.0))
        iconLabel.textColor = UIColor.iconsColor()
        iconLabel.textAlignment = NSTextAlignment.Center
        iconLabel.backgroundColor = UIColor.clearColor()
        iconLabel.text = " \u{f078}"
        self.addSubview(iconLabel)



        return resultString
    }

    func alignNavigationWithNoButtons()
    {
        var buttonWidth : CGFloat = 0.0
        
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5
        {
            buttonWidth =  AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH
            padding = AppConstant.Static.NAVIGATION_BAR_PADDING_X
        }
        else
        {
            buttonWidth =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_BUTTON_WIDTH)
            padding = deviceManager!.deviceXvalue(xPos: AppConstant.Static.NAVIGATION_BAR_PADDING_X)
            
        }
        
        titleLabel = UILabel()
        titleLabel!.translatesAutoresizingMaskIntoConstraints = false
        titleLabel!.text = "TITLE"
        titleLabel!.textAlignment = NSTextAlignment.Center
//        titleLabel!.textColor = UIColor.primaryTextColor()
          titleLabel!.textColor = UIColor.whiteColor()
        titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 20.0))
        self.addSubview(titleLabel!)
        
        viewDictionary!["titleLabel"] = titleLabel!
        
        metricDictionary!["buttonWidth"] = buttonWidth
        metricDictionary!["padding"] = padding
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))        
    }
    
    //MARK : Local methods
    
    func updateNavigationBadgeLabel()
    {
        
    }
    
// these methods will help to realign navigation bar elements as the screen
    //MARK: ReAlignment sections methods
    
    // Following method will remove the unwanted views for the screen. We need to check for nil and not nil because the Navigation Bar is singleton and thus will be referring to the elements as and when needed.
    func realignNavigationBarWithOnlyRigthButton()
    {
        
        additionalFilterButton!.hidden = true
        
        
        if additionalFilterButton != nil
        {
            removeAllContraints()
            
            additionalFilterButton!.hidden = true
            additionalFilterButton!.removeFromSuperview()
            additionalFilterButton = nil
        }
        if additionalAddToCartButton != nil
        {
            additionalAddToCartButton!.hidden = true
            additionalAddToCartButton!.removeFromSuperview()
            additionalAddToCartButton = nil
        }
        if leftButton != nil
        {
            leftButton!.hidden = true
            leftButton!.removeFromSuperview()
            leftButton = nil
        }
        
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[titleLabel][rightButton(buttonWidth)]-padding-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
    }
    
    func removeAllContraints()
    {
        self.removeConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[leftButton(buttonWidth)][titleLabel][additionalFilterButton(buttonWidth)][additionalAddToCartButton(buttonWidth)][rightButton(buttonWidth)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        
        self.removeConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.removeConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[titleLabel]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.removeConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[additionalFilterButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.removeConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[additionalAddToCartButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
        self.removeConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metricDictionary!, views: viewDictionary!))
    }
    
    
    
    //MARK: Button Event Handlers
    
    func backButtonPressed()
    {
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(NimapNavigationBarViewDelegate.backButtonPressed))
            {
                delegate!.backButtonPressed!()
            }
        }
    }
    
    func rightButtonPressed()
    {
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(NimapNavigationBarViewDelegate.rightButtonPressed))
            {
                delegate!.rightButtonPressed!()
            }
        }
    }
    
    func filterButtonPressed()
    {
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(NimapNavigationBarViewDelegate.filterButtonPressed))
            {
                delegate!.filterButtonPressed!()
            }
        }
    }
    
    func addToCartButtonPressed()
    {
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(NimapNavigationBarViewDelegate.addToCartButtonPressed))
            {
                delegate!.addToCartButtonPressed!()
            }
        }
    }



}
