//
//  UITextField+Border.swift
//  BananaUniform
//
//  Created by Apple on 28/07/15.
//  Copyright (c) 2015 NimapInfotech. All rights reserved.
//

import UIKit

extension UITextField
{
    /** 
    Shows the border of the textField with specific width and color
    */
    func showBorder(xPos: CGFloat, yPos: CGFloat, borderLength: CGFloat, borderThickness : CGFloat, borderColor: CGColor)
    {
        let border = CALayer()
        border.borderColor = borderColor
        border.frame = CGRect(x: xPos, y: yPos, width:  borderLength, height: borderThickness)
        border.borderWidth = borderThickness
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    /**
    Shows the border of the textField with specific width and color
    */
    func showOnlyBottomBorder()
    {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.secondaryTextColor().CGColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: width)
        border.borderWidth = self.frame.size.width
        self.layer.addSublayer(border)
        
        let border1 = CALayer()
        border1.borderColor = UIColor.secondaryTextColor().CGColor
        border1.frame = CGRect(x: 0, y: self.frame.size.height - 4.0, width:1.0, height: 4.0)
        border1.borderWidth = 1.0
        self.layer.addSublayer(border1)
        
        let border2 = CALayer()
        border2.borderColor = UIColor.secondaryTextColor().CGColor
        border2.frame = CGRect(x: self.frame.size.width - 1.0, y: self.frame.size.height - 4.0, width:1.0, height: 4.0)
        border2.borderWidth = 1.0
        self.layer.addSublayer(border2)
        
        self.layer.masksToBounds = true
    }
    
}