//
//  ReverseGeoCoderParser.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 05/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol ReverseGeoCoderParserDelegate : NSObjectProtocol {
    func didReceivedReverseGeoName(reverseName : ReverseGeoCoderModel?, pinIndex : Int)
}

class ReverseGeoCoderParser: NSObject, AsyncLoaderModelDelegate {

    var loader : AsyncLoaderModel?
    var pinIndex : Int?
    var latitude : Double?
    var longitude : Double?
    
    weak var delegate : ReverseGeoCoderParserDelegate?
    
    func getReverseGeoData(latitude : Double,longitude : Double, pinIndex : Int){
        
        self.pinIndex = pinIndex
        self.latitude = latitude
        self.longitude = longitude
        
        loader = AsyncLoaderModel()
        loader!.getDataFromURLStringWithOutSecurity(webURL:"http://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&sensor=false", dataIndex: pinIndex)
        
        print("http://maps.googleapis.com/maps/api/geocode/json?latlng=\(latitude),\(longitude)&sensor=false")
        
        loader!.delegate = self
    }
    
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("reverseGeo responseString is \(responseString!)")
//
        processData(data: data)
        
        self.loader = nil
    }
    
    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int){
        if delegate != nil {
            
            delegate!.didReceivedReverseGeoName(nil, pinIndex:dataIndex)
        }
    }
    
    func processData(data data : NSData)
    {
        var result : NSDictionary?
        
        do
        {
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary
            
            let results = result!.objectForKey("results") as? [NSDictionary]
            
            let resultsDictionary = results![0]
            
            let reverseGeoModel = ReverseGeoCoderModel()
        
            reverseGeoModel.latitude = latitude
            reverseGeoModel.longitude = longitude
            
            reverseGeoModel.formatted_address =  resultsDictionary.objectForKey("formatted_address") as? String != nil ? resultsDictionary.objectForKey("formatted_address") as? String : "na"
            
            let address_components = resultsDictionary.objectForKey("address_components") as? [NSDictionary]
            
            for i in 0..<address_components!.count{
                
                let address_component = address_components![i]
                
                let types : [String]?  = address_component.objectForKey("types") as? [String] != nil ? address_component.objectForKey("types") as? [String] : nil
                
                if types != nil {
                    
                    var index = 0
                    for k in 0..<types!.count{
                        
                        if types![k].lowercaseString == "street_number"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "route"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "neighborhood" || types![k].lowercaseString == "sublocality_level_2"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "sublocality_level_1"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "locality"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "administrative_area_level_2"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "administrative_area_level_1"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "country"{
                            index = k
                            break
                        }
                        else if types![k].lowercaseString == "postal_code"{
                            index = k
                            break
                        }
                    }
                    
                    let type = types![index]
                    
                    if type.lowercaseString == "route"
                    {
                        reverseGeoModel.route =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type.lowercaseString == "sublocality_level_2" || type.lowercaseString == "neighborhood"
                    {
                        reverseGeoModel.sublocality =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type == "sublocality_level_1"
                    {
                        reverseGeoModel.locality =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type == "administrative_area_level_2"
                    {
                        reverseGeoModel.administrative_area_level_2_city =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type == "administrative_area_level_1"
                    {
                        reverseGeoModel.administrative_area_level_1_state =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type == "country"
                    {
                        reverseGeoModel.country =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type == "postal_code"
                    {
                        reverseGeoModel.postal_code =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    else if type == "street_number"
                    {
                        reverseGeoModel.street_number =  address_component.objectForKey("long_name") as? String != nil ? address_component.objectForKey("long_name") as? String : "na"
                    }
                    
                }
                
            }
            
            print("SUCCESS")
            
            if delegate != nil {
                
                delegate!.didReceivedReverseGeoName(reverseGeoModel,pinIndex: pinIndex!)
            }
            
            
        }
        catch
        {
            
            // Error Occured
            if delegate != nil {
                
                delegate!.didReceivedReverseGeoName(nil,pinIndex: pinIndex!)
            }
            
        }
    }
}
