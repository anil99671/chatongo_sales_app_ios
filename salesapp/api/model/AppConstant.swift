//
//  AppConstant.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 25/06/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

class AppConstant: NSObject {
   
    
    struct Static {
    
        static var BASE_URL = "http://test.chatongo.com/api/"
//        static var BASE_URL = "http://chatongo.com/api/"
        static var BASE_CHAT_URL = "http://52.27.87.26:3002" // test
//        static var BASE_CHAT_URL = "http://180.149.247.131:3002" //live

        static var NIMAP_ALERT_BASE_VIEW_WIDTH  : CGFloat =   280.0
        static var NIMAP_ALERT_BASE_VIEW_HEIGHT : CGFloat =   130.0
        static var NIMAP_ALERT_BASE_VIEW_EXTRA_HEIGHT : CGFloat =   180.0
        static var NIMAP_ALERT_BASE_VIEW_HUGE_HEIGHT : CGFloat =   220.0
        static var NIMAP_ALERT_LOGO_Y_PADDING   : CGFloat =   4.0
        static var NIMAP_ALERT_LOGO_Y_PADDING_FLOAT : Float = 4.0
        static var NIMAP_ALERT_LOGO_Y_PADDING_CGFLOAT : CGFloat = 4.0
        static var NIMAP_ALERT_TEXTFIELD_HEIGHT : Float = 40.0
        
        static var BUTTON_HEIGHT : Float = 40.0
        static var BUTTON_HEIGHT_CGFLOAT : CGFloat = 40.0

        static var SEARCH_BAR_HEIGHT_CGFLOAT : CGFloat = 35.0
        
        static var CONNECTION_ERROR = 1
        static var PROCESSING_ERROR = 2
        static var DATA_NOT_FOUND_ERROR = 404
        static var UNAUTHORISED_USER = 401
        static var UNREGISTERED_USER = 204
        
        static var SELECT_AREA_MESSAGE = "Select area to proceed!"
        static var AUTHENTICATION_FAIL_MESSAGE = "Unauthorized user"
        
        static var GET_USER_TYPE_SUCCESS = 3
        static var GET_USER_TYPE_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_USER_TYPE_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var TOTAL_START_ANIMATION_FRAMES : Int = 4
        static var FRAME_RATE = 1.0

        static var ADD_NOTES_SUCCESS = 3
        static var ADD_NOTE_INVALID_ERROR = 5
        static var ADD_NOTES_SUCCESS_MESSEGE = "Note added successfully."
        static var ADD_NOTE_INVALID_ERROR_MESSEGE = "Invalid error."
        static var NO_NOTES_DATA_AVAILABLE_MESSEGE = "No Notes Found."

        static var GET_SHOP_SUCCESS = 3
        static var NO_SHOP_AVAILABLE = 5
        static var SHOP_ALREADY_MAPPED = 6
        static var NO_SHOP_AVAILABLE_MESSEGE = "No Shops Found."
        static var SHOP_ALREADY_MAPPED_MESSEGE = "Shop already mapped."
        
        static var REGISTRATION_PAGE_TITLE_HEIGHT : Float = 40.0
//        static var REGISTRATION_PAGE_TITLE_TOP_MARGIN : Float = 40.0
        static var REGISTRATION_PAGE_TITLE_TOP_MARGIN : Float = 70.0
        
        static var REGISTRATION_FORM_COUNTRY_CODE_WIDTH : Float = 40.0
        static var REGISTRATION_FORM_FIELD_HEIGHT : Float = 30.0
        static var REGISTRATION_FORM_TITLE_PADDING_Y : Float = 20.0
        static var REGISTRATION_FORM_PADDING_Y : Float = 4.0
        static var REGISTRATION_FORM_PADDING_X : Float = 30.0
        
        static var REGISTRATION_BUTTON_PADDING_X : CGFloat = 30.0
        static var REGISTRATION_BUTTON_HEIGHT : CGFloat = 30.0
        
        static var REGISTRATION_BOTTOM_PATCH_PERCENTAGE : Float = 0.25
        static var REGISTRATION_BOTTOM_PATCH_TITLE_PADDING_Y : Float = 16.0
        static var REGISTRATION_BOTTOM_PATCH_PADDING_X : Float = 30.0
        static var REGISTRATION_BOTTOM_PATCH_BUTTON_PADDING_Y : Float = 10.0
        static var REGISTRATION_BOTTOM_PATCH_BUTTON_HEIGHT : Float = 30.0
        
        static var REGISTRATION_FORM_NAME_VALIDATION_ERROR_MESSAGE = "Name can not be blank!"
        static var REGISTRATION_FORM_LAST_NAME_VALIDATION_ERROR_MESSAGE = "Business name can not be blank!"
        static var REGISTRATION_FORM_MOBILE_VALIDATION_ERROR_MESSAGE = "Mobile Number can not be blank!"
        static var REGISTRATION_FORM_MOBILE_NO_INVALID_ERROR_MESSAGE = "Mobile Number invalid."
        static var REGISTRATION_FORM_CC_VALIDATION_ERROR_MESSAGE = "Country Code is not selected. Tap on + 91 to select your country code."
        static var REGISTRATION_FORM_EMAIL_VALIDATION_ERROR_MESSAGE = "Invalid email id"
        static var REGISTRATION_FORM_EMAIL_VALIDATION_NOT_PRESENT_ERROR_MESSAGE = "Email id can not be blank!"
        static var REGISTRATION_FORM_USER_TYPE_VALIDATION_ERROR_MESSAGE = "Please select one user type"
        static var REGISTRATION_USER_MESSAGE = "Please Wait... User Registering"
        
        static var GET_COUNTRY_CODE_SUCCESS = 3
        static var GET_COUNTRY_CODE_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_COUNTRY_CODE_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var ADD_USER_SUCCESS = 3
        static var ADD_USER_EMAIL_INVALID = 4
        static var ADD_USER_MOBILE_INVALID = 5

        static var VISITED_SHOP_SUCCESS = 3
        
        static var ADD_USER_CONNECTION_ERROR_MESSAGE = "Connection error"
        static var ADD_USER_PROCESSING_ERROR_MESSAGE = "Processing error"
        static var ADD_USER_EMAIL_INVALID_ERROR_MESSAGE = "Invalid email id"
        static var ADD_USER_MOBILE_INVALID_ERROR_MESSAGE = "Mobile number invalid"

        static var SALES_REGISTRATION_FAIL_ERROR = "Please, Register with the admin to use the app."


        static var LOCALITY_CONNECTION_ERROR_MESSAGE = "Connection error"
        static var LOCALITY_PROCESSING_ERROR_MESSAGE = "Processing error"
        
        static var OTP_TIME_LIMIT_ERROR_MESSAGE = "Your OTP has expired."
        static var OTP_INVALID_ERROR_MESSAGE = "Invalid OTP. Please try again."
        
        
        static var SIGN_UP_PAGE_TITLE_HEIGHT : Float = 30.0
        static var SIGN_UP_PAGE_TITLE_TOP_MARGIN : Float = 60.0
        
        static var SIGN_UP_FORM_COUNTRY_CODE_WIDTH : Float = 40.0
        static var SIGN_UP_FORM_FIELD_HEIGHT : Float = 30.0
        static var SIGN_UP_FORM_TITLE_PADDING_Y : Float = 20.0
        static var SIGN_UP_FORM_PADDING_Y : Float = 4.0
        static var SIGN_UP_FORM_PADDING_X : Float = 30.0
        
        static var SIGN_UP_BUTTON_PADDING_X : CGFloat = 30.0
        static var SIGN_UP_BUTTON_HEIGHT : CGFloat = 30.0
        
        static var LOGIN_USER_SUCCESS = 3
        static var LOGIN_USER_NOT_EXISTS = 4
        
        static var LOGIN_USER_MESSAGE = "Please Wait... User Signing"
        static var LOGIN_USER_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var LOGIN_USER_PROCESSING_ERROR_MESSAGE = "Processing Error"
        static var LOGIN_USER_NOT_EXISTS_ERROR_MESSAGE = "User not found."
        
        
       
        
        static var CATEGORY_CELL_CATEGORY_SCROLL_HEIGHT : CGFloat = 140.0
        static var CATEGORY_CELL_TITLE_HEIGHT : CGFloat = 30.0
        static var CATEGORY_CELL_HEIGHT : CGFloat = 200.0
        static var AREA_CHARACTER_LENGHT = 7
        
        static var GET_CATEGORIES_MESSAGE = "Please Wait... Getting categories"
        
        static var GET_CATEGORIES_CODE_SUCCESS = 3
        static var GET_CATEGORIES_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_CATEGORIES_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var SUB_CATEGORY_CELL_CATEGORY_SCROLL_HEIGHT : CGFloat = 180.0
        static var SUB_CATEGORY_CELL_TITLE_HEIGHT : CGFloat = 32.0
        
        static var SUB_CATEGORY_CELL_HEIGHT : CGFloat = 240.0
        static var SUB_CATEGORY_MORE_WIDTH : CGFloat = 60.0
        
        static var SUB_CATEGORY_CELL_IMAGE_PADDING : CGFloat = 4.0
        static var SUB_CATEGORY_CELL_PADDING : CGFloat = 8.0
        
        static var GET_SUB_CATEGORIES_MESSAGE = "Please Wait... Getting sub categories"
        static var GET_SUB_CATEGORIES_CODE_SUCCESS = 3
        static var GET_SUB_CATEGORIES_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_SUB_CATEGORIES_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        
        static var GET_PRODUCTS_PAGINATION_SIZE = 10
        static var GET_PRODUCTS_MESSAGE = "Please Wait... Getting products"
        static var GET_PRODUCTS_CODE_SUCCESS = 3
        static var GET_PRODUCTS_CODE_ALL_PRODUCTS_DOWNLOADED = 4
        static var GET_PRODUCTS_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_PRODUCTS_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var PRODUCT_CELL_HEIGHT : CGFloat = 220.0
        static var PRODUCT_CELL_WIDTH : CGFloat = 146.0
        
        static var GET_PRODUCT_DETAIL_MESSAGE = "Please Wait... Getting product"
        static var GET_PRODUCT_DETAIL_CODE_SUCCESS = 3
        static var GET_PRODUCT_DETAIL_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_PRODUCT_DETAIL_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var GET_PRODUCT_DETAIL_BOTTOM_PATCH_PERCENTAGE : Float = 0.25
        static var PRODUCT_RATING_IMAGE_HEIGHT : Float = 30.0
        
        static var NO_PRODUCT_IN_CART_MESSAGE = "The cart is currently empty. Please add products to cart."
        
        static var MY_CART_FIELD_HEIGHT : Float = 30.0
        static var MY_CART_FIELD_PADDING_X : Float = 10.0
        static var MY_CART_FIELD_PADDING_Y : Float = 4.0
        
        static var MY_CART_CELL_PADDING_X : CGFloat = 10.0
        static var MY_CART_CELL_IMAGE_DIM : CGFloat = 80.0
        static var MY_CART_CELL_HEIGHT : CGFloat = 120.0
        static var MY_CART_WEIGHT_WIDTH : CGFloat = 100.0
        static var MY_CART_WEIGHT_HEIGHT : CGFloat = 30.0
        static var MY_CART_ELEMENT_HEIGHT : CGFloat = 40.0

        static var CHECK_OUT_MESSAGE = "Please Wait... Placing Order"
        static var CHECK_OUT_SUCCESS = 3
        static var CHECK_OUT_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var CHECK_OUT_PROCESSING_ERROR_MESSAGE = "Processing Error"
        static var CHECK_OUT_SUCCESS_MESSAGE = "Your order has been placed successfully. Please track your order in the My Orders section."


        static var FEEDBACK_MAIL_SUBJECT = "Feedback : ChatOnGo"
        static var FEEDBACK_TO = "feedback@chatongo.com"
        static var FEEDBACK_MESSAGE = "Thank you for your valuable feedback to help us continue to serve you better."
        static var SUPPORT_MAIL_SUBJECT = "Support : Zar Jewels"
        static var SUPPORT_TO = "support@zarjewels.com"
        static var SUPPORT_MESSAGE = "Your query has been received. We will  address it shortly."
        
        static var EMAIL_FAILED = "Email sending failed."
        static var EMAIL_SAVED = "Email saved in drafts."
        static var EMAIL_CANCELED = "Email Cancelled."
        
        static var ADD_REVIEW_SUCCESS = 3
        static var NO_RATING_SELECTED_ERROR_MESSAGE = "Please select the stars to rate the vendor."
        static var PRODUCT_RATING_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var PRODUCT_RATING_PROCESSING_ERROR_MESSAGE = "Processing Error"
        static var PRODUCT_RATING_SUCCESS_MESSAGE = "Thank you for your valuable feedback."
        static var PRODUCT_RATING_MESSAGE = "Please Wait... Adding Rating and Reviews"
        
        static var GET_PRODUCT_REVIEWS_SUCCESS = 3
        static var GET_PRODUCT_REVIEWS_MESSAGE = "Please Wait... Getting Rating and Reviews"
        static var GET_PRODUCT_REVIEWS_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_PRODUCT_REVIEWS_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var REVIEW_CELL_USERNAME_HEIGHT : CGFloat = 30.0
        static var REVIEW_CELL_RATING_IMAGE_HEIGHT : CGFloat = 30.0
        static var REVIEW_CELL_PADDING_Y : CGFloat = 0.0
        // shrikant prev 8.0
        
        static var REVIEW_CELL_READ_MORE_CHARACTER_LIMIT  = 110
        
        static var NO_DATA_AVAILABLE = -1
        static var NO_DATA_MESSAGE = "No data available"
        
        static var NAVIGATION_BAR_HEIGHT : CGFloat = 48.0
        static var NAVIGATION_BAR_BUTTON_WIDTH : CGFloat = 30.0
        static var NAVIGATION_BAR_PADDING_X : Float = 8.0
        
        static var GET_MYORDERS_MESSAGE = "Please Wait... Getting Orders"
        static var GET_MYORDERS_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_MYORDERS_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var PADDING : CGFloat = 8.0
        
        static var GET_MYORDERS_DETAILS_MESSAGE = "Please Wait... Getting Order Details"
        static var GET_MYORDERS_DETAILS_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var GET_MYORDERS_DETAILS_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var SEARCH_PRODUCT_MESSAGE = "Please Wait... Searching product"
        static var SEARCH_PRODUCT_CODE_SUCCESS = 3
        static var SEARCH_PRODUCT_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var SEARCH_PRODUCT_PROCESSING_ERROR_MESSAGE = "Processing Error"
        
        static var SEARCH_TEXT_VALIDATION_ERROR_MESSAGE = "Search bar can not be blank!"
        
        static var FILTER_MASTER_PERCENTAGE_WIDTH : CGFloat = 0.40
        
        static var NO_PRODUCT_FILTER_RESULT = "No products found on the filter. Please apply another filters.";
        
        static var NO_SUBCATEGORIES = "No subcategories available!"
        
        
        static var UPDATE_PROFILE_SUCCESS = 3
        static var UPDATE_PROFILE_PROCESSING_ERROR_MESSAGE = "Processing Error"
        static var UPDATE_PROFILE_CONNECTION_ERROR_MESSAGE = "Connection Error"
        static var UPDATE_PROFILE_CC_VALIDATION_ERROR_MESSAGE = "Country Code is not selected. Tap on + 91 to select your country code."
        static var UPDATE_PROFILE_MESSAGE = "Please wait.. Updating profile"
        static var UPDATE_PROFILE_SUCCESS_MESSAGE = "Your profile has been updated."
        
        static var DEVICE_TOKEN_SUCCESS = 3
        
        //MARK: FontAwesome 

        static var LOCATION_ICON = "\u{f041}"
        static var CANCEL_ICON = "\u{f00d}"
        static var BACK_ICON = "\u{f104}"
        static var PERSONAL_ICON = "\u{f007}"
        static var ADD_ICON = "\u{f067}"
        static var ATTACHMENT_ICON = "\u{f0c6}"
        static var DONE_CHECKED_ICON = "\u{f00c}"
        static var FILTER_ICON = "\u{f0b0}"
        static var CONTACT_MORE_ICON = "\u{f10b}"
        static var DELETE_ICON = "\u{f014}"
        static let EDIT_ICON = "\u{f040}"
        static let TIMER_ICON = "\u{f017}"
        static let BROCHURE_MORE = "\u{f02d}"
        static var NOTES_ICON = "\u{f040}"
        static var BOOK_ICON = "\u{f02d}"
        static var UNCHECKED_CHECKBOX_ICON = "\u{f096}"
        static var CHECKED_CHECKBOX_ICON = "\u{f046}"

    }
    
}
