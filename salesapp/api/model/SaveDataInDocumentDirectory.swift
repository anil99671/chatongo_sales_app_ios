//
//  SaveDataInDocumentDirectory.swift
//  Chat
//
//  Created by Priyank Ranka on 17/05/15.
//
//

import UIKit

class SaveDataInDocumentDirectory: NSObject {
    
    let SUB_DIRECTORY_EXIST                 =   0
    let SUB_DIRECTORY_CREATED               =   1
    let SUB_DIRECTORY_CAN_NOT_BE_CREATED    =   2
    
    let FILE_EXIST                          =   0
    let FILE_DOES_NOT_EXIST                 =   1
    
    
    struct Static {
        static var instance : SaveDataInDocumentDirectory? = nil
        static var onceToken : dispatch_once_t = 0
    }
    class func sharedInstance() -> SaveDataInDocumentDirectory!
    {
        dispatch_once(&Static.onceToken)
            {
                Static.instance = SaveDataInDocumentDirectory()
        }
        return Static.instance
    }
    
    override init()
    {
        assert(Static.instance == nil, "Attempted to create second instance of SaveDataInDocumentDirectory")// assert are conditional logs
        
        super.init()
    }
   
    func writeData(data data : NSData!,withFileName fileName: String!) -> Bool{
        
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + fileName
        
        let fileManager =   NSFileManager.defaultManager()
        
        let fileHandler = NSFileHandle(forWritingAtPath: strFilePath)
        
        if fileManager.createFileAtPath(strFilePath, contents: nil, attributes: nil){
            fileHandler!.writeData(data)
            return true
        }
        else
        {
            return false
        }
    }
    
    func readData(fileName fileName : String) -> NSData!
    {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + fileName
        
        let fileManager =   NSFileManager.defaultManager()
        
        if fileManager.fileExistsAtPath(strFilePath)
        {
            return fileManager.contentsAtPath(strFilePath)
        }
        else
        {
            return nil
        }
    }
    
    func isFileExist(fileName fileName : String!) -> Bool
    {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + fileName
        
        let fileManager =   NSFileManager.defaultManager()
        
        if fileManager.fileExistsAtPath(strFilePath)
        {
            //println("strFilePath \(strFilePath)")
            return true
        }
        else
        {
            return false
        }
    }
    
    func filePathInDocumentDirectoryFor(fileName fileName : String) -> String!
    {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + fileName
        
        let fileManager =   NSFileManager.defaultManager()
        
        if fileManager.fileExistsAtPath(strFilePath)
        {
            return strFilePath
        }
        else
        {
            return nil
        }
    }
    
    func updateData(data data : NSData ,withFileName fileName: String!) -> Bool{
        
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + fileName

        //println("strFilePath \(strFilePath)")
        
        
        let fileManager = NSFileManager.defaultManager()
        
        if fileManager.fileExistsAtPath(strFilePath) == false
        {
            fileManager.createFileAtPath(strFilePath, contents: nil, attributes: nil)
        }
        
        let fileHandler = NSFileHandle(forWritingAtPath: strFilePath)
        
        fileHandler!.seekToEndOfFile()
        fileHandler!.writeData(data)
        fileHandler!.synchronizeFile()
        fileHandler!.closeFile()
        
        return true
        
    }
    
    func removeFile(name name: String) -> Bool
    {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + name
        
        let fileManager =   NSFileManager.defaultManager()
        
        if isFileExist(fileName: name)
        {
           do{
                
                try fileManager.removeItemAtPath(strFilePath)
                return true
            }
            catch{
               return false
            }
            
            
        }
        
        return false
        
    }
    
    func lenghtOfFile(fileName fileName : String) -> Int64
    {
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath = paths[0] as String
        let strFilePath  =   documentDirectoryPath + "/" + fileName
        
        let fileManager =   NSFileManager.defaultManager()
        
        var attrs : NSDictionary!
        
        var result : Int64?
        
        do{
            
            try attrs = fileManager.attributesOfFileSystemForPath(strFilePath) as NSDictionary!
        }
        catch
        {
            
        }
        
        result = attrs.objectForKey(NSFileSize)!.longLongValue
        
        return result!
    }
    
    func removeOldFile(fileName fileName : String, modifiedDate : String)
    {
        if isFileExist(fileName: fileName)
        {
            let offlineFileDB = AsynFileManagerDB.sharedInstance()
            
            let fileLocalDBModifiedDate = offlineFileDB!.selectModifiedDateFromOfflineFileWhere(fileName: fileName);
            
            var seconds = -1.0;
            
            if fileLocalDBModifiedDate != nil
            {
                if fileLocalDBModifiedDate != "na" {
                    
                    let curFormater = NSDateFormatter()
                    curFormater.dateFormat = "MM-dd-yyyy HH:mm:ss"
                    
                    let storeModifiedDate = curFormater.dateFromString(fileLocalDBModifiedDate!);
                    let currentModifiedDate = curFormater.dateFromString(modifiedDate);
                    
                    seconds = currentModifiedDate!.timeIntervalSinceDate(storeModifiedDate!)

                    print("seconds \(seconds)")

                    if fileLocalDBModifiedDate!.isEmpty || modifiedDate == "na" || seconds != 0
                    {
                        removeFile(name: fileName)
                    }
                }
            }
        }
    }
    
    func getImageBase64String(imageFileName fileName : String) -> String{
        
        let data = readData(fileName: fileName)
        
        return data.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
    }

}
