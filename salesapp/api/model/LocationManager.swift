//
//  LocationManager.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 31/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

@objc protocol LocationManagerDelegate : NSObjectProtocol
{
    optional func didReceiveLocation(currentLatitude latitude : Double, currentLongitude longitude : Double)
    optional func didReceiveLocation(currentLocation location : LocationManagerModel?)
}

class LocationManager: NSObject, CLLocationManagerDelegate, ReverseGeoCoderParserDelegate {
    
    var locationManager : CLLocationManager?
    var isForceEnable : Bool?
    var isReverserGeoLocationEnable : Bool?
    var isLocationContinous : Bool?
    
    var reverseGeoParser : ReverseGeoCoderParser?
    
    weak var delegate : LocationManagerDelegate?
    
    init(isForceEnable : Bool, isReverserGeoLocationEnable : Bool, isLocationContinous: Bool) {
        
        super.init()
        
        self.isForceEnable = isForceEnable
        self.isReverserGeoLocationEnable = isReverserGeoLocationEnable
        self.isLocationContinous = isLocationContinous;
    }
    
    //MARK: Helper Methods
    
    func startLocationService()
    {
        if locationManager == nil
        {
            locationManager = CLLocationManager()
            
            print("startLocationService")
            if CLLocationManager.locationServicesEnabled()
            {
                locationManager!.delegate = self;
                if isForceEnable == true
                {
                    requestAlwaysAuthorization()
                }
                
                print("startLocationService 1")
                
                locationManager!.desiredAccuracy = kCLLocationAccuracyBest
                locationManager!.distanceFilter = kCLDistanceFilterNone;
                locationManager!.requestWhenInUseAuthorization()
                locationManager!.startUpdatingLocation()
            }
            else
            {
                if isForceEnable == true
                {
                    requestAlwaysAuthorization()
                }
            }
        }
    }
    
    func stopLocationService()
    {
        if locationManager != nil
        {
            locationManager!.stopUpdatingHeading()
            locationManager = nil
        }
    }
    
    //MARK: Local Methods
    
    func requestAlwaysAuthorization()
    {
        let status  = CLLocationManager.authorizationStatus()
        
        // If the status is denied or only granted for when in use, display an alert
        print("status \(status)")
        
        if status == CLAuthorizationStatus.AuthorizedWhenInUse || status == CLAuthorizationStatus.Denied{
            
            var title = ""
            
            title = (status == CLAuthorizationStatus.Denied) ? "Location Service if Off. We need Location service to be enable for this app." : ""
            
            if title.isEmpty == false{
                let alert = UIAlertView(title: "Location", message: title, delegate: self, cancelButtonTitle: "OK")
                alert.show()
            }
            
            
        }
    }
    
    //MARK: UIAlertViewDelegate Methodss
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if (buttonIndex == 0) {
            // Send the user to the Settings for this app
            let settingsURL =  NSURL(string: UIApplicationOpenSettingsURLString)
            UIApplication.sharedApplication().openURL(settingsURL!)
        }
    }
    
    // MARK: CLLocationManagerDelegate Method
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationValue : CLLocationCoordinate2D = (manager.location?.coordinate)!
        
        print("latitude \(locationValue.latitude) longitude \(locationValue.longitude)")
        
        //        let locationDetails = LocationManagerModel()
        //        locationDetails.latitude = locationValue.latitude
        //        locationDetails.longitude = locationValue.longitude
        
        if isReverserGeoLocationEnable == true
        {
            
            if reverseGeoParser == nil {
                
                reverseGeoParser =  ReverseGeoCoderParser()
                reverseGeoParser!.delegate = self
                reverseGeoParser!.getReverseGeoData(locationValue.latitude, longitude: locationValue.longitude, pinIndex: -1)
            }
            
            //            let location = CLLocation(latitude: locationValue.latitude, longitude: locationValue.longitude)
            //
            //            CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            //
            //                if error != nil
            //                {
            //                    print("Reverse geocoder failed with error" + error!.localizedDescription)
            //                    return
            //                }
            //
            //                if placemarks!.count > 0
            //                {
            //                    let pm = placemarks![0]
            //
            //                    // Location name
            //
            //                    locationDetails.name = (pm.addressDictionary!["SubLocality"] as? String != nil) ? pm.addressDictionary!["SubLocality"] as? String : "na"
            //                    locationDetails.thoroughfare = (pm.addressDictionary!["thoroughfare"] as? String != nil) ? pm.addressDictionary!["thoroughfare"] as? String : "na"
            //                    locationDetails.subThoroughfare = (pm.addressDictionary!["subThoroughfare"] as? String != nil) ? pm.addressDictionary!["subThoroughfare"] as? String : "na"
            //                    locationDetails.locality = (pm.addressDictionary!["locality"] as? String != nil) ? pm.addressDictionary!["locality"] as? String : "na"
            //                    locationDetails.subLocality = (pm.addressDictionary!["subLocality"] as? String != nil) ? pm.addressDictionary!["subLocality"] as? String : "na"
            //                    locationDetails.administrativeArea = (pm.addressDictionary!["administrativeArea"] as? String != nil) ? pm.addressDictionary!["administrativeArea"] as? String : "na"
            //                    locationDetails.subAdministrativeArea = (pm.addressDictionary!["subAdministrativeArea"] as? String != nil) ? pm.addressDictionary!["subAdministrativeArea"] as? String : "na"
            //                    locationDetails.postalCode = (pm.addressDictionary!["postalCode"] as? String != nil) ? pm.addressDictionary!["postalCode"] as? String : "na"
            //                    locationDetails.ISOcountryCode = (pm.addressDictionary!["ISOcountryCode"] as? String != nil) ? pm.addressDictionary!["ISOcountryCode"] as? String : "na"
            //                    locationDetails.ISOcountryCode = (pm.addressDictionary!["ISOcountryCode"] as? String != nil) ? pm.addressDictionary!["ISOcountryCode"] as? String : "na"
            //                    locationDetails.inlandWater = (pm.addressDictionary!["inlandWater"] as? String != nil) ? pm.addressDictionary!["inlandWater"] as? String : "na"
            //                    locationDetails.ocean = (pm.addressDictionary!["ocean"] as? String != nil) ? pm.addressDictionary!["ocean"] as? String : "na"
            //                    locationDetails.areasOfInterest = (pm.addressDictionary!["ocean"] as? String != nil) ? pm.addressDictionary!["areasOfInterest"] as? [String] : []
            //                }
            //                else
            //                {
            //                    print("Problem with the data received from geocoder")
            //                }
            //            })
        }
        else{
            
            if delegate != nil
            {
                if delegate!.respondsToSelector(#selector(LocationManagerDelegate.didReceiveLocation(currentLatitude:currentLongitude:))){
                    
                    delegate!.didReceiveLocation!(currentLatitude: locationValue.latitude, currentLongitude: locationValue.longitude)
                }
            }
        }
        
        if isLocationContinous == false
        {
            stopLocationService()
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError){
        
        print("\(error.description)")
        if isForceEnable == true
        {
            requestAlwaysAuthorization()
        }
    }
    
    //MARK: ReverseGeoCoderParserDelegate methods
    
    func didReceivedReverseGeoName(reverseName : ReverseGeoCoderModel?, pinIndex : Int){
        
        reverseGeoParser = nil
        
        var locationDetails : LocationManagerModel?
        
        if reverseName == nil {
            locationDetails = nil
        }
        else{
            
            locationDetails = LocationManagerModel()
            locationDetails!.latitude = reverseName!.latitude
            locationDetails!.longitude = reverseName!.longitude
            
            locationDetails!.name = reverseName!.route
            locationDetails!.country = reverseName!.country
            locationDetails!.postalCode = reverseName!.postal_code
            locationDetails!.subLocality = reverseName!.sublocality
            locationDetails!.locality = reverseName!.locality
            locationDetails!.administrativeArea = reverseName!.administrative_area_level_1_state
            
            if locationDetails!.subLocality!.lowercaseString == "na" && locationDetails!.locality!.lowercaseString != "na"{
                
                locationDetails!.subLocality = locationDetails!.locality
            }
            else if locationDetails!.subLocality!.lowercaseString == "na" && locationDetails!.name!.lowercaseString != "na"{
                
                locationDetails!.subLocality = locationDetails!.name
            }
            else if locationDetails!.subLocality!.lowercaseString == "na" && locationDetails!.subAdministrativeArea!.lowercaseString != "na"{
                
                locationDetails!.subLocality = locationDetails!.subAdministrativeArea
            }
            
            
            
            locationDetails!.subAdministrativeArea = reverseName!.administrative_area_level_2_city
            locationDetails!.formattedAddess = reverseName!.formatted_address
            locationDetails!.streetName = reverseName!.street_number
            
            print("latitude \(locationDetails!.latitude!)")
            print("longitude \(locationDetails!.longitude!)")
            print("name \(locationDetails!.name!)")
            print("country \(locationDetails!.country!)")
            print("postalCode \(locationDetails!.postalCode!)")
            print("subLocality \(locationDetails!.subLocality!)")
            print("locality \(locationDetails!.locality!)")
            print("administrativeArea \(locationDetails!.administrativeArea!)")
            print("subAdministrativeArea \(locationDetails!.subAdministrativeArea!)")
            print("formattedAddess \(locationDetails!.formattedAddess!)")
        }
        
        if delegate != nil
        {
            if delegate!.respondsToSelector(#selector(LocationManagerDelegate.didReceiveLocation(currentLocation:))){
                
                delegate!.didReceiveLocation!(currentLocation: locationDetails)
            }
        }
    }
    
}
