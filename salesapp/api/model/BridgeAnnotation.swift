//
//  BridgeAnnotation.swift
//  Project2
//
//  Created by Priyank Ranka on 19/01/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit
import MapKit

class BridgeAnnotation: NSObject,MKAnnotation {
   
    var latitude: Double?
    var longitude: Double?
    
    var strTitle : String?
    var strSubTitle: String?
    
    var annotationIcon: UIImage?
    
    var annotationIndex: Int?
    
    var innerCoordinate: CLLocationCoordinate2D?
    
    /*
    
    - (CLLocationCoordinate2D) coordinate{
        return self.innerCoordinate
    }
    
    -(void) setCoordinate:(CLLocationCoordinate2D) newValue{
        self.innerCoordinate = newValue
    }
    
    //JAVA
    CLLocationCoordinate2D getCoordinate(){
        return self.innerCoordinate
    }
    
    void setCoordinate(CLLocationCoordinate2D newValue){
        self.innerCoordinate = newValue
    }
    
*/
    
    var coordinate: CLLocationCoordinate2D {
        get {
            return self.innerCoordinate!
        }
        set {
            self.innerCoordinate = newValue
        }
    }
    
    var title: String? {
        get{
            return self.strTitle
        }
    }
    
    var subtitle: String? {
        get{
            return self.strSubTitle
        }
    }
    
    init(latitude: Double, longitude:Double){
        
        super.init()
        
        self.innerCoordinate = CLLocationCoordinate2DMake(latitude, longitude)
        
        self.latitude   =   latitude
        self.longitude  =   longitude
    }
}
