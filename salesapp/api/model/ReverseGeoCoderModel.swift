//
//  ReverseGeoCoderModel.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 05/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

class ReverseGeoCoderModel: NSObject {

    var street_number : String? = "na"
    var route : String? = "na"
    var locality : String? = "na"
    var sublocality : String? = "na"
    var administrative_area_level_2_city : String? = "na"
    var administrative_area_level_1_state : String? = "na"
    var country : String? = "na"
    var postal_code : String? = "na"
    var formatted_address : String? = "na"
    
    var latitude : Double?
    var longitude : Double?
}
