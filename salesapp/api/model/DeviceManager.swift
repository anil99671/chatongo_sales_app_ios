//
//  DeviceManager.swift
//  APIsProject
//
//  Created by Priyank Ranka on 20/02/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import UIKit

/*
In swift you can declare an enum, variable or function outside of any class or function and it will be available in all your classes (globally)(without the need to import a specific file).
*/


@objc class DeviceManager: NSObject {
   
    var deviceOrientation = -1;
    var deviceType: Int = -1;
    
    let iPhone      =   0
    let iPad        =   1
    let iPhone5     =   2
    let iPhone6     =   3
    let iPhone6plus =   4
    
    let LANDSCAPE   =   1
    let POTRAIT     =   2
    
    let screenHeight    =   UIScreen.mainScreen().bounds.size.height
    let screenWidth     =   UIScreen.mainScreen().bounds.size.width
    
    struct Static {
        static var onceToken : dispatch_once_t = 0
        static var instance : DeviceManager! = nil
    }
    
    class func sharedDeviceManagement() -> DeviceManager!{
        
        dispatch_once(&Static.onceToken) {
            Static.instance = DeviceManager()
        }
        return Static.instance!
    }
    
    private override init()
    {
        assert(Static.instance == nil, "Attempted to create second instance of DeviceManager")
        
        super.init()
        
        deviceType = getDeviceType()
        
        switch (UIApplication.sharedApplication().statusBarOrientation)
        {
            case .Portrait:
                deviceOrientation   =   POTRAIT
            case .PortraitUpsideDown:
                deviceOrientation   =   POTRAIT
            case .LandscapeLeft:
                deviceOrientation   =   LANDSCAPE
            case .LandscapeRight:
                deviceOrientation   =   LANDSCAPE
            default:
                deviceOrientation   =   POTRAIT
        }
        
        
    }
    
    func getDeviceType() -> Int {
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad{
            return iPad
        }
        else
        {
            if screenWidth == 375.0 && screenHeight == 667.0 {
                return iPhone6;
            }
            else if screenWidth == 414.0 && screenHeight == 736.0 {
                return iPhone6plus;
            }
            else if screenHeight == 568.0
            {
                return iPhone5;
            }
            else
            {
                return iPhone;
            }
        }
    }

    func resourceNameAsPerDevice(fileName  fileName : String!) -> String!
    {
        var actualFileName : String! = nil
        
        switch (deviceType)
        {
        case iPhone:
            return fileName;
        case iPad:
            actualFileName = fileName + "_ipad"
            return actualFileName;
        case iPhone5:
            actualFileName = fileName + "_5"
            return actualFileName;
        case iPhone6:
            actualFileName = fileName + "_6"
            return actualFileName;
        case iPhone6plus:
            actualFileName = fileName + "_6plus"
            return actualFileName;
        default:
            return fileName;
        }
    }
    
    func deviceXvalue(xPos  xPos : Float) -> Float{
        
        var actualXPos : Float = 0.0
        
        
        
        switch (deviceType)
        {
        case iPhone:
            return xPos;
        case iPad:
            if(deviceOrientation == POTRAIT)
            {
                actualXPos = (768.0/320.0) * xPos;
                return actualXPos;
            }
            else
            {
                actualXPos = (1024.0/480.0) * xPos;
                return actualXPos;
            }
            
        case iPhone5:
            if(deviceOrientation == POTRAIT)
            {
                return xPos;
            }
            else
            {
                actualXPos  =   (568.0/480.0) * xPos;
                return actualXPos;
            }
        case iPhone6:
            if(deviceOrientation == POTRAIT)
            {
                actualXPos  =   (375.0/320.0) * xPos;
                return actualXPos;
            }
            else
            {
                actualXPos  =   (667.0/480.0) * xPos;
                return actualXPos;
            }
        case iPhone6plus:
            if(deviceOrientation == POTRAIT)
            {
                actualXPos  =   (414.0/320.0) * xPos;
                return actualXPos;
            }
            else
            {
                actualXPos  =   (736.0/480.0) * xPos;
                return actualXPos;
            }
        default:
            return xPos;
        }
    }
    
    
    func deviceYvalue(yPos  yPos : Float) -> Float{
        
        var actualYPos : Float = 0.0
        
        switch (deviceType)
        {
        case iPhone:
            return yPos;
        case iPad:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (1024.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                actualYPos = (768.0/320.0) * yPos;
                return actualYPos;
            }
            
        case iPhone5:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (568.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                return yPos;
            }
        case iPhone6:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (667.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                actualYPos = (375.0/320.0) * yPos;
                return actualYPos;
            }
        case iPhone6plus:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (736.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                actualYPos = (414.0/320.0) * yPos;
                return actualYPos;
            }
        default:
            return yPos;
        }
    }
    
    func deviceXCGFloatValue(xPos  xPos : CGFloat) -> CGFloat{
        
        var actualXPos : CGFloat = 0.0
        
        switch (deviceType)
        {
        case iPhone:
            return xPos;
        case iPad:
            if(deviceOrientation == POTRAIT)
            {
                actualXPos = (768.0/320.0) * xPos;
                return actualXPos;
            }
            else
            {
                actualXPos = (1024.0/480.0) * xPos;
                return actualXPos;
            }
            
        case iPhone5:
            if(deviceOrientation == POTRAIT)
            {
                return xPos;
            }
            else
            {
                actualXPos  =   (568.0/480.0) * xPos;
                return actualXPos;
            }
        case iPhone6:
            if(deviceOrientation == POTRAIT)
            {
                actualXPos  =   (375.0/320.0) * xPos;
                return actualXPos;
            }
            else
            {
                actualXPos  =   (667.0/480.0) * xPos;
                return actualXPos;
            }
        case iPhone6plus:
            if(deviceOrientation == POTRAIT)
            {
                actualXPos  =   (414.0/320.0) * xPos;
                return actualXPos;
            }
            else
            {
                actualXPos  =   (736.0/480.0) * xPos;
                return actualXPos;
            }
        default:
            return xPos;
        }
    }
    
    func deviceYCGFloatValue(yPos  yPos : CGFloat) -> CGFloat{
        
        var actualYPos : CGFloat = 0.0
        
        switch (deviceType)
        {
        case iPhone:
            return yPos;
        case iPad:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (1024.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                actualYPos = (768.0/320.0) * yPos;
                return actualYPos;
            }
            
        case iPhone5:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (568.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                return yPos;
            }
        case iPhone6:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (667.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                actualYPos = (375.0/320.0) * yPos;
                return actualYPos;
            }
        case iPhone6plus:
            if(deviceOrientation == POTRAIT)
            {
                actualYPos = (736.0/480.0) * yPos;
                return actualYPos;
            }
            else
            {
                actualYPos = (414.0/320.0) * yPos;
                return actualYPos;
            }
        default:
            return yPos;
        }
    }
    
    func iOSVersionFamily () -> Float{
        
        /*
           we don't want exact version but want to check iOS 6,7 or 8 thus using
        */
        
        let floatVersion = (UIDevice.currentDevice().systemVersion as NSString).floatValue
        
        return floatVersion
    }
    
    func standardMarginX () -> Float
    {
        return deviceXvalue(xPos: 8.0)
    }
    
    func standardMarginY () -> Float
    {
        return deviceYvalue(yPos: 8.0)
    }
}
