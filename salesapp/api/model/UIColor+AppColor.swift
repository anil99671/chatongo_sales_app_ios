//
//  UIColor+AppColor.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 01/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import Foundation
import UIKit

/*
 This UIColor extension provides all the color which we need to use in the application
*/

extension UIColor {

    class func primaryColor() -> UIColor{
        
//        return UIColor(red: (3.0/255.0), green: (169.0/255.0), blue: (244.0/255.0), alpha: 1.0)
        return UIColor(red: (2.0/255.0), green: (168.0/255.0), blue: (243.0/255.0), alpha: 1.0)
        
    }
    
    class func primaryDarkColor() -> UIColor{
        
//        return UIColor(red: (2.0/255.0), green: (136.0/255.0), blue: (209.0/255.0), alpha: 1.0)
        return UIColor(red: (8.0/255.0), green: (134.0/255.0), blue: (182.0/255.0), alpha: 1.0)
    }
    
    class func primaryLightColor() -> UIColor{
        
//        return UIColor(red: (179.0/255.0), green: (229.0/255.0), blue: (252.0/255.0), alpha: 1.0)
        return UIColor(red: (103.0/255.0), green: (203.0/255.0), blue: (248.0/255.0), alpha: 1.0)
    }
    
    class func accentColor() -> UIColor{
        
        return UIColor(red: (0.0/255.0), green: (150.0/255.0), blue: (136.0/255.0), alpha: 1.0)
    }
    
    class func primaryTextColor() -> UIColor{
        
        return UIColor(red: (33.0/255.0), green: (33.0/255.0), blue: (33.0/255.0), alpha: 1.0)
    }
    
    class func secondaryTextColor() -> UIColor{
        
//        return UIColor(red: (114.0/255.0), green: (114.0/255.0), blue: (114.0/255.0), alpha: 1.0)
        return UIColor(red: (200.0/255.0), green: (200.0/255.0), blue: (200.0/255.0), alpha: 1.0)
        
    }
    
    class func iconsColor() -> UIColor{
        
        return UIColor(red: (255.0/255.0), green: (255.0/255.0), blue: (255.0/255.0), alpha: 1.0)
    }
    
    class func dividerColor() -> UIColor{
        
        return UIColor(red: (182.0/255.0), green: (182.0/255.0), blue: (182.0/255.0), alpha: 1.0)
    }
    
    class func appBGColor() -> UIColor{
        
        return UIColor(red: (255.0/255.0), green: (255/255.0), blue: (255.0/255.0), alpha: 1.0)
    }
    
    class func appAlertBoxBGColor() -> UIColor{
        
        return UIColor(red: (255.0/255.0), green: (255.0/255.0), blue: (255.0/255.0), alpha:0.0)
    }
    
    class func appButtonBGColor() -> UIColor{
        
        return UIColor(red: (3/255.0), green: (101.0/255.0), blue: (100.0/255.0), alpha: 1.0)
    }
    
    class func appButtonBorderColor() -> UIColor{
        
        return UIColor(red: (255.0/255.0), green: (255.0/255.0), blue: (255.0/255.0), alpha: 1.0)
    }
    
    class func appDarkFontColor() -> UIColor{
        
        return UIColor(red: (255.0/255.0), green: (255.0/255.0), blue: (255.0/255.0), alpha: 1.0)
    }
    
    class func appGrayColor() -> UIColor{
        
        return UIColor(red: (235.0/255.0), green: (235.0/255.0), blue: (235.0/255.0), alpha: 1.0)
    }
    
    class func appBlueColor() -> UIColor{
        
        return UIColor(red: (51.0/255.0), green: (102.0/255.0), blue: (153.0/255.0), alpha: 1.0)
    }
    
    class func appGreenColor() -> UIColor{
        
        return UIColor(red: (51.0/255.0), green: (204.0/255.0), blue: (153.0/255.0), alpha: 1.0)
    }
    
    class func appLightFontColor() -> UIColor{
        
        return UIColor(red: (200.0/255.0), green: (200.0/255.0), blue: (200.0/255.0), alpha: 1.0)
    }
    
    class func appActivityColor() -> UIColor{
        
        return UIColor(red: (205.0/255.0), green: (179.0/255.0), blue: (128.0/255.0), alpha: 1.0)
    }
    
    class func appAlertBoxFontColor() -> UIColor{
        
        return UIColor(red: (232.0/255.0), green: (221.0/255.0), blue: (203.0/255.0), alpha: 1.0)
    }
    
    class func appAlertBGColor() -> UIColor{
        
        return UIColor(red: (0.0/255.0), green: (0.0/255.0), blue: (0.0/255.0), alpha: 0.5)
    }
    
    class func startScreenTopGradiantColor() -> UIColor{
        
        return UIColor(red: 60.0/255.0, green: 141.0/255.0, blue: 72.0/255.0, alpha: 1.0)
    }
    
    class func startScreenBottomGradiantColor() -> UIColor{
        
        return UIColor(red: 9.0/255.0, green: 164.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    }
    
    class func dualButtonDividerColor() -> UIColor{
    
        return UIColor(red: 37.0/255.0, green: 120.0/255.0, blue: 162.0/255.0, alpha: 1.0)
    }
    
}

//MARK: For screen gradient

class Colors
{
    let colorTop = UIColor(red: 60.0/255.0, green: 141.0/255.0, blue: 72.0/255.0, alpha: 1.0).CGColor
    let colorBottom = UIColor(red: 9.0/255.0, green: 164.0/255.0, blue: 230.0/255.0, alpha: 1.0).CGColor
    
    let gradientLayer: CAGradientLayer
    
    init()
    {
        gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop,colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
    }
}

//MARK: For shadow

func applyPlainShadowToButton(button : UIButton)
{
    let layer = button.layer
    layer.shadowColor = UIColor.blackColor().CGColor
    layer.shadowOffset = CGSize(width: 0, height: 10)
    layer.shadowOpacity = 0.4
    layer.shadowRadius = 5
    
}

func applyPlainShadowToView(view: UIView)
{
    let layer = view.layer
    layer.shadowColor = UIColor.blackColor().CGColor
    layer.shadowOffset = CGSize(width: 0, height: 10)
    layer.shadowOpacity = 0.3
    layer.shadowRadius = 5
}
