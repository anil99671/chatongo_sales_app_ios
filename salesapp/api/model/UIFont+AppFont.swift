//
//  UIFont+AppFont.swift
//  ZarJewels
//
//  Created by Priyank Ranka on 05/08/15.
//  Copyright (c) 2015 Nimap Infotech. All rights reserved.
//

import Foundation
import UIKit
/*
This UIFont extension provides all the fonts which we need to use in the application
*/

extension UIFont {
    
    class func appFont(forSize forSize: CGFloat) -> UIFont!
    {
        return UIFont(name: "HelveticaNeue-Light", size: forSize)
    }

    class func appFontBold(forSize forSize: CGFloat) -> UIFont!
    {
        return UIFont(name: "HelveticaNeue-Bold", size: forSize)
    }
    
     class func appFontMedium(forSize forSize: CGFloat) -> UIFont!
    {
        return UIFont(name: "HelveticaNeue-Medium", size: forSize)
    }

    class func appFontAwesome(forSize forSize: CGFloat) -> UIFont!
    {
        return UIFont(name: "FontAwesome", size: forSize)
    }
}