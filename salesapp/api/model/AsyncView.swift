//
//  AsyncView.swift
//  TableViewController
//
//  Created by Atit Modi on 16/06/15.
//  Copyright (c) 2015 Atit Modi. All rights reserved.
//

import UIKit
@objc protocol AsyncViewDelegate : NSObjectProtocol {

    optional func didReceivedImage(img: UIImage,imgIndex : Int )
    optional func didReceivedImage(img: UIImage,imgIndex : Int, groupIndex : Int)
}

class AsyncView: UIView , AsyncLoaderModelDelegate {
    
    static let IMAGE_FILE = 1
    
    static let ASPECT_FILL = 1
    static let ASPECT_FIT = 2
    
    var loader : AsyncLoaderModel?
    weak var delegate : AsyncViewDelegate?
    var bg : UIImageView?
    var activity : UIActivityIndicatorView?
    var imgIndex : Int?
    var groupIndex : Int?
    var isOffline : Bool?
    var imageFileName : String?
    var modifiedDate : String?
    var fileType = AsyncView.IMAGE_FILE
    var fittingType = AsyncView.ASPECT_FIT
    
    var imagePlaceholderName : String?
    
    var captionLabel : UILabel?
    
    //MARK: Init Methods
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        // initialization code
        self.userInteractionEnabled = false
        self.loader = AsyncLoaderModel()
        
        //add image view
        self.bg = UIImageView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
        self.addSubview(self.bg!)
        
        //add activityIndicator
        self.activity = UIActivityIndicatorView ()
        self.activity!.center = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0)
        self.activity!.hidden = true
        self.activity!.color = UIColor.primaryColor()
        self.addSubview(self.activity!)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    //MARK: Local Methods Methods
    
    func addRoundEdge(){
        
        self.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = DeviceManager.sharedDeviceManagement()!.deviceXCGFloatValue(xPos: 6.0)
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.dividerColor().CGColor
        self.clipsToBounds = true
    }
    
    func addCaptionLabel(text : String){
        
        captionLabel = UILabel(frame: CGRectMake(0,0,self.frame.size.width,self.frame.size.height))
        captionLabel!.backgroundColor = UIColor.appAlertBGColor()
        captionLabel!.textColor = UIColor.whiteColor()
        captionLabel!.textAlignment = NSTextAlignment.Center
        captionLabel!.font = UIFont.appFontMedium(forSize: DeviceManager.sharedDeviceManagement()!.deviceXCGFloatValue(xPos: 18.0))
        captionLabel!.text = text
        self.addSubview(captionLabel!)
    }
    
    func getViewForURL(strURL : String, imgIndex : Int){

        self.hidden = false
        self.activity!.startAnimating()
        
        self.imgIndex = imgIndex
        groupIndex = -1
        isOffline = false
        
        if imagePlaceholderName != nil{
            
            let deviceManager = DeviceManager.sharedDeviceManagement()
            
            bg!.image = UIImage(named: deviceManager.resourceNameAsPerDevice(fileName: imagePlaceholderName!))
        }
        
        self.loader!.getDataFromURLString(webURL: strURL, dataIndex: self.imgIndex)
        self.loader!.delegate = self
    }
    
    func getViewForURL(strURL : String, groupIndex: Int, imgIndex : Int){
        
        self.hidden = false
        self.activity!.startAnimating()
        
        self.imgIndex = imgIndex
        self.groupIndex = groupIndex
        self.isOffline = false
        
        if imagePlaceholderName != nil{
            
            let deviceManager = DeviceManager.sharedDeviceManagement()
            
            bg!.image = UIImage(named: deviceManager.resourceNameAsPerDevice(fileName: imagePlaceholderName!))
        }
        
        self.loader!.getDataFromURLString(webURL: strURL, dataIndex: self.imgIndex)
        self.loader!.delegate = self
    }
    
    func getViewForURL(strURL : String, imgIndex : Int, offline:Bool, imageFileName: String!){
        
        self.imgIndex = imgIndex
        groupIndex = -1
        isOffline = offline
        self.imageFileName = imageFileName
        
        if imagePlaceholderName != nil{
            
            let deviceManager = DeviceManager.sharedDeviceManagement()
            
            bg!.image = UIImage(named: deviceManager.resourceNameAsPerDevice(fileName: imagePlaceholderName!))
        }
        
        let fileManager = SaveDataInDocumentDirectory.sharedInstance()
        
        if fileManager.isFileExist(fileName: imageFileName)
        {
            bg!.image = UIImage(contentsOfFile: fileManager.filePathInDocumentDirectoryFor(fileName: imageFileName))
            
            if fittingType == AsyncView.ASPECT_FILL{
                bg!.contentMode = UIViewContentMode.ScaleAspectFill
            }
            else{
                bg!.contentMode = UIViewContentMode.ScaleAspectFit
            }
            
            activity = nil
            loader = nil
        }
        else
        {
            
            self.hidden = false
            if activity == nil
            {
                self.activity = UIActivityIndicatorView ()
                self.activity!.center = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0)
                self.activity!.hidden = true
                self.activity!.color = UIColor.appDarkFontColor()
                self.addSubview(self.activity!)
            }
            self.activity!.startAnimating()
            
            if loader != nil
            {
                loader = nil
            }
            loader = AsyncLoaderModel()
            self.loader!.getDataFromURLString(webURL: strURL, dataIndex: self.imgIndex)
            self.loader!.delegate = self
        }
        
        
    }
    
    func getViewForURL(strURL : String, groupIndex: Int, imgIndex : Int, offline:Bool, imageFileName: String!){
        
        self.imgIndex = imgIndex
        self.groupIndex = groupIndex
        isOffline = offline
        self.imageFileName = imageFileName
        
        if imagePlaceholderName != nil{
            
            let deviceManager = DeviceManager.sharedDeviceManagement()
            
            bg!.image = UIImage(named: deviceManager.resourceNameAsPerDevice(fileName: imagePlaceholderName!))
        }
        
        let fileManager = SaveDataInDocumentDirectory.sharedInstance()
        
        if fileManager.isFileExist(fileName: imageFileName)
        {
            bg!.image = UIImage(contentsOfFile: fileManager.filePathInDocumentDirectoryFor(fileName: imageFileName))
            if fittingType == AsyncView.ASPECT_FILL{
                bg!.contentMode = UIViewContentMode.ScaleAspectFill
            }
            else{
                bg!.contentMode = UIViewContentMode.ScaleAspectFit
            }
            activity = nil
            loader = nil
        }
        else
        {
            self.hidden = false
            self.activity!.startAnimating()
            
            if loader != nil
            {
                loader = nil
            }
            loader = AsyncLoaderModel()
            self.loader!.getDataFromURLStringWithOutSecurity(webURL: strURL, dataIndex: self.imgIndex)
            self.loader!.delegate = self
        }
        
        
    }
    
    //MARK: AsyncLoaderModelDelegate Methods
    
    func didReceivedData(data data: NSData!, loader: AsyncLoaderModel!, dataIndex: Int) {
        
        if isOffline == true
        {
            let fileManager = SaveDataInDocumentDirectory.sharedInstance()
            
            if fileManager.isFileExist(fileName: imageFileName)
            {
                fileManager.removeFile(name: imageFileName!)
            }
            
            let offlineFileDB = AsynFileManagerDB.sharedInstance()
            
            let fileLocalDBModifiedDate = offlineFileDB!.selectModifiedDateFromOfflineFileWhere(fileName: imageFileName!);
            
            // We need to make entry in the offline DB as well
            
            if fileManager.updateData(data: data, withFileName: imageFileName) == true{
                
                if fileLocalDBModifiedDate != nil{
                    
                    if fileLocalDBModifiedDate == "na"{
                        
                        offlineFileDB!.updateOfflineFile(offlineId: imageFileName!, modifiedDate: modifiedDate!, type: fileType)
                    }
                    else{
                        offlineFileDB!.insertIntoOfflineFile(offlineId: imageFileName!, modifiedDate: modifiedDate!, type: fileType)
                    }
                    
                }
                else
                {
                    offlineFileDB!.insertIntoOfflineFile(offlineId: imageFileName!, modifiedDate: modifiedDate!, type: fileType)
                }
            }
            else{
             
                if fileLocalDBModifiedDate == "na"{
                    
                    offlineFileDB!.updateOfflineFile(offlineId: imageFileName!, modifiedDate: "na", type: fileType)
                }
                else{
                    offlineFileDB!.insertIntoOfflineFile(offlineId: imageFileName!, modifiedDate: "na", type: fileType)
                }
            }
        }
        
        
        self.bg!.image = UIImage (data: data)
        if fittingType == AsyncView.ASPECT_FILL{
            bg!.contentMode = UIViewContentMode.ScaleAspectFill
        }
        else{
            bg!.contentMode = UIViewContentMode.ScaleAspectFit
        }
        
        self.loader = nil
        if self.activity != nil
        {
            self.activity!.stopAnimating()
            self.activity = nil
        }
        
        if(self.delegate != nil){
            
            if delegate!.respondsToSelector(#selector(AsyncViewDelegate.didReceivedImage(_:imgIndex:)))
            {
                delegate!.didReceivedImage!(UIImage (data: data)!, imgIndex: dataIndex)
            }
        }
        
    }
    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int) {
        
        self.bg!.image = nil
        self.loader = nil
        
        self.activity!.stopAnimating()
        self.activity = nil
    }

}

