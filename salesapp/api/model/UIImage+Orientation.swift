//
//  UIImage+Orientation.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 15/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import Foundation
import UIKit

/*
 This UIColor extension provides all the color which we need to use in the application
 */

extension UIImage {
    
    func normalizedImage() -> UIImage {
        
        if (self.imageOrientation == UIImageOrientation.Up) {
            return self;
        }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        self.drawInRect(rect)
        
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return normalizedImage;
    }
    
    func fixOrientation() -> UIImage {
    
    // No-op if the orientation is already correct
        if (self.imageOrientation == UIImageOrientation.Up){
            return self;
        }
        
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    var transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
    
    case UIImageOrientation.Down, UIImageOrientation.DownMirrored:
    
        transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
        transform = CGAffineTransformRotate(transform, CGFloat(M_PI));
    case UIImageOrientation.Left,UIImageOrientation.LeftMirrored:
    
        transform = CGAffineTransformTranslate(transform, self.size.width, 0);
        transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2));
    case UIImageOrientation.Right,UIImageOrientation.RightMirrored:
    
        transform = CGAffineTransformTranslate(transform, 0, self.size.height);
        transform = CGAffineTransformRotate(transform, CGFloat(-M_PI_2));
    case UIImageOrientation.Up,UIImageOrientation.UpMirrored:
        _ = 0
    }
    
    switch (self.imageOrientation) {
    case UIImageOrientation.UpMirrored,UIImageOrientation.DownMirrored:
   
        transform = CGAffineTransformTranslate(transform, self.size.width, 0);
        transform = CGAffineTransformScale(transform, -1, 1);
    case UIImageOrientation.LeftMirrored,UIImageOrientation.RightMirrored:
    
        transform = CGAffineTransformTranslate(transform, self.size.height, 0);
        transform = CGAffineTransformScale(transform, -1, 1);
    case UIImageOrientation.Up,UIImageOrientation.Down,UIImageOrientation.Left,UIImageOrientation.Right:
        _ = 0
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    let ctx = CGBitmapContextCreate(nil, Int(self.size.width), Int(self.size.height),
    CGImageGetBitsPerComponent(self.CGImage), 0,
    CGImageGetColorSpace(self.CGImage),
    CGImageGetBitmapInfo(self.CGImage).rawValue);
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
    case UIImageOrientation.Left,UIImageOrientation.LeftMirrored,UIImageOrientation.Right,UIImageOrientation.RightMirrored:
    // Grr...
    CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
    default:
    CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
    }
    
    // And now we just create a new UIImage from the drawing context
    let cgimg = CGBitmapContextCreateImage(ctx);
    let img = UIImage(CGImage: cgimg!)
    
    return img;
    }
}
