//
//  LocationManagerModel.swift
//  ChatOnGo
//
//  Created by Priyank Ranka on 31/05/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

class LocationManagerModel: NSObject {

    var name: String?
    var formattedAddess: String?
    var streetName: String?
    var thoroughfare: String?
    var subThoroughfare: String?
    var locality: String?
    var subLocality: String?
    var administrativeArea: String?
    var subAdministrativeArea: String?
    var postalCode: String?
    var ISOcountryCode: String?
    var country: String?
    var inlandWater: String?
    var ocean: String?
    var areasOfInterest: [String]?
    var latitude : Double?
    var longitude : Double?
}
