//
//  SearchScreenViewController.swift
//  COG Sales
//
//  Created by Apple on 01/06/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class SearchScreenViewController: UIViewController,NimapNavigationBarViewDelegate, NimapAlertViewDelegate, AsyncViewDelegate, UITableViewDelegate, UITableViewDataSource,SearchShopByBusinessParserDelegate, UISearchBarDelegate,NimapSearchBarViewDelegate, AddShopViewControllerDelegate, LocationManagerDelegate
    
    //
{
    //MARK: VARIABLE

    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0

    var searchBar : NimapSearchBarView?
    var tableOfShops : UITableView?
    
    var isSearchOn : Bool?
    var alert : NimapAlertView?
    var shopsList : [ShopModel]?
    var todayShops : [ShopModel]?
    var pendingShop : [ShopModel]?
    var searchShops : [ShopModel]?
    var searchTodayShops : [ShopModel]?
    var searchPendingShops : [ShopModel]?
    var getAllShopParser : SearchShopByBusinessParser?
    var baseCategoryView : UIView?

    var noShopImageView : UIImageView?
    var updateShopButton : UIButton?
    var searchShopButton : UIButton?

    var searchString : String?

    var salesDB : SalesAppDBModel?
    var addShopController : AddShopViewController?
    var shopDetailController : ShopDetailViewController?
    var searchShopController : SearchShopController?
    var noteListController : NoteListViewController?

    var cellHeight : CGFloat?
    var headerHeight : CGFloat?
    var pageHeight : CGFloat?
    var searchBarHeight : CGFloat?
    var shadowHeight : CGFloat = 0.0
    var buttonHeight : CGFloat = 0.0
    var latitude : Double? = 0.0
    var longitude : Double? = 0.0

    var shopId : Int?

    //Location service variables

    var locationManager :LocationManager?

    // MARK: Life Cycle

    init()
    {
        super.init(nibName: nil, bundle: nil)

        deviceManager = DeviceManager.sharedDeviceManagement()

        isSearchOn = false
        salesDB = SalesAppDBModel.sharedInstance()
        cellHeight = deviceManager!.deviceXCGFloatValue(xPos: 56.0)
        shadowHeight = deviceManager!.deviceYCGFloatValue(yPos: 2.0)
        buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: 30.0)
        headerHeight = deviceManager!.deviceYCGFloatValue(yPos: 25.0)

        locationManager = LocationManager(isForceEnable: false, isReverserGeoLocationEnable: true,isLocationContinous: false)
        locationManager!.delegate = self
        locationManager!.startLocationService()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }

    override func viewWillAppear(animated: Bool)
    {
        searchShops = []
        loadPage()
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    //MARK: Loading method

    func loadPage()
    {
        searchShops = []
        loadnavigationBar()
        loadButtons()
        loadGetAllShpParser()
    }
    
    func loadnavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }

            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"Shop List")
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)

            navigationBar!.addRightButtonWithIcon()//addRightButton()
            navigationBar!.updateRightButtonTitle(AppConstant.Static.BOOK_ICON)

            navigationHeight = navigationBar!.frame.size.height
            pageHeight = self.view.frame.height - navigationHeight
        }

    }

    func loadGetAllShpParser()
    {

        // Activity Indicator
        if NSUserDefaults.standardUserDefaults().boolForKey("downloadAllShops") == false
        {

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if noShopImageView == nil
        {
            noShopImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "noshops")))
            noShopImageView!.center = CGPointMake(self.view.center.x, navigationHeight + (pageHeight!/2.0))
            noShopImageView!.userInteractionEnabled = true
            self.view.insertSubview(noShopImageView!, atIndex: 0)
        }
        alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
        alert!.delegate = self
        self.view.addSubview(alert!)

        getAllShopParser = SearchShopByBusinessParser()
        getAllShopParser!.delegate = self
        getAllShopParser!.getShops()
        }
        else
        {
           loadTableView()
        }
    }

    func loadTableView()
    {
        todayShops = salesDB!.selectALLFromShop()
        pendingShop = salesDB!.selectAllPendingShops()

        shopsList = todayShops! + pendingShop!

        if shopsList!.count > 0
        {
            var posY = navigationBar!.frame.size.height
            let padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
            let baseContorllerHeight = self.view.frame.size.height - posY - buttonHeight

            if baseCategoryView == nil
            {
                baseCategoryView = UIView(frame: CGRectMake(0.0,posY,self.view.frame.width,baseContorllerHeight))
                baseCategoryView!.backgroundColor = UIColor.iconsColor()
                self.view.addSubview(baseCategoryView!)

                if searchBar == nil
                {
                    if deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
                    {
                        searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.SEARCH_BAR_HEIGHT_CGFLOAT)
                    }
                    else
                    {
                        searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
                    }

                    searchBar = NimapSearchBarView(frame: CGRectMake(padding,0,self.view.frame.width - (2 * padding),searchBarHeight!))
                    searchBar!.center = CGPointMake(self.view.center.x, navigationHeight + (searchBar!.frame.size.height/2.0) + padding)
                    searchBar!.delegate = self
                    searchBar!.searchTextField!.placeholder = "Search Shops"
                    self.view.addSubview(searchBar!)

                }

                posY = searchBar!.frame.size.height + deviceManager!.deviceYCGFloatValue(yPos: 8.0)

                let tableHeight = baseCategoryView!.frame.height - (searchBar!.frame.size.height) - deviceManager!.deviceYCGFloatValue(yPos: 8.0)

                if tableOfShops == nil
                {
                    tableOfShops = UITableView(frame: CGRectMake(0.0,posY,baseCategoryView!.frame.size.width,tableHeight))
                    tableOfShops!.delegate = self
                    tableOfShops!.dataSource = self
                    tableOfShops!.separatorStyle = UITableViewCellSeparatorStyle.None
                    baseCategoryView!.addSubview(tableOfShops!)
                }
                
            }

            if noShopImageView != nil{
                noShopImageView!.hidden = true
            }
        }
        else
        {
            if noShopImageView == nil
            {
                noShopImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "noshops")))
                noShopImageView!.center = CGPointMake(self.view.center.x, navigationHeight + (pageHeight!/2.0))
                self.view.insertSubview(noShopImageView!, atIndex: 0)
            }
            else{
                noShopImageView!.hidden = false
            }
        }
    }

    func loadButtons()
    {
        let gap = deviceManager!.deviceXCGFloatValue(xPos: 1.0)
        let buttonWidth = (self.view.frame.width - gap) * 0.5

        updateShopButton = UIButton(frame:CGRectMake(0,self.view.frame.height - buttonHeight,buttonWidth,buttonHeight))
        updateShopButton!.setTitle("Upload shop", forState: UIControlState.Normal)
        updateShopButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        updateShopButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        updateShopButton!.backgroundColor = UIColor.primaryColor()
        updateShopButton!.addTarget(self, action: #selector(SearchScreenViewController.updateShopButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(updateShopButton!)

        let buttonXPosition = buttonWidth + gap

        searchShopButton = UIButton(frame:CGRectMake(buttonXPosition,updateShopButton!.frame.origin.y,buttonWidth,buttonHeight))
        searchShopButton!.setTitle("Search Shop", forState: UIControlState.Normal)
        searchShopButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        searchShopButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        searchShopButton!.backgroundColor = UIColor.primaryColor()
        searchShopButton!.addTarget(self, action: #selector(SearchScreenViewController.searchshopButtonPressed), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(searchShopButton!)
    }

    //MARK: Unloading method

    func unloadPage()
    {
        unloadNavigationBar()
        unloadAlert()
        unloadSearchBar()
        unloadShopTable()
        unloadViews()
        unloadArrays()

        isSearchOn = false
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil

        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func unloadSearchBar()
    {
        if searchBar != nil
        {
            searchBar!.removeFromSuperview()
            searchBar = nil
        }
    }

    func unloadShopTable()
    {
        if tableOfShops != nil
        {
            tableOfShops!.removeFromSuperview()
            tableOfShops = nil
        }
    }

    func unloadViews()
    {
        if baseCategoryView != nil
        {
            baseCategoryView!.removeFromSuperview()
            baseCategoryView = nil
        }

        if noteListController != nil
        {
            noteListController = nil
        }

        if addShopController != nil
        {
            addShopController = nil
        }

        if shopDetailController != nil
        {
            shopDetailController = nil
        }

        if searchShopController != nil
        {
            searchShopController = nil
        }
    }

    func unloadArrays()
    {
        if shopsList != nil
        {
            shopsList!.removeAll(keepCapacity: false)
        }

        if todayShops != nil
        {
            todayShops!.removeAll(keepCapacity: false)
        }

        if pendingShop != nil
        {
            pendingShop!.removeAll(keepCapacity: false)
        }

        if searchShops != nil
        {
            searchShops!.removeAll(keepCapacity: false)
        }

        if searchTodayShops != nil
        {
            searchTodayShops!.removeAll(keepCapacity: false)
        }

        if searchPendingShops != nil
        {
            searchPendingShops!.removeAll(keepCapacity: false)
        }

        searchShops = nil
        shopsList = nil
    }

    //MARK: NimapSearchBarViewDelegate methods

    func nimapSearchBarViewDidBeginEditing(textField: UITextField)
    {
            searchBar!.searchTextField!.placeholder = ""
    }

    func nimapSearchBarViewEnteredCharacters(string: String)
    {
        searchString = string
        searchString = searchString!.stringByReplacingOccurrencesOfString("\n", withString: "")

        if searchString!.isEmpty{
            isSearchOn = false
        }
        else{
            if searchString!.characters.count > 1{

                searchTodayShops = []
                searchPendingShops = []

                searchShops!.removeAll(keepCapacity: false)
                searchTodayShops!.removeAll(keepCapacity: false)
                searchPendingShops!.removeAll(keepCapacity: false)

                for i in 0..<todayShops!.count{

                    let shop = todayShops![i]

                    if shop.name!.lowercaseString.rangeOfString(searchString!.lowercaseString) != nil {

                        searchTodayShops!.append(shop)
                    }
                }

                for j in 0..<pendingShop!.count{

                    let shop = pendingShop![j]

                    if shop.name!.lowercaseString.rangeOfString(searchString!.lowercaseString) != nil
                    {
                        searchPendingShops!.append(shop)
                    }
                }
                searchShops = searchTodayShops! + searchPendingShops!
                isSearchOn = true

                if searchTodayShops!.count <= 0{

                    let shop = ShopModel()
                    shop.shopId = -1
                    shop.name = "No shop found"
                    shop.rgbColor = "2,168,243"
                    shop.initials = "CG"
                    searchTodayShops!.append(shop)
                }
            }
            else
            {
                isSearchOn = false
            }
        }
        tableOfShops!.reloadData()
    }
    func nimapSearchBarViewShouldReturn(textField: UITextField)
    {
        searchBar!.searchTextField!.resignFirstResponder()
    }

    
    // MARK: UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearchOn == true
        {
            if searchTodayShops!.count > 0 && searchPendingShops!.count > 0
            {
                if section == 0
                {
                    return searchTodayShops!.count
                }
                else if section == 1
                {
                    return searchPendingShops!.count
                }
            }
            else if searchTodayShops!.count > 0 && searchPendingShops!.count == 0
            {
                if section == 0
                {
                    return searchTodayShops!.count
                }
            }
            else if searchTodayShops!.count == 0 && searchPendingShops!.count > 0
            {
                if section == 0
                {
                    return searchPendingShops!.count
                }
            }
        }
        else
        {
            if todayShops!.count > 0 && pendingShop!.count > 0
            {
                if section == 0
                {
                    return todayShops!.count
                }
                else if section == 1
                {
                    return pendingShop!.count
                }
            }
            else if todayShops!.count > 0 && pendingShop!.count == 0
            {
                if section == 0
                {
                    return todayShops!.count
                }
            }
            else if todayShops!.count == 0 && pendingShop!.count > 0
            {
                if section == 0
                {
                    return pendingShop!.count
                }
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {

        var cell : ShopListTableViewCell?
        
        cell = tableView.dequeueReusableCellWithIdentifier("shopList") as! ShopListTableViewCell?
        
        if cell == nil
        {
            cell = ShopListTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "shopList", cellWidth: tableView.frame.size.width, cellHeigth: cellHeight!)
            cell!.selectionStyle = UITableViewCellSelectionStyle.None

        }


        var shop : ShopModel?
        
        if isSearchOn == true
        {
            if searchTodayShops!.count > 0 && searchPendingShops!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = searchTodayShops![indexPath.row]
                }
                else if indexPath.section == 1
                {
                    shop = searchPendingShops![indexPath.row]
                }
            }
            else if searchTodayShops!.count > 0 && searchPendingShops!.count == 0
            {
                if indexPath.section == 0
                {
                    shop = searchTodayShops![indexPath.row]
                }
            }
            else if searchTodayShops!.count == 0 && searchPendingShops!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = searchPendingShops![indexPath.row]
                }
            }
        }
        else
        {
            if todayShops!.count > 0 && pendingShop!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = todayShops![indexPath.row]
                }
                else if indexPath.section == 1
                {
                    shop = pendingShop![indexPath.row]
                }
            }
            else if todayShops!.count > 0 && pendingShop!.count == 0
            {
                if indexPath.section == 0
                {
                    shop = todayShops![indexPath.row]
                }
            }
            else if todayShops!.count == 0 && pendingShop!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = pendingShop![indexPath.row]
                }
            }
        }
        
        
        
        cell!.iconLabel!.text = shop!.initials
        cell!.shopAddressLabel!.text = shop!.streetAddress
        cell!.shopNameLabel!.text = shop!.name
        cell!.shopId = shop!.shopId
        
        return cell!
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if isSearchOn == true
        {
            if searchTodayShops!.count > 0 && searchPendingShops!.count > 0
            {
                return 2
            }
            else if searchTodayShops!.count > 0 && searchPendingShops!.count == 0
            {
                return 1
            }
            else if searchTodayShops!.count == 0 && searchPendingShops!.count > 0
            {
                return 1
            }
        }
        else
        {
            if todayShops!.count > 0 && pendingShop!.count > 0
            {
                return 2
            }
            else if todayShops!.count > 0 && pendingShop!.count == 0
            {
                return 1
            }
            else if todayShops!.count == 0 && pendingShop!.count > 0
            {
                return 1
            }
        }

        return 0
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 15.0)

        let returnedView = UIView(frame: CGRectMake(0, 0, tableView.frame.width, headerHeight!)) //set these values as necessary
        returnedView.backgroundColor = UIColor.whiteColor()

        let label = UILabel(frame: CGRectMake(padding, 0, tableView.frame.width, headerHeight!))
        label.textColor = UIColor.primaryColor()
        label.font = UIFont.appFontBold(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))

        if isSearchOn == true
        {
            if searchTodayShops!.count > 0 && searchPendingShops!.count > 0
            {
                if section == 0
                {
                    label.text = "Today"
                }
                else if section == 1
                {
                    label.text = "Pending"
                }
            }
            else if searchTodayShops!.count > 0 && searchPendingShops!.count == 0
            {
                if section == 0
                {
                    label.text = "Today"
                }
            }
            else if searchTodayShops!.count == 0 && searchPendingShops!.count > 0
            {
                if section == 0
                {
                    label.text = "Pending"
                }
            }
        }
        else
        {
            if todayShops!.count > 0 && pendingShop!.count > 0
            {
                if section == 0
                {
                    label.text = "Today"
                }
                else if section == 1
                {
                    label.text = "Pending"
                }
            }
            else if todayShops!.count > 0 && pendingShop!.count == 0
            {
                if section == 0
                {
                    label.text = "Today"
                }
            }
            else if todayShops!.count == 0 && pendingShop!.count > 0
            {
                if section == 0
                {
                    label.text = "Pending"
                }
            }
        }

        returnedView.addSubview(label)

        let dividerLabel = UILabel(frame: CGRectMake(0.0,headerHeight! - (0.2),returnedView.frame.width,0.2))
        dividerLabel.backgroundColor = UIColor.dividerColor()
        returnedView.addSubview(dividerLabel)
        
        return returnedView
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return headerHeight!
    }

    // MARK: UITableViewDelegate

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return cellHeight!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        var shop : ShopModel? = ShopModel()

        if isSearchOn == true{
            if searchTodayShops!.count > 0 && searchPendingShops!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = searchTodayShops![indexPath.row]
                }
                else if indexPath.section == 1
                {
                    shop = searchPendingShops![indexPath.row]
                }
            }
            else if searchTodayShops!.count > 0 && searchPendingShops!.count == 0
            {
                if indexPath.section == 0
                {
                    shop = searchTodayShops![indexPath.row]
                }
            }
            else if searchTodayShops!.count == 0 && searchPendingShops!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = searchPendingShops![indexPath.row]
                }
            }
        }
        else{
            if todayShops!.count > 0 && pendingShop!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = todayShops![indexPath.row]
                }
                else if indexPath.section == 1
                {
                    shop = pendingShop![indexPath.row]
                }
            }
            else if todayShops!.count > 0 && pendingShop!.count == 0
            {
                if indexPath.section == 0
                {
                    shop = todayShops![indexPath.row]
                }
            }
            else if todayShops!.count == 0 && pendingShop!.count > 0
            {
                if indexPath.section == 0
                {
                    shop = pendingShop![indexPath.row]
                }
            }
        }

        if shop!.shopId != -1
        {
        if shopDetailController == nil {

            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isShopBrochureDownloaded")
            shopDetailController = ShopDetailViewController(shopID: shop!.shopId!, latitude: latitude!, longitude: longitude!)
            self.navigationController!.pushViewController(shopDetailController!, animated: true)
        }
        }
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    //MARK: Button events

    func updateShopButtonPressed()
    {
        if addShopController == nil {
            addShopController = AddShopViewController()
            addShopController!.delegate = self
            self.navigationController!.pushViewController(addShopController!, animated: true)
        }
    }

    func searchshopButtonPressed()
    {
        print("searchshopButtonPressed")
        if searchShopController == nil
        {
            searchShopController = SearchShopController()
        self.navigationController!.pushViewController(searchShopController!, animated: true)
        }
    }

    //MARK: NimapAlertViewDelegate Methods
    
    // This method is called when user presses the primary actiona button on the NimapAlertView and depending upon the tag value we understand which alertview callback has come
    
    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if noShopImageView == nil
        {
            noShopImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "noshops")))
            noShopImageView!.center = CGPointMake(self.view.center.x, navigationHeight + (pageHeight!/2.0))
            self.view.insertSubview(noShopImageView!, atIndex: 0)
        }
        else{
            noShopImageView!.hidden = false
        }

        if tag == "shopError"
        {
            if getAllShopParser != nil
            {
                getAllShopParser = nil
            }
            getAllShopParser = SearchShopByBusinessParser()
            getAllShopParser!.delegate = self
            getAllShopParser!.getShops()
            
        }
        if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
        if tag == "shopNotFoundError"
        {
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }
        }
        
    }

    //MARK: UIScrollViewDelegate Methods

    func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        searchBar!.resignFirstResponder()
    }

    //MARK: Event Handlers

    //MARK: AddShopViewControllerDelegate methods

    func didShopAdded(resultCode : Int){
        if addShopController != nil {
            self.navigationController!.popViewControllerAnimated(true)
            addShopController = nil
        }
    }

    //MARK: NimapNavigationBarDelegate methods

    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }

    func rightButtonPressed()
    {
        print("rightButtonPressed")
        if noteListController == nil {
            noteListController = NoteListViewController()
            self.navigationController!.pushViewController(noteListController!, animated: true)
        }
    }
    
    
    //MARK: Shops list delegate methods
    
    func didReceivedSearchShopsParserError(status : Int)
    {
        getAllShopParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        
        if status == AppConstant.Static.ADD_USER_SUCCESS
        {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "downloadAllShops")

            loadTableView()
        }
        else
        {
            var errorMessege = ""
            var errorTag = ""

            if status == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessege = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "shopError"
            }
            else if status == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessege = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "shopError"
            }
            else if status == AppConstant.Static.NO_SHOP_AVAILABLE
            {
                errorMessege = AppConstant.Static.NO_SHOP_AVAILABLE_MESSEGE
                errorTag = "shopNotFoundError"

            }
            else if status == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessege = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }

            if errorMessege != ""
            {
                alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessege, actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }
    }

    //MARK: LocationManagerDelegate methods

    func didReceiveLocation(currentLocation location : LocationManagerModel?)
    {
        locationManager = nil
        if location!.subLocality != nil {

            if location!.subLocality != "na"
            {
                latitude = location!.latitude!
                longitude = location!.longitude!
            }
            else{
                latitude = 0.0
                longitude = 0.0
            }
        }
        else{
            latitude = 0.0
            longitude = 0.0
        }
    }
    
    //MARK: UItextField
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        searchBar!.searchTextField!.resignFirstResponder()
        textField.resignFirstResponder()
        return true
    }
    
}
