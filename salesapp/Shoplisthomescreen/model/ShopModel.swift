//
//  ShopModel.swift
//  COG Sales
//
//  Created by Apple on 31/05/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class ShopModel: NSObject
{
    var id : Int?
    var shopId : Int?
    var userId : Int?
    var userName : String?
    var name : String?
    var mobileNo : String?
    var contactNo : String?
    var country : String?
    var state : String?
    var city : String?
    var streetAddress : String?
    var subscriptionTypeId : String?
    var zipcode : String?
    var latitude : Double?
    var longitude : Double?
    var timings : String?
    var info : String?
    var isPending : String?
    var startDate : String?
    var endDate : String?
    var locality : String?
    var visitDate : String?
    var isVisited : String?
    var isAppDownloaded : String?
    var rating : Double?
    var ratingCount : Int?
    var reviewCount : Int?
    var osType : String?
    
    var isSelected :Bool? = false
    
    var rgbColor : String?
    var initials : String?
}