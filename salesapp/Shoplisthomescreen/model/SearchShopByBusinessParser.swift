//
//  SearchShopByBusinessParser.swift
//  COG Sales
//
//  Created by Apple on 07/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

@objc protocol SearchShopByBusinessParserDelegate : NSObjectProtocol
{
    optional func didReceivedSearchShopsParserError(status : Int)
}
class SearchShopByBusinessParser: NSObject,AsyncLoaderModelDelegate
{
    var loader : AsyncLoaderModel?
    weak var delegate : SearchShopByBusinessParserDelegate?
    var salesDB : SalesAppDBModel?

    var index : Int?

    func getShops()
    {
        let userId = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! Int
        loader = AsyncLoaderModel()
        loader!.delegate = self
        loader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"GetSalesUserShops?UserId=\(userId)",dataIndex: -1)
    }

    func getSalesUserVisitedShops(index index : Int)
    {
        self.index = index
        let userId = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! Int
        loader = AsyncLoaderModel()
        loader!.delegate = self
        loader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"GetLatestVisitedShops?UserId=\(userId)&Id=\(index)",dataIndex: -1)
    }

    func getGetOldestvisitedShops(index index : Int)
    {
        self.index = index
        let userId = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! Int
        loader = AsyncLoaderModel()
        loader!.delegate = self
        loader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"GetOldestVisitedShops?UserId=\(userId)&Id=\(index)",dataIndex: -1)
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("GetAllShopParser responseString \(responseString!)")

        processAddUserData(data: data)

        self.loader = nil
    }

    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.CONNECTION_ERROR)
        }


        self.loader = nil
    }

    /*
     this method will extract the data from the JSON into the preferences which will remain persistent
     */

    func processAddUserData(data data : NSData)
    {
        var result : NSDictionary?


        do{
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let status = result!.objectForKey("Status") as! Int

            if status == 200
            {
                let shopData = result!.objectForKey("data") as! NSArray

                if shopData.count == 0
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.NO_SHOP_AVAILABLE)
                    }
                }
                else
                {
                    salesDB = SalesAppDBModel.sharedInstance()
                    if index == -1
                    {
                        salesDB!.deleteAllVisitedShop()
                    }

                    for i in 0..<shopData.count
                    {
                        let shop = ShopModel()

                        shop.shopId = shopData[i].valueForKey("ShopId") as! Int!
                        shop.userId = shopData[i].valueForKey("UserId") as! Int!
                        shop.name = shopData[i].valueForKey("ShopName") as! String!
                        shop.streetAddress = shopData[i].valueForKey("StreetAddress") as! String!
                        shop.mobileNo = shopData[i].valueForKey("MobileNo") as! String!
                        shop.contactNo = shopData[i].valueForKey("ContactNo") as! String!
//                      shop.country = shopData[i].valueForKey("Country") as! String!
                        shop.state = shopData[i].valueForKey("State") as! String!
                        shop.city = shopData[i].valueForKey("City") as! String!
                        shop.zipcode = shopData[i].valueForKey("ZipCode") as! String!
                        shop.latitude = Double(shopData[i].valueForKey("Latitude") as! String!)
                        shop.longitude = Double(shopData[i].valueForKey("Longitude") as! String!)
                        shop.visitDate = shopData[i].valueForKey("VisitDate") as! String!
    
                        shop.isVisited = shopData[i].valueForKey("IsVisited") as! String!
                        shop.isAppDownloaded = shopData[i].valueForKey("IsAppDownloaded") as! String!
                        shop.info = shopData[i].valueForKey("Info") as! String!
                        shop.locality = shopData[i].valueForKey("Locality") as! String!
                        shop.timings = shopData[i].valueForKey("Timing") as! String!
                        shop.startDate = shopData[i].valueForKey("CreatedDate") as! String!
                        shop.endDate = shopData[i].valueForKey("ModifiedDate") as! String!
                        
                       shop.endDate = shop.endDate!.stringByReplacingOccurrencesOfString("/", withString: "-")
                        shop.isPending = shopData[i].valueForKey("IsPending") as! String!
                        shop.id = shopData[i].valueForKey("Id") as! Int!
                        shop.osType = shopData[i].valueForKey("OsType") as? String


                        let shopResult = salesDB!.isShopExists(shopId: shop.shopId!)
                        
                        if shopResult == true
                        {
                            salesDB!.updateShop(shopId: shop.shopId!, userId: shop.userId!, name: shop.name!, streetAddress: shop.streetAddress!, mobileNo: shop.mobileNo!, contactNo: shop.contactNo!, state: shop.state!, city: shop.city!, zipCode: shop.zipcode!, latitude: shop.latitude!, longitude: shop.longitude!, visitDate: shop.visitDate!, isVisited: shop.isVisited!, isAppDownloaded: shop.isAppDownloaded!, info: shop.info!, timings: shop.timings!, locality: shop.locality!, startDate: shop.startDate!, endDate: shop.endDate!, isPending: shop.isPending!, id: shop.id!, osType: shop.osType!)
                        }
                        else
                        {
                            salesDB!.insertIntoShop(id: shop.id!, shopId: shop.shopId!, vendorId: shop.userId!, name: shop.name!, streetAddress: shop.streetAddress!, mobileNo: shop.mobileNo!, contactNo: shop.contactNo!, state: shop.state!, city: shop.city!, zipCode: shop.zipcode!, latitude: shop.latitude!, longitude: shop.longitude!, visitDate: shop.visitDate!, isVisited: shop.isVisited!, isAppDownloaded: shop.isAppDownloaded!, info: shop.info!, timings: shop.timings!, locality: shop.locality!, startDate: shop.startDate!, endDate: shop.endDate!, isPending: shop.isPending!,osType: shop.osType!)
                        }

                        let businessCategoryData = shopData[i].objectForKey("businesscategorydata") as? NSArray

                        for j in 0 ..< businessCategoryData!.count
                        {
                            let businessDict = businessCategoryData![j] as! NSDictionary

                            let buninessCategoryId = businessDict.objectForKey("BusinessCategoryId") as! Int
                            let businessCategoryName = businessDict.objectForKey("BusinessCategoryName") as! String


                            if  salesDB!.isBusinessCategoryExistForShop(shopId: shop.shopId!, businessCategoryId: buninessCategoryId) == true
                            {
                                salesDB!.updateIntoSelectedBusinessShop(shopId: shop.shopId!, businessCategoryId: buninessCategoryId, businessCategoryName: businessCategoryName, isChecked: 1)
                            }

                            else
                            {
                                salesDB!.insertIntoSelectedBusinessShop(shopId: shop.shopId!, businessCategoryId: buninessCategoryId, businessCategoryName: businessCategoryName, isChecked: 1)
                            }
                        }

                    }
                    if delegate != nil
                    {
                        delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.ADD_USER_SUCCESS)
                    }
                }
            }
            else
            {

                if status == 404
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.NO_SHOP_AVAILABLE)
                    }
                }
                else if status == 401
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.UNAUTHORISED_USER)
                    }
                }
                else
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.PROCESSING_ERROR)
                    }
                    
                }
                
            }
            
        }
        catch
        {
            // Error Occured
            if delegate != nil
            {
                delegate!.didReceivedSearchShopsParserError!(AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}

