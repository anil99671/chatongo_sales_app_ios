//
//  ShopListTableViewCell.swift
//  COG Sales
//
//  Created by Apple on 31/05/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class ShopListTableViewCell: UITableViewCell {

    var iconLabel : UILabel?
    var shopNameLabel : UILabel?
    var shopAddressLabel : UILabel?
    var dividerLabel : UILabel?

    var defaultLabel : UILabel?
    var shopId : Int?
    var osTypeLabel : UILabel?

    var deviceManager : DeviceManager?

    init(style: UITableViewCellStyle, reuseIdentifier: String?, cellWidth : CGFloat, cellHeigth : CGFloat) {

        super.init(style: style, reuseIdentifier: reuseIdentifier)
        deviceManager = DeviceManager.sharedDeviceManagement()

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 9.0)
        let width_height = cellHeigth - (2*padding)

        let labelWidth =  cellWidth - (width_height + (3 * padding))
        let labelHeight = (cellHeigth - (2.5 * padding))/2.0

        iconLabel = UILabel(frame: CGRectMake(padding,padding,width_height,width_height))
        iconLabel!.layer.cornerRadius = iconLabel!.frame.size.width/2
        iconLabel!.clipsToBounds = true
        iconLabel!.textColor = UIColor.whiteColor()
        iconLabel!.textAlignment = .Center
        iconLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        iconLabel!.backgroundColor = UIColor.primaryColor()
        self.addSubview(iconLabel!)

        shopNameLabel = UILabel(frame: CGRectMake(width_height + (2 * padding),(1.25*padding),labelWidth,labelHeight))
        shopNameLabel!.textColor = UIColor.primaryColor()
        shopNameLabel!.font = UIFont.appFontBold(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))
        self.addSubview(shopNameLabel!)

        let posY = shopNameLabel!.frame.origin.y + shopNameLabel!.frame.size.height
        if deviceManager!.deviceType == deviceManager!.iPad
        {
        osTypeLabel = UILabel(frame: CGRectMake(shopNameLabel!.frame.origin.x + shopNameLabel!.frame.size.width - deviceManager!.deviceXCGFloatValue(xPos: 30) ,(1.25*padding),(self.frame.size.width - shopNameLabel!.frame.size.width)/4,labelHeight))
        }
        else{
              osTypeLabel = UILabel(frame: CGRectMake(shopNameLabel!.frame.origin.x + shopNameLabel!.frame.size.width - deviceManager!.deviceXCGFloatValue(xPos: 30) ,(1.25*padding),(self.frame.size.width - shopNameLabel!.frame.size.width)/2,labelHeight))
        }
        osTypeLabel!.textColor = UIColor.primaryColor()
        osTypeLabel!.textAlignment = NSTextAlignment.Center
        osTypeLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))
        self.addSubview(osTypeLabel!)
        
        shopAddressLabel = UILabel(frame: CGRectMake(shopNameLabel!.frame.origin.x,posY,labelWidth,labelHeight))
        shopAddressLabel!.textColor = UIColor.blackColor()
        shopAddressLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 12.0))
        self.addSubview(shopAddressLabel!)

        dividerLabel = UILabel(frame: CGRectMake(0.0,cellHeigth - (0.2),cellWidth,0.2))
        dividerLabel!.backgroundColor = UIColor.dividerColor()
        self.addSubview(dividerLabel!)

        defaultLabel = UILabel(frame: CGRectMake(width_height + (2 * padding),padding,labelWidth,cellHeigth - (2 * padding)))
        defaultLabel!.textColor = UIColor.primaryColor()
        defaultLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 15.0))
        defaultLabel!.text = "No Shops!!"
        defaultLabel!.hidden = true
        self.addSubview(defaultLabel!)
    }


    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
