//
//  GetCityParser.swift
//  ChatOnGo
//
//  Created by Apple on 14/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

@objc protocol GetCityParserDelegate : NSObjectProtocol
{
    optional func didRecievedCitiesResultError(resultCode resultCode : Int)
    func didRevievedCities(Cities Cities : [GetCityModel])
}

class GetCityParser: NSObject, AsyncLoaderModelDelegate
{
    var asyncloader : AsyncLoaderModel?
    var delegate : GetCityParserDelegate?

    func getCityList(stateId:Int)
    {
        asyncloader = AsyncLoaderModel()
        asyncloader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"GetCityList?StateId=\(stateId)", dataIndex: -1)
        asyncloader!.delegate = self
    }


    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("responseString City \(responseString!)")
        processData(data: data)
        self.asyncloader = nil
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        delegate!.didRecievedCitiesResultError!(resultCode: AppConstant.Static.CONNECTION_ERROR)
    }

    func processData(data data : NSData)
    {
        var json : AnyObject?
        do
        {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary

            let msgCode = json!.objectForKey("Status") as! Int

            if msgCode == 200
            {
                let cityData = json!.objectForKey("data") as! NSArray
                var citiesArray : [GetCityModel] = []

                for i in 0..<cityData.count
                {
                    let cities = GetCityModel()

                    cities.cityId = cityData[i].valueForKey("CityId") as! Int!
                    cities.cityName = cityData[i].valueForKey("Name") as! String!
                    citiesArray.append(cities)
                }

                if (delegate != nil)
                {
                    delegate!.didRevievedCities(Cities: citiesArray)
                }
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didRecievedCitiesResultError!(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}

