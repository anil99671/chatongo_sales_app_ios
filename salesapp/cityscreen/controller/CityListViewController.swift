//
//  CityListViewController.swift
//  ChatOnGo
//
//  Created by Apple on 14/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

protocol CityListViewControllerDelegate : NSObjectProtocol
{
    func didRecievedUserSelectedCity(cityName : String, cityId : Int)
}

class CityListViewController: UIViewController, NimapNavigationBarViewDelegate, UITableViewDataSource, UITableViewDelegate, GetCityParserDelegate, NimapAlertViewDelegate, NimapSearchBarViewDelegate
{

    // MARK: - Variable Declarations

    var deviceManager : DeviceManager?
    weak var citydelegate : CityListViewControllerDelegate?

    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    var pageHeight : CGFloat?
    var padding : CGFloat?

    var alert : NimapAlertView?

    var cityTable : UITableView?

    var cityParser : GetCityParser?

    var cityModel : GetCityModel?
    var arrayOfCities : [GetCityModel!]!
    var searchCities : [GetCityModel!]!

    var activity : UIActivityIndicatorView?
    var cityId : Int?
    var stateId : Int?
    var isSearching = false
    var searchView : UIView?
    var searchBar : NimapSearchBarView?
    var searchBarHeight : CGFloat = 0.0

    init()
    {
        super.init(nibName: nil, bundle: nil)

        deviceManager = DeviceManager.sharedDeviceManagement()
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    //MARK: ViewController Life Cycle Methods

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool)
    {
        loadPage()
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }

    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    func loadPage() {
        loadNavigationBar()
        loadActivity()
        loadCityParser()
    }

    //MARK: Load Methods

    func loadNavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }

            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"City List")
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)

            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)

            navigationHeight = navigationBar!.frame.size.height
            pageHeight = self.view.frame.height - navigationHeight
        }

    }

    func loadActivity()
    {
        activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        activity!.color = UIColor.appActivityColor()
        activity!.center = self.view.center
        self.view.addSubview(activity!)
        activity!.startAnimating()
    }

    func loadCityParser() {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        cityParser = GetCityParser()
        cityParser!.delegate = self
        cityParser!.getCityList(stateId!)
    }

    func loadCityTable()
    {
        if cityTable == nil
        {
            cityTable = UITableView(frame: CGRectMake(0,searchBarHeight + deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING), self.view.frame.width, self.view.frame.height - navigationHeight - searchBarHeight - deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING)))
            cityTable!.delegate = self
            cityTable!.dataSource = self
            cityTable!.separatorStyle = UITableViewCellSeparatorStyle.None
            searchView!.addSubview(cityTable!)
        }
    }

    //MARK: Unloading Methods

    func unloadPage()
    {
        unloadNavigationBar()
        unloadCityTable()
        unloadViews()
        unloadSearchBar()
        unloadArrays()
        unloadAlert()
        unloadActivity()
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
    }

    func unloadCityTable()
    {
        if cityTable != nil
        {
            cityTable!.removeFromSuperview()
            cityTable = nil
        }
    }

    func unloadViews()
    {
        if searchView != nil
        {
            searchView!.removeFromSuperview()
            searchView = nil
        }
    }

    func unloadSearchBar()
    {
        if searchBar != nil
        {
            searchBar!.removeFromSuperview()
            searchBar = nil
        }
    }

    func unloadArrays()
    {
        if arrayOfCities != nil
        {
            arrayOfCities!.removeAll(keepCapacity: false)
            arrayOfCities = nil
        }

        if searchCities != nil
        {
            searchCities!.removeAll(keepCapacity: false)
            searchCities = nil
        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func unloadActivity()
    {
        if activity != nil
        {
            activity!.removeFromSuperview()
            activity = nil
        }
    }

    //MARK: TableView Delegates

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearching == true
        {
            return searchCities!.count
        }
        else
        {
            return arrayOfCities!.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        cityModel = arrayOfCities[indexPath.row]
        var cell : CityListTableViewCell?
        cell = cityTable!.dequeueReusableCellWithIdentifier("cell") as! CityListTableViewCell?

        if cell == nil
        {
            cell = CityListTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell", cellWidth: self.view.frame.size.width, cellHeigth: deviceManager!.deviceYCGFloatValue(yPos: 45))
        }

        if isSearching == true
        {
            cityModel = searchCities![indexPath.row]
        }
        else
        {
            cityModel = arrayOfCities![indexPath.row]
        }

        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell!.cityNameLable!.text = cityModel!.cityName

        return cell!
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return deviceManager!.deviceYCGFloatValue(yPos: 45)
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var selectedCity = arrayOfCities![indexPath.row]

        if isSearching == true
        {
            selectedCity = searchCities![indexPath.row]
        }
        else
        {
            selectedCity = arrayOfCities![indexPath.row]
        }

        if selectedCity.cityId != -1
        {
            citydelegate!.didRecievedUserSelectedCity(selectedCity.cityName!, cityId: selectedCity.cityId!)

            searchBar!.resignFirstResponder()
            self.navigationController!.popViewControllerAnimated(true)
        }
    }

    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    //MARK: city parser callbacks

    func didRevievedCities(Cities Cities: [GetCityModel])
    {
        cityParser = nil
        unloadActivity()
        if searchView == nil
        {
            searchView = UIView(frame: CGRectMake(0,navigationHeight + deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.PADDING),self.view.frame.width,pageHeight!))
            self.view.addSubview(searchView!)
        }
        if searchBar == nil
        {
            searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
            padding = deviceManager!.deviceYCGFloatValue(yPos: 10)

            searchBar = NimapSearchBarView(frame: CGRectMake(padding!,0,self.view.frame.width - (2 * padding!),searchBarHeight))
            searchBar!.center = CGPointMake(self.view.center.x, (searchBar!.frame.size.height/2.0))
            searchBar!.delegate = self
            searchBar!.searchTextField!.placeholder = "Search City"
            searchView!.addSubview(searchBar!)
        }

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if arrayOfCities != nil
        {
            arrayOfCities = nil
        }

        arrayOfCities = Cities
        loadCityTable()
        cityTable!.reloadData()
    }

    func didRecievedCitiesResultError(resultCode resultCode: Int)
    {
        var errorMessage = ""
        var errorTag = ""

        if resultCode == AppConstant.Static.CONNECTION_ERROR
        {
            errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
            errorTag = "error"
        }
        else if resultCode == AppConstant.Static.PROCESSING_ERROR
        {
            errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
            errorTag = "error"
        }

        else if resultCode == AppConstant.Static.UNAUTHORISED_USER
        {
            errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
            errorTag = "unauthorisedUserError"
        }

        alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
        alert!.delegate = self
        self.view.addSubview(alert!)

    }

    //MARK: Alert View Delegate methods

    func didEnterText(tag : String!, text: String!)
    {

    }

    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        if tag == "error"
        {
            if cityParser != nil
            {
                cityParser = nil
            }
            cityParser = GetCityParser()
            cityParser!.delegate = self
            cityParser!.getCityList(stateId!)

        }
        else if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
    }

    func didSecondaryActionButtonPressed(tag: String!)
    {

    }


    // MARK: NimapNavigationBarViewDelegate

    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }

    func rightButtonPressed()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    //MARK: NimapSearchBarViewDelegate callbacks

    func nimapSearchBarViewEnteredCharacters(string: String)
    {
        if searchCities == nil
        {
            searchCities = []
        }

        searchCities!.removeAll(keepCapacity: false)

        if string.isEmpty
        {
            isSearching = false
        }
        else
        {
            for i in 0..<arrayOfCities!.count
            {
                let code = arrayOfCities![i]

                if code.cityName!.lowercaseString.indexOf(string.lowercaseString) == 0
                {
                    searchCities!.append(code)
                }
            }
            isSearching = true

            if searchCities!.count <= 0{

                let city = GetCityModel()
                city.cityId = -1
                city.cityName = "No city found"
                searchCities!.append(city)
            }

        }
        cityTable!.reloadData()
    }
}



