//
//  CityListTableViewCell.swift
//  ChatOnGo
//
//  Created by Apple on 23/06/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

class CityListTableViewCell: UITableViewCell
{
    var cityNameLable : UILabel?
    var dividerLabel : UILabel?


    var deviceManager : DeviceManager?
    var padding : CGFloat?

    var cellWidth: CGFloat?
    var labelWidth : CGFloat?

    init(style: UITableViewCellStyle, reuseIdentifier: String?, cellWidth : CGFloat, cellHeigth : CGFloat)
    {

        super.init(style: style, reuseIdentifier: reuseIdentifier)

        deviceManager = DeviceManager.sharedDeviceManagement()

        padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)

        self.cellWidth = cellWidth

        cityNameLable = UILabel(frame: CGRectMake(padding!, padding!, cellWidth - (2*padding!), cellHeigth - (2*padding!)))
        cityNameLable!.textColor = UIColor.primaryTextColor()
        cityNameLable!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        self.addSubview(cityNameLable!)

        dividerLabel = UILabel(frame: CGRectMake(0.0,cellHeigth - (0.2),cellWidth,0.2))
        dividerLabel!.backgroundColor = UIColor.dividerColor()
        self.addSubview(dividerLabel!)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
