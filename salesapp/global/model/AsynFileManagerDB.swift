//
//  AsynFileManagerDB.swift
//  JSONPractice
//
//  Created by Apple on 06/05/16.
//  Copyright © 2016 Nimapinfotech. All rights reserved.
//

import UIKit

class AsynFileManagerDB: SQLiteDataBase
{
    // 1. fileManager
    
    var insertIntoOfflineFile : COpaquePointer = nil
    var updateIntoOfflineFile : COpaquePointer = nil
    var deleteOfflineFileForID : COpaquePointer = nil
    var deleteAllOfflineFile : COpaquePointer = nil
    var selectModifiedDateFromOfflineFile : COpaquePointer = nil
    
    struct Static
    {
        static var onceToken : dispatch_once_t = 0
        static var instance : AsynFileManagerDB? = nil
        static var OFFLINE_FILE = "OfflineFile"
        static var IMAGE_FILE = 1;
        static var VIDEO_FILE = 2;
        static var AUDIO_FILE = 3;
    }
    
    class func sharedInstance() -> AsynFileManagerDB?
    {
        
        dispatch_once(&Static.onceToken)
            {
                Static.instance = AsynFileManagerDB()
        }
        
        return Static.instance
    }
    
    init()
    {
        
        assert(Static.instance == nil, "Attempted to create second instance of AsynFileManagerDB")
        
        super.init(dbFileName: "FileManagerDB.sqlite", deleteEditableCopy: false)
        
        if sqlite3_open((writableDBPath! as NSString).UTF8String, &database) == SQLITE_OK
        {
             print("Query initialized...")
        }
    }
    
    func insertIntoOfflineFile(offlineId offlineId : String,modifiedDate : String,type :Int) -> Bool
    {
        if insertIntoOfflineFile == nil
        {
            initializeStatement(sqlStatement: &insertIntoOfflineFile, query: "INSERT INTO OfflineFile (offlineId,modifiedDate,type) VALUES (?,?,?)")
        }
        sqlite3_bind_text(insertIntoOfflineFile, 1, (offlineId as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoOfflineFile, 2, (modifiedDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(insertIntoOfflineFile, 3, CInt(type))
        return executeUpdate(sqlStatement: insertIntoOfflineFile)
    }

    func selectModifiedDateFromOfflineFileWhere(fileName fileName : String) -> String?
    {
        if selectModifiedDateFromOfflineFile == nil
        {
            initializeStatement(sqlStatement: &selectModifiedDateFromOfflineFile, query: "SELECT modifiedDate FROM OfflineFile WHERE offlineId = ?")
        }
        var modifiedDate : String? = nil
        sqlite3_bind_text(selectModifiedDateFromOfflineFile, 1, (fileName as NSString).UTF8String, -1, nil)
        while executeSelect(sqlStatement: selectModifiedDateFromOfflineFile)
        {
            print("modifiedDate ")
            let name = sqlite3_column_text(selectModifiedDateFromOfflineFile, 0)
            let cString  =   UnsafePointer<Int8>(name);
            modifiedDate    =    String.fromCString(cString)!;
        }
        sqlite3_reset(selectModifiedDateFromOfflineFile)
        return modifiedDate
    }
    
    func updateOfflineFile(offlineId offlineId : String,modifiedDate : String,type :Int) -> Bool
    {
        if updateIntoOfflineFile == nil
        {
            initializeStatement(sqlStatement: &updateIntoOfflineFile, query: "UPDATE OfflineFile SET modifiedDate = ?, type = ? WHERE offlineId = ?")
        }
        sqlite3_bind_text(updateIntoOfflineFile, 1, (modifiedDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoOfflineFile, 2, CInt(type))
        sqlite3_bind_text(updateIntoOfflineFile, 3, (offlineId as NSString).UTF8String, -1, nil)
        
        return executeUpdate(sqlStatement: updateIntoOfflineFile)
    }

    func deleteOfflineFileForId(offlineID offlineID : String) -> Bool
    {
        if deleteOfflineFileForID == nil
        {
            initializeStatement(sqlStatement: &deleteOfflineFileForID, query: "DELETE FROM OfflineFile WHERE offlineId = ?")
        }
        sqlite3_bind_text(deleteOfflineFileForID, 1, (offlineID as NSString).UTF8String, -1, nil)
        return executeUpdate(sqlStatement: deleteOfflineFileForID)
    }

    func deleteOfflineFile() -> Bool
    {
        if deleteAllOfflineFile == nil
        {
            initializeStatement(sqlStatement: &deleteAllOfflineFile, query: "DELETE FROM OfflineFile")
        }
        return executeUpdate(sqlStatement: deleteAllOfflineFile)
    }
    
    
}
