//
//  SalesAppDB.swift
//  VendorAppDB
//
//  Created by Apple on 05/07/16.
//  Copyright © 2016 nimap. All rights reserved.
//

import UIKit

class SalesAppDBModel: SQLiteDataBase
{
    //MARK: 1. UserType

    var insertIntoUserType : COpaquePointer = nil
    var updateIntoUserType : COpaquePointer = nil
    var selectAllFromUserType : COpaquePointer = nil
    var selectUserTypeIdFromUserType : COpaquePointer = nil
    var deleteAllUserType : COpaquePointer = nil

    //MARK: 2. SHOP

    var insertIntoShop : COpaquePointer = nil
    var updateIntoShop : COpaquePointer = nil
    var updateIntoShopWithIsVisited : COpaquePointer = nil
    var insertIntoShopDetail : COpaquePointer = nil
    var updateIntoShopDetail : COpaquePointer = nil
    var isShopExistsInShop : COpaquePointer = nil
    var lastShopMappingId : COpaquePointer = nil
    var selectAllFromShopIsVisited : COpaquePointer = nil
    var selectAllFilterShopIsVisited : COpaquePointer = nil
    var selectAllFromShop : COpaquePointer = nil
    var selectAllFromShopIsPending : COpaquePointer = nil
    var selectShopFromShopId : COpaquePointer = nil
    var deleteALlShops : COpaquePointer = nil
    var DeleteShopsVisited : COpaquePointer = nil

    //MARK: 3. Brochure

    var insertIntoBrochure : COpaquePointer = nil
    var updateBrochure : COpaquePointer = nil
    var isBrochureExists : COpaquePointer = nil
    var getBrochureCountForShop : COpaquePointer = nil
    var selectAllFromBrochure : COpaquePointer = nil
    var selectAllFromBrochureFromId : COpaquePointer = nil
    var deleteAllBrochure : COpaquePointer = nil
    var deleteBrochureForBrochureId : COpaquePointer = nil

    //MARK: 4. NotesShopMapping

    var insertIntoNotes : COpaquePointer = nil
    var updateNotes : COpaquePointer = nil
    var isNotesExists : COpaquePointer = nil
    var selectNotesForShopId : COpaquePointer = nil
    var deleteAllNotes : COpaquePointer = nil

    //MARK: 5. VisitedShops

    var insertIntoVisitedShops : COpaquePointer = nil
    var updateIntoVisitedShops : COpaquePointer = nil
    var isVisitedShopsExists : COpaquePointer = nil

    //MARK: 6. BusinessCategory

    var insertIntoBusinessCategory : COpaquePointer = nil
    var updateIntoBusinessCategory : COpaquePointer = nil
    var iSBusinessCategoryExists : COpaquePointer = nil
    var selectAllBusinessCategory : COpaquePointer = nil
    var deleteAllBusinessCategory : COpaquePointer = nil
    var updateBusnessCategoryToZero : COpaquePointer = nil
    var updateBusinessToUnChecked : COpaquePointer = nil
    var updateBusinessToChecked : COpaquePointer = nil
    var selectAllCheckedBusniessCategory : COpaquePointer = nil
    var getCheckedBusinessCount : COpaquePointer = nil

    //MARK: 7. SelectedBusinessShop

    var insertIntoSelectedBusinessShop : COpaquePointer = nil
    var updateIntoSelectedBusinessShop : COpaquePointer = nil
    var selectAllSelectedBusinessCategory : COpaquePointer = nil
    var selectAllSelectedBusinessFromBusinessCategory : COpaquePointer = nil
    var selectAllSelectedBusinessFromBusinessCategoryChecked : COpaquePointer = nil
    var deleteAllSelectedBusinessShopForShopId : COpaquePointer = nil
    var deleteAllSelectedBusinessShop : COpaquePointer = nil
    var selectCountFromSelectedBusinessShop : COpaquePointer = nil

    //MAR: 8. State

    var insertIntoState :COpaquePointer = nil
    var selectAllState : COpaquePointer = nil
    var isStateExists: COpaquePointer = nil
    var selectIdFromStateName : COpaquePointer = nil
    var deleteAllState : COpaquePointer = nil

    //MARK: Singleton Methods

    struct Static
    {
        static var onceToken : dispatch_once_t = 0
        static var instance : SalesAppDBModel? = nil
    }

    class func sharedInstance() -> SalesAppDBModel?
    {

        dispatch_once(&Static.onceToken)
        {
            Static.instance = SalesAppDBModel()
        }

        return Static.instance
    }

    init()
    {

        assert(Static.instance == nil, "Attempted to create second instance of SalesAppDBModel")

        super.init(dbFileName: "SalesAppDB.sqlite", deleteEditableCopy: false)

        if sqlite3_open((writableDBPath! as NSString).UTF8String, &database) == SQLITE_OK
        {
            print("SalesAppDB.sqlite Initialized")
        }
    }

    //MARK: CountryCode Query methods

    //MARK: 1. UserType Function

    func insertIntoUserType(userTypeId userTypeId : Int, userType : String) -> Bool
    {
        if insertIntoUserType == nil
        {
             initializeStatement(sqlStatement: &insertIntoUserType, query: "INSERT INTO UserType (userTypeId,userType) VALUES (?,?)")
        }
        sqlite3_bind_int(insertIntoUserType, 1, CInt(userTypeId))
        sqlite3_bind_text(insertIntoUserType, 2, (userType as NSString).UTF8String, -1, nil)
        return executeUpdate(sqlStatement: insertIntoUserType)
    }

    func updateIntoUsertType(userTypeId userTypeId : Int, userType : String) -> Bool
    {
        if updateIntoUserType == nil
        {
            initializeStatement(sqlStatement: &updateIntoUserType, query: "UPDATE UserType SET userType = ? WHERE userTypeId = ?")
        }
        sqlite3_bind_text(updateIntoUserType, 1, (userType as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoUserType, 2, CInt(userTypeId))
        return executeUpdate(sqlStatement: updateIntoUserType)
    }

    func selectALLFromUserType() -> [UserTypeModel]
    {
        if selectAllFromUserType == nil
        {
            initializeStatement(sqlStatement: &selectAllFromUserType, query: "SELECT * FROM UserType")
        }
        var userTypes : [UserTypeModel] = []
        while executeSelect(sqlStatement: selectAllFromUserType)
        {
            let userType : UserTypeModel = UserTypeModel()

            userType.userTypeID = Int(sqlite3_column_int(selectAllFromUserType, 0))

            let userTypeName = sqlite3_column_text(selectAllFromUserType, 1)
            let uStringPtr  =   UnsafePointer<Int8>(userTypeName);
            userType.userType    =    String.fromCString(uStringPtr);

            userTypes.append(userType)
        }
        sqlite3_reset(selectAllFromUserType)
        return userTypes
    }

    func isUserTypeIdExists(userTypeId userTypeId : Int)-> Bool?
    {
        if selectUserTypeIdFromUserType == nil
        {
            initializeStatement(sqlStatement: &selectUserTypeIdFromUserType, query: "SELECT userTypeId FROM UserType WHERE userTypeId = ?")
        }
        sqlite3_bind_int(selectUserTypeIdFromUserType, 1, CInt(userTypeId))
        var rowPresent = false
        while executeSelect(sqlStatement: selectUserTypeIdFromUserType)
        {
            rowPresent = true
        }
        sqlite3_reset(selectUserTypeIdFromUserType)
        return rowPresent
    }

    func deleteUserType() -> Bool
    {
        if deleteAllUserType == nil
        {
            initializeStatement(sqlStatement: &deleteAllUserType, query: "DELETE FROM UserType")
        }
        return executeUpdate(sqlStatement: deleteAllUserType)
    }

    //MARK: 2. SHOP Function

    func insertIntoShop(id id : Int, shopId : Int, vendorId : Int, name : String, streetAddress : String, mobileNo : String, contactNo : String, state : String, city : String, zipCode : String, latitude : Double, longitude : Double, visitDate : String, isVisited : String, isAppDownloaded : String, info : String, timings : String, locality : String, startDate : String, endDate : String, isPending : String,osType : String) -> Bool
    {
        if insertIntoShop == nil
        {
            initializeStatement(sqlStatement: &insertIntoShop, query: "INSERT INTO Shop (id,shopId,vendorId,name,streetAddress,mobileNo,contactNo,country,state,city,zipCode,latitude,longitude,visitDate,isVisited,isAppDownloaded,info,timings,locality,subscriptionTypeId,startDate,endDate,ratingCount,reviewCount,rating,isPending,osType) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        }
        sqlite3_bind_int(insertIntoShop, 1, CInt(id))
        sqlite3_bind_int(insertIntoShop, 2, CInt(shopId))
        sqlite3_bind_int(insertIntoShop, 3, CInt(vendorId))
        sqlite3_bind_text(insertIntoShop, 4, (name as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 5, (streetAddress as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 6, (mobileNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 7, (contactNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 9, (state as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 10, (city as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 11, (zipCode as NSString).UTF8String, -1, nil)
        sqlite3_bind_double(insertIntoShop, 12, latitude)
        sqlite3_bind_double(insertIntoShop, 13, longitude)
        sqlite3_bind_text(insertIntoShop, 14, (visitDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 15, (isVisited as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 16, (isAppDownloaded as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 17, (info as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 18, (timings as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 19, (locality as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 21, (startDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 22, (endDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 26, (isPending as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShop, 27, (osType as NSString).UTF8String, -1, nil)

        return executeUpdate(sqlStatement: insertIntoShop)
    }

    func updateShop(shopId shopId : Int, userId : Int, name : String, streetAddress : String, mobileNo : String, contactNo : String,  state : String, city : String, zipCode : String, latitude : Double, longitude : Double, visitDate : String, isVisited : String, isAppDownloaded : String , info : String, timings : String, locality : String, startDate : String, endDate : String, isPending : String, id : Int, osType:String) -> Bool
    {
        if updateIntoShop == nil
        {
            initializeStatement(sqlStatement: &updateIntoShop, query: "UPDATE Shop SET vendorId = ?, name = ?, streetAddress = ?, mobileNo = ?, contactNo = ?, state = ?, city = ?, zipCode = ?, latitude = ?, longitude = ?, visitDate = ?, isVisited = ?, isAppDownloaded = ?, info = ?, timings = ?, locality = ?, startDate = ?, endDate = ?, isPending = ?, id = ? , osType = ?WHERE shopId = ?")
        }
        sqlite3_bind_int(updateIntoShop, 1, CInt(userId))
        sqlite3_bind_text(updateIntoShop, 2, (name as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 3, (streetAddress as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 4, (mobileNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 5, (contactNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 6, (state as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 7, (city as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 8, (zipCode as NSString).UTF8String, -1, nil)
        sqlite3_bind_double(updateIntoShop, 9, latitude)
        sqlite3_bind_double(updateIntoShop, 10, longitude)
        sqlite3_bind_text(updateIntoShop, 11, (visitDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 12, (isVisited as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 13, (isAppDownloaded as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 14, (info as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 15, (timings as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 16, (locality as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 17, (startDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 18, (endDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShop, 19, (isPending as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoShop, 20, CInt(id))
        sqlite3_bind_text(updateIntoShop, 21, (osType as NSString).UTF8String, -1, nil)

        sqlite3_bind_int(updateIntoShop, 22, CInt(shopId))
        return executeUpdate(sqlStatement: updateIntoShop)
    }

    func updateShopWithIsVisited(shopId shopId : Int, visitDate : String, isVisited : String, isAppDownloaded : String ) -> Bool
    {
        if updateIntoShopWithIsVisited == nil
        {
            initializeStatement(sqlStatement: &updateIntoShopWithIsVisited, query: "UPDATE Shop SET visitDate = ?, isVisited = ?, isAppDownloaded = ?, isPending = ? WHERE shopId = ?")
        }
        sqlite3_bind_text(updateIntoShopWithIsVisited, 1, (visitDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopWithIsVisited, 2, (isVisited as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopWithIsVisited, 3, (isAppDownloaded as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopWithIsVisited, 4, ("False" as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoShopWithIsVisited, 5, CInt(shopId))

        return executeUpdate(sqlStatement: updateIntoShopWithIsVisited)
    }

    func insertIntoShopDetails(id id : Int, shopId : Int, vendorId : Int, name : String, streetAddress : String, mobileNo : String, contactNo : String, country : String, state : String, city : String, zipCode : String, latitude : Double, longitude : Double, visitDate : String, isVisited : String, isAppDownloaded : String, info : String, timings : String, locality : String, subscriptionTypeId : String, startDate : String, endDate : String, isPending : String, osType : String) -> Bool
    {
        if insertIntoShopDetail == nil
        {
            initializeStatement(sqlStatement: &insertIntoShopDetail, query: "INSERT INTO Shop (id,shopId,vendorId,name,streetAddress,mobileNo,contactNo,country,state,city,zipCode,latitude,longitude,visitDate,isVisited,isAppDownloaded,info,timings,locality,subscriptionTypeId,startDate,endDate,ratingCount,reviewCount,rating,isPending, osType) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
        }
        sqlite3_bind_int(insertIntoShopDetail, 1, CInt(id))
        sqlite3_bind_int(insertIntoShopDetail, 2, CInt(shopId))
        sqlite3_bind_int(insertIntoShopDetail, 3, CInt(vendorId))
        sqlite3_bind_text(insertIntoShopDetail, 4, (name as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 5, (streetAddress as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 6, (mobileNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 7, (contactNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 8, (country as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 9, (state as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 10, (city as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 11, (zipCode as NSString).UTF8String, -1, nil)
        sqlite3_bind_double(insertIntoShopDetail, 12, latitude)
        sqlite3_bind_double(insertIntoShopDetail, 13, longitude)
        sqlite3_bind_text(insertIntoShopDetail, 14, (visitDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 15, (isVisited as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 16, (isAppDownloaded as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 17, (info as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 18, (timings as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 19, (locality as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 20, (subscriptionTypeId as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 21, (startDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 22, (endDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 26, (isPending as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoShopDetail, 27, (osType as NSString).UTF8String, -1, nil)

        return executeUpdate(sqlStatement: insertIntoShopDetail)
    }

    func updateShopDetails(shopId shopId : Int, userId : Int, name : String, streetAddress : String, mobileNo : String, contactNo : String, country : String, state : String, city : String, zipCode : String, latitude : Double, longitude : Double, info : String, timings : String, locality : String,subscriptionTypeId : String, startDate : String, endDate : String, osType : String) -> Bool
    {
        if updateIntoShopDetail == nil
        {
            initializeStatement(sqlStatement: &updateIntoShopDetail, query: "UPDATE Shop SET vendorId = ?, name = ?, streetAddress = ?, mobileNo = ?, contactNo = ?, country = ?, state = ?, city = ?, zipCode = ?, latitude = ?, longitude = ?, info = ?, timings = ?, locality = ?, subscriptionTypeId = ?, startDate = ?, endDate = ?, osType = ? WHERE shopId = ?")
        }
        sqlite3_bind_int(updateIntoShopDetail, 1, CInt(userId))
        sqlite3_bind_text(updateIntoShopDetail, 2, (name as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 3, (streetAddress as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 4, (mobileNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 5, (contactNo as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 6, (country as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 7, (state as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 8, (city as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 9, (zipCode as NSString).UTF8String, -1, nil)
        sqlite3_bind_double(updateIntoShopDetail, 10, latitude)
        sqlite3_bind_double(updateIntoShopDetail, 11, longitude)
        sqlite3_bind_text(updateIntoShopDetail, 12, (info as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 13, (timings as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 14, (locality as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 15, (subscriptionTypeId as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 16, (startDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 17, (endDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoShopDetail, 18, (osType as NSString).UTF8String, -1, nil)

        sqlite3_bind_int(updateIntoShopDetail, 19, CInt(shopId))
        
        return executeUpdate(sqlStatement: updateIntoShopDetail)
    }

    func isShopExists(shopId shopId : Int)-> Bool?
    {
        if isShopExistsInShop == nil
        {
            initializeStatement(sqlStatement: &isShopExistsInShop, query: "SELECT shopId FROM Shop WHERE shopId = ?")
        }
        sqlite3_bind_int(isShopExistsInShop, 1, CInt(shopId))
        var rowPresent = false
        while executeSelect(sqlStatement: isShopExistsInShop)
        {
            rowPresent = true
        }
        sqlite3_reset(isShopExistsInShop)
        return rowPresent
    }

//    var lastShopMappingId : COpaquePointer = nil

    func lastShopMappingID()-> Int?
    {
        if lastShopMappingId == nil
        {
            initializeStatement(sqlStatement: &lastShopMappingId, query: "SELECT * FROM Shop WHERE isVisited = 'True' ORDER BY id ASC LIMIT 1")
        }
        var shopId : Int = Int()
        while executeSelect(sqlStatement: lastShopMappingId)
        {
            shopId = Int(sqlite3_column_int(lastShopMappingId, 0))
        }
        sqlite3_reset(lastShopMappingId)
        return shopId
    }

    func selectAllVisitedShops() -> [ShopModel]
    {
        if selectAllFromShopIsVisited == nil
        {
            initializeStatement(sqlStatement: &selectAllFromShopIsVisited, query: "SELECT id,shopId,vendorId,name,streetAddress,mobileNo,contactNo,state,city,zipCode,latitude,longitude,locality,timings,subscriptionTypeId,startDate,endDate,ratingCount,reviewCount,rating,osType,visitDate FROM Shop WHERE isVisited = 'True' ORDER BY id DESC")
        }
        var Shops : [ShopModel] = []
        while executeSelect(sqlStatement: selectAllFromShopIsVisited)
        {
            let shopDetails : ShopModel = ShopModel()

            shopDetails.id = Int(sqlite3_column_int(selectAllFromShopIsVisited, 0))

            shopDetails.shopId = Int(sqlite3_column_int(selectAllFromShopIsVisited, 1))

            shopDetails.userId = Int(sqlite3_column_int(selectAllFromShopIsVisited, 2))

            let ShopName = sqlite3_column_text(selectAllFromShopIsVisited, 3)
            let sStringPtr  =   UnsafePointer<Int8>(ShopName);
            shopDetails.name    =    String.fromCString(sStringPtr);

            let ShopStreetAddress = sqlite3_column_text(selectAllFromShopIsVisited, 4)
            let sStringPtr1  =   UnsafePointer<Int8>(ShopStreetAddress);
            shopDetails.streetAddress    =    String.fromCString(sStringPtr1);

            let ShopMobileNo = sqlite3_column_text(selectAllFromShopIsVisited, 5)
            let sStringPtr2  =   UnsafePointer<Int8>(ShopMobileNo);
            shopDetails.mobileNo    =    String.fromCString(sStringPtr2);

            let ShopContactNo = sqlite3_column_text(selectAllFromShopIsVisited, 6)
            let sStringPtr3  =   UnsafePointer<Int8>(ShopContactNo);
            shopDetails.contactNo    =    String.fromCString(sStringPtr3);

            shopDetails.country = "India"
            shopDetails.info = "na"

            let ShopState = sqlite3_column_text(selectAllFromShopIsVisited, 7)
            let sStringPtr4  =   UnsafePointer<Int8>(ShopState);
            shopDetails.state    =    String.fromCString(sStringPtr4);

            let ShopCity = sqlite3_column_text(selectAllFromShopIsVisited, 8)
            let sStringPtr5  =   UnsafePointer<Int8>(ShopCity);
            shopDetails.city    =    String.fromCString(sStringPtr5);

            let ShopZipCode = sqlite3_column_text(selectAllFromShopIsVisited, 9)
            let sStringPtr6  =   UnsafePointer<Int8>(ShopZipCode);
            shopDetails.zipcode    =    String.fromCString(sStringPtr6);

            shopDetails.latitude    =    sqlite3_column_double(selectAllFromShopIsVisited, 10)

            shopDetails.latitude    =    sqlite3_column_double(selectAllFromShopIsVisited, 11)

            let ShopArea = sqlite3_column_text(selectAllFromShopIsVisited, 12)
            let sStringPtr11  =   UnsafePointer<Int8>(ShopArea);
            shopDetails.locality   =    String.fromCString(sStringPtr11);

            let ShopTimings = sqlite3_column_text(selectAllFromShopIsVisited, 13)
            let sStringPtr10  =   UnsafePointer<Int8>(ShopTimings);
            shopDetails.timings =    String.fromCString(sStringPtr10);

            let ShopSubscriptionTypeId = sqlite3_column_text(selectAllFromShopIsVisited, 14)
            let sStringPtr12  =   UnsafePointer<Int8>(ShopSubscriptionTypeId);
            shopDetails.subscriptionTypeId    =    String.fromCString(sStringPtr12);

            let ShopStartDate = sqlite3_column_text(selectAllFromShopIsVisited, 15)
            let sStringPtr13  =   UnsafePointer<Int8>(ShopStartDate);
            shopDetails.startDate    =    String.fromCString(sStringPtr13);

            let ShopEndDate = sqlite3_column_text(selectAllFromShopIsVisited, 16)
            let sStringPtr14  =   UnsafePointer<Int8>(ShopEndDate);
            shopDetails.endDate    =    String.fromCString(sStringPtr14);

            shopDetails.ratingCount = Int(sqlite3_column_int(selectAllFromShopIsVisited, 17))

            shopDetails.reviewCount = Int(sqlite3_column_int(selectAllFromShopIsVisited, 18))

            shopDetails.rating    =    sqlite3_column_double(selectAllFromShopIsVisited, 19)
            
            let osType = sqlite3_column_text(selectAllFromShopIsVisited, 20)
            let sStringPtr15  =   UnsafePointer<Int8>(osType);
            shopDetails.osType    =    String.fromCString(sStringPtr15);
            


            let ShopVisitDate = sqlite3_column_text(selectAllFromShopIsVisited, 21)
            let sStringPtr16  =   UnsafePointer<Int8>(ShopVisitDate);
            shopDetails.visitDate    =    String.fromCString(sStringPtr16);

            shopDetails.initials  =  getInitialsFromBusinessCategoryString(shopDetails.name!)
            
            Shops.append(shopDetails)
        }
        sqlite3_reset(selectAllFromShopIsVisited)
        return Shops
    }

    
    func selectAllFilteredVisitedShops(osTypeString : String) -> [ShopModel]
    {
        if selectAllFilterShopIsVisited == nil
        {
            initializeStatement(sqlStatement: &selectAllFilterShopIsVisited, query: "SELECT * FROM Shop WHERE isVisited = 'True' AND Ostype IN \(osTypeString) ORDER BY Id DESC")
        }
        var Shops : [ShopModel] = []
        while executeSelect(sqlStatement: selectAllFilterShopIsVisited)
        {
            let shopDetails : ShopModel = ShopModel()
            
            shopDetails.id = Int(sqlite3_column_int(selectAllFilterShopIsVisited, 0))
            
            shopDetails.shopId = Int(sqlite3_column_int(selectAllFilterShopIsVisited, 1))
            
            shopDetails.userId = Int(sqlite3_column_int(selectAllFilterShopIsVisited, 2))
            
            let ShopName = sqlite3_column_text(selectAllFilterShopIsVisited, 3)
            let sStringPtr  =   UnsafePointer<Int8>(ShopName);
            shopDetails.name    =    String.fromCString(sStringPtr);
            
            let ShopStreetAddress = sqlite3_column_text(selectAllFilterShopIsVisited, 4)
            let sStringPtr1  =   UnsafePointer<Int8>(ShopStreetAddress);
            shopDetails.streetAddress    =    String.fromCString(sStringPtr1);
            
            let ShopMobileNo = sqlite3_column_text(selectAllFilterShopIsVisited, 5)
            let sStringPtr2  =   UnsafePointer<Int8>(ShopMobileNo);
            shopDetails.mobileNo    =    String.fromCString(sStringPtr2);
            
            let ShopContactNo = sqlite3_column_text(selectAllFilterShopIsVisited, 6)
            let sStringPtr3  =   UnsafePointer<Int8>(ShopContactNo);
            shopDetails.contactNo    =    String.fromCString(sStringPtr3);
            
            shopDetails.country = "India"
            shopDetails.info = "na"
            
            let ShopState = sqlite3_column_text(selectAllFilterShopIsVisited, 7)
            let sStringPtr4  =   UnsafePointer<Int8>(ShopState);
            shopDetails.state    =    String.fromCString(sStringPtr4);
            
            let ShopCity = sqlite3_column_text(selectAllFilterShopIsVisited, 8)
            let sStringPtr5  =   UnsafePointer<Int8>(ShopCity);
            shopDetails.city    =    String.fromCString(sStringPtr5);
            
            let ShopZipCode = sqlite3_column_text(selectAllFilterShopIsVisited, 9)
            let sStringPtr6  =   UnsafePointer<Int8>(ShopZipCode);
            shopDetails.zipcode    =    String.fromCString(sStringPtr6);
            
            shopDetails.latitude    =    sqlite3_column_double(selectAllFilterShopIsVisited, 10)
            
            shopDetails.latitude    =    sqlite3_column_double(selectAllFilterShopIsVisited, 11)
            
            let ShopArea = sqlite3_column_text(selectAllFilterShopIsVisited, 12)
            let sStringPtr11  =   UnsafePointer<Int8>(ShopArea);
            shopDetails.locality   =    String.fromCString(sStringPtr11);
            
            let ShopTimings = sqlite3_column_text(selectAllFilterShopIsVisited, 13)
            let sStringPtr10  =   UnsafePointer<Int8>(ShopTimings);
            shopDetails.timings =    String.fromCString(sStringPtr10);
            
            let ShopSubscriptionTypeId = sqlite3_column_text(selectAllFilterShopIsVisited, 14)
            let sStringPtr12  =   UnsafePointer<Int8>(ShopSubscriptionTypeId);
            shopDetails.subscriptionTypeId    =    String.fromCString(sStringPtr12);
            
            let ShopStartDate = sqlite3_column_text(selectAllFilterShopIsVisited, 15)
            let sStringPtr13  =   UnsafePointer<Int8>(ShopStartDate);
            shopDetails.startDate    =    String.fromCString(sStringPtr13);
            
            let ShopEndDate = sqlite3_column_text(selectAllFilterShopIsVisited, 16)
            let sStringPtr14  =   UnsafePointer<Int8>(ShopEndDate);
            shopDetails.endDate    =    String.fromCString(sStringPtr14);
            
            shopDetails.ratingCount = Int(sqlite3_column_int(selectAllFilterShopIsVisited, 17))
            
            shopDetails.reviewCount = Int(sqlite3_column_int(selectAllFilterShopIsVisited, 18))
            
            shopDetails.rating    =    sqlite3_column_double(selectAllFilterShopIsVisited, 19)
            
            let osType = sqlite3_column_text(selectAllFilterShopIsVisited, 20)
            let sStringPtr15  =   UnsafePointer<Int8>(osType);
            shopDetails.osType    =    String.fromCString(sStringPtr15);
            
            
            
            let ShopVisitDate = sqlite3_column_text(selectAllFilterShopIsVisited, 21)
            let sStringPtr16  =   UnsafePointer<Int8>(ShopVisitDate);
            shopDetails.visitDate    =    String.fromCString(sStringPtr16);
            
            shopDetails.initials  =  getInitialsFromBusinessCategoryString(shopDetails.name!)
            
            Shops.append(shopDetails)
        }
        sqlite3_reset(selectAllFilterShopIsVisited)
        return Shops
    }

    func selectALLFromShop() -> [ShopModel]
    {
        if selectAllFromShop == nil
        {
            initializeStatement(sqlStatement: &selectAllFromShop, query: "SELECT id,shopId,vendorId,name,streetAddress,mobileNo,contactNo,state,city,zipCode,latitude,longitude,locality,timings,subscriptionTypeId,startDate,endDate,ratingCount,reviewCount,rating,visitDate,isPending,osType,isVisited FROM Shop WHERE isPending = 'False' And isVisited = 'False'")
        }
        var Shops : [ShopModel] = []
        while executeSelect(sqlStatement: selectAllFromShop)
        {
            let shopDetails : ShopModel = ShopModel()

            shopDetails.id = Int(sqlite3_column_int(selectAllFromShop, 0))

            shopDetails.shopId = Int(sqlite3_column_int(selectAllFromShop, 1))

            shopDetails.userId = Int(sqlite3_column_int(selectAllFromShop, 2))

            let ShopName = sqlite3_column_text(selectAllFromShop, 3)
            let sStringPtr  =   UnsafePointer<Int8>(ShopName);
            shopDetails.name    =    String.fromCString(sStringPtr);

            let ShopStreetAddress = sqlite3_column_text(selectAllFromShop, 4)
            let sStringPtr1  =   UnsafePointer<Int8>(ShopStreetAddress);
            shopDetails.streetAddress    =    String.fromCString(sStringPtr1);

            let ShopMobileNo = sqlite3_column_text(selectAllFromShop, 5)
            let sStringPtr2  =   UnsafePointer<Int8>(ShopMobileNo);
            shopDetails.mobileNo    =    String.fromCString(sStringPtr2);

            let ShopContactNo = sqlite3_column_text(selectAllFromShop, 6)
            let sStringPtr3  =   UnsafePointer<Int8>(ShopContactNo);
            shopDetails.contactNo    =    String.fromCString(sStringPtr3);

            shopDetails.country = "India"
            shopDetails.info = "na"

            let ShopState = sqlite3_column_text(selectAllFromShop, 7)
            let sStringPtr4  =   UnsafePointer<Int8>(ShopState);
            shopDetails.state    =    String.fromCString(sStringPtr4);

            let ShopCity = sqlite3_column_text(selectAllFromShop, 8)
            let sStringPtr5  =   UnsafePointer<Int8>(ShopCity);
            shopDetails.city    =    String.fromCString(sStringPtr5);

            let ShopZipCode = sqlite3_column_text(selectAllFromShop, 9)
            let sStringPtr6  =   UnsafePointer<Int8>(ShopZipCode);
            shopDetails.zipcode    =    String.fromCString(sStringPtr6);

            shopDetails.latitude    =    sqlite3_column_double(selectAllFromShop, 10)

            shopDetails.longitude    =    sqlite3_column_double(selectAllFromShop, 11)

            let ShopArea = sqlite3_column_text(selectAllFromShop, 12)
            let sStringPtr11  =   UnsafePointer<Int8>(ShopArea);
            shopDetails.locality   =    String.fromCString(sStringPtr11);

            let ShopTimings = sqlite3_column_text(selectAllFromShop, 13)
            let sStringPtr10  =   UnsafePointer<Int8>(ShopTimings);
            shopDetails.timings =    String.fromCString(sStringPtr10);

            let ShopSubscriptionTypeId = sqlite3_column_text(selectAllFromShop, 14)
            let sStringPtr12  =   UnsafePointer<Int8>(ShopSubscriptionTypeId);
            shopDetails.subscriptionTypeId    =    String.fromCString(sStringPtr12);

            let ShopStartDate = sqlite3_column_text(selectAllFromShop, 15)
            let sStringPtr13  =   UnsafePointer<Int8>(ShopStartDate);
            shopDetails.startDate    =    String.fromCString(sStringPtr13);

            let ShopEndDate = sqlite3_column_text(selectAllFromShop, 16)
            let sStringPtr14  =   UnsafePointer<Int8>(ShopEndDate);
            shopDetails.endDate    =    String.fromCString(sStringPtr14);

            shopDetails.ratingCount = Int(sqlite3_column_int(selectAllFromShop, 17))

            shopDetails.reviewCount = Int(sqlite3_column_int(selectAllFromShop, 18))

            shopDetails.rating    =    sqlite3_column_double(selectAllFromShop, 19)
          
            let osType = sqlite3_column_text(selectAllFromShop, 20)
            let sStringPtr15  =   UnsafePointer<Int8>(osType);
            shopDetails.osType    =    String.fromCString(sStringPtr15);
            
            


            let ShopVisitDate = sqlite3_column_text(selectAllFromShop, 21)
            let sStringPtr16  =   UnsafePointer<Int8>(ShopVisitDate);
            shopDetails.visitDate    =    String.fromCString(sStringPtr16);

            shopDetails.initials  =  getInitialsFromBusinessCategoryString(shopDetails.name!)

            Shops.append(shopDetails)
        }
        sqlite3_reset(selectAllFromShop)
        return Shops
    }

    func selectAllPendingShops() -> [ShopModel]
    {
        if selectAllFromShopIsPending == nil
        {
            initializeStatement(sqlStatement: &selectAllFromShopIsPending, query: "SELECT id,shopId,vendorId,name,streetAddress,mobileNo,contactNo,state,city,zipCode,latitude,longitude,locality,timings,subscriptionTypeId,startDate,endDate,ratingCount,reviewCount,rating,osType,visitDate FROM Shop WHERE isPending = 'True'")
        }
        var Shops : [ShopModel] = []
        while executeSelect(sqlStatement: selectAllFromShopIsPending)
        {
            let shopDetails : ShopModel = ShopModel()

            shopDetails.id = Int(sqlite3_column_int(selectAllFromShopIsPending, 0))

            shopDetails.shopId = Int(sqlite3_column_int(selectAllFromShopIsPending, 1))

            shopDetails.userId = Int(sqlite3_column_int(selectAllFromShopIsPending, 2))

            let ShopName = sqlite3_column_text(selectAllFromShopIsPending, 3)
            let sStringPtr  =   UnsafePointer<Int8>(ShopName);
            shopDetails.name    =    String.fromCString(sStringPtr);

            let ShopStreetAddress = sqlite3_column_text(selectAllFromShopIsPending, 4)
            let sStringPtr1  =   UnsafePointer<Int8>(ShopStreetAddress);
            shopDetails.streetAddress    =    String.fromCString(sStringPtr1);

            let ShopMobileNo = sqlite3_column_text(selectAllFromShopIsPending, 5)
            let sStringPtr2  =   UnsafePointer<Int8>(ShopMobileNo);
            shopDetails.mobileNo    =    String.fromCString(sStringPtr2);

            let ShopContactNo = sqlite3_column_text(selectAllFromShopIsPending, 6)
            let sStringPtr3  =   UnsafePointer<Int8>(ShopContactNo);
            shopDetails.contactNo    =    String.fromCString(sStringPtr3);

            shopDetails.country = "India"
            shopDetails.info = "na"

            let ShopState = sqlite3_column_text(selectAllFromShopIsPending, 7)
            let sStringPtr4  =   UnsafePointer<Int8>(ShopState);
            shopDetails.state    =    String.fromCString(sStringPtr4);

            let ShopCity = sqlite3_column_text(selectAllFromShopIsPending, 8)
            let sStringPtr5  =   UnsafePointer<Int8>(ShopCity);
            shopDetails.city    =    String.fromCString(sStringPtr5);

            let ShopZipCode = sqlite3_column_text(selectAllFromShopIsPending, 9)
            let sStringPtr6  =   UnsafePointer<Int8>(ShopZipCode);
            shopDetails.zipcode    =    String.fromCString(sStringPtr6);

            shopDetails.latitude    =    sqlite3_column_double(selectAllFromShopIsPending, 10)

            shopDetails.longitude    =    sqlite3_column_double(selectAllFromShopIsPending, 11)

            let ShopArea = sqlite3_column_text(selectAllFromShopIsPending, 12)
            let sStringPtr11  =   UnsafePointer<Int8>(ShopArea);
            shopDetails.locality   =    String.fromCString(sStringPtr11);

            let ShopTimings = sqlite3_column_text(selectAllFromShopIsPending, 13)
            let sStringPtr10  =   UnsafePointer<Int8>(ShopTimings);
            shopDetails.timings =    String.fromCString(sStringPtr10);

            let ShopSubscriptionTypeId = sqlite3_column_text(selectAllFromShopIsPending, 14)
            let sStringPtr12  =   UnsafePointer<Int8>(ShopSubscriptionTypeId);
            shopDetails.subscriptionTypeId    =    String.fromCString(sStringPtr12);

            let ShopStartDate = sqlite3_column_text(selectAllFromShopIsPending, 15)
            let sStringPtr13  =   UnsafePointer<Int8>(ShopStartDate);
            shopDetails.startDate    =    String.fromCString(sStringPtr13);

            let ShopEndDate = sqlite3_column_text(selectAllFromShopIsPending, 16)
            let sStringPtr14  =   UnsafePointer<Int8>(ShopEndDate);
            shopDetails.endDate    =    String.fromCString(sStringPtr14);

            shopDetails.ratingCount = Int(sqlite3_column_int(selectAllFromShopIsPending, 17))

            shopDetails.reviewCount = Int(sqlite3_column_int(selectAllFromShopIsPending, 18))

            shopDetails.rating    =    sqlite3_column_double(selectAllFromShopIsPending, 19)

            let osType = sqlite3_column_text(selectAllFromShopIsPending, 20)
            let sStringPtr15  =   UnsafePointer<Int8>(osType);
            shopDetails.osType    =    String.fromCString(sStringPtr15);
            
            

            let ShopVisitDate = sqlite3_column_text(selectAllFromShopIsPending, 21)
            let sStringPtr16  =   UnsafePointer<Int8>(ShopVisitDate);
            shopDetails.visitDate    =    String.fromCString(sStringPtr16);

            shopDetails.initials  =  getInitialsFromBusinessCategoryString(shopDetails.name!)
            
            Shops.append(shopDetails)
        }
        sqlite3_reset(selectAllFromShopIsPending)
        return Shops
    }

    func selectShopForId(shopId shopId : Int) -> ShopModel
    {
        if selectShopFromShopId == nil
        {
            initializeStatement(sqlStatement: &selectShopFromShopId, query: "SELECT id,shopId,vendorId,name,streetAddress,mobileNo,contactNo,state,city,zipCode,latitude,longitude,locality,timings,subscriptionTypeId,startDate,endDate,ratingCount,reviewCount,rating,visitDate,osType,info FROM Shop WHERE shopId = ?")
        }
        let shopDetails : ShopModel = ShopModel()
        sqlite3_bind_int(selectShopFromShopId, 1, CInt(shopId))
        while executeSelect(sqlStatement: selectShopFromShopId)
        {

            shopDetails.id = Int(sqlite3_column_int(selectShopFromShopId, 0))

            shopDetails.shopId = Int(sqlite3_column_int(selectShopFromShopId, 1))

            shopDetails.userId = Int(sqlite3_column_int(selectShopFromShopId, 2))

            let ShopName = sqlite3_column_text(selectShopFromShopId, 3)
            let sStringPtr  =   UnsafePointer<Int8>(ShopName);
            shopDetails.name    =    String.fromCString(sStringPtr);

            let ShopStreetAddress = sqlite3_column_text(selectShopFromShopId, 4)
            let sStringPtr1  =   UnsafePointer<Int8>(ShopStreetAddress);
            shopDetails.streetAddress    =    String.fromCString(sStringPtr1);

            let ShopMobileNo = sqlite3_column_text(selectShopFromShopId, 5)
            let sStringPtr2  =   UnsafePointer<Int8>(ShopMobileNo);
            shopDetails.mobileNo    =    String.fromCString(sStringPtr2);

            let ShopContactNo = sqlite3_column_text(selectShopFromShopId, 6)
            let sStringPtr3  =   UnsafePointer<Int8>(ShopContactNo);
            shopDetails.contactNo    =    String.fromCString(sStringPtr3);

            shopDetails.country = "India"
            shopDetails.info = "na"

            let ShopState = sqlite3_column_text(selectShopFromShopId, 7)
            let sStringPtr4  =   UnsafePointer<Int8>(ShopState);
            shopDetails.state    =    String.fromCString(sStringPtr4);

            let ShopCity = sqlite3_column_text(selectShopFromShopId, 8)
            let sStringPtr5  =   UnsafePointer<Int8>(ShopCity);
            shopDetails.city    =    String.fromCString(sStringPtr5);

            let ShopZipCode = sqlite3_column_text(selectShopFromShopId, 9)
            let sStringPtr6  =   UnsafePointer<Int8>(ShopZipCode);
            shopDetails.zipcode    =    String.fromCString(sStringPtr6);

            shopDetails.latitude    =    sqlite3_column_double(selectShopFromShopId, 10)

            shopDetails.longitude    =    sqlite3_column_double(selectShopFromShopId, 11)

            let ShopArea = sqlite3_column_text(selectShopFromShopId, 12)
            let sStringPtr11  =   UnsafePointer<Int8>(ShopArea);
            shopDetails.locality   =    String.fromCString(sStringPtr11);

            let ShopTimings = sqlite3_column_text(selectShopFromShopId, 13)
            let sStringPtr10  =   UnsafePointer<Int8>(ShopTimings);
            shopDetails.timings =    String.fromCString(sStringPtr10);

            let ShopSubscriptionTypeId = sqlite3_column_text(selectShopFromShopId, 14)
            let sStringPtr12  =   UnsafePointer<Int8>(ShopSubscriptionTypeId);
            shopDetails.subscriptionTypeId    =    String.fromCString(sStringPtr12);

            let ShopStartDate = sqlite3_column_text(selectShopFromShopId, 15)
            let sStringPtr13  =   UnsafePointer<Int8>(ShopStartDate);
            shopDetails.startDate    =    String.fromCString(sStringPtr13);

            let ShopEndDate = sqlite3_column_text(selectShopFromShopId, 16)
            let sStringPtr14  =   UnsafePointer<Int8>(ShopEndDate);
            shopDetails.endDate    =    String.fromCString(sStringPtr14);

            shopDetails.ratingCount = Int(sqlite3_column_int(selectShopFromShopId, 17))

            shopDetails.reviewCount = Int(sqlite3_column_int(selectShopFromShopId, 18))

            shopDetails.rating    =    sqlite3_column_double(selectShopFromShopId, 19)

            

            let ShopVisitDate = sqlite3_column_text(selectShopFromShopId, 20)
            let sStringPtr15  =   UnsafePointer<Int8>(ShopVisitDate);
            shopDetails.visitDate    =    String.fromCString(sStringPtr15);

            let osType = sqlite3_column_text(selectShopFromShopId, 21)
            let sStringPtr16  =   UnsafePointer<Int8>(osType);
            shopDetails.osType    =    String.fromCString(sStringPtr16);
            

            let ShopInfo = sqlite3_column_text(selectShopFromShopId, 22)
            let sStringPtr17  =   UnsafePointer<Int8>(ShopInfo);
            shopDetails.info    =    String.fromCString(sStringPtr17);

            shopDetails.initials  =  getInitialsFromBusinessCategoryString(shopDetails.name!)
        }
        sqlite3_reset(selectShopFromShopId)
        return shopDetails
    }

    func deleteAllFromShop() -> Bool
    {
        if deleteALlShops == nil
        {
            initializeStatement(sqlStatement: &deleteALlShops, query: "DELETE FROM Shop")
        }
        return executeUpdate(sqlStatement: deleteALlShops)
    }

    func deleteAllVisitedShop() -> Bool
    {
        if DeleteShopsVisited == nil
        {
            initializeStatement(sqlStatement: &DeleteShopsVisited, query: "DELETE FROM Shop WHERE isVisited = 'True'")
        }
        return executeUpdate(sqlStatement: DeleteShopsVisited)
    }

    //MARK: 3. Brochure Function

    func insertIntoBrochure(shopId shopId : Int, brochureId : Int, brochureURL : String, modifiedDate : String) -> Bool
    {
        if insertIntoBrochure == nil
        {
            initializeStatement(sqlStatement: &insertIntoBrochure, query: "INSERT INTO Brochure (shopId,brochureId,brochureURL,modifiedDate) VALUES (?,?,?,?)")
        }
        sqlite3_bind_int(insertIntoBrochure, 1, CInt(shopId))
        sqlite3_bind_int(insertIntoBrochure, 2, CInt(brochureId))
        sqlite3_bind_text(insertIntoBrochure, 3, (brochureURL as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoBrochure, 4, (modifiedDate as NSString).UTF8String, -1, nil)
        return executeUpdate(sqlStatement: insertIntoBrochure)
    }

    func updateIntoBrochure(shopId shopId : Int, brochureId : Int, brochureURL : String, modifiedDate : String) -> Bool
    {
        if updateBrochure == nil
        {
            initializeStatement(sqlStatement: &updateBrochure, query: "UPDATE Brochure SET brochureURL = ?, modifiedDate = ? WHERE shopId = ? AND brochureId = ?")
        }
        sqlite3_bind_text(updateBrochure, 1, (brochureURL as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateBrochure, 2, (modifiedDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateBrochure, 3, CInt(shopId))
        sqlite3_bind_int(updateBrochure, 4, CInt(brochureId))
        return executeUpdate(sqlStatement: updateBrochure)
    }

    func isBrochureExists(brochureId brochureId : Int)-> Bool?
    {
        if isBrochureExists == nil
        {
            initializeStatement(sqlStatement: &isBrochureExists, query: "SELECT brochureId FROM Brochure WHERE brochureId = ?")
        }
        sqlite3_bind_int(isBrochureExists, 1, CInt(brochureId))
        var rowPresent = false
        while executeSelect(sqlStatement: isBrochureExists)
        {
            rowPresent = true
        }
        sqlite3_reset(isBrochureExists)
        return rowPresent
    }

    func getVisitingCardCountForShop(shopId shopId : Int)-> Int?
    {
        if getBrochureCountForShop == nil
        {
            initializeStatement(sqlStatement: &getBrochureCountForShop, query: "SELECT COUNT(brochureId) FROM Brochure WHERE shopId = ?")
        }
        var brochureIDCount : Int = Int()
        sqlite3_bind_int(getBrochureCountForShop, 1, CInt(shopId))
        while executeSelect(sqlStatement: getBrochureCountForShop)
        {
            brochureIDCount = Int(sqlite3_column_int(getBrochureCountForShop, 0))
        }
        sqlite3_reset(getBrochureCountForShop)
        return brochureIDCount
    }

    func getAllBrochure() -> [BrochureModel]
    {
        if selectAllFromBrochure == nil
        {
            initializeStatement(sqlStatement: &selectAllFromBrochure, query: "SELECT * FROM Brochure")
        }
        var brochuremodels : [BrochureModel] = []
        while executeSelect(sqlStatement: selectAllFromBrochure)
        {
            let brochuremodel : BrochureModel = BrochureModel()

            brochuremodel.shopId = Int(sqlite3_column_int(selectAllFromBrochure, 0))

            brochuremodel.brochureId = Int(sqlite3_column_int(selectAllFromBrochure, 1))

            let stateId = sqlite3_column_text(selectAllFromBrochure, 2)
            let bStringPtr  =   UnsafePointer<Int8>(stateId);
            brochuremodel.brochureURL    =    String.fromCString(bStringPtr);

            let businesscategoryName = sqlite3_column_text(selectAllFromBrochure, 3)
            let sStringPtr1  =   UnsafePointer<Int8>(businesscategoryName);
            brochuremodel.modifiedDate    =    String.fromCString(sStringPtr1);

            brochuremodels.append(brochuremodel)
        }
        sqlite3_reset(selectAllFromBrochure)
        return brochuremodels
    }

    func selectALLFromBrochureFromId(shopId shopId : Int) -> [BrochureModel]
    {
        if selectAllFromBrochureFromId == nil
        {
             initializeStatement(sqlStatement: &selectAllFromBrochureFromId, query: "SELECT * FROM Brochure WHERE shopId = ?")
        }
        var brochuremodels : [BrochureModel] = []
        sqlite3_bind_int(selectAllFromBrochureFromId, 1, CInt(shopId))
        while executeSelect(sqlStatement: selectAllFromBrochureFromId)
        {
            let brochuremodel : BrochureModel = BrochureModel()

            brochuremodel.shopId = Int(sqlite3_column_int(selectAllFromBrochureFromId, 0))

            brochuremodel.brochureId = Int(sqlite3_column_int(selectAllFromBrochureFromId, 1))

            let imagePath = sqlite3_column_text(selectAllFromBrochureFromId, 2)
            let bStringPtr  =   UnsafePointer<Int8>(imagePath);
            brochuremodel.brochureURL    =    String.fromCString(bStringPtr);

            let date = sqlite3_column_text(selectAllFromBrochureFromId, 3)
            let sStringPtr1  =   UnsafePointer<Int8>(date);
            brochuremodel.modifiedDate    =    String.fromCString(sStringPtr1);

            brochuremodels.append(brochuremodel)
        }
        sqlite3_reset(selectAllFromBrochureFromId)
        return brochuremodels
    }

    func deleteALlBrochures() -> Bool
    {
        if deleteAllBrochure == nil
        {
            initializeStatement(sqlStatement: &deleteAllBrochure, query: "DELETE FROM Brochure")
        }
        return executeUpdate(sqlStatement: deleteAllBrochure)
    }

    func deleteBrochureForId(brochureId brochureId : Int) -> Bool
    {
        if deleteBrochureForBrochureId == nil
        {
            initializeStatement(sqlStatement: &deleteBrochureForBrochureId, query: "DELETE FROM Brochure WHERE brochureId = ?")
        }
        sqlite3_bind_int(deleteBrochureForBrochureId, 1, CInt(brochureId))
        return executeUpdate(sqlStatement: deleteBrochureForBrochureId)
    }

    //MARK: 4. NotesShopMapping Function

    func insertIntoNotesShopMapping(noteId noteId : Int, shopId : Int, notes :String, createdDate :String, modifiedDate :String) -> Bool
    {
        if insertIntoNotes == nil
        {
            initializeStatement(sqlStatement: &insertIntoNotes, query: "INSERT INTO NotesShopMapping (noteId,shopId,notes,createdDate,modifiedDate) VALUES (?,?,?,?,?)")
        }
        sqlite3_bind_int(insertIntoNotes, 1, CInt(noteId))
        sqlite3_bind_int(insertIntoNotes, 2, CInt(shopId))
        sqlite3_bind_text(insertIntoNotes, 3, (notes as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoNotes, 4, (createdDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoNotes, 5, (modifiedDate as NSString).UTF8String, -1, nil)
        return executeUpdate(sqlStatement: insertIntoNotes)
    }

    func updateIntoNotesShopMapping(noteId noteId : Int, shopId : Int, notes :String, createdDate :String, modifiedDate :String) -> Bool
    {
        if updateNotes == nil
        {
            initializeStatement(sqlStatement: &updateNotes, query: "UPDATE NotesShopMapping SET notes = ?, createdDate =?, modifiedDate = ? WHERE noteId = ? AND shopId = ?")
        }
        sqlite3_bind_text(updateNotes, 1, (notes as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateNotes, 2, (createdDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateNotes, 3, (modifiedDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateNotes, 4, CInt(noteId))
        sqlite3_bind_int(updateNotes, 5, CInt(shopId))
        return executeUpdate(sqlStatement: updateNotes)
    }

    func isNotesForShopExist(noteId noteId : Int, shopId : Int)-> Bool?
    {
        if isNotesExists == nil
        {
            initializeStatement(sqlStatement: &isNotesExists, query: "SELECT noteId,shopId FROM NotesShopMapping WHERE noteId =? AND shopId =?")
        }
        sqlite3_bind_int(isNotesExists, 1, CInt(noteId))
        sqlite3_bind_int(isNotesExists, 2, CInt(shopId))
        var rowPresent = false
        while executeSelect(sqlStatement: isNotesExists)
        {
            rowPresent = true
        }
        sqlite3_reset(isNotesExists)
        return rowPresent
    }

    func getNotesForShopId(shopId shopId : Int) -> [ShopNotesModel]
    {
        if selectNotesForShopId == nil
        {
            initializeStatement(sqlStatement: &selectNotesForShopId, query: "Select noteId,shopId,notes,createdDate,modifiedDate from NotesShopMapping WHERE shopId = ?")
        }
        var shopNotes : [ShopNotesModel] = []
        sqlite3_bind_int(selectNotesForShopId, 1, CInt(shopId))
        while executeSelect(sqlStatement: selectNotesForShopId)
        {
            let note : ShopNotesModel = ShopNotesModel()

            note.id = Int(sqlite3_column_int(selectNotesForShopId, 0))

            note.shopId = Int(sqlite3_column_int(selectNotesForShopId, 1))

            let notes = sqlite3_column_text(selectNotesForShopId, 2)
            let bStringPtr  =   UnsafePointer<Int8>(notes);
            note.notes    =    String.fromCString(bStringPtr);

            let modifiedDate = sqlite3_column_text(selectNotesForShopId, 3)
            let bStringPtr1 =   UnsafePointer<Int8>(modifiedDate);
            note.notes    =    String.fromCString(bStringPtr1);

            let createdDate = sqlite3_column_text(selectNotesForShopId, 4)
            let bStringPtr2  =   UnsafePointer<Int8>(createdDate);
            note.notes    =    String.fromCString(bStringPtr2);

            shopNotes.append(note)
        }
        sqlite3_reset(selectNotesForShopId)
        return shopNotes
    }

    func deleteNotesShopMapping() -> Bool
    {
        if deleteAllNotes == nil
        {
            initializeStatement(sqlStatement: &deleteAllNotes, query: "DELETE FROM NotesShopMapping")
        }
        return executeUpdate(sqlStatement: deleteAllNotes)
    }

    //MARK: 5. VisitedShops Function

    func insertIntoVisitedShops(shopMappingId shopMappingId : Int, shopId : Int, visitDate :String, isVisited :String, isAppDownloaded :String, createdDate :String, modifiedDate :String) -> Bool
    {
        if insertIntoVisitedShops == nil
        {
            initializeStatement(sqlStatement: &insertIntoVisitedShops, query: "INSERT INTO VisitedShops (shopMappingId,shopId,visitDate,isVisited,isAppDownloaded,createdDate,modifiedDate) VALUES (?,?,?,?,?,?,?)")
        }
        sqlite3_bind_int(insertIntoVisitedShops, 1, CInt(shopMappingId))
        sqlite3_bind_int(insertIntoVisitedShops, 2, CInt(shopId))
        sqlite3_bind_text(insertIntoVisitedShops, 3, (visitDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoVisitedShops, 4, (isVisited as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoVisitedShops, 5, (isAppDownloaded as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoVisitedShops, 6, (createdDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(insertIntoVisitedShops, 7, (modifiedDate as NSString).UTF8String, -1, nil)
        return executeUpdate(sqlStatement: insertIntoVisitedShops)
    }

    func updateIntoVisitedShop(shopMappingId shopMappingId : Int, shopId : Int, visitDate :String, isVisited :String, isAppDownloaded :String, createdDate :String, modifiedDate :String) -> Bool
    {
        if updateIntoVisitedShops == nil
        {
            initializeStatement(sqlStatement: &updateIntoVisitedShops, query: "UPDATE VisitedShops SET visitDate = ?, isVisited = ?, isAppDownloaded = ?, createdDate =?, modifiedDate = ? WHERE shopMappingId = ? AND shopId = ?")
        }
        sqlite3_bind_text(updateIntoVisitedShops, 1, (visitDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoVisitedShops, 2, (isVisited as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoVisitedShops, 3, (isAppDownloaded as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoVisitedShops, 4, (createdDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_text(updateIntoVisitedShops, 5, (modifiedDate as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoVisitedShops, 6, CInt(shopMappingId))
        sqlite3_bind_int(updateIntoVisitedShops, 7, CInt(shopId))
        return executeUpdate(sqlStatement: updateIntoVisitedShops)
    }

    func isVisitedShopExist(shopMappingId shopMappingId : Int, shopId : Int)-> Bool?
    {
        if isVisitedShopsExists == nil
        {
            initializeStatement(sqlStatement: &isVisitedShopsExists, query: "SELECT shopMappingId,shopId FROM VisitedShops WHERE shopMappingId =? AND shopId =?")
        }
        sqlite3_bind_int(isVisitedShopsExists, 1, CInt(shopMappingId))
        sqlite3_bind_int(isVisitedShopsExists, 2, CInt(shopId))
        var rowPresent = false
        while executeSelect(sqlStatement: isVisitedShopsExists)
        {
            rowPresent = true
        }
        sqlite3_reset(isVisitedShopsExists)
        return rowPresent
    }

    //MARK: 6. BusinessCategory Function

    func insertIntoBusinessCategory(businessCategoryId businessCategoryId : Int, businessCategoryName : String) -> Bool
    {
        if insertIntoBusinessCategory == nil
        {
            initializeStatement(sqlStatement: &insertIntoBusinessCategory, query: "INSERT INTO BusinessCategory (businessCategoryId,businessCategoryName,isChecked) VALUES (?,?,?)")
        }
        sqlite3_bind_int(insertIntoBusinessCategory, 1, CInt(businessCategoryId))
        sqlite3_bind_text(insertIntoBusinessCategory, 2, (businessCategoryName as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(insertIntoBusinessCategory, 3, CInt(0))
        return executeUpdate(sqlStatement: insertIntoBusinessCategory)
    }

    func updateIntoBusinessCategory(businessCategoryId businessCategoryId : Int, businessCategoryName : String, isChecked :Int) -> Bool
    {
        if updateIntoBusinessCategory == nil
        {
            initializeStatement(sqlStatement: &updateIntoBusinessCategory, query: "UPDATE BusinessCategory SET businessCategoryName = ?, isChecked = ? WHERE businessCategoryId = ?")
        }
        sqlite3_bind_text(updateIntoBusinessCategory, 1, (businessCategoryName as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoBusinessCategory, 2, CInt(isChecked))
        sqlite3_bind_int(updateIntoBusinessCategory, 3, CInt(businessCategoryId))
        return executeUpdate(sqlStatement: updateIntoBusinessCategory)
    }

    func isBusinessCategoryExist(businessCategoryId businessCategoryId : Int)-> Bool?
    {
        if iSBusinessCategoryExists == nil
        {
            initializeStatement(sqlStatement: &iSBusinessCategoryExists, query: "SELECT businessCategoryId FROM BusinessCategory WHERE businessCategoryId = ?")
        }
        sqlite3_bind_int(iSBusinessCategoryExists, 1, CInt(businessCategoryId))
        var rowPresent = false
        while executeSelect(sqlStatement: iSBusinessCategoryExists)
        {
            rowPresent = true
        }
        sqlite3_reset(iSBusinessCategoryExists)
        return rowPresent
    }

    func selectAllFromBusinessCategory() -> [BusinessCategoryModel]
    {
        if selectAllBusinessCategory == nil
        {
            initializeStatement(sqlStatement: &selectAllBusinessCategory, query: "SELECT * FROM BusinessCategory")
        }
        
        
        var businessCategories : [BusinessCategoryModel] = []
        
        while executeSelect(sqlStatement: selectAllBusinessCategory)
        {
            let businessCategory = BusinessCategoryModel()
            
            businessCategory.businessCategoryId = Int(sqlite3_column_int(selectAllBusinessCategory, 0))
            
            let BusinessCategoryName            =  UnsafePointer<Int8> (sqlite3_column_text(selectAllBusinessCategory,1))
            businessCategory.businessCategoryName   =    String.fromCString(BusinessCategoryName)!;
                        let isChecked = Int(sqlite3_column_int(selectAllBusinessCategory, 2))
            
            if isChecked == 1{
                businessCategory.isSelected = true
            }
            else{
                businessCategory.isSelected = false
            }
            
            
          
            businessCategory.initials  =  getInitialsFromBusinessCategoryString(businessCategory.businessCategoryName!)
            
            businessCategories.append(businessCategory)

        }
        sqlite3_reset(selectAllBusinessCategory)
        return businessCategories
    }

    func deleteBusinessCategory() -> Bool
    {
        if deleteAllBusinessCategory == nil
        {
            initializeStatement(sqlStatement: &deleteAllBusinessCategory, query: "DELETE FROM BusinessCategory")
        }
        return executeUpdate(sqlStatement: deleteAllBusinessCategory)
    }

    func updateAllBusinessCategoryCheckedToIdeal() -> Bool
    {
        if updateBusnessCategoryToZero == nil
        {
            initializeStatement(sqlStatement: &updateBusnessCategoryToZero, query: "UPDATE BusinessCategory SET isChecked = 0")
        }
        return executeUpdate(sqlStatement: updateBusnessCategoryToZero)
    }



    func updateBusinessCategoryAsUnChecked(businessCategoryId businessCategoryId : Int) -> Bool
    {
        if updateBusinessToUnChecked == nil
        {
            initializeStatement(sqlStatement: &updateBusinessToUnChecked, query: "UPDATE BusinessCategory SET isChecked = 0 WHERE businessCategoryId = ?")
        }
        sqlite3_bind_int(updateBusinessToUnChecked, 1, CInt(businessCategoryId))
        return executeUpdate(sqlStatement: updateBusinessToUnChecked)
    }



    func updateBusinessCategoryAsChecked(businessCategoryId businessCategoryId : Int) -> Bool
    {
        if updateBusinessToChecked == nil
        {
            initializeStatement(sqlStatement: &updateBusinessToChecked, query: "UPDATE BusinessCategory SET isChecked = 1 WHERE businessCategoryId = ?")
        }
        sqlite3_bind_int(updateBusinessToChecked, 1, CInt(businessCategoryId))
        return executeUpdate(sqlStatement: updateBusinessToChecked)
    }

    func getAllCheckedBusinessCategories() -> [BusinessCategoryModel]
    {
        if selectAllCheckedBusniessCategory == nil
        {
            initializeStatement(sqlStatement: &selectAllCheckedBusniessCategory, query: "Select * from BusinessCategory WHERE isChecked = 1")
        }
        var businessCategories : [BusinessCategoryModel] = []
        while executeSelect(sqlStatement: selectAllCheckedBusniessCategory)
        {
            let businessCategory : BusinessCategoryModel = BusinessCategoryModel()

            businessCategory.businessCategoryId = Int(sqlite3_column_int(selectAllCheckedBusniessCategory, 0))

            let businesscategoryName = sqlite3_column_text(selectAllCheckedBusniessCategory, 1)
            let bStringPtr  =   UnsafePointer<Int8>(businesscategoryName);
            businessCategory.businessCategoryName    =    String.fromCString(bStringPtr);
            

            let isChecked = Int(sqlite3_column_int(selectAllCheckedBusniessCategory, 2))
           
            if isChecked == 1{
                businessCategory.isSelected = true
            }
            else{
                businessCategory.isSelected = false
            }

            businessCategories.append(businessCategory)
        }
        sqlite3_reset(selectAllCheckedBusniessCategory)
        return businessCategories
    }


    func getCountOfSelectedBusinessCategories()-> Int
    {
        if getCheckedBusinessCount == nil
        {
            initializeStatement(sqlStatement: &getCheckedBusinessCount, query: "SELECT COUNT(businessCategoryId) FROM BusinessCategory WHERE isChecked = 1")
        }

        var businessCategoryIdCount = 0
        while executeSelect(sqlStatement: getCheckedBusinessCount)
        {
            businessCategoryIdCount = Int(sqlite3_column_int(getCheckedBusinessCount, 0))
        }
        sqlite3_reset(getCheckedBusinessCount)
        return businessCategoryIdCount
    }

    //MARK: 7. SelectedBusinessShop Function

    func insertIntoSelectedBusinessShop(shopId shopId : Int, businessCategoryId : Int , businessCategoryName : String, isChecked :Int) -> Bool
    {
        if insertIntoSelectedBusinessShop == nil
        {
            initializeStatement(sqlStatement: &insertIntoSelectedBusinessShop, query: "INSERT INTO SelectedBusinessShop (shopId,businessCategoryId,businessCategoryName,isChecked) VALUES (?,?,?,?)")
        }
        sqlite3_bind_int(insertIntoSelectedBusinessShop, 1, CInt(shopId))
        sqlite3_bind_int(insertIntoSelectedBusinessShop, 2, CInt(businessCategoryId))
        sqlite3_bind_text(insertIntoSelectedBusinessShop, 3, (businessCategoryName as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(insertIntoSelectedBusinessShop, 4, CInt(isChecked))
        return executeUpdate(sqlStatement: insertIntoSelectedBusinessShop)
    }

    func updateIntoSelectedBusinessShop(shopId shopId : Int, businessCategoryId : Int , businessCategoryName : String, isChecked :Int) -> Bool
    {
        if updateIntoSelectedBusinessShop == nil
        {
            initializeStatement(sqlStatement: &updateIntoSelectedBusinessShop, query: "UPDATE SelectedBusinessShop SET businessCategoryName = ?, isChecked = ? WHERE shopId = ? And businessCategoryId = ?")
        }
        sqlite3_bind_text(updateIntoSelectedBusinessShop, 1, (businessCategoryName as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(updateIntoSelectedBusinessShop, 2, CInt(isChecked))
        sqlite3_bind_int(updateIntoSelectedBusinessShop, 3, CInt(shopId))
        sqlite3_bind_int(updateIntoSelectedBusinessShop, 4, CInt(businessCategoryId))
        return executeUpdate(sqlStatement: updateIntoSelectedBusinessShop)
    }

    func isBusinessCategoryExistForShop(shopId shopId : Int, businessCategoryId: Int) -> Bool?
    {
        if selectAllSelectedBusinessCategory == nil
        {
            initializeStatement(sqlStatement: &selectAllSelectedBusinessCategory, query: "SELECT * FROM SelectedBusinessShop WHERE shopId = ? AND businessCategoryId = ?")
        }
        sqlite3_bind_int(selectAllSelectedBusinessCategory, 1, CInt(shopId))
        sqlite3_bind_int(selectAllSelectedBusinessCategory, 2, CInt(businessCategoryId))
        var rowPresent = false
        while executeSelect(sqlStatement: selectAllSelectedBusinessCategory)
        {
            rowPresent = true
        }
        sqlite3_reset(selectAllSelectedBusinessCategory)
        return rowPresent
    }

    func selectAllSelectedBusinessCategory(shopId shopId : Int) -> [BusinessCategoryModel]
    {
        if selectAllSelectedBusinessFromBusinessCategory == nil
        {
            initializeStatement(sqlStatement: &selectAllSelectedBusinessFromBusinessCategory, query: "SELECT * FROM SelectedBusinessShop WHERE shopId =?")
        }
        var businessCategories : [BusinessCategoryModel] = []
        sqlite3_bind_int(selectAllSelectedBusinessFromBusinessCategory, 1, CInt(shopId))
        while executeSelect(sqlStatement: selectAllSelectedBusinessFromBusinessCategory)
        {
            let businessCategory : BusinessCategoryModel = BusinessCategoryModel()

            businessCategory.shopId = Int(sqlite3_column_int(selectAllSelectedBusinessFromBusinessCategory, 0))

            businessCategory.businessCategoryId = Int(sqlite3_column_int(selectAllSelectedBusinessFromBusinessCategory, 1))

            let businesscategoryName = sqlite3_column_text(selectAllSelectedBusinessFromBusinessCategory, 2)
            let bStringPtr  =   UnsafePointer<Int8>(businesscategoryName);
            businessCategory.businessCategoryName    =    String.fromCString(bStringPtr);

            businessCategory.isChecked = Int(sqlite3_column_int(selectAllSelectedBusinessFromBusinessCategory, 3))

            businessCategories.append(businessCategory)
        }
        sqlite3_reset(selectAllSelectedBusinessFromBusinessCategory)
        return businessCategories
    }

    func selectAllCheckedSelectedBusinessCategory(shopId shopId : Int) -> [BusinessCategoryModel]
    {
        if selectAllSelectedBusinessFromBusinessCategoryChecked == nil
        {
            initializeStatement(sqlStatement: &selectAllSelectedBusinessFromBusinessCategoryChecked, query: "SELECT * FROM SelectedBusinessShop WHERE shopId =? AND isChecked = 1")
        }
        var businessCategories : [BusinessCategoryModel] = []
        sqlite3_bind_int(selectAllSelectedBusinessFromBusinessCategoryChecked, 1, CInt(shopId))
        while executeSelect(sqlStatement: selectAllSelectedBusinessFromBusinessCategoryChecked)
        {
            let businessCategory : BusinessCategoryModel = BusinessCategoryModel()

            businessCategory.shopId = Int(sqlite3_column_int(selectAllSelectedBusinessFromBusinessCategoryChecked, 0))

            businessCategory.businessCategoryId = Int(sqlite3_column_int(selectAllSelectedBusinessFromBusinessCategoryChecked, 1))

            let businesscategoryName = sqlite3_column_text(selectAllSelectedBusinessFromBusinessCategoryChecked, 2)
            let bStringPtr  =   UnsafePointer<Int8>(businesscategoryName);
            businessCategory.businessCategoryName    =    String.fromCString(bStringPtr);

            businessCategory.isChecked = Int(sqlite3_column_int(selectAllSelectedBusinessFromBusinessCategoryChecked, 3))

            businessCategories.append(businessCategory)
        }
        sqlite3_reset(selectAllSelectedBusinessFromBusinessCategoryChecked)
        return businessCategories
    }

    func deleteSelectedBusinessShopForId(shopId shopId : Int) -> Bool
    {
        if deleteAllSelectedBusinessShopForShopId == nil
        {
            initializeStatement(sqlStatement: &deleteAllSelectedBusinessShopForShopId, query: "DELETE FROM SelectedBusinessShop WHERE shopId = ?")
        }
        sqlite3_bind_int(deleteAllSelectedBusinessShopForShopId, 1, CInt(shopId))
        return executeUpdate(sqlStatement: deleteAllSelectedBusinessShopForShopId)
    }

    func deleteSelectedBusinessShop() -> Bool
    {
        if deleteAllSelectedBusinessShop == nil
        {
            initializeStatement(sqlStatement: &deleteAllSelectedBusinessShop, query: "DELETE FROM SelectedBusinessShop")
        }
        return executeUpdate(sqlStatement: deleteAllSelectedBusinessShop)
    }

    func getBusinessCategoryCountByShop(shopId shopId : Int)-> Int?
    {
        if selectCountFromSelectedBusinessShop == nil
        {
            initializeStatement(sqlStatement: &deleteAllSelectedBusinessShop, query: "SELECT COUNT(businessCategoryId) FROM SelectedBusinessShop WHERE shopId = ?")
        }
        var categoryIdCount : Int = Int()
        sqlite3_bind_int(selectCountFromSelectedBusinessShop, 1, CInt(shopId))
        while executeSelect(sqlStatement: selectCountFromSelectedBusinessShop)
        {
            categoryIdCount = Int(sqlite3_column_int(selectCountFromSelectedBusinessShop, 0))
        }
        sqlite3_reset(selectCountFromSelectedBusinessShop)
        return categoryIdCount
    }

    //MARK: 8. State Function

    func insertIntoState(stateId stateId : Int, stateName : String) -> Bool
    {
        if insertIntoState == nil
        {
            initializeStatement(sqlStatement: &insertIntoState, query: "INSERT INTO State (stateId,stateName) VALUES (?,?)")
        }
        sqlite3_bind_int(insertIntoState, 1, CInt(stateId))
        sqlite3_bind_text(insertIntoState, 2, (stateName as NSString).UTF8String, -1, nil)
        return executeUpdate(sqlStatement: insertIntoState)
    }

    func selectAllFromState() -> [GetStateModel]
    {
        if selectAllState == nil
        {
            initializeStatement(sqlStatement: &selectAllState, query: "SELECT * FROM State")
        }
        var models : [GetStateModel] = []
        while executeSelect(sqlStatement: selectAllState)
        {
            let model : GetStateModel = GetStateModel()

            model.stateId = Int(sqlite3_column_int(selectAllState, 0))

            let stateName = sqlite3_column_text(selectAllState, 1)
            let sStringPtr1  =   UnsafePointer<Int8>(stateName);
            model.stateName    =    String.fromCString(sStringPtr1);

            models.append(model)
        }
        sqlite3_reset(selectAllState)
        return models
    }

    func isStateExists(stateId stateId : Int)-> Bool?
    {
        if isStateExists == nil
        {
             initializeStatement(sqlStatement: &isStateExists, query: "SELECT stateId FROM State WHERE stateId = ?")
        }
        sqlite3_bind_int(isStateExists, 1, CInt(stateId))
        var rowPresent = false
        while executeSelect(sqlStatement: isStateExists)
        {
            rowPresent = true
        }
        sqlite3_reset(isStateExists)
        return rowPresent
    }

    func getStateId(stateName stateName : String)-> Int?
    {
        if selectIdFromStateName == nil
        {
            initializeStatement(sqlStatement: &selectIdFromStateName, query: "SELECT * FROM State WHERE stateName = ?")
        }
        var stateId : Int = Int()
        sqlite3_bind_text(selectIdFromStateName, 1, (stateName as NSString).UTF8String, -1, nil)
        while executeSelect(sqlStatement: selectIdFromStateName)
        {
            stateId = Int(sqlite3_column_int(selectIdFromStateName, 0))
        }
        sqlite3_reset(selectIdFromStateName)
        return stateId
    }

    func deleteStates() -> Bool
    {
        if deleteAllState == nil
        {
            initializeStatement(sqlStatement: &deleteAllState, query: "DELETE FROM State")
        }
        return executeUpdate(sqlStatement: deleteAllState)
    }

    //MARK: Get initial two characters from businessCategory String

    func getInitialsFromBusinessCategoryString(categoryString : String) -> String
    {
        var initials : String? = ""
        let removedSpecialCharaterString = removeSpecialCharsFromString(categoryString)
        let newBusinessCategoryString = removedSpecialCharaterString.stringByReplacingOccurrencesOfString("and", withString: "")
        let finalBusinessCategoryString = newBusinessCategoryString.stringByReplacingOccurrencesOfString("  ", withString: " ")

        let fullNameString = finalBusinessCategoryString.characters.split{$0 == " "}.map(String.init)

        initials = String(fullNameString[0].characters.first!)
        if fullNameString.count > 1
        {
            initials = initials! + String(fullNameString[1].characters.first!)
        }
        return initials!

    }

    //MARK: Remove special characters from string

    func removeSpecialCharsFromString(text: String) -> String
    {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
        
    }

}



