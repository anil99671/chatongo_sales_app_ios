//
//  CountryCodeDB.swift
//  JSONPractice
//
//  Created by Apple on 09/05/16.
//  Copyright © 2016 Nimapinfotech. All rights reserved.
//

import UIKit

class CountryCodeDBModel: SQLiteDataBase
{
    
    //MARK: CountryCode Query Statement
    //name TEXT,countryId INTEGER
    
    var insertIntoCountryCode : COpaquePointer = nil
    var selectNameFromDialCode : COpaquePointer = nil
    var selectAllCountryCode : COpaquePointer = nil
    
    //MARK: Singleton Methods
    
    struct Static
    {
        static var onceToken : dispatch_once_t = 0
        static var instance : CountryCodeDBModel? = nil
    }
    
    class func sharedInstance() -> CountryCodeDBModel?
    {
        
        dispatch_once(&Static.onceToken)
            {
                Static.instance = CountryCodeDBModel()
        }
        
        return Static.instance
    }
    
    init()
    {
        
        assert(Static.instance == nil, "Attempted to create second instance of CountryCodeDBModel")
        
        super.init(dbFileName: "CountryCodesDB.sqlite", deleteEditableCopy: false)
        
        if sqlite3_open((writableDBPath! as NSString).UTF8String, &database) == SQLITE_OK
        {
            print("CountryCodesDB.sqlite Initialized")
        }
    }
    
    //MARK: CountryCode Query methods
    
    func insertIntoCountryCode(countryName countryName : String,countryId : Int) -> Bool
    {
        if insertIntoCountryCode == nil
        {
            initializeStatement(sqlStatement: &insertIntoCountryCode, query: "INSERT INTO CountryCode (name,countryId) VALUES (?,?)")
        }
        sqlite3_bind_text(insertIntoCountryCode, 1, (countryName as NSString).UTF8String, -1, nil)
        sqlite3_bind_int(insertIntoCountryCode, 2, CInt(countryId))
        return executeUpdate(sqlStatement: insertIntoCountryCode)
    }
    
    func isCountryCodeExist(countryCode countryCode : Int) -> Bool?
    {
        if selectNameFromDialCode == nil
        {
            initializeStatement(sqlStatement: &selectNameFromDialCode, query: "SELECT name FROM CountryCode WHERE countryId = ?")
        }
        sqlite3_bind_int(selectNameFromDialCode, 1, CInt(countryCode))
        var rowPresent = false
        while executeSelect(sqlStatement: selectNameFromDialCode)
        {
            rowPresent = true
        }
        sqlite3_reset(selectNameFromDialCode)
        return rowPresent
    }
    
//    func getAllCountryCodes()-> [GetCountryModel]
//    {
//        if selectAllCountryCode == nil
//        {
//            initializeStatement(sqlStatement: &selectAllCountryCode, query: "SELECT * FROM CountryCode")
//        }
//        var countryDetails : [GetCountryModel] = []
//        
//        while executeSelect(sqlStatement: selectAllCountryCode)
//        {
//            let country : GetCountryModel = GetCountryModel()
//            
//            let countryName = sqlite3_column_text(selectAllCountryCode, 0)
//            let gStringPtr1  =   UnsafePointer<Int8>(countryName);
//            country.countryName    =    String.fromCString(gStringPtr1);
//            
//            country.countryId    =    Int(sqlite3_column_int(selectAllCountryCode, 1))
//            
//            countryDetails.append(country)
//        }
//        sqlite3_reset(selectAllCountryCode)
//        return countryDetails
//    }
}
