//
//  ShopVisitedParser.swift
//  COG Sales
//
//  Created by Apple on 08/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

protocol ShopVisitedParserDelegate : NSObjectProtocol
{
    func didReceivedShopVisitedStatus(status : Int)
}

class ShopVisitedParser: NSObject, AsyncLoaderModelDelegate
{

    var loader : AsyncLoaderModel?
    weak var delegate : ShopVisitedParserDelegate?

    func shopVisited(shopId shopId : Int, note : String, isAppDownload : Bool, latitude : Double, longitude : Double)
    {
        loader = AsyncLoaderModel()

        let jsonDictionary = NSMutableDictionary()

        jsonDictionary.setValue(0, forKey: "Id")
        jsonDictionary.setValue(shopId, forKey: "ShopId")
        jsonDictionary.setValue(note, forKey: "Note")
        jsonDictionary.setValue(true, forKey: "IsVisited")
        jsonDictionary.setValue(isAppDownload, forKey: "IsAppDownloaded")
        jsonDictionary.setValue(latitude, forKey: "Latitude")
        jsonDictionary.setValue(longitude, forKey: "Longitude")

        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options: NSJSONWritingOptions.PrettyPrinted)
        } catch{
            jsonData = nil
        }
                let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
                print("ShopVisited requestString is \(requestString!)")

        let jsonDataLenght = "\(jsonData!.length)"

        let url = NSURL(string: AppConstant.Static.BASE_URL+"ShopVisited")

        let request = NSMutableURLRequest(URL: url!)

        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")

        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData

        loader!.getDataFromRequest(request: request, dataIndex: -1)
        loader!.delegate = self
    }

    //MARK: AsyncLoaderModelDelegate methods
    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("AddShopNote responseString \(responseString!)")
        processData(data: data)
        self.loader = nil
    }

    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            delegate!.didReceivedShopVisitedStatus(AppConstant.Static.CONNECTION_ERROR)
        }

        self.loader = nil
    }

    //MARK: Local Methods : Processing methods

    func processData(data data : NSData)
    {
        var result : NSDictionary!

        do{

            try result = NSJSONSerialization.JSONObjectWithData(data
                , options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let status = result!.objectForKey("Status") as! Int

            if status == 200
            {
                let salesDB = SalesAppDBModel.sharedInstance()
                let shopVisitedModel = ShopVisitedModel()

                let results = result!.objectForKey("data") as! NSDictionary

                shopVisitedModel.id = results.objectForKey("Id") as? Int
                shopVisitedModel.shopId = results.objectForKey("ShopId") as? Int
                shopVisitedModel.VisitDate = results.objectForKey("VisitDate") as? String
                shopVisitedModel.isVisited = String(results.objectForKey("IsVisited") as! Bool)
                shopVisitedModel.isAppDownloaded = String(results.objectForKey("IsAppDownloaded") as! Bool)
                shopVisitedModel.createdDate = results.objectForKey("CreatedDate") as? String
                shopVisitedModel.modifiedDate = results.objectForKey("ModifiedDate") as? String
                //shopVisitedModel.osType = results.objectForKey("OsType") as! String
               
                shopVisitedModel.modifiedDate = shopVisitedModel.modifiedDate!.stringByReplacingOccurrencesOfString("/", withString: "-")

               salesDB!.updateShopWithIsVisited(shopId: shopVisitedModel.shopId!, visitDate: shopVisitedModel.VisitDate!, isVisited: "True", isAppDownloaded: shopVisitedModel.isAppDownloaded!)

                let noteDict = results.objectForKey("Notes") as! [NSDictionary]

                for note in noteDict
                {
                    let noteModel = ShopNotesModel()
                    noteModel.id = note.objectForKey("Id") as? Int
                    noteModel.shopId = note.objectForKey("ShopId") as? Int
                    noteModel.notes = note.objectForKey("Notes") as? String
                    noteModel.createdDate = note.objectForKey("CreatedDate") as? String
                    noteModel.modifiedDate = note.objectForKey("ModifiedDate") as? String

                    if (salesDB!.isNotesForShopExist(noteId: noteModel.id!, shopId: noteModel.shopId!)) == true
                    {
                        salesDB!.updateIntoNotesShopMapping(noteId: noteModel.id!, shopId: noteModel.shopId!, notes: noteModel.notes!, createdDate: noteModel.createdDate!, modifiedDate: noteModel.modifiedDate!)
                    }
                    else
                    {
                        salesDB!.insertIntoNotesShopMapping(noteId: noteModel.id!, shopId: noteModel.shopId!, notes: noteModel.notes!, createdDate: noteModel.createdDate!, modifiedDate: noteModel.modifiedDate!)
                    }
                }

                if delegate != nil
                {
                    delegate!.didReceivedShopVisitedStatus(AppConstant.Static.VISITED_SHOP_SUCCESS)
                }
            }
            else
            {
                if delegate != nil
                {
                    delegate!.didReceivedShopVisitedStatus(AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            // Error Occured
            if delegate != nil
            {
                delegate!.didReceivedShopVisitedStatus(AppConstant.Static.PROCESSING_ERROR)
            }
        }
        
        self.loader = nil
    }
}

