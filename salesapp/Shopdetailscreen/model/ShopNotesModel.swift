//
//  ShopNotesModel.swift
//  VendorAppDB
//
//  Created by Apple on 05/07/16.
//  Copyright © 2016 nimap. All rights reserved.
//

import UIKit

class ShopNotesModel: NSObject {
    var id : Int?
    var shopId : Int?
    var notes : String?
    var createdDate : String?
    var modifiedDate : String?
    var isExpanded : Bool?
}