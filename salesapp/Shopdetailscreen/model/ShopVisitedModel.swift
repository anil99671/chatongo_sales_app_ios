//
//  ShopVisitedModel.swift
//  COG Sales
//
//  Created by Apple on 08/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class ShopVisitedModel: NSObject {
    var id : Int?
    var shopId : Int?
    var VisitDate : String?
    var isVisited : String?
    var isAppDownloaded : String?
    var createdDate : String?
    var modifiedDate : String?
    var osType :String?
}