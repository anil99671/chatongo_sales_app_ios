//
//  ShopDetailViewController.swift
//  COG Sales
//
//  Created by Apple on 01/06/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ShopDetailViewController: UIViewController, NimapNavigationBarViewDelegate, NimapAlertViewDelegate, UIMapViewDelegate, AddShopViewControllerDelegate, ShopVisitedParserDelegate,GetBrochureByShopParserDelegate, LocationManagerDelegate {

    static let GOOGLE_MAP_HEIGHT_PERCENTAGE : CGFloat = 0.3
    static let GOOGLE_MAP_HEIGHT_PERCENTAGE_IPAD : CGFloat = 0.3
    static let TOTAL_GRID_ELEMENTS = 5

    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?

    var alert : NimapAlertView?

    var shopID : Int? = -1
    var shop: ShopModel?

    var pageHeight : CGFloat?
    var navigationHeight : CGFloat = 0.0

    var googleMapHeight : CGFloat?
    var labelWidth : CGFloat = 0.0
    var labelHeight : CGFloat = 0.0
    var padding : CGFloat = 0.0
    var paddingY : CGFloat = 0.0
    var buttonHeight : CGFloat = 0.0
    var googleMapView : UIMapView?

    var salesDB : SalesAppDBModel?

    var shopLabel : UILabel?
    var addressLabel : UILabel?
    var shopAddressLabel : UILabel?
    var mobileLabel : UILabel?
    var shopMobileLabel : UILabel?
    var visitingCardLabel : UILabel?
    var visitedButton : UIButton?
    var addNotesButton : UIButton?
    var isAppLabel : UILabel?
    var appCheckBoxLabel : UILabel?
    var noBrochureImageView : UIImageView?
    var brochureGridView : UIView?

    var brochures : [BrochureModel]?

    var shopVisitedParser : ShopVisitedParser?
    var getVisitingParser : GetBrochureByShopParser?

    var isAppDownloaded : Bool = false
    var noteString : String = ""
    var visitingCardCount = 0
    var noBrochureCenterY : CGFloat = 0.0

    var enteredNotes : String? = "na"

    var searchShopScreen : SearchScreenViewController?
    var addShopController : AddShopViewController?
    var addVisitingContorller : AddVisitingCardViewController?

    //Location service variables

    var locationManager : LocationManager?
    var latitude : Double?
    var longitude : Double?

    //MARK: Constructor

    init(shopID : Int, latitude : Double, longitude : Double)
    {
        self.shopID = shopID
        self.latitude = latitude
        self.longitude = longitude

        super.init(nibName: nil, bundle: nil)

        deviceManager = DeviceManager.sharedDeviceManagement()
        salesDB = SalesAppDBModel.sharedInstance()

        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None

        buttonHeight = deviceManager!.deviceYCGFloatValue(yPos: 35.0)
    }


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        loadPage()

    }

    override func viewDidAppear(animated: Bool) {
        searchShopScreen = nil
    }
    override func viewDidDisappear(animated: Bool) {

        unloadPage()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    //MARK: Loading Methods

    func loadPage()
    {
        googleMapHeight = ShopDetailViewController.GOOGLE_MAP_HEIGHT_PERCENTAGE * self.view.frame.size.height

        if deviceManager!.deviceType == deviceManager!.iPad{
            googleMapHeight = ShopDetailViewController.GOOGLE_MAP_HEIGHT_PERCENTAGE_IPAD * self.view.frame.size.height
        }

        shop = salesDB!.selectShopForId(shopId: shopID!)

        loadNavigationBar()
        if shop != nil {

            loadGoogleMapView()
            loadShopElements()
        }

        loadButtons()
        loadBrochureParser()
    }

    func loadBrochureParser(){

        if NSUserDefaults.standardUserDefaults().boolForKey("isShopBrochureDownloaded") == false{

            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }

            alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
            alert!.delegate = self
            self.view.addSubview(alert!)

            if getVisitingParser == nil{

                getVisitingParser =  GetBrochureByShopParser()
                getVisitingParser!.delegate = self
                getVisitingParser!.getAllBrochureData(shopId: shopID!)
            }
        }
        else{
            loadBrochureGrid()
        }

    }

    func loadNavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }

            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title: shop!.name!)
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)

            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)

            navigationBar!.addRightButtonWithIcon()
            navigationBar!.updateRightButtonTitle(AppConstant.Static.NOTES_ICON)

            navigationHeight = navigationBar!.frame.size.height
            pageHeight = self.view.frame.height - navigationHeight
        }

    }

    func loadGoogleMapView()
    {
        if googleMapView == nil {

            googleMapView = UIMapView(frame: CGRectMake(0,navigationHeight,self.view.frame.size.width,googleMapHeight!),showInitialView:true, initials:shop!.initials!)
            googleMapView!.delegate = self
            self.view.addSubview(googleMapView!)

            // Need to add Annotation

            googleMapView!.addAnnotation(shop!.latitude!, longitude: shop!.longitude!, title: shop!.name!, subtitle: shop!.streetAddress!, icon: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "yes"))!, index: 0)

            googleMapView!.addMapAnnotations()
            googleMapView!.centerMapWithAnnotation(0)
            googleMapView!.showAutoMapAnnotationViewForPinIndex(0)

        }
    }

    func loadShopElements()
    {
        labelWidth = 0.3 * self.view.frame.size.width
        labelHeight = deviceManager!.deviceYCGFloatValue(yPos: 20.0)
        padding = deviceManager!.deviceYCGFloatValue(yPos: 8.0)
        paddingY = deviceManager!.deviceYCGFloatValue(yPos: 4.0)

        var yPos = navigationHeight + googleMapHeight! + padding

        shopLabel = UILabel(frame:
            CGRectMake(padding,yPos,labelWidth,labelHeight))

        shopLabel!.text = "Shop Details"

        shopLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        shopLabel!.textColor = UIColor.primaryColor()

        self.view.addSubview(shopLabel!)

        yPos = yPos + labelHeight + paddingY

        addressLabel = UILabel(frame:
            CGRectMake(padding,yPos,labelWidth,labelHeight-deviceManager!.deviceYCGFloatValue(yPos: 6)))
        addressLabel!.text = "Address:"

        addressLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        addressLabel!.textColor = UIColor.primaryColor()

        self.view.addSubview(addressLabel!)

        shopAddressLabel = UILabel(frame:
            CGRectMake(labelWidth + padding,yPos,(self.view.frame.size.width - ( labelWidth + padding + padding)),self.view.frame.size.height))
        shopAddressLabel!.text = shop!.streetAddress!
        if deviceManager!.deviceType == deviceManager!.iPhone{
            shopAddressLabel!.numberOfLines = 2
        }
        else{
            shopAddressLabel!.numberOfLines = 3
        }
        shopAddressLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        shopAddressLabel!.textColor = UIColor.secondaryTextColor()
        self.view.addSubview(shopAddressLabel!)

        shopAddressLabel!.sizeToFit()
        shopAddressLabel!.frame = CGRectMake(labelWidth + padding,yPos,(self.view.frame.size.width - ( labelWidth + padding + padding)),shopAddressLabel!.frame.size.height)

        yPos = yPos + shopAddressLabel!.frame.size.height + paddingY

        mobileLabel = UILabel(frame:
            CGRectMake(padding,yPos,labelWidth,labelHeight))

        mobileLabel!.text = "Mobile No:"

        mobileLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        mobileLabel!.textColor = UIColor.primaryColor()

        self.view.addSubview(mobileLabel!)

        shopMobileLabel = UILabel(frame:
            CGRectMake(labelWidth + padding,yPos,(self.view.frame.size.width - ( labelWidth + padding + padding)),labelHeight))

        shopMobileLabel!.text = shop!.mobileNo

        shopMobileLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        shopMobileLabel!.textColor = UIColor.secondaryTextColor()

        self.view.addSubview(shopMobileLabel!)

        yPos = yPos + labelHeight + paddingY

        visitingCardLabel = UILabel(frame:
            CGRectMake(padding,yPos,labelWidth,labelHeight))

        visitingCardLabel!.text = "Visiting Card"

        visitingCardLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        visitingCardLabel!.textColor = UIColor.primaryColor()

        self.view.addSubview(visitingCardLabel!)

        yPos = yPos + labelHeight + paddingY

        noBrochureImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "nobrochure")))
        noBrochureImageView!.userInteractionEnabled = true
        noBrochureImageView!.center = CGPointMake(self.view.center.x, yPos + (noBrochureImageView!.frame.size.height/2.0))
        self.view.addSubview(noBrochureImageView!)

        noBrochureCenterY = noBrochureImageView!.center.y
//        ratingViewPosY = yPos + labelHeight + paddingY + (noBrochureImageView!.frame.size.height/2.0) + paddingY

        yPos = yPos + noBrochureImageView!.frame.height + paddingY

        isAppLabel = UILabel(frame: CGRectMake(padding,yPos,deviceManager!.deviceXCGFloatValue(xPos: 140),labelHeight))
        isAppLabel!.text = "Is App Downloaded ?"
        isAppLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        isAppLabel!.textColor = UIColor.secondaryTextColor()
        self.view.addSubview(isAppLabel!)

        let posX = padding + isAppLabel!.frame.width + deviceManager!.deviceXCGFloatValue(xPos: 8.0)

        appCheckBoxLabel = UILabel(frame: CGRectMake(posX,yPos,labelHeight,labelHeight))
        appCheckBoxLabel!.userInteractionEnabled = true
        appCheckBoxLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        appCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
        appCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
        self.view.addSubview(appCheckBoxLabel!)
    }

    func loadButtons()
    {
        let gap = deviceManager!.deviceXCGFloatValue(xPos: 1.0)
        let buttonWidth = (self.view.frame.width - gap) * 0.5

        visitedButton = UIButton(frame:CGRectMake(0,self.view.frame.height - buttonHeight,buttonWidth,buttonHeight))
        visitedButton!.setTitle("Visited", forState: UIControlState.Normal)
        visitedButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        visitedButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        visitedButton!.backgroundColor = UIColor.primaryColor()
        visitedButton!.addTarget(self, action: #selector(ShopDetailViewController.visitButtonClicked), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(visitedButton!)

        let buttonXPosition = buttonWidth + gap

        addNotesButton = UIButton(frame:CGRectMake(buttonXPosition,visitedButton!.frame.origin.y,buttonWidth,buttonHeight))
        addNotesButton!.setTitle("Add Notes", forState: UIControlState.Normal)
        addNotesButton!.titleLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 15.0))
        addNotesButton!.setTitleColor(UIColor.iconsColor(), forState: UIControlState.Normal)
        addNotesButton!.backgroundColor = UIColor.primaryColor()
        addNotesButton!.addTarget(self, action: #selector(ShopDetailViewController.addNotesButtonClicked), forControlEvents: UIControlEvents.TouchDown)
        self.view.addSubview(addNotesButton!)

    }

    func loadBrochureGrid()
    {
        brochures = salesDB!.selectALLFromBrochureFromId(shopId: shopID!)

        if brochures!.count > 0{

            brochureGridView = UIView(frame: CGRectMake(padding,noBrochureImageView!.frame.origin.y,self.view.frame.size.width - (2 * padding),noBrochureImageView!.frame.size.height))
            brochureGridView!.backgroundColor = UIColor.whiteColor()
            brochureGridView!.userInteractionEnabled = true
            self.view.addSubview(brochureGridView!)

            var count = 0
            var isMoreBrochures = false

            if brochures!.count > 5{
                isMoreBrochures = true
                count = 5
            }
            else{
                count = brochures!.count
            }

            let padding1 = deviceManager!.deviceXCGFloatValue(xPos: 4.0)

            let totalElements = CGFloat(ShopDetailViewController.TOTAL_GRID_ELEMENTS - 1)
            let actualtotalElements = CGFloat(ShopDetailViewController.TOTAL_GRID_ELEMENTS)

            let gridWidth = (brochureGridView!.frame.size.width - ((totalElements) * padding1))/actualtotalElements

            var posX : CGFloat = 0.0

            for i in 0..<count{

                let brochure = brochures![i]

                let fileName = "shop_\(brochure.shopId!)_brochure_\(brochure.brochureId!)"

                let thumbnail = AsyncView(frame: CGRectMake(posX,0,gridWidth,gridWidth))
                thumbnail.modifiedDate = brochure.modifiedDate
                thumbnail.fittingType = AsyncView.ASPECT_FILL
                thumbnail.addRoundEdge()
                thumbnail.userInteractionEnabled = true
                thumbnail.getViewForURL(brochure.brochureURL!, imgIndex: i, offline: true, imageFileName: fileName)
                thumbnail.center = CGPointMake(thumbnail.center.x,brochureGridView!.frame.size.height/2.0)
                brochureGridView!.addSubview(thumbnail)
                thumbnail.tag = i + 1

                posX = posX + gridWidth + padding1
            }

            if isMoreBrochures == true{

                // showing caption on the last grid

                (brochureGridView!.viewWithTag(5) as? AsyncView)!.addCaptionLabel("\(brochures!.count - 5)")
            }

        }
        else{
            noBrochureImageView!.hidden =  false
        }
    }

    //MARK: Unloading Methods

    func unloadPage()
    {
        unloadNavigationBar()
        unloadBrochureParser()
        unloadGoogleMapView()
        unloadShopElements()
        unloadButtons()
        unloadBrochureGrid()
        shop = nil
        if addShopController != nil
        {
            addShopController = nil
        }
    }

    func unloadBrochureParser()
    {
        if getVisitingParser != nil
        {
            getVisitingParser!.asyncloader!.cancel()
            getVisitingParser!.delegate = nil
            getVisitingParser = nil
        }
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
        }
    }

    func unloadGoogleMapView()
    {
        if googleMapView != nil {
            googleMapView!.removeFromSuperview()
            googleMapView = nil
        }
    }

    func unloadShopElements()
    {
        if shopLabel != nil {
            shopLabel!.removeFromSuperview()
            shopLabel = nil
        }

        if addressLabel != nil {
            addressLabel!.removeFromSuperview()
            addressLabel = nil
        }

        if shopAddressLabel != nil {
            shopAddressLabel!.removeFromSuperview()
            shopAddressLabel = nil
        }

        if mobileLabel != nil {
            mobileLabel!.removeFromSuperview()
            mobileLabel = nil
        }

        if shopMobileLabel != nil {
            shopMobileLabel!.removeFromSuperview()
            shopMobileLabel = nil
        }

        if visitingCardLabel != nil {
            visitingCardLabel!.removeFromSuperview()
            visitingCardLabel = nil
        }

        if visitingCardLabel != nil {
            visitingCardLabel!.removeFromSuperview()
            visitingCardLabel = nil
        }
    }

    func unloadButtons()
    {
        if visitedButton != nil {
            visitedButton!.removeFromSuperview()
            visitedButton = nil
        }

        if addNotesButton != nil {
            addNotesButton!.removeFromSuperview()
            addNotesButton = nil
        }

    }

    func unloadBrochureGrid()
    {
        if brochureGridView != nil {
            brochureGridView!.removeFromSuperview()
            brochureGridView = nil
        }

        if brochures != nil{

            brochures = nil
        }
    }

    //MARK: Webservices Callback

    //MARK: ShopVisitedParserDelegate methods

    func didReceivedShopVisitedStatus(status : Int)
    {
        shopVisitedParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if status == AppConstant.Static.VISITED_SHOP_SUCCESS{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isShopBrochureDownloaded")
            self.navigationController!.popViewControllerAnimated(true)


        }
        else if status == AppConstant.Static.DATA_NOT_FOUND_ERROR{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isShopBrochureDownloaded")
            noBrochureImageView!.hidden = false
        }
        else{

            var errorMessage = ""
            var errorTag = ""
            if status == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            else if status == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "brochureError"
            }
            else if status == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "brochureError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message:errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }


    //MARK: GetBrochureByShopParserDelegate method
    func didReceivedAllBrochureResult(resultCode resultCode : Int){

        getVisitingParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == GetBrochureByShopParser.GET_ALL_BROCHURE_SUCCESS{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isShopBrochureDownloaded")
            noBrochureImageView!.hidden = true
            loadBrochureGrid()

        }
        else if resultCode == GetBrochureByShopParser.NO_BROCHURE_FOUND{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isShopBrochureDownloaded")
            noBrochureImageView!.hidden = false
        }
        else{

            var errorMessage = ""
            var errorTag = ""
            if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "brochureError"
            }
            else if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "brochureError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message:errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }


    //MARK: AddShopViewControllerDelegate methods

    func didShopAdded(resultCode : Int){
        if addShopController != nil {
            self.navigationController!.popViewControllerAnimated(true)
            addShopController = nil
        }
    }

    //MARK: Event Handlers

    //MARK: BUtton Actions

    func visitButtonClicked()
    {
        visitingCardCount = salesDB!.getVisitingCardCountForShop(shopId: shopID!)!
        if noteString == ""
        {
            alert = NimapAlertView(frame: CGRectZero, tag: "visiterror", title: "", message: "Please write notes!", actionButtonTitle: "OK", animate: true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
        else if visitingCardCount == 0
        {
            alert = NimapAlertView(frame: CGRectZero, tag: "visiterror", title: "", message: "Please add Visiting card", actionButtonTitle: "OK", animate: true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
        else if latitude == 0.0 && longitude == 0.0
        {
            locationManager = LocationManager(isForceEnable: false, isReverserGeoLocationEnable: true,isLocationContinous: false)
            locationManager!.startLocationService()
            locationManager!.delegate = self
        }
        else
        {
            if shopVisitedParser == nil
            {
                shopVisitedParser = ShopVisitedParser()
                shopVisitedParser!.delegate = self
                shopVisitedParser!.shopVisited(shopId: shopID!, note: noteString, isAppDownload: isAppDownloaded, latitude: latitude!, longitude: longitude!)
            }
        }

    }

    func addNotesButtonClicked()
    {
        print("addNotesButtonClicked")
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        alert = NimapAlertView(frame: CGRectZero, noteTag: "addnotetag", titleForTextView: "Notes", message: "na", actionButtonTitle: "OK", secondActionButtonTitle: "CANCEL", animate: true)
        alert!.delegate = self
        self.view.addSubview(alert!)

    }



    //MARK: LocationManagerDelegate methods

    func didReceiveLocation(currentLocation location : LocationManagerModel?)
    {
        locationManager = nil
        if location!.subLocality != nil {

            if location!.subLocality != "na"
            {
                latitude = location!.latitude!
                longitude = location!.longitude!
                visitButtonClicked()
            }
            else{
                latitude = 0.0
                longitude = 0.0
            }
        }
        else{
            latitude = 0.0
            longitude = 0.0
        }
    }

    //MARK: NimapAlertViewDelegate
    func didActionButtonPressed(tag : String!)
    {
        if tag == "unauthorisedUserError"
        {
            if alert != nil{
                alert!.removeFromSuperview()
                alert = nil
            }

            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
        else if tag == "brochureError"{
            if alert != nil{
                alert!.removeFromSuperview()
                alert = nil
            }
            loadBrochureGrid()
        }
        else if tag == "addnotetag"
        {
            if alert!.inputTextView!.text == ""
            {
                if alert != nil{
                    alert!.removeFromSuperview()
                    alert = nil
                }
                alert = NimapAlertView(frame: CGRectZero, tag: "noteemptytag", title: "", message: "Please write notes!", actionButtonTitle: "OK", animate: true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
            else
            {
                noteString = alert!.inputTextView!.text
                if alert != nil{
                    alert!.removeFromSuperview()
                    alert = nil
                }
                alert = NimapAlertView(frame: CGRectZero, tag: "noteaddedtag", title: "", message: "Note added successfully!", actionButtonTitle: "OK", animate: true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }
        else
        {
            if alert != nil{
                alert!.removeFromSuperview()
                alert = nil
            }
        }
    }

    func didSecondaryActionButtonPressed(tag : String!){

        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func didEnterText(tag : String!, text: String!){

        if tag == "Enter Review"{

            var strings = text.componentsSeparatedByString("|")

        }
    }

    //MARK: UIResponder Methods

    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {

        let touch = touches.first

        if touch!.view == appCheckBoxLabel{
            if isAppDownloaded == false
            {
                isAppDownloaded = true
                print(isAppDownloaded)
                appCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
                appCheckBoxLabel!.textColor = UIColor.primaryColor()
            }
            else
            {
                isAppDownloaded = false
                print(isAppDownloaded)
                appCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
                appCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            }
        }
        else if touch!.view == noBrochureImageView
        {
            addVisitingContorller = AddVisitingCardViewController(shopID: shopID!, currentIndex: 0)
            self.navigationController!.pushViewController(addVisitingContorller!, animated: true)
        }
        else{

            if brochures != nil {

                for i in 0..<brochures!.count{

                    if touch!.view == (brochureGridView!.viewWithTag(i+1) as? AsyncView)!{

                        addVisitingContorller = AddVisitingCardViewController(shopID: shopID!, currentIndex: i)

                        self.navigationController!.pushViewController(addVisitingContorller!, animated: true)

                        break
                    }
                }
            }
        }
    }

    //MARK: NimapNavigationBarDelegate

    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }

    func rightButtonPressed()
    {
        if addShopController == nil
        {
            salesDB!.updateAllBusinessCategoryCheckedToIdeal()
            addShopController = AddShopViewController(shopID: shopID!)
            addShopController!.delegate = self
            self.navigationController!.pushViewController(addShopController!, animated: true)
        }
    }

    //MARK: UIMapViewDelegate methods

    func didAnnotationSelected(index:Int){

        googleMapView!.showDirections(index)
    }
}
