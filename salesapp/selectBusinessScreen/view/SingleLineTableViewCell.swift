//
//  SingleLineTableViewCell.swift
//  COGVendor
//
//  Created by Priyank Ranka on 02/07/16.
//  Copyright © 2016 Nimap Infotech. All rights reserved.
//

import UIKit

class SingleLineTableViewCell: UITableViewCell {

    var deviceManager : DeviceManager?
    
    var iconLabel : UILabel?
    
    var posX : CGFloat?
    var posY : CGFloat?
    
    var defaultLabel : UILabel?
    var dividerLabel : UILabel?
    
    init(style: UITableViewCellStyle, reuseIdentifier: String?, cellWidth : CGFloat, cellHeigth : CGFloat) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        let padding = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        let width_height = cellHeigth - (2 * padding)
        
        let labelWidth =  cellWidth - (2 * padding)
        
        iconLabel = UILabel(frame: CGRectMake(padding,padding,width_height,width_height))
        iconLabel!.layer.cornerRadius = iconLabel!.frame.size.width/2
        iconLabel!.clipsToBounds = true
        iconLabel!.textColor = UIColor.whiteColor()
        iconLabel!.textAlignment = .Center
        iconLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
        iconLabel!.backgroundColor = UIColor.primaryColor()
        self.addSubview(iconLabel!)
        
        posX = iconLabel!.frame.origin.x + iconLabel!.frame.size.width + padding
        
        defaultLabel = UILabel(frame: CGRectMake(width_height + (2 * padding),padding,labelWidth,cellHeigth - (2 * padding)))
        defaultLabel!.textColor = UIColor.primaryColor()
        defaultLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 16.0))
        defaultLabel!.text = "Submit Query"
        self.addSubview(defaultLabel!)
        
        dividerLabel = UILabel(frame: CGRectMake(0.0,cellHeigth - (0.2),cellWidth,0.2))
        dividerLabel!.backgroundColor = UIColor.dividerColor()
        self.addSubview(dividerLabel!)
        
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
