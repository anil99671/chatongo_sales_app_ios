
//
//  BusinessCategoriesViewController.swift
//  ChatOnGoVendor
//
//  Created by Zubin Gala on 26/05/16.
//  Copyright © 2016 nimapinfotech. All rights reserved.
//

import UIKit

class BusinessCategoriesViewController: UIViewController,NimapNavigationBarViewDelegate,UITableViewDelegate,UITableViewDataSource,NimapSearchBarViewDelegate,NimapAlertViewDelegate
{
    
    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    var pageHeight : CGFloat?
    var searchBarHeight : CGFloat = 0.0
    
    var deviceManager : DeviceManager?
    
    var tableOfBusinessCategories : UITableView?
    var businessString : String?

    var businessCategories : [BusinessCategoryModel!]?
    var searchBusinessCategories : [BusinessCategoryModel!]?
    var preselectedCategories : [BusinessCategoryModel!]?

    var SalesDB : SalesAppDBModel?
    var searchBar : NimapSearchBarView?
    var alert : NimapAlertView?
    
    var businessCategoryController : BusinessCategoriesViewController?
    var addShopController : AddShopViewController?
    
    var isSearchOn = false
    var searchString : String?
    var cellHeight : CGFloat?
    var shopId : Int?


    
    init(shopId : Int)
     {
        self.shopId = shopId
        super.init(nibName: nil, bundle: nil)
        deviceManager = DeviceManager.sharedDeviceManagement()
        SalesDB = SalesAppDBModel.sharedInstance()
        cellHeight = deviceManager!.deviceXCGFloatValue(xPos: 56.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }
    
    override func viewWillAppear(animated: Bool)
    {
        searchBusinessCategories = []
       // SalesDB!.updateAllBusinessCategoryCheckedToIdeal()
        loadnavigationBar()
        loadTableOfBusinessCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    

    
    func loadnavigationBar()
    {
        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
        {
            navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            searchBarHeight = 40.0
        }
        else
        {
            navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: 40.0)
        }
        pageHeight = self.view.frame.height - navigationHeight
        
        if navigationBar == nil
        {
            
            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0, UIScreen.mainScreen().bounds.size.width, navigationHeight), title: "Category List")
            navigationBar!.backgroundColor = UIColor.primaryColor()
            navigationBar!.delegate = self
            navigationBar!.addRightButtonWithIcon()
            navigationBar!.updateRightButtonTitle(AppConstant.Static.DONE_CHECKED_ICON)
            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)
            self.view.addSubview(navigationBar!)
        }
    }

    func loadTableOfBusinessCategories()
    {
        let padding = deviceManager!.deviceYCGFloatValue(yPos: 8.0)
        
        if searchBar == nil
        {
            searchBar = NimapSearchBarView(frame: CGRectMake(padding,navigationHeight + padding,self.view.frame.width - (2 * padding),searchBarHeight))
            searchBar!.center = CGPointMake(self.view.center.x, navigationHeight + padding + (searchBarHeight/2.0))
            searchBar!.searchTextField!.placeholder = "Search Business Category"
            searchBar!.delegate = self
            self.view.addSubview(searchBar!)
        }
        
        
        if shopId == -1{
            businessCategories = SalesDB!.selectAllFromBusinessCategory()
        }
        else {
            businessCategories = SalesDB!.selectAllSelectedBusinessCategory(shopId: shopId!)
        }
        
        preselectedCategories = []
        
        for i in 0..<businessCategories!.count{
            
            let business = businessCategories![i]
            
            if business.isSelected {
                preselectedCategories!.append(business)
            }
        }

        //businessCategories = SalesDB!.selectAllFromBusinessCategory()
        
        let posY = searchBarHeight + navigationHeight + padding + padding
        
        let tableHeight = self.view.frame.height - (searchBarHeight + navigationHeight + padding + padding)
        
        tableOfBusinessCategories = UITableView(frame: CGRectMake(0, posY, UIScreen.mainScreen().bounds.size.width, tableHeight))
        tableOfBusinessCategories!.delegate = self
        tableOfBusinessCategories!.dataSource = self
        tableOfBusinessCategories!.separatorStyle = UITableViewCellSeparatorStyle.None
        self.view.addSubview(tableOfBusinessCategories!)
    }
    
    //Mark: UITableViewDatasource Methods

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearchOn == true
        {
            return searchBusinessCategories!.count
        }
        else
        {
            return businessCategories!.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var cell : SingleLineTableViewCell?
        
        cell = tableView.dequeueReusableCellWithIdentifier("single") as? SingleLineTableViewCell
        
        if cell == nil
        {
            
            cell = SingleLineTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "single", cellWidth: tableView.frame.size.width,cellHeigth: cellHeight!)
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
        }
        var  business : BusinessCategoryModel! = nil
        
        if isSearchOn == true
        {
            business = searchBusinessCategories![indexPath.row]
        }
        else
        {
            business = businessCategories![indexPath.row]
        }

        cell!.defaultLabel!.text = business!.businessCategoryName
        cell!.iconLabel!.text = business!.initials
        cell!.selectionStyle = .None
        
        if business!.isSelected == true
        {
            cell!.accessoryType = .Checkmark
        }
        else
        {
            cell!.accessoryType = .None
        }
        
        return cell!
    }

    //MARK: UITableViewDelegate Methods
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return cellHeight!
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var  business : BusinessCategoryModel! = nil

        if isSearchOn == true
        {
            business = searchBusinessCategories![indexPath.row]
        }
        else
        {
            business = businessCategories![indexPath.row]
        }

        if business!.businessCategoryId != -1{

            if business!.isSelected == true
            {
                business!.isSelected = false
                SalesDB!.updateBusinessCategoryAsUnChecked(businessCategoryId: business!.businessCategoryId!)
                
            }
            else
            {
                business!.isSelected = true
                SalesDB!.updateBusinessCategoryAsChecked(businessCategoryId: business!.businessCategoryId!)
                
            }
            tableOfBusinessCategories!.reloadData()
        }
    }

    
    //MARK: UIScrollViewDelegate Methods
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        searchBar!.resignFirstResponder()
    }
    
    //MARK: NimapSearchBarViewDelegate methods
    
    func nimapSearchBarViewEnteredCharacters(string: String)
    {
        searchString = string
        
        if searchString!.isEmpty{
            isSearchOn = false
        }
        else{
            if searchString!.characters.count > 0{
                
                searchBusinessCategories!.removeAll(keepCapacity: false)
                
                for i in 0..<businessCategories!.count{
                    
                    let category = businessCategories![i]
                    
                    if category.businessCategoryName!.lowercaseString.rangeOfString(searchString!.lowercaseString) != nil {
                        
                        searchBusinessCategories!.append(category)
                    }
                }
                
                isSearchOn = true
                
                if searchBusinessCategories!.count <= 0{
                    
                    let category = BusinessCategoryModel()
                    category.businessCategoryId = -1
                    category.businessCategoryName = "No Business Category found"
                    category.initials = "CG"
                    searchBusinessCategories!.append(category)
                }
            }
            else{
                isSearchOn = false
            }
        }
        
        tableOfBusinessCategories!.reloadData()
    }

    func nimapSearchBarViewShouldReturn(textField: UITextField)
    {
        searchBar!.searchTextField!.resignFirstResponder()
    }
    
    //MARK: NimapNavigationBarViewDelegate

    func backButtonPressed(){
        SalesDB!.updateAllBusinessCategoryCheckedToIdeal()
        
        for i in 0..<preselectedCategories!.count{
            
            let business = preselectedCategories![i]
            SalesDB!.updateBusinessCategoryAsChecked(businessCategoryId: business.businessCategoryId!)
        }
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func rightButtonPressed(){
        commitBusinessCategoriesToLocalDB()
    }
    
    //MARK: NimapAlertViewDelegate
    
    func didActionButtonPressed(tag: String!){
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
            
        }
    }
    
    //Local Methods
    func commitBusinessCategoriesToLocalDB()
    {
        if businessCategories != nil && businessCategories!.count > 0
        {
            var count = 0

            count = SalesDB!.getCountOfSelectedBusinessCategories()
    
            print("count \(count)")
            
            if count == 0
            {
                if alert != nil
                {
                    alert!.removeFromSuperview()
                    alert = nil
                }
                
                alert = NimapAlertView(frame: CGRectZero, tag:"Error", title: "ValidationError", message: "You need to select atleast one business category." , actionButtonTitle: "Ok", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
            else{
                self.navigationController!.popViewControllerAnimated(true)
            }
        }
    }
    
}
