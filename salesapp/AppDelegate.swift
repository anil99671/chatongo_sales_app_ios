//
//  AppDelegate.swift
//  COG Sales
//
//  Created by Apple on 30/05/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, NimapAlertViewDelegate{

    var window: UIWindow?
    var baseController : UINavigationController?
    var homeController : UITabBarController?

    var alert : NimapAlertView?
    var composerTag : String?
    var app : UIApplication?

    // Need to use this static number while running on the simulator as simulator vendor UDID changes every launch of the app.
    let imeiNumber = "AF907D9C-517D-4F6E-B6E8-D06D6108A614"

    var notificationUserInfo : [NSObject : AnyObject]?

    var notificationKey = 0

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.

        print("onLaunch")

        //handling notification

        if let notification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String: AnyObject] {

            notificationKey = Int((notification["NType"] as? String)!)!

            if notificationKey == 2{
                let groupId = Int((notification["gId"] as? String)!)
                NSUserDefaults.standardUserDefaults().setInteger(groupId!, forKey: "chatGroupId")
            }
        }
        else{
            NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "chatGroupId")
        }

        if NSUserDefaults.standardUserDefaults().boolForKey("firstTime") == false
        {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "firstTime")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "notificationOn")
        }

        /*
         Need to set some flag to the default state at every app launch
         */

        NSUserDefaults.standardUserDefaults().setObject("na", forKey: "deviceToken")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "downloadSubscriptionMaster")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "downloadTopKeywords")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "downloadAllShops")
        NSUserDefaults.standardUserDefaults().boolForKey("downloadAllVisitedShops")

        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isDealOfDayBannerDownloaded")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isSearchBannerDownloaded")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isOfferDownloaded")

        NSUserDefaults.standardUserDefaults().setObject("Select Area", forKey:"currentLocation" )
        NSUserDefaults.standardUserDefaults().setDouble(0.0, forKey:"currentLatitude" )
        NSUserDefaults.standardUserDefaults().setDouble(0.0, forKey:"currentLongitude" )

        //Chat oriented the some global variables

        NSUserDefaults.standardUserDefaults().setInteger(-1, forKey: "activeGroupId")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isChatDone")
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "receivingMessage")

        app = application

        let viewController = ViewController()
        baseController = UINavigationController(rootViewController: viewController)
        baseController!.setNavigationBarHidden(true, animated: false)
        window!.rootViewController = baseController

        //Lines for registering the notifcations

        application.applicationIconBadgeNumber = 0

        if UIDevice.currentDevice().systemVersion >= "8.0"
        {
            if application.respondsToSelector(#selector(UIApplication.registerUserNotificationSettings(_:)))
            {
                let settings = UIUserNotificationSettings(forTypes: ([UIUserNotificationType.Badge, UIUserNotificationType.Sound]), categories: nil)
                application.registerUserNotificationSettings(settings)
            }
            else
            {
                let notificationTypes: UIRemoteNotificationType = [UIRemoteNotificationType.Badge, UIRemoteNotificationType.Alert, UIRemoteNotificationType.Sound]
                application.registerForRemoteNotificationTypes(notificationTypes)
            }
        }

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //MARK: LIFE cycle method for notification

    //Use afetr iOS 8.0

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings)
    {
        application.registerForRemoteNotifications()
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData)
    {
        let strDeviceToken = NSMutableString(format: "%@", deviceToken)

        var newStr = strDeviceToken as String!


        //var strDeviceToken = NSString(data: deviceToken, encoding: NSUTF8StringEncoding)

        newStr = newStr.stringByReplacingOccurrencesOfString("<", withString: "")
        newStr = newStr.stringByReplacingOccurrencesOfString(" ", withString: "")
        newStr = newStr.stringByReplacingOccurrencesOfString(">", withString: "")

        newStr = newStr.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())

        print("device token is \(newStr)");

        NSUserDefaults.standardUserDefaults().setObject(newStr, forKey: "deviceToken")


    }

    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError)
    {
        print("didFailToRegisterForRemoteNotificationsWithError")
        NSUserDefaults.standardUserDefaults().setObject("na", forKey: "deviceToken")
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        print("\(userInfo)")

        notificationUserInfo = userInfo
        let notificationKey = Int((userInfo["NType"] as? String)!)

        print("notificationKey \(notificationKey!)")

        if notificationKey == 2{
            //this indicates it is chat notification

            let groupId = Int((userInfo["gId"] as? String)!)

            NSUserDefaults.standardUserDefaults().setInteger(groupId!, forKey: "chatGroupId")
        }
        else if notificationKey == 3{
            // we need to call all offer parser again.

            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isOfferDownloaded")
        }
    }

    func startIgnoringTouches()
    {
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()
    }

    func endIgnoringTouches()
    {
        UIApplication.sharedApplication().endIgnoringInteractionEvents()
    }

    func applicationTerminateWithUnauthorisedUserAccess()
    {
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isVerified")
        exit(0)
    }

    // MARK: NimapAlertViewDelegate Callbacks
    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }
}