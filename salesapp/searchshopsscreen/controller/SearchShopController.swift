//
//  SearchShopController.swift
//  COG Sales
//
//  Created by Apple on 12/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class SearchShopController: UIViewController,NimapNavigationBarViewDelegate, NimapAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,NimapSearchBarViewDelegate, SearchShopsParserDelegate, ShopMapParserDelegate
{
    //MARK: VARIABLE

    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0

    var searchBar : NimapSearchBarView?
    var alert : NimapAlertView?

    var tableOfShops : UITableView?
    var baseCategoryView : UIView?
    var searchShopParser : SearchShopsParser?
    var shopMapParser : ShopMapParser?
    var salesDB : SalesAppDBModel?
    var shopsList : [ShopModel]?

    var cellHeight : CGFloat?
    var pageHeight : CGFloat?
    var searchBarHeight : CGFloat?
    var padding : CGFloat?
    var posY : CGFloat?
    var selectedShopId : Int?

    var shopId : Int?
    var searchString = ""

    // MARK: Life Cycle

    init()
    {
        super.init(nibName: nil, bundle: nil)

        deviceManager = DeviceManager.sharedDeviceManagement()

        salesDB = SalesAppDBModel.sharedInstance()
        padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
        cellHeight = deviceManager!.deviceXCGFloatValue(xPos: 56.0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }

    override func viewWillAppear(animated: Bool)
    {
        loadPage()
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    //MARK: Loading method

    func loadPage()
    {
//        searchShops = []
        loadnavigationBar()
        loadSearchBar()
    }

    func loadnavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }

            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"Search Shops")
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)

            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)
            

            navigationHeight = navigationBar!.frame.size.height
            pageHeight = self.view.frame.height - navigationHeight
        }

    }

    func loadSearchBar()
    {
        posY = navigationBar!.frame.size.height + padding!
        let baseContorllerHeight = navigationBar!.frame.size.height + padding!

        if baseCategoryView == nil
        {
            baseCategoryView = UIView(frame: CGRectMake(0.0,posY!,self.view.frame.width,baseContorllerHeight))
            baseCategoryView!.backgroundColor = UIColor.iconsColor()
            self.view.addSubview(baseCategoryView!)

            if searchBar == nil
            {
                if deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
                {
                    searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.SEARCH_BAR_HEIGHT_CGFLOAT)
                }
                else
                {
                    searchBarHeight = deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.BUTTON_HEIGHT_CGFLOAT)
                }

                searchBar = NimapSearchBarView(frame: CGRectMake(padding!,0,self.view.frame.width - (2 * padding!),searchBarHeight!))
                searchBar!.center = CGPointMake(self.view.center.x, (searchBar!.frame.size.height/2.0))
                searchBar!.delegate = self
                searchBar!.searchTextField!.placeholder = "Search Shop"
                baseCategoryView!.addSubview(searchBar!)
            }
        }
    }

    func loadTableView()
    {
        posY = baseCategoryView!.frame.size.height + baseCategoryView!.frame.origin.y

        let tableHeight = self.view.frame.size.height - posY!

        if tableOfShops == nil
        {
            tableOfShops = UITableView(frame: CGRectMake(0.0,posY!,self.view.frame.size.width,tableHeight))
            tableOfShops!.delegate = self
            tableOfShops!.dataSource = self
            tableOfShops!.separatorStyle = UITableViewCellSeparatorStyle.None
            self.view.addSubview(tableOfShops!)
        }
    }

    //MARK: Unloading method

    func unloadPage()
    {
        unloadNavigationBar()
        unloadAlert()
        unloadSearchBar()
        unloadCategoryBaseView()
        unloadShopMapParser()
      unloadSearchParser()
        unloadTableOfShops()

        if shopsList != nil {
            shopsList!.removeAll(keepCapacity: false)
        }

        shopsList = nil
        searchString = ""
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil

        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func unloadSearchBar()
    {
        if searchBar != nil
        {
            searchBar!.removeFromSuperview()
            searchBar = nil
        }
    }

    func unloadCategoryBaseView()
    {
        if baseCategoryView != nil
        {
            baseCategoryView!.removeFromSuperview()
            baseCategoryView = nil
        }
    }

    func unloadSearchParser()
    {
        if searchShopParser != nil
        {
            searchShopParser = nil
        }
    }

    func unloadShopMapParser()
    {
        if shopMapParser != nil
        {
            shopMapParser = nil
        }
    }

    func unloadTableOfShops()
    {
        if tableOfShops != nil
        {
            tableOfShops!.removeFromSuperview()
            tableOfShops = nil
        }
    }
    
    //MARK: UIScrollViewDelegate Methods
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        searchBar!.resignFirstResponder()
    }

    // MARK: UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return shopsList!.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {

        var cell : ShopListTableViewCell?

        cell = tableView.dequeueReusableCellWithIdentifier("shopList") as! ShopListTableViewCell?

        if cell == nil
        {
            cell = ShopListTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "shopList", cellWidth: tableView.frame.size.width, cellHeigth: cellHeight!)
        }

        var shop : ShopModel?

        shop = shopsList![indexPath.row]

        if shop!.shopId == -1
        {
            cell!.defaultLabel!.hidden = false
            cell!.shopAddressLabel!.hidden = true
            cell!.shopNameLabel!.hidden = true

            cell!.iconLabel!.text = shop!.initials
            cell!.defaultLabel!.text = shop!.name
            cell!.shopId = shop!.shopId
        }
        else
        {
            cell!.defaultLabel!.hidden = true
            cell!.shopAddressLabel!.hidden = false
            cell!.shopNameLabel!.hidden = false

            cell!.iconLabel!.text = shop!.initials
            cell!.shopAddressLabel!.text = shop!.streetAddress
            cell!.shopNameLabel!.text = shop!.name
            cell!.shopId = shop!.shopId
        }

        return cell!


    }
    // MARK: UITableViewDelegate

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return cellHeight!
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        let shop : ShopModel?

        shop = shopsList![indexPath.row]
        selectedShopId = shop!.shopId!

        if shop!.shopId != -1
        {
            if shopMapParser == nil {

                shopMapParser = ShopMapParser()
                shopMapParser!.delegate = self
                shopMapParser!.mapShopsWithUser(shopId: selectedShopId!)
            }
        }
    }


    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

    //MARK: Button events

    //MARK: NimapSearchBarViewDelegate methods
    func ad(textField: UITextField)
    {
        searchBar!.searchTextField!.resignFirstResponder()
    }
//    func nimapSearchBarViewDidBeginEditing(textField: UITextField)
//    {
//        searchBar!.searchTextField!.placeholder = ""
//    }

    func nimapSearchBarViewEnteredCharacters(string: String)
    {
        searchString = string
     //   searchString = searchString.stringByReplacingOccurrencesOfString("\n", withString: "")

        if searchString.characters.count > 2{

            searchShopParser = SearchShopsParser()
            searchShopParser!.delegate = self
            searchShopParser!.searchShops(searchString: searchString)
        }
        else
        {
           unloadSearchParser()
            unloadTableOfShops()
        }
    }

    //MARK: NimapAlertViewDelegate Methods

    // This method is called when user presses the primary actiona button on the NimapAlertView and depending upon the tag value we understand which alertview callback has come

    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if tag == "shopmapsuccess"
        {
            self.navigationController!.popViewControllerAnimated(true)
        }
        else if tag == "searchshopError"
        {
            if searchShopParser != nil
            {
                searchShopParser = nil
            }
            if searchString.characters.count > 1{

                searchShopParser = SearchShopsParser()
                searchShopParser!.delegate = self
                searchShopParser!.searchShops(searchString: searchString)
            }
            
        }
        else if tag == "shopError"
        {
            if shopMapParser != nil
            {
                shopMapParser = nil
            }
            shopMapParser = ShopMapParser()
            shopMapParser!.delegate = self
            shopMapParser!.mapShopsWithUser(shopId: selectedShopId!)
        }
        else if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
        else
        {
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }
        }

    }

    //MARK: Event Handlers

    //MARK: NimapNavigationBarDelegate methods

    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }

    //MARK: SearchShopsParserDelegate method

    func didReceivedSearchShops(shops shops : [ShopModel], searchString : String)
    {

        searchShopParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if shopsList != nil
        {
            shopsList!.removeAll(keepCapacity: false)
            shopsList = nil
        }

        shopsList = shops
        if tableOfShops != nil
        {
            tableOfShops!.reloadData()
        }
        else
        {
            loadTableView()
        }
    }

    func didReceivedErrorOnSearchShops(resultCode resultCode : Int)
    {
        searchShopParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        var errorMessege = ""
        var errorTag = ""

        if resultCode == AppConstant.Static.CONNECTION_ERROR
        {
            errorMessege = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
            errorTag = "searchshopError"
        }
        else if resultCode == AppConstant.Static.PROCESSING_ERROR
        {
            errorMessege = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
            errorTag = "searchshopError"
        }
        else if resultCode == AppConstant.Static.NO_SHOP_AVAILABLE
        {
            shopsList = []
            let shop = ShopModel()
            shop.shopId = -1
            shop.name = "No Shops!!"
            shop.initials = "CG"
            shopsList!.append(shop)

            if tableOfShops != nil
            {
                tableOfShops!.reloadData()
            }
            else{
                loadTableView()
            }
        }

        if errorTag == "searchshopError"
        {
            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessege, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    func unauthorizedAccessForSearchShops()
    {
        searchShopParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        alert = NimapAlertView(frame: CGRectZero, tag: "unauthorisedUserError", title: "Error", message: AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE, actionButtonTitle: "OK", animate : true)
        alert!.delegate = self
        self.view.addSubview(alert!)
    }

    //MARK: ShopMapParserDelegate method

    func didReceivedShopMapResult(resultCode resultCode : Int)
    {
        shopMapParser = nil
        searchBar!.searchTextField!.resignFirstResponder()
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        if resultCode == AppConstant.Static.GET_SHOP_SUCCESS
        {
            alert = NimapAlertView(frame: CGRectZero, tag: "shopmapsuccess", title: "Error", message: "Shop Map Successfully.", actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
        else
        {
            var errorMessege = ""
            var errorTag = ""

            if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessege = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "shopError"
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessege = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "shopError"
            }
            else if resultCode == AppConstant.Static.NO_SHOP_AVAILABLE
            {
                errorMessege = AppConstant.Static.NO_SHOP_AVAILABLE_MESSEGE
                errorTag = "noshopvailableError"

            }
            else if resultCode == AppConstant.Static.SHOP_ALREADY_MAPPED
            {
                errorMessege = AppConstant.Static.SHOP_ALREADY_MAPPED_MESSEGE
                errorTag = "shopalreadymapped"

            }
            else if resultCode == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessege = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessege, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }
    
    
    }
