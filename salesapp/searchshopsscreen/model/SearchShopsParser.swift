//
//  SearchShopsParser.swift
//  COG Sales
//
//  Created by Apple on 12/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

@objc protocol SearchShopsParserDelegate : NSObjectProtocol
{
    optional func didReceivedErrorOnSearchShops(resultCode resultCode : Int)
    func didReceivedSearchShops(shops shops : [ShopModel], searchString : String)
    func unauthorizedAccessForSearchShops()
}

class SearchShopsParser: NSObject, AsyncLoaderModelDelegate
{
    var asyncloader : AsyncLoaderModel?
    var delegate : SearchShopsParserDelegate?

    var isUnauthorized : Int = -1
    var searchString : String?

    func searchShops(searchString searchString : String)
    {
        self.searchString = searchString
        asyncloader = AsyncLoaderModel()
        asyncloader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"SearchShopsByCustomer?SearchString=\(searchString)&BusinessCategoryId=0&Lat=0&Lng=0", dataIndex: -1)
        asyncloader!.delegate = self
    }


    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
//        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
//        print("responseString SearchShopsByCustomer \(responseString!)")
        processData(data: data)
        self.asyncloader = nil
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        if (delegate != nil)
        {
            delegate!.didReceivedErrorOnSearchShops!(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func processData(data data : NSData)
    {
        var json : AnyObject?
        do
        {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary

            let Status = json!.objectForKey("Status") as! Int

            if Status == 200
            {
                let shopData = json!.objectForKey("data") as! NSArray

                var shopArray : [ShopModel] = []
                let salesAppDB = SalesAppDBModel.sharedInstance()

                for i in 0..<shopData.count
                {
                    let shop = ShopModel()

                    shop.shopId = shopData[i].valueForKey("ShopId") as! Int!
                    shop.userId = shopData[i].valueForKey("UserId") as! Int!
                    shop.name = shopData[i].valueForKey("ShopName") as! String!
                    shop.streetAddress = shopData[i].valueForKey("StreetAddress") as! String!
                    shop.mobileNo = shopData[i].valueForKey("MobileNo") as! String!
                    shop.contactNo = shopData[i].valueForKey("ContactNo") as! String!
                    shop.state = shopData[i].valueForKey("State") as! String!
                    shop.city = shopData[i].valueForKey("City") as! String!
                    shop.zipcode = shopData[i].valueForKey("ZipCode") as! String!
                    shop.latitude = shopData[i].valueForKey("Latitude") as! Double!
                    shop.longitude = shopData[i].valueForKey("Longitude") as! Double!
                    shop.info = shopData[i].valueForKey("Info") as! String!
                    shop.locality = shopData[i].valueForKey("Locality") as! String!
                    shop.timings = shopData[i].valueForKey("Timing") as! String!
                    shop.initials = salesAppDB!.getInitialsFromBusinessCategoryString(shop.name!)
                    shopArray.append(shop)
                }

                if (delegate != nil)
                {
                    delegate!.didReceivedSearchShops(shops: shopArray, searchString: searchString!)

                }
            }
            else if Status == 401
            {
                if (delegate != nil)
                {
                    delegate!.unauthorizedAccessForSearchShops()
                }
            }
            else if Status == 404
            {
                if (delegate != nil)
                {
                    delegate!.didReceivedErrorOnSearchShops!(resultCode: AppConstant.Static.NO_SHOP_AVAILABLE)
                }
            }
            else
            {
                if (delegate != nil)
                {
                    delegate!.didReceivedErrorOnSearchShops!(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didReceivedErrorOnSearchShops!(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}