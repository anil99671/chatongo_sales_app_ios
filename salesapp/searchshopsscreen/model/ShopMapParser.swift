//
//  ShopMapParser.swift
//  COG Sales
//
//  Created by Apple on 12/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

@objc protocol ShopMapParserDelegate : NSObjectProtocol
{
    func didReceivedShopMapResult(resultCode resultCode : Int)
}

class ShopMapParser: NSObject, AsyncLoaderModelDelegate
{
    var asyncloader : AsyncLoaderModel?
    var delegate : ShopMapParserDelegate?

    func mapShopsWithUser(shopId shopId : Int)
    {
        let userId = NSUserDefaults.standardUserDefaults().valueForKey("userID") as! Int
        asyncloader = AsyncLoaderModel()
        asyncloader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"ShopMapToSalesUser?UserId=\(userId)&ShopId=\(shopId)", dataIndex: -1)
        asyncloader!.delegate = self
    }


    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("responseString ShopMapToSalesUser \(responseString!)")
        processData(data: data)
        self.asyncloader = nil
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        if (delegate != nil)
        {
            delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func processData(data data : NSData)
    {
        var result : NSDictionary?

        do{
            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let status = result!.objectForKey("Status") as! Int

            if status == 200
            {
                let shopData = result!.objectForKey("data") as! NSDictionary

                if shopData.count == 0
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.NO_SHOP_AVAILABLE)
                    }
                }
                else
                {
                    let salesDB = SalesAppDBModel.sharedInstance()

                    let shop = ShopModel()

                    shop.shopId = shopData.valueForKey("ShopId") as! Int!
                    shop.userId = shopData.valueForKey("UserId") as! Int!
                    shop.name = shopData.valueForKey("ShopName") as! String!
                    shop.streetAddress = shopData.valueForKey("StreetAddress") as! String!
                    shop.mobileNo = shopData.valueForKey("MobileNo") as! String!
                    shop.contactNo = shopData.valueForKey("ContactNo") as! String!
                    //shop.country = shopData.valueForKey("Country") as! String!
                    shop.state = shopData.valueForKey("State") as! String!
                    shop.city = shopData.valueForKey("City") as! String!
                    shop.zipcode = shopData.valueForKey("ZipCode") as! String!
                    shop.latitude = Double(shopData.valueForKey("Latitude") as! String!)
                    shop.longitude = Double(shopData.valueForKey("Longitude") as! String!)
                    shop.visitDate = shopData.valueForKey("VisitDate") as! String!
                    shop.isVisited = shopData.valueForKey("IsVisited") as! String!
                    shop.isAppDownloaded = shopData.valueForKey("IsAppDownloaded") as! String!
                    shop.info = shopData.valueForKey("Info") as! String!
                    shop.locality = shopData.valueForKey("Locality") as! String!
                    shop.timings = shopData.valueForKey("Timing") as! String!
                    shop.startDate = shopData.valueForKey("CreatedDate") as! String!
                    shop.endDate = shopData.valueForKey("ModifiedDate") as! String!
                    shop.isPending = shopData.valueForKey("IsPending") as! String!
                    shop.id = shopData.valueForKey("Id") as! Int!
                    shop.osType = shopData.valueForKey("OsType") as! String

                    let shopResult = salesDB!.isShopExists(shopId: shop.shopId!)

                    if shopResult == true
                    {
                        salesDB!.updateShop(shopId: shop.shopId!, userId: shop.userId!, name: shop.name!, streetAddress: shop.streetAddress!, mobileNo: shop.mobileNo!, contactNo: shop.contactNo!, state: shop.state!, city: shop.city!, zipCode: shop.zipcode!, latitude: shop.latitude!, longitude: shop.longitude!, visitDate: shop.visitDate!, isVisited: shop.isVisited!, isAppDownloaded: shop.isAppDownloaded!, info: shop.info!, timings: shop.timings!, locality: shop.locality!, startDate: shop.startDate!, endDate: shop.endDate!, isPending: shop.isPending!, id: shop.id!,osType: shop.osType!)
                    }
                    else
                    {
                        salesDB!.insertIntoShop(id: shop.id!, shopId: shop.shopId!, vendorId: shop.userId!, name: shop.name!, streetAddress: shop.streetAddress!, mobileNo: shop.mobileNo!, contactNo: shop.contactNo!, state: shop.state!, city: shop.city!, zipCode: shop.zipcode!, latitude: shop.latitude!, longitude: shop.longitude!, visitDate: shop.visitDate!, isVisited: shop.isVisited!, isAppDownloaded: shop.isAppDownloaded!, info: shop.info!, timings: shop.timings!, locality: shop.locality!, startDate: shop.startDate!, endDate: shop.endDate!, isPending: shop.isPending!,osType: shop.osType!)
                    }

                    if delegate != nil
                    {
                        delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.GET_SHOP_SUCCESS)
                    }
                }
            }
            else
            {

                if status == 404
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.NO_SHOP_AVAILABLE)
                    }
                }
                else if status == 401
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
                    }
                }
                else if status == 409
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.SHOP_ALREADY_MAPPED)
                    }
                }
                else
                {
                    if delegate != nil
                    {
                        delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                    }

                }

            }

        }
        catch
        {
            // Error Occured
            if delegate != nil
            {
                delegate!.didReceivedShopMapResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
    
    
}

