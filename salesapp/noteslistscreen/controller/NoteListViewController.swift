//
//  NoteListViewController.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class NoteListViewController: UIViewController,NimapNavigationBarViewDelegate, NimapAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, SearchShopByBusinessParserDelegate, UIScrollViewDelegate,NimapDualRoundButtonViewDelegate
    
    //
{
    //MARK: VARIABLE
    
    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    
    var tableOfShops : UITableView?
    
    var alert : NimapAlertView?
    var shopsList : [ShopModel]?
    var filterShopModel  :[ShopModel]?
    var getvisitedShopParser : SearchShopByBusinessParser?
    
    var noteDetailController : NoteDetailViewController?
    var shopDetailController : ShopDetailViewController?
    
    var actionButton : NimapDualRoundButtonView?
    //MARK: filter variable
    
    var filterLabel : UILabel?
    var webTypeLabel : UILabel?
    var androidTypeLabel : UILabel?
    var iphoneTypeLabel : UILabel?
    var webTypeCheckBoxLabel : UILabel?
    var androidTypeCheckBoxLabel : UILabel?
    var iphoneTypeCheckBoxLabel : UILabel?
    var isWebChecked : Bool = false
    var isAndroidChecked : Bool = false
    var isIphoneChecked : Bool = false
    
    
    
    var fullBgView : UIView?
    var filterView : UIView?
    
    
    var noShopVisitedImageView : UIImageView?
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    
    var salesDB : SalesAppDBModel?
    var isNoShopParserBool : Bool = false
    var isFilterOn : Bool = false
    
    var cellHeight : CGFloat?
    var pageHeight : CGFloat?
    
    // MARK: Life Cycle
    
    init()
    {
        super.init(nibName: nil, bundle: nil)
        
        deviceManager = DeviceManager.sharedDeviceManagement()
        
        salesDB = SalesAppDBModel.sharedInstance()
        cellHeight = deviceManager!.deviceXCGFloatValue(xPos: 56.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }
    
    override func viewWillAppear(animated: Bool)
    {
        loadPage()
    }
    
    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    //MARK: Loading method
    
    func loadPage()
    {
        filterShopModel = []
        loadnavigationBar()
        loadGetAllShpParser()
    }
    
    func loadnavigationBar()
    {
        if navigationBar == nil
        {
            if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
            {
                navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
            }
            else
            {
                navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
            }
            
            navigationBar = NimapNavigationBarView(frame: CGRectMake(0,0,self.view.frame.size.width,navigationHeight),title:"Visited Shop")
            navigationBar!.delegate = self
            self.view.addSubview(navigationBar!)
            
            navigationBar!.addLeftButtonWithIcon()
            navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)
            navigationBar!.addRightButtonWithIcon()
            navigationBar!.updateRightButtonTitle(AppConstant.Static.FILTER_ICON)
            navigationHeight = navigationBar!.frame.size.height
            pageHeight = self.view.frame.height - navigationHeight

        }
        
    }
    
    func loadGetAllShpParser()
    {
        
        // Activity Indicator
        if NSUserDefaults.standardUserDefaults().boolForKey("downloadAllVisitedShops") == false
        {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        if noShopVisitedImageView == nil
        {
            noShopVisitedImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "noshops")))
            noShopVisitedImageView!.center = CGPointMake(self.view.center.x, navigationHeight + (pageHeight!/2.0))
            noShopVisitedImageView!.userInteractionEnabled = true
            self.view.insertSubview(noShopVisitedImageView!, atIndex: 0)
        }
        alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
        alert!.delegate = self
        self.view.addSubview(alert!)
        
        if getvisitedShopParser == nil
        {
            getvisitedShopParser = SearchShopByBusinessParser()
            getvisitedShopParser!.delegate = self
            getvisitedShopParser!.getSalesUserVisitedShops(index: -1)
        }
        
    }
        else{
             loadTableView()

        }
    }
    
    func loadTableView()
    {
        shopsList = salesDB!.selectAllVisitedShops()
        
        if shopsList!.count > 0
        {
            let padding = deviceManager!.deviceXCGFloatValue(xPos: 8.0)
            let tableHeight = self.view.frame.height - (navigationHeight +  padding)

            if tableOfShops == nil
            {
                tableOfShops = UITableView(frame: CGRectMake(0.0,navigationHeight,self.view.frame.size.width,pageHeight!))
                tableOfShops!.delegate = self
                tableOfShops!.dataSource = self
                tableOfShops!.bounces = false
                tableOfShops!.separatorStyle = UITableViewCellSeparatorStyle.None
                self.view.addSubview(tableOfShops!)
            }
            
            if noShopVisitedImageView != nil{
                noShopVisitedImageView!.hidden = true
            }
        }
        else
        {
            if noShopVisitedImageView == nil
            {
                noShopVisitedImageView = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "noshops")))
                noShopVisitedImageView!.center = CGPointMake(self.view.center.x, navigationHeight + (pageHeight!/2.0))
                self.view.insertSubview(noShopVisitedImageView!, atIndex: 0)
            }
            else{
                noShopVisitedImageView!.hidden = false
            }
        }
    }
    
    func  loadActionSheet(shopId : Int)
    {
        self.shopDetailController = nil
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let shopDetailAction = UIAlertAction(title: "Shop Details", style: .Default) { _ in
            if self.shopDetailController == nil {
                
                self.shopDetailController = ShopDetailViewController(shopID: shopId, latitude: self.latitude, longitude: self.longitude)
                self.navigationController!.pushViewController(self.shopDetailController!, animated: true)
            }
            
            
        }
        let notesAction = UIAlertAction(title: "Notes", style: .Default) { _ in
            
            if self.noteDetailController == nil {
                
                NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isShopBrochureDownloaded")
                self.noteDetailController = NoteDetailViewController(shopId: shopId)
                self.navigationController!.pushViewController(self.noteDetailController!, animated: true)
                
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { _ in
        }
        
        // Add the actions.
        alertController.addAction(shopDetailAction)
        alertController.addAction(notesAction)
        alertController.addAction(cancelAction)
        
        if let popoverPresentationController = alertController.popoverPresentationController {
            
            if deviceManager!.deviceType == deviceManager!.iPad{
                popoverPresentationController.sourceRect = CGRectMake(0.0, self.view.frame.size.height - 600.0, self.view.frame.size.width, 5.0)
            }
            else{
                popoverPresentationController.sourceRect = CGRectMake(0.0, self.view.frame.size.height - 5.0, self.view.frame.size.width, 5.0)
            }
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.permittedArrowDirections = .Up
        }
        
        presentViewController(alertController, animated: true, completion: nil)
        
        
    }
    
    //MARK: Unloading method
    
    func unloadPage()
    {
        unloadNavigationBar()
        unloadAlert()
        unloadNotesController()
        unloadTableOfNotes()
        
        if shopsList != nil {
            shopsList!.removeAll(keepCapacity: false)
        }
        shopsList = nil
    }
    
    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil
            
        }
    }
    
    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }
    
    func unloadNotesController()
    {
        if noteDetailController != nil
        {
            noteDetailController = nil
        }
    }
    
    func unloadTableOfNotes()
    {
        if tableOfShops != nil
        {
            tableOfShops!.removeFromSuperview()
            tableOfShops = nil
        }
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isFilterOn == true
        {
            return filterShopModel!.count
        }
        else
        {
            
            return shopsList!.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell : ShopListTableViewCell?
        
        cell = tableView.dequeueReusableCellWithIdentifier("shopList") as! ShopListTableViewCell?
        
        if cell == nil
        {
            cell = ShopListTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "shopList", cellWidth: tableView.frame.size.width, cellHeigth: cellHeight!)
        }
        
        var shop : ShopModel?
        
        if isFilterOn == true
        {
            shop =  filterShopModel![indexPath.row]
        }
        else
        {
            shop = shopsList![indexPath.row]
            
        }
             if shop!.shopId == -1
        {
            
            cell!.iconLabel!.text = shop!.initials
            cell!.defaultLabel!.text = shop!.name
            cell!.shopId = shop!.shopId
            cell!.defaultLabel!.hidden = false
            cell!.shopAddressLabel!.hidden = true
            cell!.shopNameLabel!.hidden = true
            cell!.osTypeLabel!.text = shop!.osType

            //cell!.osTypeLabel!.hidden = true

        }
        else
            {   cell!.defaultLabel!.hidden = true
                cell!.shopAddressLabel!.hidden = false
                cell!.shopNameLabel!.hidden = false
                
                cell!.iconLabel!.text = shop!.initials
                cell!.shopAddressLabel!.text = shop!.visitDate
                cell!.shopNameLabel!.text = shop!.name
                cell!.shopId = shop!.shopId
            cell!.osTypeLabel!.text = shop!.osType
        }
        
        
//        cell!.iconLabel!.text = shop!.initials
//        cell!.shopAddressLabel!.text = shop!.visitDate
//        cell!.shopNameLabel!.text = shop!.name
//        cell!.shopId = shop!.shopId
//        cell!.osTypeLabel!.text = shop!.osType
        return cell!
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return cellHeight!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let shop : ShopModel?
        
        if isFilterOn == true
        {
            shop =  filterShopModel![indexPath.row]
//            loadActionSheet(shop!.shopId!)

        }
        else
        {
            shop = shopsList![indexPath.row]
            loadActionSheet(shop!.shopId!)

        }
   
        
    }
    
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove seperator inset
        if cell.respondsToSelector(Selector("setSeparatorInset:")) {
            cell.separatorInset = UIEdgeInsetsZero
        }
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        // Explictly set your cell's layout margins
        if cell.respondsToSelector(Selector("setLayoutMargins:")) {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    //MARK: ScrollViewDelegate method
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if scrollView == tableOfShops
        {
            if self.tableOfShops!.contentOffset.y >= (self.tableOfShops!.contentSize.height - self.tableOfShops!.bounds.size.height)
            {
                if getvisitedShopParser == nil
                {
                    if isNoShopParserBool == false
                    {
                        getvisitedShopParser = SearchShopByBusinessParser()
                        getvisitedShopParser!.delegate = self
                        getvisitedShopParser!.getGetOldestvisitedShops(index: shopsList![shopsList!.count-1].id!)
                    }
                }
            }
        }
    }
    
    
    
    //MARK: NimapDualRoundButtonViewDelegate
    
    func didPrimaryDualRoundActionButtonPressed()
    {
        fullBgView!.removeFromSuperview()
        fullBgView = nil
        filterView!.removeFromSuperview()
        filterView = nil
        
        tableOfShops!.userInteractionEnabled = true
        navigationBar!.userInteractionEnabled = true
        
        if isWebChecked == true || isAndroidChecked == true || isIphoneChecked == true
        {
            filterShopModel!.removeAll(keepCapacity: true)
            //navigationBar!.rightButton!.hidden = true
            if isWebChecked == true
            {
                for i in 0..<shopsList!.count
                {
                    let shop = shopsList![i]
                    
                    if shop.osType!.lowercaseString == "web"
                    {
                        filterShopModel!.append(shop)
                    }
                }
            }
            
            if isAndroidChecked == true
            {
                for i in 0..<shopsList!.count
                {
                    let shop = shopsList![i]
                    
                    if shop.osType!.lowercaseString == "a"
                    {
                        filterShopModel!.append(shop)
                    }
                }
            }
            
            if isIphoneChecked == true
            {
                for i in 0..<shopsList!.count
                {
                    let shop = shopsList![i]
                    
                    if shop.osType!.lowercaseString == "i"
                    {
                        filterShopModel!.append(shop)
                    }
                }
            }
            
            isFilterOn = true
            
            if filterShopModel!.count <= 0{
                
                let shop = ShopModel()
                shop.shopId = -1
                shop.name = "No Shops for this Category."
                shop.initials = "CG"
                shop.rgbColor = "2,168,243"
                filterShopModel!.append(shop)
            }
            
            tableOfShops!.reloadData()
        }
        else
        {
            isFilterOn = false
        }
        
        shopsList = salesDB!.selectAllVisitedShops()
        tableOfShops!.reloadData()
    }
    
    
    
    
    func didSecondaryDualRoundActionButtonPressed()
    {
        fullBgView!.removeFromSuperview()
        fullBgView = nil
        filterView!.removeFromSuperview()
        filterView = nil
        
        tableOfShops!.userInteractionEnabled = true
        navigationBar!.userInteractionEnabled = true
        
        
    }
    
    //MARK: NimapAlertViewDelegate Methods
    
    // This method is called when user presses the primary actiona button on the NimapAlertView and depending upon the tag value we understand which alertview callback has come
    
    func didActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        if tag == "shopError"
        {
            self.navigationController!.popViewControllerAnimated(true)
            
        }
        if tag == "unauthorisedUserError"
        {
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
        if tag == "shopNotFoundError"
        {
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }
        }
        
    }
    
    //MARK: Event Handlers
    
    //MARK: NimapNavigationBarDelegate methods
    
    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func rightButtonPressed()
    {
        shopsList = salesDB!.selectAllVisitedShops()

        if shopsList!.count > 0
        {
            tableOfShops!.userInteractionEnabled = false
            navigationBar!.userInteractionEnabled = false
            
            
            fullBgView = UIView(frame: CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height))
            fullBgView!.center = self.view.center
            fullBgView!.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
            fullBgView!.userInteractionEnabled = false
            self.view.addSubview(fullBgView!)
            
            let height : CGFloat = (0.35 * self.view.frame.size.height)
            if deviceManager!.deviceType == deviceManager!.iPad
            {
                filterView = UIView(frame: CGRectMake(0,0,(0.4 * self.view.frame.size.width),height))
            }
            else
            {
                filterView = UIView(frame: CGRectMake(0,0,(0.6 * self.view.frame.size.width),height))
                
            }
            filterView!.center = self.view.center
            filterView!.layer.cornerRadius = 10.0
            filterView!.backgroundColor = UIColor.whiteColor()
            filterView!.userInteractionEnabled = true
            self.view.addSubview(filterView!)
            
            var posY = deviceManager!.deviceYCGFloatValue(yPos: 15.0)
            let YPadding = deviceManager!.deviceYCGFloatValue(yPos: 10.0)
            let XPadding = deviceManager!.deviceYCGFloatValue(yPos: 30.0)
            let labelHeight = deviceManager!.deviceYCGFloatValue(yPos: 25.0)
            
            filterLabel = UILabel(frame: CGRectMake(0,posY,filterView!.frame.size.width,labelHeight))
            
            filterLabel!.translatesAutoresizingMaskIntoConstraints = false
            filterLabel!.text = "Filter"
            filterLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceYCGFloatValue(yPos: 16.0))
            filterLabel!.textAlignment = NSTextAlignment.Center
            filterLabel!.textColor = UIColor.primaryColor()
            filterLabel!.userInteractionEnabled = false
            filterView!.addSubview(filterLabel!)
            
            posY = filterLabel!.frame.origin.y + filterLabel!.frame.size.height + YPadding
            let checkBoxWidth = (filterView!.frame.size.width - (XPadding)) * 0.3
            let LabelWidth = filterView!.frame.size.width - checkBoxWidth
            
            webTypeCheckBoxLabel = UILabel(frame: CGRectMake(XPadding,posY,checkBoxWidth,labelHeight))
            webTypeCheckBoxLabel!.userInteractionEnabled = true
            webTypeCheckBoxLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
            webTypeCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
            webTypeCheckBoxLabel!.textAlignment = NSTextAlignment.Center
            webTypeCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            filterView!.addSubview(webTypeCheckBoxLabel!)
            
            var posX = webTypeCheckBoxLabel!.frame.origin.x + webTypeCheckBoxLabel!.frame.size.width
            
            webTypeLabel = UILabel(frame: CGRectMake(posX,posY,LabelWidth,labelHeight))
            webTypeLabel!.translatesAutoresizingMaskIntoConstraints = false
            webTypeLabel!.text = "Web"
            webTypeLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 16.0))
            webTypeLabel!.textAlignment = NSTextAlignment.Left
            webTypeLabel!.textColor = UIColor.primaryColor()
            
            //  webTypeLabel!.backgroundColor = UIColor.blueColor()
            
            webTypeLabel!.userInteractionEnabled = false
            filterView!.addSubview(webTypeLabel!)
            
            
            posY = webTypeLabel!.frame.origin.y + webTypeLabel!.frame.size.height + deviceManager!.deviceYCGFloatValue(yPos: 5)
            
            androidTypeCheckBoxLabel = UILabel(frame: CGRectMake(XPadding,posY,checkBoxWidth,labelHeight))
            androidTypeCheckBoxLabel!.userInteractionEnabled = true
            androidTypeCheckBoxLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
            androidTypeCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
            androidTypeCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            androidTypeCheckBoxLabel!.textAlignment = NSTextAlignment.Center
            filterView!.addSubview(androidTypeCheckBoxLabel!)
            
            posX = androidTypeCheckBoxLabel!.frame.origin.x + androidTypeCheckBoxLabel!.frame.size.width
            
            androidTypeLabel = UILabel(frame: CGRectMake(posX,posY,LabelWidth,labelHeight))
            androidTypeLabel!.translatesAutoresizingMaskIntoConstraints = false
            androidTypeLabel!.text = "Android"
            androidTypeLabel!.textAlignment = NSTextAlignment.Left
            androidTypeLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 16.0))
            androidTypeLabel!.textColor = UIColor.primaryColor()
            androidTypeLabel!.userInteractionEnabled = false
            filterView!.addSubview(androidTypeLabel!)
            
            posY = androidTypeLabel!.frame.origin.y + androidTypeLabel!.frame.size.height + deviceManager!.deviceYCGFloatValue(yPos: 5)
            
            
            iphoneTypeCheckBoxLabel = UILabel(frame: CGRectMake(XPadding,posY,checkBoxWidth,labelHeight))
            iphoneTypeCheckBoxLabel!.userInteractionEnabled = true
            iphoneTypeCheckBoxLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 14.0))
            iphoneTypeCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
            iphoneTypeCheckBoxLabel!.textAlignment = NSTextAlignment.Center
            iphoneTypeCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            filterView!.addSubview(iphoneTypeCheckBoxLabel!)
            
            posX = iphoneTypeCheckBoxLabel!.frame.origin.x + iphoneTypeCheckBoxLabel!.frame.size.width
            
            iphoneTypeLabel = UILabel(frame: CGRectMake(posX,posY,LabelWidth,labelHeight))
            iphoneTypeLabel!.translatesAutoresizingMaskIntoConstraints = false
            iphoneTypeLabel!.text = "Iphone"
            iphoneTypeLabel!.font = UIFont.appFont(forSize: deviceManager!.deviceYCGFloatValue(yPos: 16.0))
            iphoneTypeLabel!.textAlignment = NSTextAlignment.Left
            iphoneTypeLabel!.textColor = UIColor.primaryColor()
            iphoneTypeLabel!.userInteractionEnabled = false
            filterView!.addSubview(iphoneTypeLabel!)
            
            
            actionButton = NimapDualRoundButtonView(frame: CGRectMake(deviceManager!.deviceXCGFloatValue(xPos: 10.0),0,filterView!.frame.size.width - deviceManager!.deviceXCGFloatValue(xPos: 20.0), deviceManager!.deviceYCGFloatValue(yPos:24.0)))
            actionButton!.center = CGPointMake(filterView!.frame.size.width/2, filterView!.frame.size.height)
            actionButton!.delegate = self
            filterView!.addSubview(actionButton!)
            actionButton!.leftActionButton!.setTitle("Ok", forState: UIControlState.Normal)
            actionButton!.rightActionButton!.setTitle("Cancel", forState: UIControlState.Normal)
            
            actionButton!.leftActionButton!.titleLabel!.font = UIFont.appFontMedium(forSize:deviceManager!.deviceXCGFloatValue(xPos: deviceManager!.deviceXCGFloatValue(xPos: 12)))
            actionButton!.rightActionButton!.titleLabel!.font = UIFont.appFontMedium(forSize: deviceManager!.deviceXCGFloatValue(xPos: 12))
            
            addCheckedProperties()
            
            
        }
            
        else{
            
            alert = NimapAlertView(frame: CGRectZero, tag: "", title: "Error", message: "No Shop", actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
            
        }
        
        
    }
    
    func addCheckedProperties()
    {
        if isWebChecked
        {
            webTypeCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
            webTypeCheckBoxLabel!.textColor = UIColor.primaryColor()
            
            
        }
        if isAndroidChecked
        {
            androidTypeCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
            androidTypeCheckBoxLabel!.textColor = UIColor.primaryColor()
            
        }
        
        if isIphoneChecked
        {
            iphoneTypeCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
            iphoneTypeCheckBoxLabel!.textColor = UIColor.primaryColor()
            
        }
        
        shopsList = salesDB!.selectAllVisitedShops()
        tableOfShops!.reloadData()
    }
    
    
    
    //MARK: UIResponder Methods
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = touches.first
        
        if touch!.view == webTypeCheckBoxLabel{
            if isWebChecked == false
            {
                isWebChecked = true
                webTypeCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
                webTypeCheckBoxLabel!.textColor = UIColor.primaryColor()
            }
            else
            {
                isWebChecked = false
                webTypeCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
                webTypeCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            }
        }
        if touch!.view == androidTypeCheckBoxLabel{
            if isAndroidChecked == false
            {
                isAndroidChecked = true
                androidTypeCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
                androidTypeCheckBoxLabel!.textColor = UIColor.primaryColor()
            }
            else
            {
                isAndroidChecked = false
                androidTypeCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
                androidTypeCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            }
        }
        if touch!.view == iphoneTypeCheckBoxLabel{
            if isIphoneChecked == false
            {
                isIphoneChecked = true
                iphoneTypeCheckBoxLabel!.text = AppConstant.Static.CHECKED_CHECKBOX_ICON
                iphoneTypeCheckBoxLabel!.textColor = UIColor.primaryColor()
            }
            else
            {
                isIphoneChecked = false
                iphoneTypeCheckBoxLabel!.text = AppConstant.Static.UNCHECKED_CHECKBOX_ICON
                iphoneTypeCheckBoxLabel!.textColor = UIColor.secondaryTextColor()
            }
        }
    }
    
    //MARK: SearchShopByBusinessParserDelegate methods
    
    func didReceivedSearchShopsParserError(status : Int)
    {
        getvisitedShopParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        
        if status == AppConstant.Static.ADD_USER_SUCCESS
        {
            if tableOfShops != nil
            {
                shopsList = salesDB!.selectAllVisitedShops()
                tableOfShops!.reloadData()
            }
            else
            {
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "downloadAllVisitedShops")

                loadTableView()
            }
            
        }
        else
        {
            var errorMessege = ""
            var errorTag = ""
            
            if status == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessege = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
                errorTag = "shopError"
            }
            else if status == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessege = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
                errorTag = "shopError"
            }
            else if status == AppConstant.Static.NO_SHOP_AVAILABLE
            {
                isNoShopParserBool = true
            }
            else if status == AppConstant.Static.UNAUTHORISED_USER
            {
                errorMessege = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                errorTag = "unauthorisedUserError"
            }
            
            if isNoShopParserBool == false
            {
                alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessege, actionButtonTitle: "OK", animate : true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
        }
    }
}