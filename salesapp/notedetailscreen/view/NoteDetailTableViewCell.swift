//
//  NoteDetailTableViewCell.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

protocol NoteDetailTableViewCellDelegate : NSObjectProtocol
{
    func didReadMoreSelected(index : Int!)
    func didCompressReadMoreButton(index : Int!)
}

class NoteDetailTableViewCell: UITableViewCell
{

    weak var delegate : NoteDetailTableViewCellDelegate?
    var deviceManager : DeviceManager?

    var NotesTextView : UITextView?
    var NotesDateLabel : UILabel?
    var bottomLine : UIView?

    var cellIndex : Int?
    var isCellExpanded : Bool?

    var isFirstTime = false

    var starHeight :  CGFloat = 0.0
    var yPos : CGFloat = 0.0

    init(style: UITableViewCellStyle, reuseIdentifier: String?, cellWidth : CGFloat, cellHeigth : CGFloat) {

        super.init(style: style, reuseIdentifier: reuseIdentifier)

        deviceManager = DeviceManager.sharedDeviceManagement()

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 15.0)
        let paddingY = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        let labelWidth = cellWidth - (2 * padding)
        let labelHeight = (cellHeigth - (3 * paddingY))/2

        NotesTextView = UITextView(frame: CGRectMake(padding,paddingY,labelWidth,labelHeight))
        NotesTextView!.text = "Review"
        NotesTextView!.textColor = UIColor.primaryTextColor()
        NotesTextView!.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))
        NotesTextView!.textAlignment = NSTextAlignment.Left
        NotesTextView!.scrollEnabled = false
        NotesTextView!.userInteractionEnabled = true
        NotesTextView!.contentInset = UIEdgeInsetsMake(deviceManager!.deviceXCGFloatValue(xPos: -5.0), 0, 0, 0)
        NotesTextView!.editable = false
        NotesTextView!.backgroundColor = UIColor.whiteColor()
        self.addSubview(NotesTextView!)
        NotesTextView!.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(NoteDetailTableViewCell.reviewTapped(_:))))

        let widthNoteLabel = (cellWidth*0.40) - (1.5*padding)
        let posYNoteLabel = paddingY + labelHeight + paddingY

        NotesDateLabel = UILabel(frame: CGRectMake(cellWidth*0.60,posYNoteLabel,widthNoteLabel,labelHeight))
        NotesDateLabel!.textColor = UIColor.secondaryTextColor()
        NotesDateLabel!.font = UIFont.appFontAwesome(forSize: deviceManager!.deviceXCGFloatValue(xPos: 12.0))
        self.addSubview(NotesDateLabel!)
        

        bottomLine = UIView(frame: CGRectMake(0, self.frame.size.height - 0.2, self.frame.size.width, 0.2))
        bottomLine!.backgroundColor = UIColor.dividerColor()
        self.addSubview(bottomLine!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {

        if isFirstTime == false
        {
            isFirstTime = true

            bottomLine!.frame = CGRectMake(0.0,self.frame.size.height - 1.0,self.frame.size.width,1.0)
        }
    }

    // MARK: Local event handler
    //MARK: UITextView tap gesture recognizer
    /*
     This function finds the character index where you are touching the UITextView and if it is within text range it trys to get the custom tag which was set for the attributed text on which we need to interact. Once we get the touch detection we send the control to controller for increasing the cell height
     */
    func reviewTapped(gesture : UITapGestureRecognizer)
    {
        let textView = gesture.view as! UITextView

        if isCellExpanded == true
        {
            if delegate != nil
            {
                delegate!.didCompressReadMoreButton(cellIndex)
            }
        }
        else
        {
            // Location of the tap in text-container coordinates

            let layoutManager : NSLayoutManager = textView.layoutManager
            var location = gesture.locationInView(textView)

            location.x = location.x - textView.textContainerInset.left;
            location.y = location.y - textView.textContainerInset.top;

            // Find the character that's been tapped on

            var characterIndex : Int?
            characterIndex = layoutManager.characterIndexForPoint(location, inTextContainer: textView.textContainer, fractionOfDistanceBetweenInsertionPoints: nil)

            if (characterIndex < textView.textStorage.length) {

                //var range : NSRangePointer?

                var range : NSRange? = NSMakeRange(0, 1)

                let value: AnyObject? = textView.attributedText.attribute("READMORE", atIndex: characterIndex!, effectiveRange: &range!)

                if value != nil
                {
                    if delegate != nil
                    {
                        delegate!.didReadMoreSelected(cellIndex!)
                    }
                }
                
                
            }
        }
        
        
    }
    
}
