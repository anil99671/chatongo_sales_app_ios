//
//  NoteDetailViewController.swift
//  COG Sales
//
//  Created by Apple on 13/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController , NimapNavigationBarViewDelegate, NimapAlertViewDelegate, UITableViewDelegate, UITableViewDataSource, NoteDetailTableViewCellDelegate, AddShopNoteParserDelegate, GetShopNotesParserDelegate
{

    // MARK: - Variable Declarations

    var deviceManager : DeviceManager?
    var navigationBar : NimapNavigationBarView?
    var navigationHeight : CGFloat = 0.0
    var pageHeight : CGFloat = 0.0

    var bottomLine : UIView?
    var tableOfNotes: UITableView?
    var alert : NimapAlertView?
    var salesDB : SalesAppDBModel?
    var addNoteParser : AddShopNoteParser?
    var getNoteParser : GetShopNotesParser?
    var notes : [ShopNotesModel]?

    var padding : CGFloat?
    var posY : CGFloat?
    var ratingViewHeight : CGFloat?
    var cellHeight : CGFloat?
    var shopId : Int?
    var noteString : String?

    //MARK: - Constructor
    init(shopId : Int){

        super.init(nibName: nil, bundle: nil)

        self.shopId = shopId
        deviceManager = DeviceManager.sharedDeviceManagement()
        salesDB = SalesAppDBModel.sharedInstance()
        cellHeight = deviceManager!.deviceXCGFloatValue(xPos: 56.0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    //MARK: - Life Cycle methods

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.view.backgroundColor = UIColor.appBGColor()
        self.edgesForExtendedLayout = UIRectEdge.None
    }

    override func viewWillAppear(animated: Bool)
    {
        loadPage()
    }

    override func viewDidDisappear(animated: Bool)
    {
        unloadPage()
    }

    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Load & Unload functions

    func loadPage()
    {
        loadNavigationBar()
        loadGetAllNoteParser()
    }

    func loadNavigationBar()
    {
        navigationHeight = 0.0

        if deviceManager!.deviceType == deviceManager!.iPhone || deviceManager!.deviceType == deviceManager!.iPhone5 || deviceManager!.deviceType == deviceManager!.iPhone6 || deviceManager!.deviceType == deviceManager!.iPhone6plus
        {
            navigationHeight =  AppConstant.Static.NAVIGATION_BAR_HEIGHT
        }
        else
        {
            navigationHeight =  deviceManager!.deviceYCGFloatValue(yPos: AppConstant.Static.NAVIGATION_BAR_HEIGHT)
        }

        navigationBar = NimapNavigationBarView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, navigationHeight), title: "Notes")
        navigationBar!.delegate = self
        self.view.addSubview(navigationBar!)
        navigationBar!.addLeftButtonWithIcon()
        navigationBar!.updateLeftButtonTitle(AppConstant.Static.BACK_ICON)

        navigationBar!.addRightButtonWithIcon()
        navigationBar!.updateRightButtonTitle(AppConstant.Static.ADD_ICON)

        navigationHeight = navigationBar!.frame.size.height
        pageHeight = self.view.frame.height - navigationHeight
    }

    func loadGetAllNoteParser()
    {
        // Activity Indicator

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        alert = NimapAlertView(frame: CGRectZero, tag: "verifyingUser", isActivity: true, animate: false)
        alert!.delegate = self
        self.view.addSubview(alert!)

        if getNoteParser == nil
        {
            getNoteParser = GetShopNotesParser()
            getNoteParser!.delegate = self
            getNoteParser!.getShopnotes(shopId: shopId!)
        }
        
    }

    func laodNotesTable()
    {
        if tableOfNotes == nil
        {
            tableOfNotes = UITableView(frame: CGRectMake(0.0,navigationHeight, self.view.frame.width, pageHeight))
            tableOfNotes!.dataSource = self
            tableOfNotes!.delegate = self
            tableOfNotes!.backgroundColor = UIColor.appBGColor()
            tableOfNotes!.separatorStyle = UITableViewCellSeparatorStyle.None
            self.view.addSubview(tableOfNotes!)

            bottomLine = UIView(frame: CGRectMake(0, tableOfNotes!.frame.origin.y - deviceManager!.deviceYCGFloatValue(yPos: 1.0), self.view.frame.size.width, 1))
            bottomLine!.backgroundColor = UIColor.primaryColor()
            self.view.addSubview(bottomLine!)
        }
    }

    //MARK: Unloading method

    func unloadPage()
    {
        unloadNavigationBar()
        unloadAlert()
        unloadParser()
        unloadNoteTable()

        if notes != nil {
            notes!.removeAll(keepCapacity: false)
        }
        notes = nil
    }

    func unloadNavigationBar()
    {
        if navigationBar != nil
        {
            navigationBar!.removeFromSuperview()
            navigationBar = nil

        }
    }

    func unloadAlert()
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    func unloadParser()
    {
        if addNoteParser != nil
        {
            addNoteParser = nil
        }

        if getNoteParser != nil
        {
            getNoteParser = nil
        }
    }

    func unloadNoteTable()
    {
        if tableOfNotes != nil
        {
            tableOfNotes!.removeFromSuperview()
            tableOfNotes = nil
        }
    }

    //MARK: - UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notes!.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell : NoteDetailTableViewCell?

        cell = tableOfNotes!.dequeueReusableCellWithIdentifier("review") as! NoteDetailTableViewCell!

        if cell == nil
        {
            cell = NoteDetailTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "review", cellWidth: tableView.frame.size.width,cellHeigth: cellHeight!)
            cell!.backgroundColor = UIColor.appBGColor()
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            cell!.delegate = self
        }

        let note = notes![indexPath.row]

        cell!.cellIndex = indexPath.row
        cell!.isCellExpanded = note.isExpanded
        cell!.NotesTextView!.attributedText = nil

        cell!.NotesTextView!.text = note.notes
        cell!.NotesDateLabel!.text = note.createdDate

        if note.isExpanded == false
        {
            // We need to process the count and read more logic only when cell is not extended.
            if note.notes!.characters.count > AppConstant.Static.REVIEW_CELL_READ_MORE_CHARACTER_LIMIT + 11
            {
                // This means content is more we need to compress the content and provide read more option for user to expand the cell.

                let attrs = [NSFontAttributeName : UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))]
                let reviewComment = NSMutableAttributedString(string:"", attributes:attrs)

                let initialString : String =  (note.notes! as NSString).substringWithRange(NSRange(location: 0, length: AppConstant.Static.REVIEW_CELL_READ_MORE_CHARACTER_LIMIT))

                cell!.NotesTextView!.text = initialString

                // We need to make the ...ReadMore look like clickable so when user clicks it the cell extends and complete review is visible

                let attributes = [ NSFontAttributeName: UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))]

                let attributedMutableString: NSMutableAttributedString = NSMutableAttributedString(string: initialString, attributes: attributes)

                let readMoreAttributedString = NSAttributedString(string: "...ReadMore", attributes: ["READMORE":true,NSForegroundColorAttributeName: UIColor.primaryColor(),NSFontAttributeName: UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))])

                attributedMutableString.appendAttributedString(readMoreAttributedString)

                reviewComment.appendAttributedString(attributedMutableString)

                cell!.NotesTextView!.text =  initialString + "...ReadMore"
                cell!.NotesTextView!.attributedText = reviewComment
            }
        }

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 15.0)
        let paddingY = deviceManager!.deviceXCGFloatValue(xPos: 10.0)


        // Realigning the frames as per the cell content.
        cell!.NotesTextView!.frame = CGRectMake(padding, paddingY, self.view.frame.size.width - (2 * padding),calculateHeight(content: cell!.NotesTextView!.text))

        var yPos = paddingY + cell!.NotesTextView!.frame.size.height + paddingY

        cell!.NotesDateLabel!.frame = CGRectMake(cell!.NotesDateLabel!.frame.origin.x, yPos, cell!.NotesDateLabel!.frame.width,cell!.NotesDateLabel!.frame.height)

        // Realigning THE BOTTOM line 1 pixel above the bottom margin.

        yPos = yPos + cell!.NotesDateLabel!.frame.size.height + paddingY

        cell!.bottomLine!.frame = CGRectMake(0.0,yPos - 1.0,self.view.frame.size.width,1.0)

//        cell!.bottomLine!.hidden = false

        return cell!
    }

    // MARK: - UITableViewDelegate

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        let note = notes![indexPath.row]

        let padding = deviceManager!.deviceXCGFloatValue(xPos: 10.0)
        let labelHeight : CGFloat = (cellHeight! - (3 * padding))/2

        if note.isExpanded == false
        {
            // We need to process the count and read more logoc only when cell is not extended.
            if note.notes!.characters.count > AppConstant.Static.REVIEW_CELL_READ_MORE_CHARACTER_LIMIT + 11
            {
                // This means content is more we need to compress the content and provide read more option for user to expand the cell.
                var initialString : String =  (note.notes! as NSString).substringWithRange(NSRange(location: 0, length: AppConstant.Static.REVIEW_CELL_READ_MORE_CHARACTER_LIMIT))
                initialString += "...ReadMore"
                return calculateHeight(content: initialString) + labelHeight + (3*padding)
            }
        }

        return calculateHeight(content: note.notes!) + labelHeight + (3*padding)
    }

    //MARK: Various Web service callbacks

    //MARK:  AddShopNoteParserDelegate callbacks

    func didReceivedAddShopNoteResult(resultCode resultCode : Int)
    {
        addNoteParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        var errorMessage = ""
        var tag = ""

        if resultCode == AppConstant.Static.ADD_NOTES_SUCCESS
        {
            errorMessage = AppConstant.Static.ADD_NOTES_SUCCESS_MESSEGE
            tag = "successnoteadded"
        }
        else if resultCode == AppConstant.Static.CONNECTION_ERROR
        {
            errorMessage = AppConstant.Static.GET_PRODUCT_REVIEWS_CONNECTION_ERROR_MESSAGE
            tag = "gettingnoteError"
        }
        else if resultCode == AppConstant.Static.PROCESSING_ERROR
        {
            errorMessage = AppConstant.Static.GET_PRODUCT_REVIEWS_PROCESSING_ERROR_MESSAGE
            tag = "gettingnoteError"
        }
        else if resultCode == AppConstant.Static.ADD_NOTE_INVALID_ERROR
        {
            errorMessage = AppConstant.Static.ADD_NOTE_INVALID_ERROR_MESSEGE
            tag = "addRatingError"
        }
        else if resultCode == AppConstant.Static.UNAUTHORISED_USER
        {
            errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
            tag = "unauthorisedUserError"
        }

        if errorMessage != ""
        {
            alert = NimapAlertView(frame: CGRectZero, tag: tag, title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    //MARK: NoteDetailTableViewCellDelegate Methods

    func didReadMoreSelected(index : Int!)
    {
        let note = notes![index!]

        note.isExpanded = true

        tableOfNotes!.reloadData()
    }

    func didCompressReadMoreButton(index : Int!)
    {
        let note = notes![index]

        if note.isExpanded == true
        {
            note.isExpanded = false
            tableOfNotes!.reloadData()
        }
    }

    //MARK: Local Helper Methods

    func calculateHeight(content content : String!) -> CGFloat
    {
        let padding = deviceManager!.deviceXCGFloatValue(xPos: 15.0)
        let dummylabel = UITextView(frame: CGRectMake(padding,CGFloat(0.0),self.view.frame.width-(2*padding),self.view.frame.height))

        dummylabel.font = UIFont.appFont(forSize: deviceManager!.deviceXCGFloatValue(xPos: 13.0))
        dummylabel.text = content
        dummylabel.sizeToFit()

        return dummylabel.frame.size.height
    }

    // MARK: NimapNavigationBarViewDelegate


    func backButtonPressed()
    {
        self.navigationController!.popViewControllerAnimated(true)

    }

    func rightButtonPressed(){

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        alert = NimapAlertView(frame: CGRectZero, noteTag: "addnotetag", titleForTextView: "Notes", message: "na", actionButtonTitle: "OK", secondActionButtonTitle: "CANCEL", animate: true)
        alert!.delegate = self
        self.view.addSubview(alert!)
    }

    // MARK: NimapAlertViewDelegate callbacks

    func didActionButtonPressed(tag : String!)
    {
        if tag == "unauthorisedUserError"
        {
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }
            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            appDelegate!.applicationTerminateWithUnauthorisedUserAccess()
        }
        else if tag == "addnotetag"
        {

            if alert!.inputTextView!.text == ""
            {
                if alert != nil{
                    alert!.removeFromSuperview()
                    alert = nil
                }
                alert = NimapAlertView(frame: CGRectZero, tag: "noteemptytag", title: "", message: "Please write notes!", actionButtonTitle: "OK", animate: true)
                alert!.delegate = self
                self.view.addSubview(alert!)
            }
            else
            {

                noteString = alert!.inputTextView!.text
                if addNoteParser == nil
                {
                    if alert != nil{
                        alert!.removeFromSuperview()
                        alert = nil
                    }
                    addNoteParser = AddShopNoteParser()
                    addNoteParser!.delegate = self
                    addNoteParser!.addShopNote(shopId: shopId!, note: noteString!)
                }
            }
        }
        else if tag == "successnoteadded"
        {
            if alert != nil
            {
                alert!.removeFromSuperview()
                alert = nil
            }
            
            loadGetAllNoteParser()
        }
        else
        {
            if alert != nil{
                alert!.removeFromSuperview()
                alert = nil
            }
        }
    }

    func didSecondaryActionButtonPressed(tag : String!)
    {
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
    }

    //MARK: GetShopNotesParserDelegate method

    func didReceivedGetShopNotesError(resultCode resultCode : Int)
    {
        getNoteParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        var errorMessege = ""
        var errorTag = ""

        if resultCode == AppConstant.Static.CONNECTION_ERROR
        {
            errorMessege = AppConstant.Static.ADD_USER_CONNECTION_ERROR_MESSAGE
            errorTag = "NoteError"
        }
        else if resultCode == AppConstant.Static.PROCESSING_ERROR
        {
            errorMessege = AppConstant.Static.ADD_USER_PROCESSING_ERROR_MESSAGE
            errorTag = "NoteError"
        }
        else if resultCode == AppConstant.Static.NO_DATA_AVAILABLE
        {
            errorMessege = AppConstant.Static.NO_NOTES_DATA_AVAILABLE_MESSEGE
            errorTag = "NoteNotFoundError"

        }

        alert = NimapAlertView(frame: CGRectZero, tag: errorTag, title: "Error", message: errorMessege, actionButtonTitle: "OK", animate : true)
        alert!.delegate = self
        self.view.addSubview(alert!)


    }

    func didReceivedGetShopNotes(shopNote shopNote : [ShopNotesModel])
    {
        getNoteParser = nil
        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }
        notes = shopNote
        unloadNoteTable()
        laodNotesTable()
    }
}
