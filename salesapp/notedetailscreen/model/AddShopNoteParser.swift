//
//  AddShopNoteParser.swift
//  COG Sales
//
//  Created by Apple on 12/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

@objc protocol AddShopNoteParserDelegate : NSObjectProtocol
{
    func didReceivedAddShopNoteResult(resultCode resultCode : Int)
}

class AddShopNoteParser: NSObject,AsyncLoaderModelDelegate {

    //MARK: Varibale Decalaration

    var loader : AsyncLoaderModel?
    weak var delegate : AddShopNoteParserDelegate?


    func addShopNote(shopId shopId: Int, note : String)
    {
        let jsonDictionary = NSMutableDictionary()
        jsonDictionary.setObject(0, forKey: "Id")
        jsonDictionary.setObject(shopId, forKey: "ShopId")
        jsonDictionary.setObject(note, forKey: "Note")
        jsonDictionary.setObject("", forKey: "IsVisited")
        jsonDictionary.setObject("", forKey: "IsAppDownloaded")

        var jsonData: NSData?
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(jsonDictionary, options:NSJSONWritingOptions.PrettyPrinted)
        } catch {
            jsonData = nil
        }
        let jsonDataLenght = "\(jsonData!.length)"

        let requestString = NSString(data: jsonData!, encoding: NSUTF8StringEncoding)
        print("AddShopNote requestString is \(requestString!)")

        let url = NSURL(string: AppConstant.Static.BASE_URL+"AddShopNote")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue(jsonDataLenght, forHTTPHeaderField: "Content-Length")
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData

        loader = AsyncLoaderModel()
        loader!.getDataFromRequest(request: request, dataIndex: -1)
        loader!.delegate = self
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("AddShopNote responseString is \(responseString!)")
        processGenerateOTP(data: data)
        self.loader = nil
    }

    func didReceivedErrorLoader(loader loader : AsyncLoaderModel!, dataIndex : Int)
    {
        if delegate != nil
        {
            delegate!.didReceivedAddShopNoteResult(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func processGenerateOTP(data data : NSData)
    {
        let result : NSDictionary?

        do{

            try result = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary

            let Status = result!.objectForKey("Status") as! Int

            if Status == 200
            {
                let shopNoteModel = ShopNotesModel()
                let shopNoteDict = result!.objectForKey("data") as! NSDictionary

                shopNoteModel.id = shopNoteDict.objectForKey("Id") as? Int
                shopNoteModel.shopId = shopNoteDict.objectForKey("ShopId") as? Int
                shopNoteModel.notes = shopNoteDict.objectForKey("Notes") as? String
                shopNoteModel.createdDate = shopNoteDict.objectForKey("CreatedDate") as? String
                shopNoteModel.modifiedDate = shopNoteDict.objectForKey("ModifiedDate") as? String
                
                if delegate != nil
                {
                    delegate!.didReceivedAddShopNoteResult(resultCode: AppConstant.Static.ADD_NOTES_SUCCESS)
                }
            }
            else if Status == 401
            {
                if delegate != nil
                {
                    delegate!.didReceivedAddShopNoteResult(resultCode: AppConstant.Static.UNAUTHORISED_USER)
                }
            }
            else if Status == 422
            {
                if delegate != nil
                {
                    delegate!.didReceivedAddShopNoteResult(resultCode: AppConstant.Static.ADD_NOTE_INVALID_ERROR)
                }
            }
            else
            {
                if delegate != nil
                {
                    delegate!.didReceivedAddShopNoteResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }


        }
        catch
        {
            if delegate != nil
            {
               delegate!.didReceivedAddShopNoteResult(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }

}