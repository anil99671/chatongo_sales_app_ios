//
//  GetShopNotesParser.swift
//  COG Sales
//
//  Created by Apple on 12/07/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

@objc protocol GetShopNotesParserDelegate : NSObjectProtocol
{
    func didReceivedGetShopNotesError(resultCode resultCode : Int)
    func didReceivedGetShopNotes(shopNote shopNote : [ShopNotesModel])
}

class GetShopNotesParser: NSObject, AsyncLoaderModelDelegate
{
    var asyncloader : AsyncLoaderModel?
    var delegate : GetShopNotesParserDelegate?

    var isUnauthorized : Int = -1
    var searchString : String?

    func getShopnotes(shopId shopId : Int)
    {
        asyncloader = AsyncLoaderModel()
        asyncloader!.getDataFromURLString(webURL:AppConstant.Static.BASE_URL+"GetShopNotes?ShopId=\(shopId)", dataIndex: -1)
        asyncloader!.delegate = self
    }

    func didReceivedData(data data : NSData!, loader : AsyncLoaderModel!, dataIndex : Int)
    {
        let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
        print("responseString GetShopNotes \(responseString!)")
        processData(data: data)
        self.asyncloader = nil
    }

    func didReceivedErrorLoader(loader loader: AsyncLoaderModel!, dataIndex: Int)
    {
        if (delegate != nil)
        {
            delegate!.didReceivedGetShopNotesError(resultCode: AppConstant.Static.CONNECTION_ERROR)
        }
    }

    func processData(data data : NSData)
    {
        var json : AnyObject?
        do
        {
            json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary

            let Status = json!.objectForKey("Status") as! Int

            if Status == 200
            {
                let NoteDict = json!.objectForKey("data") as! NSArray
                var reviewArray : [ShopNotesModel] = []

                for i in 0..<NoteDict.count
                {
                    let shopNote = ShopNotesModel()

                    shopNote.id = NoteDict[i].valueForKey("Id") as! Int!
                    shopNote.shopId = NoteDict[i].valueForKey("ShopId") as! Int!
                    shopNote.modifiedDate = NoteDict[i].valueForKey("ModifiedDate") as! String!
                    shopNote.createdDate = NoteDict[i].valueForKey("CreatedDate") as! String!
                    shopNote.notes = NoteDict[i].valueForKey("Notes") as! String!
                    shopNote.isExpanded = false
                    reviewArray.append(shopNote)
                }

                if (delegate != nil)
                {
                    delegate!.didReceivedGetShopNotes(shopNote: reviewArray)

                }
            }
            else if Status == 404
            {
                if (delegate != nil)
                {
                    delegate!.didReceivedGetShopNotesError(resultCode: AppConstant.Static.NO_DATA_AVAILABLE)
                }
            }
            else
            {
                if (delegate != nil)
                {
                    delegate!.didReceivedGetShopNotesError(resultCode: AppConstant.Static.PROCESSING_ERROR)
                }
            }
        }
        catch
        {
            if (delegate != nil)
            {
                delegate!.didReceivedGetShopNotesError(resultCode: AppConstant.Static.PROCESSING_ERROR)
            }
        }
    }
}