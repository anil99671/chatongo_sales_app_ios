//
//  ViewController.swift
//  COG Sales
//
//  Created by Apple on 30/05/16.
//  Copyright © 2016 Nimap Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController, AddUserLogParserDelegate ,GetUserTypesParserDelegate,NimapAlertViewDelegate,UpdateDeviceTokenParserDelegate, LoginUserParserDelegate {

    let APP_UPDATE_MESSAGE = "You are using older version of the app. " +
    "Please update to continue."

    var deviceManager : DeviceManager?

    var activity : UIActivityIndicatorView?
    var bgImage : UIImageView?

    //Variouds Parsers
    var businessCategoryParser : GetBusinessCategoriesParser?
    var addUserLogParser : AddUserLogParser?
    var getUserTypeParser : GetUserTypesParser?
    var loginUserParser : LoginUserParser?

    var deviceTokenParser : UpdateDeviceTokenParser?

    var phone : String?
    var email : String?
    var isVerified : Bool?

    var alert : NimapAlertView?

    //MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.view.backgroundColor = UIColor.whiteColor()
        self.edgesForExtendedLayout = UIRectEdge.None

        deviceManager = DeviceManager.sharedDeviceManagement()

        businessCategoryParser = GetBusinessCategoriesParser()
        businessCategoryParser!.getCategoriesBanners()

        // Need to call logging parser at first

        addUserLogParser = AddUserLogParser()
        addUserLogParser!.delegate = self
        addUserLogParser!.addUserLog()

        phone = NSUserDefaults.standardUserDefaults().objectForKey("mobileNo") as! String?
        email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String?
        isVerified = NSUserDefaults.standardUserDefaults().boolForKey("isVerified")
    }

    override func viewWillAppear(animated: Bool) {
        loadCompanyScreenBG()
        loadActivity()
    }

    override func viewDidDisappear(animated: Bool) {
        unloadActivity()
        unloadCompanyScreenBG()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Loading Method

    func loadActivity()
    {
        if activity == nil{

            if deviceManager!.deviceType == deviceManager!.iPad
            {
                activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
                activity!.center = CGPointMake(self.view.center.x + deviceManager!.deviceXCGFloatValue(xPos: 6.0), self.view.center.y - deviceManager!.deviceYCGFloatValue(yPos: 32.0))
            }
            else
            {
                activity = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
                activity!.center = CGPointMake(self.view.center.x + deviceManager!.deviceXCGFloatValue(xPos: 6.0), self.view.center.y - deviceManager!.deviceYCGFloatValue(yPos: 21.0))
            }

            activity!.color = UIColor.primaryColor()
            activity!.startAnimating()
            self.view.addSubview(activity!)
        }

    }

    func loadCompanyScreenBG()
    {
        if bgImage == nil{
            bgImage = UIImageView(image: UIImage(named: deviceManager!.resourceNameAsPerDevice(fileName: "launchscreen")))
            self.view.addSubview(bgImage!)
        }

    }

    //MARK: Unloading Method

    func unloadActivity()
    {
        if activity != nil
        {
            activity!.removeFromSuperview()
            activity = nil
        }
    }

    func unloadCompanyScreenBG()
    {
        if bgImage != nil
        {
            bgImage!.removeFromSuperview()
            bgImage = nil
        }
    }

    //MARK: Various Callbacks

    //MARK: AddUserLogParserDelegate methods
    func didReceivedAddUserLogParserResult(appVersionStatus status : Int)
    {
        addUserLogParser = nil

        //let status1 = AddUserLogParser.APP_VERSION_DIFFERENT_NO_FORCE_UPDATE

        if status == AddUserLogParser.APP_VERSION_DIFFERENT_FORCE_UPDATE{

            // Need to alert the user for updating the app and navigating to the app store to update the app
            if alert != nil{
                alert!.removeFromSuperview()
                alert = nil
            }

            alert = NimapAlertView(frame: CGRectZero, tag:"updateApp", title: "Update", message: "You are using older version of the app. Please update to continue.", actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
        else
        {
            /*
             We need to check whether user is coming to the app for the first time or already registered. If user is first time than we do not have to call login service from the server. Every login we need otp to be generated for verification. As we have skip provision as well in that case we need to show user regsitration screen every time.
             */

            if phone != nil && isVerified == true
            {
                print("phone \(phone!)")
                // we need to call the login user web service and on success of that we navigate user to the home screen

                if loginUserParser == nil
                {
                    loginUserParser = LoginUserParser()
                    loginUserParser!.delegate = self
                    loginUserParser!.loginUserWith(countryCode: NSUserDefaults.standardUserDefaults().objectForKey("countryCode") as! String!, mobileno: "\(phone!)", emailid: email)
                }
            }
            else
            {
                if getUserTypeParser == nil{

                    getUserTypeParser = GetUserTypesParser()
                    getUserTypeParser!.delegate = self
                    getUserTypeParser!.getUserTypes()
                }
            }
        }
    }

    //MARK: GetUserTypesParserDelegate methods
    func didReceivedGetUserTypesResult(resultCode resultCode : Int)
    {
        getUserTypeParser = nil

        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }

        // we hide the loading activity when we get an error. As when we get the success we purposefully keep the loading active thus when delay happens that time we will hide the loading
        if resultCode == AppConstant.Static.GET_USER_TYPE_SUCCESS
        {
            // Once we get the user types from the server we make 1 sec delay and move to the registration page as user has launched this app for the first time.. We need to check whether user was logged in before or not. if user never logged in than only registration page will be shown
            isVerified = NSUserDefaults.standardUserDefaults().boolForKey("isVerified")
            if isVerified == false
            {
                NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(ViewController.navigateToStartScreen), userInfo: nil, repeats: false)
            }

        }
        else
        {
            // If Error comes we need to display appropriate Error dialogue to user

            activity!.stopAnimating()
            activity!.hidden = true

            if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                alert = NimapAlertView(frame: CGRectZero, tag:"getUserTypeError", title: "Error", message: AppConstant.Static.GET_USER_TYPE_CONNECTION_ERROR_MESSAGE, actionButtonTitle: "OK", animate : true)
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                alert = NimapAlertView(frame: CGRectZero, tag:"getUserTypeError", title: "Error", message: AppConstant.Static.GET_USER_TYPE_PROCESSING_ERROR_MESSAGE, actionButtonTitle: "OK", animate : true)
            }
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    //MARK: LoginUserParserDelegate Callbacks

    func didReceivedLoginUserResult(resultCode resultCode : Int)
    {
        loginUserParser = nil

        if alert != nil
        {
            alert!.removeFromSuperview()
            alert = nil
        }

        var errorMessage = ""
        var tag = ""

        if resultCode == AppConstant.Static.LOGIN_USER_SUCCESS
        {
            navigateToHomeScreen()
        }
        else
        {
            if resultCode == AppConstant.Static.UNAUTHORISED_USER || resultCode ==
                AppConstant.Static.DATA_NOT_FOUND_ERROR
            {
                errorMessage = AppConstant.Static.AUTHENTICATION_FAIL_MESSAGE
                tag = "unauthorisedUserError"
            }
            else if resultCode == AppConstant.Static.CONNECTION_ERROR
            {
                errorMessage = AppConstant.Static.LOGIN_USER_CONNECTION_ERROR_MESSAGE
                tag = "loginUserError"
            }
            else if resultCode == AppConstant.Static.PROCESSING_ERROR
            {
                errorMessage = AppConstant.Static.LOGIN_USER_PROCESSING_ERROR_MESSAGE
                tag = "loginUserError"
            }

            alert = NimapAlertView(frame: CGRectZero, tag: tag, title: "Error", message: errorMessage, actionButtonTitle: "OK", animate : true)
            alert!.delegate = self
            self.view.addSubview(alert!)
        }
    }

    //MARK: Event Handlers

    //MARK: NimapAlertViewDelegate
    func didActionButtonPressed(tag : String!)
    {
        if alert != nil{
            alert!.removeFromSuperview()
            alert = nil
        }

        if tag == "getUserTypeError"
        {
            activity!.startAnimating()
            activity!.hidden = false

            if getUserTypeParser == nil{

                getUserTypeParser = GetUserTypesParser()
                getUserTypeParser!.delegate = self
                getUserTypeParser!.getUserTypes()
            }
        }
        else if tag == "loginUserError"
        {
            activity!.startAnimating()
            activity!.hidden = true

            if loginUserParser == nil
            {
                loginUserParser = LoginUserParser()
                loginUserParser!.delegate = self
                loginUserParser!.loginUserWith(countryCode: NSUserDefaults.standardUserDefaults().objectForKey("countryCode") as! String!, mobileno: "\(phone!)", emailid: email)
            }
        }
        else if tag == "unauthorisedUserError"
        {
            //            let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate
            //appDelegate!.applicationTerminateWithUnauthorisedUserAccess()

            activity!.startAnimating()
            activity!.hidden = false

            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "isVerified")

            // we need to call getAllUSerTypes and move to startscreen. Also need to delete all the user related data from the DB


            if getUserTypeParser == nil{

                getUserTypeParser = GetUserTypesParser()
                getUserTypeParser!.delegate = self
                getUserTypeParser!.getUserTypes()
            }
        }
        else if tag == "updateApp"{

            let url:NSURL = NSURL(string: "http://www.chatongo.com/iphone-customer-app.html")!
            UIApplication.sharedApplication().openURL(url)
        }
    }

    //MARK: Various Web service callbacks

    //MARK: UpdateDeviceTokenParserDelegate methods

    func didReceivedUpdateDeviceTokenResult(resultCode resultCode : Int){

        deviceTokenParser = nil
    }

    //MARK: Navigation Functions

    /*
     This method is triggered by the timer after successful getting the result from the GetUserTypes web services we hide the activity here and navigate user to the registration page.
     */
    func navigateToStartScreen()
    {
        let startScreen =  RegistrationViewController()
        self.navigationController!.pushViewController(startScreen, animated: true)
    }

    /*
     This method will let user to category screen directly as user has benn auto logged in successfully
     */
    func navigateToHomeScreen()
    {
        NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "enquiryCount")

        if deviceTokenParser == nil {

            deviceTokenParser = UpdateDeviceTokenParser()
            deviceTokenParser!.delegate = self
            deviceTokenParser!.deviceTokenParser()

        }
        let shopListController = SearchScreenViewController()
        self.navigationController!.pushViewController(shopListController, animated: true)
    }
}

